package in.omkarpayment.app.chekAvailableBalance;

import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;

import java.util.ArrayList;

/**
 * Created by Krish on 11-Aug-17.
 */

public interface IAvailableBalanceView {


    void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction);
    void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction);
}
