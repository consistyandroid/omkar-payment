package in.omkarpayment.app.chekAvailableBalance;

/**
 * Created by Krish on 11-Aug-17.
 */

public interface IAvailableBalancePresenter {

    void getAvailableBalance();

    void getSaleCommission();

    void errorAlert(String errorMsg);

    void dismissPDialog();

    void AvailableBalanceFailResponse(String encryptedResponse);

    void AvailableBalanceResponse(String encryptedResponse);

    void TodaysSaleCommissionResopnse(String encryptedResponse);

    void TodaysSaleCommissionFailuerResopnse(String encryptedResponse);
}
