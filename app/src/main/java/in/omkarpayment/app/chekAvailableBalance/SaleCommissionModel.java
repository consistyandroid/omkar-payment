package in.omkarpayment.app.chekAvailableBalance;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.IWebServicesModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaleCommissionModel {
    public IAvailableBalancePresenter delegate = null;
    LogWriter log = new LogWriter();

    public void SaleCommission(String s, String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.SaleCommissionWebservice(s, keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("comm onResponseBody", response.body().getBody());
                            delegate.TodaysSaleCommissionResopnse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.TodaysSaleCommissionFailuerResopnse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }
}
