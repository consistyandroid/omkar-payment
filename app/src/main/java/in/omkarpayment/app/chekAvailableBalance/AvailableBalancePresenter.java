package in.omkarpayment.app.chekAvailableBalance;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by Krish on 11-Aug-17.
 */

public class AvailableBalancePresenter implements IAvailableBalancePresenter {
    IAvailableBalanceView iBalanceView;

    public AvailableBalancePresenter(IAvailableBalanceView iBalanceView) {
        this.iBalanceView = iBalanceView;
    }


    @Override
    public void getAvailableBalance() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateAvailableBalanceResponse = this;
        model.webserviceMethod("", keyData, "CheckAvailableBalance");
    }

    @Override
    public void getSaleCommission() {
        String keyData = new KeyDataReader().get();
        SaleCommissionModel model = new SaleCommissionModel();
        model.delegate = this;
        model.SaleCommission("", keyData);
    }

    @Override
    public void errorAlert(String errorMsg) {

    }

    @Override
    public void dismissPDialog() {

    }

    @Override
    public void AvailableBalanceFailResponse(String encryptedResponse) {

    }

    @Override
    public void AvailableBalanceResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("body", body);
            ArrayList<AvailableBalancePojo> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<AvailableBalancePojo>>() {
            }.getType());
            if (lastrasaction != null) {
                iBalanceView.availableBalance(lastrasaction);
            }
        } catch (Exception e) {
            Log.i("Error", body);
        }


    }

    @Override
    public void TodaysSaleCommissionResopnse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("sale commission ", body);
            ArrayList<SaleCommissionPojo> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<SaleCommissionPojo>>() {
            }.getType());
            if (lastrasaction != null) {
                iBalanceView.saleCommission(lastrasaction);
            }
        } catch (Exception e) {
            Log.i("Error", body);
        }
    }

    @Override
    public void TodaysSaleCommissionFailuerResopnse(String encryptedResponse) {

    }
}
