package in.omkarpayment.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import androidx.appcompat.app.AppCompatActivity;
import in.omkarpayment.app.firebase.Config;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.userContent.UserDetails;
import io.fabric.sdk.android.Fabric;
public class SplashScreenActivity extends AppCompatActivity {
    public static SharedPreferences loginPreference;
    public static SharedPreferences.Editor loginEditor;
    public Boolean saveLogin = true, logutStatus;
    protected boolean _active = true;
    protected int _splashTime = 2000;
    Context context = this;

    @Override
    protected void onResume() {
        super.onResume();
        runSplashThread();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_splash_screen);
        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        saveLogin = loginPreference.getBoolean("saveLogin", false);
        logutStatus = loginPreference.getBoolean("logoutStatus", false);
        loginEditor = loginPreference.edit();
        readFCMToken();
        runSplashThread();

    }
    private void readFCMToken() {
        SharedPreferences fcmPref = getSharedPreferences(Config.SHARED_PREF, MODE_PRIVATE);
        UserDetails.AndroidToken = fcmPref.getString("FCMToken", "1");
        Log.i("FCMToken at Splash", UserDetails.AndroidToken);
    }
    private void runSplashThread() {
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(500);
                        if (_active) {
                            waited += 500;
                        }
                    }
                } catch (Exception e) {
                } finally {
                    Intent intent = new Intent(context, LoginActivity.class);
                    startActivity(intent);
                }
            }
        };
        splashTread.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
