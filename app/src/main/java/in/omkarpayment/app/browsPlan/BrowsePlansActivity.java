package in.omkarpayment.app.browsPlan;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.widget.TabHost;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.BrowsePlanTabPager;


public class BrowsePlansActivity extends AppCompatActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

    String btnClick;
    private BrowsePlanTabPager mAdapter;
    private DTHPlansPager dthPalnTabPager;
    private ViewPager mViewPager;
    private TabHost mTabHost;

    // Method to add a TabHost
    private static void AddTab(BrowsePlansActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans);
        Bundle extras = getIntent().getExtras();
        btnClick = extras.getString("btnClick");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Brows Plan";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        if (btnClick.equals("Mobile")) {
            mAdapter = new BrowsePlanTabPager(getSupportFragmentManager());
            // Fragments and ViewPager Initialization
            mViewPager.setAdapter(mAdapter);
            initialiseTabHost();
        } else if (btnClick.equals("DTH")) {
            dthPalnTabPager = new DTHPlansPager(getSupportFragmentManager());
            // Fragments and ViewPager Initialization
            mViewPager.setAdapter(dthPalnTabPager);
            initialiseDTHTabHost();
        }


        mViewPager.setOnPageChangeListener(BrowsePlansActivity.this);
    }

    // Manages the Tab changes, synchronizing it with Pages
    public void onTabChanged(String tag) {
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    // Manages the Page changes, synchronizing it with Tabs
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        int pos = this.mViewPager.getCurrentItem();
        this.mTabHost.setCurrentTab(pos);
    }

    @Override
    public void onPageSelected(int arg0) {
    }


    // Tabs Creation
    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        // TODO Put here your Tabs
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Full").setIndicator("Full"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("TalkTime").setIndicator("TalkTime"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("2G").setIndicator("2G"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("3G/4G").setIndicator("3G/4G"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("RATECUTTER").setIndicator("RATECUTTER"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("SMS").setIndicator("SMS"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Romaing").setIndicator("Romaing"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("COMBO").setIndicator("COMBO"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("FRC").setIndicator("FRC"));

        mTabHost.setOnTabChangedListener(this);
    }

    private void initialiseDTHTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        // TODO Put here your Tabs
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Add-On Pack").setIndicator("Add-On Pack"));
        BrowsePlansActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Plans").setIndicator("Plans"));
        mTabHost.setOnTabChangedListener(this);
    }

    @Override
    public void onBackPressed() {
       /* Intent homeintent = new Intent(BrowsePlansActivity.this, RechargeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("type", "GPRS");
        bundle.putString("buttonname","Mobile");
        homeintent.putExtras(bundle);
        homeintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeintent);
        finish();*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}