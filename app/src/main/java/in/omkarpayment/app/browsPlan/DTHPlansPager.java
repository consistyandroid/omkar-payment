package in.omkarpayment.app.browsPlan;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by Consisty on 16-Aug-17.
 */

public class DTHPlansPager extends FragmentPagerAdapter {
    public DTHPlansPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;//super.getItemPosition(object)
    }

    @Override
    public Fragment getItem(int index) {
        BrowsePlanListFragment fragmet = new BrowsePlanListFragment();
        Bundle args = new Bundle();
        switch (index) {
            case 0:
                args.putString("plans", "Add-On Pack");
                fragmet.setArguments(args);
                return fragmet;
            case 1:
                args.putString("plans", "Plans");
                fragmet.setArguments(args);
                return fragmet;
        }

        return fragmet;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
