package in.omkarpayment.app.browsPlan;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.BrowsePlansAdapterList;
import in.omkarpayment.app.adapter.DTHAddOnAdapter;
import in.omkarpayment.app.adapter.DTHPlanAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.DTHPlansAddOnPojo;
import in.omkarpayment.app.json.DTHPlansMainPojo;
import in.omkarpayment.app.json.DTHPlansPojo;
import in.omkarpayment.app.json.ViewPlansDecPojo;
import in.omkarpayment.app.json.ViewplansMainpojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;


/**
 * Created by Admin on 12/5/2015.
 */
public class BrowsePlanListFragment extends Fragment {

    String strUrl, plan, opName, cirName, itemString, plansResponse = "", btnClick;
    Bundle extras;
    ListView listView;
    Bundle b;
    LogWriter log = new LogWriter();
    CustomProgressDialog progressDialog;
    AlertImpl alert;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewPlans = inflater.inflate(R.layout.browse_listview, container, false);
        listView = (ListView) viewPlans.findViewById(R.id.listView);
        b = getArguments();
        plan = b.getString("plans");
        progressDialog = new CustomProgressDialog(getContext());
        alert = new AlertImpl(getContext());
        extras = getActivity().getIntent().getExtras();
        plansResponse = extras.getString("plansResponse");
        btnClick = extras.getString("btnClick");
        log.i("plansResponse", plansResponse);
        if (!plansResponse.equals("")) {

            if (btnClick.equals("Mobile")) {
                Gson gson = new Gson();
                ViewplansMainpojo obj = gson.fromJson(plansResponse, new TypeToken<ViewplansMainpojo>() {
                }.getType());
                if (plan.equals("full")) {
                    setAdapter(obj.getfULLTT());
                } else if (plan.equals("top")) {
                    setAdapter(obj.gettOPUP());
                } else if (plan.equals("2g")) {
                    setAdapter(obj.get_2G());
                } else if (plan.equals("3g")) {
                    setAdapter(obj.get_3G4G());
                } else if (plan.equals("SMS")) {
                    setAdapter(obj.getsMS());
                } else if (plan.equals("RATECUTTER")) {
                    setAdapter(obj.getrATECUTTER());
                } else if (plan.equals("Romaing")) {
                    setAdapter(obj.getRomaing());
                } else if (plan.equals("COMBO")) {
                    setAdapter(obj.getcOMBO());
                } else if (plan.equals("FRC")) {
                    setAdapter(obj.getfRC());
                }
            } else if (btnClick.equals("DTH")) {
                Gson gson = new Gson();
                DTHPlansMainPojo obj = gson.fromJson(plansResponse, new TypeToken<DTHPlansMainPojo>() {
                }.getType());
                if (plan.equals("Add-On Pack")) {
                    setAddOnAdapter(obj.getAddOnPack());
                } else if (plan.equals("Plans")) {
                    setDTHPlanAdapter(obj.getPlan());
                }
            }


        }
   /*     listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ViewPlansDecPojo obj = (ViewPlansDecPojo) parent.getItemAtPosition(position);
                Intent intent = new Intent();
                getActivity().setResult(2, intent);
                intent.putExtra("amount", obj.getRs());
                getActivity().finish();
            }
        });*/

        return viewPlans;
    }

    private void setAddOnAdapter(List<DTHPlansAddOnPojo> addOnPack) {
        try {
            if (addOnPack.size() != 0) {
                DTHAddOnAdapter adapter = new DTHAddOnAdapter(getActivity(), addOnPack);
                listView.setAdapter(adapter);
            }
        } catch (Exception e) {

        }

    }

    private void setAdapter(List<ViewPlansDecPojo> viewPlansDsc) {
        //    log.i("viewPlansDsc", viewPlansDsc.toString());
        try {
            if (viewPlansDsc.size() != 0) {
                BrowsePlansAdapterList adapter = new BrowsePlansAdapterList(getActivity(), viewPlansDsc);
                listView.setAdapter(adapter);
            }
        } catch (Exception e) {

        }
    }

    private void setDTHPlanAdapter(List<DTHPlansPojo> viewPlansDsc) {
        //    log.i("viewPlansDsc", viewPlansDsc.toString());
        try {
            if (viewPlansDsc.size() != 0) {
                DTHPlanAdapter adapter = new DTHPlanAdapter(getActivity(), viewPlansDsc);
                listView.setAdapter(adapter);
            }
        } catch (Exception e) {

        }
    }

/*    @Override
    public void getBrowsPlan(List<BrowsPlanDataPojo> data) {
        try {
            if (data.size() == 0) {

            } else {
                BrowsePlansAdapterList adapter = new BrowsePlansAdapterList(getActivity(), data);
                listView.setAdapter(adapter);
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void getDTHPlans(List<DTHPlansPojo> data) {
        try {
            if (data.size() == 0) {

            } else {
                DTHPlansAdapter adapter = new DTHPlansAdapter(getActivity(), data);
                listView.setAdapter(adapter);
            }
        } catch (Exception e) {

        }
    }*/
}