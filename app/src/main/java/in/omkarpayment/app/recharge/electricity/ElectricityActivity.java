package in.omkarpayment.app.recharge.electricity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.SpinnerAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.recharge.ItemData;
import in.omkarpayment.app.recharge.Operator;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

public class ElectricityActivity extends Fragment implements IElectricityBillView, IAvailableBalanceView {

    ArrayAdapter<String> spAdapter;
    ArrayList<ItemData> list = new ArrayList<>();
    AlertImpl alert;
    Context context;
    View rowview;
    CustomProgressDialog pDialog;
    IElectricityBillPresenter iElectricityBillPresenter;
    Double CurrentBalance;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    Spinner spOperator, spSelectOp;
    LinearLayout LayView, LayViewdetails;
    EditText editConsumerNo, edtAmount, editOptional2, editOptional1;
    TextView editBillingUnit, editPcycle, editConsumerName, txtDueDate;
    Button btnView, btnPay;
    NetworkState ns = new NetworkState();
    private String android_id;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        rowview = inflater.inflate(R.layout.activity_electricity, container, false);
        alert = new AlertImpl(getContext());
        pDialog = new CustomProgressDialog(getContext());


        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        if (ns.isInternetAvailable(context)) {
            iAvailableBalancePresenter.getAvailableBalance();
        }

        iElectricityBillPresenter = new ElectricityBillPresenter(this);

        list = new Operator().getElectricity();

        editConsumerNo = (EditText) rowview.findViewById(R.id.editConsumerNo);
        editConsumerNo.requestFocus();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        txtDueDate = (TextView) rowview.findViewById(R.id.txtDueDate);
        editPcycle = (TextView) rowview.findViewById(R.id.txtPCycle);
        edtAmount = (EditText) rowview.findViewById(R.id.edtAmount);
        editConsumerName = (TextView) rowview.findViewById(R.id.txtConsName);
        editOptional1 = (EditText) rowview.findViewById(R.id.txtOptional1);
        editOptional2 = (EditText) rowview.findViewById(R.id.editOptional2);
        btnView = (Button) rowview.findViewById(R.id.btnView);


        btnPay = (Button) rowview.findViewById(R.id.btnPay);
        LayView = (LinearLayout) rowview.findViewById(R.id.LayView);
        LayViewdetails = (LinearLayout) rowview.findViewById(R.id.LayViewdetails);
        LayViewdetails.setVisibility(View.GONE);
        android_id = Secure.getString(getActivity().getContentResolver(),
                Secure.ANDROID_ID);
        spOperator = (Spinner) rowview.findViewById(R.id.spSelectOp);

        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),
                R.layout.spinner_layout, R.id.txt, list);
        spOperator.setAdapter(adapter);

        spOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
               /* if (position == 1) {
                    pDialog.showPDialog();
                    iElectricityBillPresenter.viewBill();

                } else if (position != 1) {
                    LayViewdetails.setVisibility(View.GONE);
                    editBillingUnit.setText("");
                    editPcycle.setText("");
                    edtAmount.setText("");
                    editConsumerName.setText("");
                    editConsumerNo.setFocusable(true);
                    editConsumerNo.setClickable(true);
                    edtAmount.setFocusable(true);
                    edtAmount.setClickable(true);
                }*/
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(getContext())) {
                    iElectricityBillPresenter.payBill();
                } else {
                    alert.errorAlert(AllMessages.internetError);
                }


            }
        });
        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ns.isInternetAvailable(getContext())) {
                    iElectricityBillPresenter.viewBill();
                } else {
                    alert.errorAlert(AllMessages.internetError);
                }

            }
        });
        return rowview;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public Integer getSelectSpinner() {
        return spOperator.getSelectedItemPosition();
    }

    @Override
    public String getOperatorCode() {
        ItemData operatorAPI = (ItemData) spOperator.getSelectedItem();
        return operatorAPI.getOpcode();
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
        editBillingUnit.setText("");
        editPcycle.setText("");
        edtAmount.setText("");
        editConsumerName.setText("");
        editConsumerNo.setText("");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((HomeActivity) getActivity()).updateBal();
            }
        }, 45000);
        ((HomeActivity) getActivity()).updateBal();
    }

    @Override
    public void SpSelectionError() {
        alert.errorAlert(String.format("Please select operator"));
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public String getConsumerNo() {
        return editConsumerNo.getText().toString();
    }

    @Override
    public String getServiceID() {
        return "14";
    }

    @Override
    public String getDeviceID() {
        Log.i("DiviceID", android_id);
        return android_id;
    }

    @Override
    public String getOptional1() {
        return editOptional1.getText().toString().trim();
    }

    @Override
    public String getOptional2() {
        return editOptional2.getText().toString().trim();
    }

    @Override
    public String getUserId() {
        return String.valueOf(UserDetails.UserId);
    }

    @Override
    public String getRechargePin() {
        return "1234";
    }

    @Override
    public String refrenceId() {
        return  UserDetails.RefreanceId;
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }


    @Override
    public String ConsumerName() {
        return editConsumerName.getText().toString();
    }

    @Override
    public String BillingUnits() {
        return editBillingUnit.getText().toString();
    }

    @Override
    public String PCycle() {
        return editPcycle.getText().toString();
    }

    @Override
    public String Amount() {
        return edtAmount.getText().toString();
    }

    @Override
    public void editConsumerNumberError() {
        editConsumerNo.setError(String.format(AllMessages.pleaseEnter, "Consumer Number"));
        editConsumerNo.requestFocus();
    }

    @Override
    public void editConsumerNameError() {
        editConsumerName.setError(String.format(AllMessages.pleaseEnter, "Consumer Name"));
        editConsumerName.requestFocus();
    }

    @Override
    public void editBillingUnitsError() {
        editBillingUnit.setError(String.format(AllMessages.pleaseEnter, "Billing Unit"));
        editBillingUnit.requestFocus();
    }

    @Override
    public void editPCycleError() {
        editPcycle.setError(String.format(AllMessages.pleaseEnter, "PCycle"));
        editPcycle.requestFocus();
    }

    @Override
    public void optional1Error() {
        editOptional1.setError(String.format(AllMessages.pleaseEnter, "billing unit"));
        editOptional1.requestFocus();
    }

    @Override
    public void optional2Error() {
        editOptional2.setError(String.format(AllMessages.pleaseEnter, "consumer mobile no"));
        editOptional2.requestFocus();
    }

    @Override
    public void editAmountError() {
        edtAmount.setError(String.format(AllMessages.pleaseEnter, "Amount"));
        edtAmount.requestFocus();
    }

    @Override
    public void viewBillResponse(ViewBillResponsePojo reportResponse) {
        UserDetails.RefreanceId=reportResponse.getReferenceId();
        if (reportResponse.getBillAmounts().equalsIgnoreCase("")) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.warning_dialog);

            TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
            text.setText("Please check consumer Number or Fill Details Manually");

            Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
            Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayView.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            });
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } else if (!reportResponse.getBillAmounts().equalsIgnoreCase("")) {
            pDialog.dismissPDialog();
            LayViewdetails.setVisibility(View.VISIBLE);
            editConsumerName.setText(reportResponse.getConsumerName());
            txtDueDate.setText(reportResponse.getBillDueDate());
            edtAmount.setText(reportResponse.getBillAmounts());
            editPcycle.setText(reportResponse.getProcessiongCycle());

            editConsumerName.setFocusable(false);
            editConsumerName.setClickable(false);
            txtDueDate.setFocusable(false);
            txtDueDate.setClickable(false);

            editPcycle.setFocusable(false);
            editPcycle.setClickable(false);
            btnView.setVisibility(View.GONE);

        } else {
            errorAlert(AllMessages.somethingWrong);
            // activityElectricityBinding.LayView.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
      /*  for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                activityElectricityBinding.txtBalance.setText(String.valueOf(obj.getCurrentBalance()));
            } catch (Exception e) {

            }

        }*/
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }


}
