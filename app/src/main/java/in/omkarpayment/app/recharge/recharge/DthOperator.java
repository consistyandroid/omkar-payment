package in.omkarpayment.app.recharge.recharge;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.DthOperatorDialogBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.recharge.IRechargePresenter;
import in.omkarpayment.app.recharge.IRechargeView;
import in.omkarpayment.app.recharge.RechargePresenterImpl;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by consisty on 7/4/18.
 */

public class DthOperator extends Fragment implements IRechargeView, IAvailableBalanceView {

    public static String OperatorCode, OperatorName;
    public static int OperatorImage;
    String str2;
    AlertImpl alert;
    Double CurrentBalance;
    DthOperatorDialogBinding fragmentOperatorBinding;
    IRechargePresenter iRechargePresenter;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    Button btnClear, btnRecharge;
    EditText editNumber, editAmonut, edtPin;
    ImageView phnBook, imgOperator;
    Dialog dialog, dialogg;
    NetworkState ns = new NetworkState();
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentOperatorBinding = DataBindingUtil.inflate(inflater, R.layout.dth_operator_dialog, container, false);
        v = fragmentOperatorBinding.getRoot();

        iRechargePresenter = new RechargePresenterImpl(this);
        alert = new AlertImpl(getActivity());
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);

        fragmentOperatorBinding.btnBigTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "29";
                OperatorImage = R.mipmap.big;
                OperatorName = fragmentOperatorBinding.btnBigTV.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnSunDirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "30";
                OperatorImage = R.drawable.sundirect;
                OperatorName = fragmentOperatorBinding.btnSunDirect.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnVideocon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "8";
                OperatorImage = R.drawable.videocondth;
                OperatorName = fragmentOperatorBinding.btnVideocon.getText().toString();
                dialogRecharge();
            }
        });

        fragmentOperatorBinding.btnAirtelDth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "4";
                OperatorImage = R.drawable.airtel;
                OperatorName = fragmentOperatorBinding.btnAirtelDth.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnTataSky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "5";
                OperatorImage = R.drawable.tatasky;
                OperatorName = fragmentOperatorBinding.btnTataSky.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnDishTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "28";
                OperatorImage = R.drawable.dishtv;
                OperatorName = fragmentOperatorBinding.btnDishTv.getText().toString();
                dialogRecharge();
            }
        });

        return v;
    }

    private void dialogRecharge() {
        dialogg = new Dialog(getActivity(), R.style.RechargeBackground);
        dialogg.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialogg.setContentView(R.layout.recharge_dialog_custom);
        dialogg.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, OperatorImage);
        dialogg.setTitle(OperatorName + " " + "Recharge");

        btnRecharge = (Button) dialogg.findViewById(R.id.btnRecharge);
        btnClear = (Button) dialogg.findViewById(R.id.btnClear);
        editNumber = (EditText) dialogg.findViewById(R.id.editNumber);
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(15);
        editNumber.setFilters(FilterArray);
        editAmonut = (EditText) dialogg.findViewById(R.id.editAmonut);
        phnBook = (ImageView) dialogg.findViewById(R.id.phnBook);
        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNumber.getText().toString().length() >= 4) {
                    if (ns.isInternetAvailable(getActivity())) {
                        iRechargePresenter.doValidation();
                    } else {
                        Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    edtNumberError();
                }
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();


            }
        });

        phnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    getActivity().startActivityForResult(localIntent, 1);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Number is not format" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogg.show();
    }

    @Override
    public String getNumber() {
        return editNumber.getText().toString();
    }

    @Override
    public String getAmount() {
        return editAmonut.getText().toString();
    }

    @Override
    public void edtNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "mobile number"));
        editNumber.requestFocus();
    }

    @Override
    public void edtAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        editAmonut.requestFocus();
    }

    @Override
    public Integer getSelectSpinner() {
        return null;
    }

    @Override
    public void SpSelectionError() {

    }

    @Override
    public String serviceId() {
        return "2";
    }

    @Override
    public String mPin() {
        return edtPin.getText().toString();
    }

    @Override
    public Double currentBalance() {
        return CurrentBalance;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(R.id.editPin);
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));
                } else {
                    dialog.dismiss();
                    showPDialog();
                }
            }
        });
        dialog.show();
    }

    @Override
    public String getOperatorName() {
        return null;
    }

    @Override
    public void errorAlert(String msg) {
        dismissPDialog();
        if (dialogg != null) {
            dialogg.dismiss();
        }
        alert.errorAlert(msg);

    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
        clear();
        if (dialogg != null) {
            dialogg.dismiss();
        }
        final Handler handler = new Handler();
        ((HomeActivity) getActivity()).updateBal();
    }

    @Override
    public void rechargeConfirmAlert() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", getNumber(), getAmount()));
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(getActivity())) {
                    iRechargePresenter.doRecharge();
                } else {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge) {

    }

    @Override
    public String getOperatorCode() {
        return OperatorCode;
    }

    @Override
    public void clear() {
        editNumber.setText("");
        editAmonut.setText("");
        dialog.dismiss();
    }

    @Override
    public void showPDialog() {
        if (dialogg != null) {
            dialogg.dismiss();
        }
    }

    @Override
    public void dismissPDialog() {
        if (dialogg != null) {
            dialogg.dismiss();
        }
    }

    @Override
    public void fetchOperator(String serviceName) {

    }

    @Override
    public void operatorResponse(ArrayList<OperatorPojo> operatorList) {

    }

    @Override
    public void lastFiveResponseDialog() {

    }

    @Override
    public void getRoffers(String rofferPojos) {

    }

    @Override
    public void browsPlans(String respo) {

    }

    @Override
    public void customerInfo(String response) {

    }

    @Override
    public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        switch (paramInt1) {
        }
        Cursor localCursor;
        do {
            while (paramInt2 != -1) {
                return;
            }
            localCursor = getActivity().managedQuery(paramIntent.getData(), null, null, null, null);
        } while (!localCursor.moveToFirst());
        String str1 = removeExtraCharacters(localCursor.getString(localCursor.getColumnIndexOrThrow("data1")));
        str2 = str1.substring(-10 + str1.length());
        editNumber.setText(str2);
    }

    private String removeExtraCharacters(String paramString) {
        try {
            paramString = paramString.replace("(", "");
            paramString = paramString.replace(")", "");
            paramString = paramString.replace("-", "");
            paramString = paramString.replace(" ", "");
            String str = paramString.replace("+", "");
            return str;
        } catch (Exception localException) {

        }
        return paramString;
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {

            }

        }

    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
