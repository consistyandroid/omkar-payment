package in.omkarpayment.app.recharge.recharge;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;

import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.FragmentOperatorBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.recharge.IRechargePresenter;
import in.omkarpayment.app.recharge.IRechargeView;
import in.omkarpayment.app.recharge.RechargePresenterImpl;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import static in.omkarpayment.app.R.drawable;
import static in.omkarpayment.app.R.id;
import static in.omkarpayment.app.R.layout;
import static in.omkarpayment.app.R.mipmap;
import static in.omkarpayment.app.R.style;

/**
 * Created by consisty on 7/4/18.
 */

public class PrepaidOperator extends Fragment implements IRechargeView, IAvailableBalanceView {

    public static String OperatorCode, OperatorName;

    String str2;
    AlertImpl alert;
    Double CurrentBalance;
    CustomProgressDialog progressDialog;
    FragmentOperatorBinding fragmentOperatorBinding;
    IRechargePresenter iRechargePresenter;
    IAvailableBalancePresenter iAvailableBalancePresenter;


    Button btnClear, btnRecharge;
    EditText editNumber, editAmonut, edtPin;

    NetworkState ns = new NetworkState();

    ImageView phnBook, imgOperator;
    Dialog dialog, dialogg;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentOperatorBinding = DataBindingUtil.inflate(inflater, layout.fragment_operator, container, false);
        v = fragmentOperatorBinding.getRoot();

        iRechargePresenter = new RechargePresenterImpl(this);
        alert = new AlertImpl(getActivity());
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        progressDialog = new CustomProgressDialog(getActivity());


        fragmentOperatorBinding.btnAirtel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "2";
                UserDetails.OperatorImage = drawable.airtel;
                OperatorName = fragmentOperatorBinding.btnAirtel.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnIdea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "6";
                UserDetails.OperatorImage = drawable.idea;
                OperatorName = fragmentOperatorBinding.btnIdea.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnVodafone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "21";
                UserDetails.OperatorImage = drawable.vodafone;
                OperatorName = fragmentOperatorBinding.btnVodafone.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnBSNL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "37";
                UserDetails.OperatorImage = drawable.bsnl;
                OperatorName = fragmentOperatorBinding.btnBSNL.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnBsnlTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "7";
                UserDetails.OperatorImage = drawable.bsnl;
                OperatorName = fragmentOperatorBinding.btnBsnlTopup.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnAIRCEL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "22";
                UserDetails.OperatorImage = drawable.aircel;
                OperatorName = fragmentOperatorBinding.btnAIRCEL.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnDOCOMO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "1";
                UserDetails.OperatorImage = drawable.docomo;
                OperatorName = fragmentOperatorBinding.btnDOCOMO.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnDOCOMOSPECIAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "39";
                UserDetails.OperatorImage = drawable.docomo;
                OperatorName = fragmentOperatorBinding.btnDOCOMO.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnTATAINDICOM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "18";
                UserDetails.OperatorImage = drawable.indicom;
                OperatorName = fragmentOperatorBinding.btnTATAINDICOM.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnTELENORTOPUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "3";
                UserDetails.OperatorImage = drawable.uninor;
                OperatorName = fragmentOperatorBinding.btnTELENORTOPUP.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnTELENORSTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "20";
                UserDetails.OperatorImage = drawable.uninor;
                OperatorName = fragmentOperatorBinding.btnTELENORSTV.getText().toString();
                dialogRecharge();
            }
        });

        fragmentOperatorBinding.btnJIO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "15";
                UserDetails.OperatorImage = drawable.jio;
                OperatorName = fragmentOperatorBinding.btnJIO.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnMTNL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "26";
                UserDetails.OperatorImage = drawable.mtnl;
                OperatorName = fragmentOperatorBinding.btnMTNL.getText().toString();
                dialogRecharge();
            }
        });
        fragmentOperatorBinding.btnMTNLMumbai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OperatorCode = "16";
                UserDetails.OperatorImage = drawable.mtnl;
                OperatorName = fragmentOperatorBinding.btnMTNLMumbai.getText().toString();
                dialogRecharge();
            }
        });
        return v;
    }

    private void dialogRecharge() {
        dialogg = new Dialog(getActivity(), style.RechargeBackground);
        dialogg.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        Log.i("OpId:", OperatorName.toString());
        dialogg.setContentView(R.layout.recharge_dialog_custom);
        dialogg.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, UserDetails.OperatorImage);
        dialogg.setTitle(OperatorName + " " + "RECHARGE");
        ImageView imgOperator = (ImageView) dialogg.findViewById(R.id.imgOperator);
        btnRecharge = (Button) dialogg.findViewById(id.btnRecharge);
        btnClear = (Button) dialogg.findViewById(id.btnClear);
        editNumber = (EditText) dialogg.findViewById(id.editNumber);
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(10);
        editNumber.setFilters(FilterArray);
        editAmonut = (EditText) dialogg.findViewById(id.editAmonut);
        phnBook = (ImageView) dialogg.findViewById(id.phnBook);
        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNumber.getText().toString().length() == 10) {
                    if (ns.isInternetAvailable(getActivity())) {
                        iRechargePresenter.doValidation();
                    } else {
                        Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    edtNumberError();
                }
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editNumber.setText("");
                editAmonut.setText("");

            }
        });

        phnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    getActivity().startActivityForResult(localIntent, 1);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Number is not format" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogg.show();
    }

    @Override
    public String getNumber() {
        return editNumber.getText().toString();
    }

    @Override
    public String getAmount() {
        return editAmonut.getText().toString();
    }

    @Override
    public void edtNumberError() {
        alert.errorAlert(String.format("Please enter 10 digit mobile number"));
        editNumber.requestFocus();
    }

    @Override
    public void edtAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        editAmonut.requestFocus();
    }

    @Override
    public Integer getSelectSpinner() {
        return null;
    }

    @Override
    public void SpSelectionError() {

    }

    @Override
    public String serviceId() {
        return "1";
    }

    @Override
    public String mPin() {
        return edtPin.getText().toString();
    }

    @Override
    public Double currentBalance() {
        return CurrentBalance;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(id.editPin);
        Button btnSubmit = (Button) dialog.findViewById(id.btnSubmmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));
                } else {
                    dialog.dismiss();
                    showPDialog();
                }
            }
        });
        dialog.show();
    }

    @Override
    public String getOperatorName() {
        return null;
    }

    @Override
    public void errorAlert(String msg) {
        if (dialogg != null) {
            dialogg.dismiss();
        }
        dismissPDialog();
        alert.errorAlert(msg);

    }

    @Override
    public void successAlert(String msg) {
        dismissPDialog();
        if (dialogg != null) {
            dialogg.dismiss();
        }
        alert.successAlert(msg);
        clear();
         ((HomeActivity) getActivity()).updateBal();
    }


    @Override
    public void rechargeConfirmAlert() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", getNumber(), getAmount()));

        Button dialogButton = (Button) dialog.findViewById(id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(getActivity())) {
                    iRechargePresenter.doRecharge();
                } else {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

                dialog.dismiss();
                showPDialog();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge) {

    }

    @Override
    public String getOperatorCode() {
        return OperatorCode;
    }

    @Override
    public void clear() {
        editNumber.setText("");
        editAmonut.setText("");
    }

    @Override
    public void showPDialog() {
        if (progressDialog == null) {
            progressDialog.showPDialog();
        }

    }

    @Override
    public void dismissPDialog() {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();

        }
    }

    @Override
    public void fetchOperator(String serviceName) {
    }

    @Override
    public void operatorResponse(ArrayList<OperatorPojo> operatorList) {

    }

    @Override
    public void lastFiveResponseDialog() {

    }

    @Override
    public void getRoffers(String rofferPojos) {

    }

    @Override
    public void browsPlans(String respo) {

    }

    @Override
    public void customerInfo(String response) {

    }

    @Override
    public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        switch (paramInt1) {
        }
        Cursor localCursor;
        do {
            while (paramInt2 != -1) {
                return;
            }
            localCursor = getActivity().managedQuery(paramIntent.getData(), null, null, null, null);
        } while (!localCursor.moveToFirst());
        String str1 = removeExtraCharacters(localCursor.getString(localCursor.getColumnIndexOrThrow("data1")));
        str2 = str1.substring(-10 + str1.length());
        editNumber.setText(str2);
    }

    private String removeExtraCharacters(String paramString) {
        try {
            paramString = paramString.replace("(", "");
            paramString = paramString.replace(")", "");
            paramString = paramString.replace("-", "");
            paramString = paramString.replace(" ", "");
            String str = paramString.replace("+", "");
            return str;
        } catch (Exception localException) {

        }
        return paramString;
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {

            }

        }

    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
