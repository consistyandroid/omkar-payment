package in.omkarpayment.app.recharge.electricity;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.ElectricityBillViewArrayPojo;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;


public class ElectricityBillPresenter implements IElectricityBillPresenter {
    IElectricityBillView iElectricityBillView;

    public ElectricityBillPresenter(IElectricityBillView iElectricityBillView) {
        this.iElectricityBillView = iElectricityBillView;
    }

    @Override
    public void errorAlert(String msg) {
        iElectricityBillView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iElectricityBillView.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        iElectricityBillView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iElectricityBillView.dismissPDialog();
    }

    @Override
    public void viewBill() {
        if (iElectricityBillView.getConsumerNo().isEmpty()) {
            iElectricityBillView.editConsumerNumberError();
        } else if (iElectricityBillView.getSelectSpinner() < 1) {
            iElectricityBillView.SpSelectionError();
        } else if (iElectricityBillView.getOptional2().isEmpty()) {
            iElectricityBillView.optional2Error();
        } else if (iElectricityBillView.getOptional1().isEmpty()) {
            iElectricityBillView.optional1Error();
        } else {
            viewBillModel();
        }
    }


    @Override
    public void payBill() {
        Double amount = Double.valueOf(iElectricityBillView.Amount());
        if (iElectricityBillView.getConsumerNo().isEmpty()) {
            iElectricityBillView.editConsumerNumberError();
        } else if (iElectricityBillView.getSelectSpinner() == 0) {
            iElectricityBillView.SpSelectionError();
        } else if (iElectricityBillView.Amount().isEmpty()) {
            iElectricityBillView.editAmountError();
        } else if (amount > UserDetails.UserBalance) {
            errorAlert(AllMessages.minBalanceMessage);
        } else {
            payBillModel();
        }
    }

    @Override
    public void payMSEBBill() {

    }

    @Override
    public void decryptPayBillResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Elect. body response", body);

            if (body != null) {
                iElectricityBillView.successAlert(body);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void decryptPayBillFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Elect. body response", body);
            if (body != null) {
                iElectricityBillView.errorAlert(body);
            }
        } catch (Exception e) {

        }
    }


    private void payBillModel() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject billRequest = new JSONObject();
        try {
            billRequest.put("OperatorId", iElectricityBillView.getOperatorCode());
            billRequest.put("Amount", iElectricityBillView.Amount());
            billRequest.put("ConsumerNo", iElectricityBillView.getConsumerNo());
            billRequest.put("CustomerMobileNo", iElectricityBillView.getOptional2());
            billRequest.put("ServiceId", iElectricityBillView.getServiceID());
            billRequest.put("ReferenceId", iElectricityBillView.refrenceId());
            billRequest.put("RechargePin", iElectricityBillView.getRechargePin());
            billRequest.put("IPAddress", iElectricityBillView.getDeviceID());
            billRequest.put("Optional1", iElectricityBillView.getConsumerNo());
            billRequest.put("Optional2", iElectricityBillView.getOptional1());
            billRequest.put("Optional3", "");
            billRequest.put("Optional4", "");
            billRequest.put("Optional5", "");
            billRequest.put("Optional6", "");
            String jsonrequest = billRequest.toString();
            Log.i("Pay Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encryptedPay String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateElectricity = this;
                model.webserviceMethod(encryptString, keyData, "ElectricityBillPay");
                showPDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void viewBillResponse(List<ElectricityBillViewArrayPojo> viewBillDetails) {

    }

    private void viewBillModel() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject ViewBill = new JSONObject();
        try {
            ViewBill.put("ConsumerNo", iElectricityBillView.getConsumerNo());
            ViewBill.put("OperatorId", iElectricityBillView.getOperatorCode());
            ViewBill.put("Optional1", iElectricityBillView.getConsumerNo());
            ViewBill.put("Optional2", iElectricityBillView.getOptional1());//getOptional2
            String jsonrequest = ViewBill.toString();
            Log.i("BillView Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateElectricity = this;
                model.webserviceMethod(encryptString, keyData, "Electricity");
                showPDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptViewBillPaymentResponse(String encryptedResponse) {

        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Elect. View  Bill", body);
            ViewBillResponsePojo lastrasaction = gson.fromJson(body, new TypeToken<ViewBillResponsePojo>() {
            }.getType());

            if (lastrasaction != null) {
                iElectricityBillView.viewBillResponse(lastrasaction);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void decryptViewBillFailureResponse(String encryptedResponse) {
        String body = "";
        Log.i("Electr. Fail Response", encryptedResponse);
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("DElectr. Fail Response", body);
            errorAlert(body);
        } catch (Exception e) {
        }
    }


}
