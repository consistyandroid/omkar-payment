package in.omkarpayment.app.recharge.electricity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by consisty on 20/11/17.
 */

public class ViewBillResponsePojo {
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ConsumerNumber")
    @Expose
    private String consumerNumber;
    @SerializedName("ConsumerName")
    @Expose
    private String consumerName;
    @SerializedName("BillingUnit")
    @Expose
    private String billingUnit;
    @SerializedName("BuDesc")
    @Expose
    private String buDesc;
    @SerializedName("ProcessiongCycle")
    @Expose
    private String processiongCycle;
    @SerializedName("BillDueDate")
    @Expose
    private String billDueDate;
    @SerializedName("BillAmounts")
    @Expose
    private String billAmounts;
    @SerializedName("PromptPaymentDiscountDate")
    @Expose
    private String promptPaymentDiscountDate;
    @SerializedName("AmountToPay")
    @Expose
    private String amountToPay;
    @SerializedName("Desc")
    @Expose
    private String desc;
    @SerializedName("ConnectionType")
    @Expose
    private String connectionType;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Consumption")
    @Expose
    private String consumption;
    @SerializedName("PaymentDiscountAmt")
    @Expose
    private String paymentDiscountAmt;
    @SerializedName("ContactNo")
    @Expose
    private String contactNo;
    @SerializedName("ReferenceId")
    @Expose
    private String referenceId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConsumerNumber() {
        return consumerNumber;
    }

    public void setConsumerNumber(String consumerNumber) {
        this.consumerNumber = consumerNumber;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getBillingUnit() {
        return billingUnit;
    }

    public void setBillingUnit(String billingUnit) {
        this.billingUnit = billingUnit;
    }

    public String getBuDesc() {
        return buDesc;
    }

    public void setBuDesc(String buDesc) {
        this.buDesc = buDesc;
    }

    public String getProcessiongCycle() {
        return processiongCycle;
    }

    public void setProcessiongCycle(String processiongCycle) {
        this.processiongCycle = processiongCycle;
    }

    public String getBillDueDate() {
        return billDueDate;
    }

    public void setBillDueDate(String billDueDate) {
        this.billDueDate = billDueDate;
    }

    public String getBillAmounts() {
        return billAmounts;
    }

    public void setBillAmounts(String billAmounts) {
        this.billAmounts = billAmounts;
    }

    public String getPromptPaymentDiscountDate() {
        return promptPaymentDiscountDate;
    }

    public void setPromptPaymentDiscountDate(String promptPaymentDiscountDate) {
        this.promptPaymentDiscountDate = promptPaymentDiscountDate;
    }

    public String getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(String amountToPay) {
        this.amountToPay = amountToPay;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getPaymentDiscountAmt() {
        return paymentDiscountAmt;
    }

    public void setPaymentDiscountAmt(String paymentDiscountAmt) {
        this.paymentDiscountAmt = paymentDiscountAmt;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
