package in.omkarpayment.app.recharge;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.GetOfferModel;
import in.omkarpayment.app.model.IGetOffer;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

public class RechargePresenterImpl implements IRechargePresenter, IGetOffer {

    IRechargeView iRechargeView;
    LogWriter log = new LogWriter();
    Double amount, currentBalance = null;

    public RechargePresenterImpl(IRechargeView iRechargeView) {
        this.iRechargeView = iRechargeView;
    }

    @Override
    public void errorAlert(String msg) {
        iRechargeView.errorAlert(msg);
    }

    @Override
    public void showDialog() {
        iRechargeView.showPDialog();
    }

    @Override
    public void dismissDialog() {
        iRechargeView.dismissPDialog();
    }

    @Override
    public void successAlert(String msg) {
        iRechargeView.successAlert(msg);
    }

    @Override
    public void doValidation() {
        if (validation()) {
            amount = Double.parseDouble(iRechargeView.getAmount());
            if (amount > UserDetails.UserBalance || amount <= 0) {
                errorAlert(AllMessages.lowBalanceMessage);
            } else {
                iRechargeView.rechargeConfirmAlert();
            }

        }
    }


    @Override
    public boolean validation() {
        if (iRechargeView.getNumber().isEmpty()) {
            iRechargeView.edtNumberError();
            return false;
        }
        if (iRechargeView.getSelectSpinner() == 0) {
            iRechargeView.SpSelectionError();
            return false;
        }
        if (iRechargeView.getAmount().isEmpty()) {
            iRechargeView.edtAmountError();
            return false;
        }

        /**/
        return true;
    }

    @Override
    public void getLastTransaction() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateRechargeResponse = this;
        model.webserviceMethod("", keyData, "LastTransactionInRecharge");
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> latTransaction) {
        iRechargeView.getLastTransaction(latTransaction);

    }

    @Override
    public void doRecharge() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("OperatorId", iRechargeView.getOperatorCode());
            loginrequest.put("Amount", iRechargeView.getAmount());
            loginrequest.put("ConsumerNo", iRechargeView.getNumber());
            loginrequest.put("ServiceID", iRechargeView.serviceId());
            loginrequest.put("DeviceId", UserDetails.IMEI);
            loginrequest.put("RechargePin", iRechargeView.mPin());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            log.i("Recharge Request", jsonrequest);
            WebServiceModel model = new WebServiceModel();
            model.delegateRechargeResponse = this;
            model.webserviceMethod(encryptString, keyData, "Recharge");
            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void rungetOperatorWebservice() {
        String keyData = new KeyDataReader().get();

        WebServiceModel model = new WebServiceModel();
        // WebModelForTesting modelForTesting = new WebModelForTesting();
        model.delegateRechargeResponse = this;
        model.webserviceMethod("", keyData, "Operator");
        showDialog();
    }

    @Override
    public void decryptRechargeResponse(String stringToDecrypt) {

        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(stringToDecrypt);
            log.i("decryptRechargeResponse", body);
            iRechargeView.successAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void decryptOperatorResponse(String stringToDecrypt) {
        log.i("OperatorResponse", stringToDecrypt);
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(stringToDecrypt);
            Log.i("Decrypted body", body);
            Gson gson = new Gson();
            ArrayList<OperatorPojo> operators = gson.fromJson(body, new TypeToken<ArrayList<OperatorPojo>>() {
            }.getType());
            iRechargeView.operatorResponse(operators);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void LastTransactionResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            ArrayList<LastFiveRechargePojo> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<LastFiveRechargePojo>>() {
            }.getType());
            if (lastrasaction != null) {
                iRechargeView.getLastTransaction(lastrasaction);

            }
        } catch (Exception e) {

        }
    }

    @Override
    public void LastTransactionFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            errorAlert(body);

        } catch (Exception e) {

        }

    }

    @Override
    public void getRoffers() {
        if (iRechargeView.getNumber().length() < 10 || iRechargeView.getNumber().isEmpty()) {
            iRechargeView.edtNumberError();
        } else if (iRechargeView.getOperatorName().equalsIgnoreCase("Select Operator")) {
            iRechargeView.SpSelectionError();
        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            try {
                loginrequest.put("OperatorName", iRechargeView.getOperatorName());
                loginrequest.put("ConsumerNo", iRechargeView.getNumber());
                String jsonrequest = loginrequest.toString();
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                log.i("Recharge Request", jsonrequest);
                GetOfferModel model = new GetOfferModel();
                model.delegate = this;
                model.getROffers(encryptString, keyData);
                showDialog();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getViewPlans() {
        if (iRechargeView.getOperatorName().equals("Select Operator")) {
            iRechargeView.SpSelectionError();
        } else {
            String opName = "";
            if (iRechargeView.getOperatorName().equals("Telenor")) {
                opName = "Telenore";
            } else if (iRechargeView.getOperatorName().equals("BSNL")) {
                opName = "Bsnl";
            } else {
                opName = iRechargeView.getOperatorName();
            }
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            try {
                loginrequest.put("OperatorName", opName);
                loginrequest.put("CircleName", "Maharashtra Goa");
                String jsonrequest = loginrequest.toString();
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                log.i("viewplan Request", jsonrequest);
                GetOfferModel model = new GetOfferModel();
                model.delegate = this;
                model.getBrowsplans(encryptString, keyData);
                showDialog();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void viewDTHPlans() {
        if (iRechargeView.getOperatorName().equals("Select Operator")) {
            iRechargeView.SpSelectionError();
        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            try {
                loginrequest.put("OperatorName", iRechargeView.getOperatorName());
                String jsonrequest = loginrequest.toString();
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                log.i("Recharge Request", jsonrequest);
                GetOfferModel model = new GetOfferModel();
                model.delegate = this;
                model.getDTHPlans(encryptString, keyData);
                showDialog();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void ViewCustomerInfo() {
        if (iRechargeView.getNumber().isEmpty()) {
            iRechargeView.edtNumberError();
        } else if (iRechargeView.getOperatorName().equals("Select Operator")) {
            iRechargeView.SpSelectionError();
        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            try {
                loginrequest.put("OperatorName", iRechargeView.getOperatorName());
                loginrequest.put("ConsumerNo", iRechargeView.getNumber());
                String jsonrequest = loginrequest.toString();
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                log.i("viewinfo Request", jsonrequest);
                GetOfferModel model = new GetOfferModel();
                model.delegate = this;
                model.getCustomerInfo(encryptString, keyData);
                showDialog();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void errorMsg(String status) {
        dismissDialog();
    }

    @Override
    public void getResponse(String response) {
        dismissDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(response);
            log.i("body", body);
            iRechargeView.getRoffers(body);
        } catch (Exception e) {

        }
    }

    @Override
    public void getFailResponse(String response) {
        dismissDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(response);
            log.i("body", body);
            errorAlert(body);
        } catch (Exception e) {

        }
    }

    @Override
    public void getViewPlans(String response) {
        dismissDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(response);
            //    log.i("body", body);
            iRechargeView.browsPlans(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getCustomerInfo(String response) {
        dismissDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(response);
            //    log.i("body", body);
            iRechargeView.customerInfo(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

