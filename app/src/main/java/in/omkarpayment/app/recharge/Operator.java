package in.omkarpayment.app.recharge;

import java.util.ArrayList;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.OperatorPojo;


/**
 * Created by Admin on 17 Nov 2016.
 */

public class Operator {
    public static ArrayList<OperatorPojo> allOperator = new ArrayList<OperatorPojo>();

    public static ArrayList<ItemData> getMobileOpertor() {
        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("AIRTEL", "2", R.mipmap.airtel));
        list.add(new ItemData("IDEAVODA", "52", R.mipmap.idea));
        list.add(new ItemData("IDEA", "6", R.mipmap.idea));
        list.add(new ItemData("VODAFONE", "21", R.mipmap.vodafone));
        list.add(new ItemData("BSNL STV", "37", R.mipmap.bsnl));
        list.add(new ItemData("BSNL TOPUP", "7", R.mipmap.bsnl));
        list.add(new ItemData("JIO", "15", R.mipmap.jio));
        list.add(new ItemData("MTNL-MUMBAI", "16", R.mipmap.mtnl));
        list.add(new ItemData("MTNL-MUMBAI TOP", "26", R.mipmap.mtnl));

        return list;
    }


    public static ArrayList<ItemData> getMobileSMSOpertor() {
        ArrayList<ItemData> opList = new ArrayList<>();
        opList.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        opList.add(new ItemData("AIRCEL", "AR", R.mipmap.aircel));
        opList.add(new ItemData("AIRTEL", "AT", R.mipmap.airtel));
        opList.add(new ItemData("BSNL TOPUP", "BR", R.mipmap.bsnl));
        opList.add(new ItemData("BSNL RECHARGE", "BRS", R.mipmap.bsnl));
        opList.add(new ItemData("IDEA", "ID", R.mipmap.idea));
        opList.add(new ItemData("RELIANCE", "RG", R.mipmap.reliance));
        opList.add(new ItemData("DOCOMO NORMAL", "DC", R.mipmap.docomo));
        opList.add(new ItemData("DOCOMO SPECIAL", "DCS", R.mipmap.docomo));
        opList.add(new ItemData("TELENOR NORMAL", "UN", R.mipmap.uninor));
        opList.add(new ItemData("TELENOR SPECIAL", "UNS", R.mipmap.uninor));
        opList.add(new ItemData("VODAFONE", "VD", R.mipmap.vodafone));
       /* opList.add(new ItemData("MTNL Mumbai", "MM", R.mipmap.mtnl));*/
        return opList;
    }

    public ArrayList<ItemData> getDthOperator() {

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("AIRTEL DTH", "4", R.mipmap.airtel));
        list.add(new ItemData("DISH TV", "28", R.mipmap.dishtv));
        list.add(new ItemData("SUN TV", "30", R.mipmap.sundirect));
        list.add(new ItemData("BIG TV", "29", R.mipmap.big));
        list.add(new ItemData("TATA SKY", "5", R.mipmap.tatasky));
        list.add(new ItemData("Videocon D2H", "8", R.mipmap.videocondth));
        // list.add(new ItemData("JIO DTH", "Jiod", R.mipmap.jio));
        return list;
    }

    public ArrayList<ItemData> getPostpaidOpertor() {
        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("AIRCEL POSTPAID", "33", R.mipmap.aircel));
        list.add(new ItemData("AIRTEL POSTPAID", "34", R.mipmap.airtel));
        list.add(new ItemData("IDEA POSTPAID", "32", R.mipmap.idea));
        list.add(new ItemData("RELIANCE POSTPAID", "35", R.mipmap.reliance));
        list.add(new ItemData("VODAFONE POSTPAID", "27", R.mipmap.vodafone));
        list.add(new ItemData("DOCOMO POSTPAID", "31", R.mipmap.docomo));
        list.add(new ItemData("BSNL POSTPAID", "19", R.mipmap.bsnl));
        // list.add(new ItemData("MTS POSTPAID", "PM", R.mipmap.mts));
        return list;
    }

    public ArrayList<ItemData> getDatacardOpertor() {

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("AIRTEL", "56", R.mipmap.airtel));
        list.add(new ItemData("IDEA", "55", R.mipmap.idea));
        list.add(new ItemData("VODAFONE", "54", R.mipmap.vodafone));
        list.add(new ItemData("RELIANCE NETCONNECT", "4", R.mipmap.reliance));


        return list;
    }

    public ArrayList<ItemData> getDthSMSOperator() {
        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("AIRTEL DTH", "ATD", R.mipmap.airtel));
        list.add(new ItemData("DISH TV", "DT", R.mipmap.dishtv));
        list.add(new ItemData("RELIANCE BIG TV", "BT", R.mipmap.reliance));
        list.add(new ItemData("SUN TV", "SD", R.mipmap.sundirect));
        list.add(new ItemData("TATA SKY", "TS", R.mipmap.tatasky));
        list.add(new ItemData("VIDEOCON D2H", "VT", R.mipmap.videocondth));

        return list;
    }

    public ArrayList<ItemData> getElectricity() {

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("MSEB", "10", R.mipmap.mseb));
        list.add(new ItemData("TOURENT POWER", "11", R.mipmap.torrent));
        list.add(new ItemData("UTAR GUJRAT POWER", "12", R.mipmap.ugcl));
        list.add(new ItemData("BEST UNDERTAKING - MUMBAI", "40", R.mipmap.best));

        return list;
    }

    public ArrayList<ItemData> getPostpaidSMSOpertor() {

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("Select Operator", "-1", R.mipmap.ic_launcher));
        list.add(new ItemData("AIRCEL POSTPAID", "ACP", R.mipmap.aircel));
        list.add(new ItemData("AIRTEL POSTPAID", "PA", R.mipmap.airtel));
        list.add(new ItemData("IDEA POSTPAID", "PI", R.mipmap.idea));
        list.add(new ItemData("RELIANCE POSTPAID", "PR", R.mipmap.reliance));
        list.add(new ItemData("VODAFONE POSTPAID", "PV", R.mipmap.vodafone));
        list.add(new ItemData("DOCOMO POSTPAID", "DP", R.mipmap.docomo));
        list.add(new ItemData("BSNL POSTPAID", "PB", R.mipmap.bsnl));
        list.add(new ItemData("MTS POSTPAID", "PM", R.mipmap.mts));


        return list;
    }


}
