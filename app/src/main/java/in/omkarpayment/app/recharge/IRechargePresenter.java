package in.omkarpayment.app.recharge;

import java.util.ArrayList;

import in.omkarpayment.app.json.LastFiveRechargePojo;

/**
 * Created by Krish on 04-Aug-17.
 */

public interface IRechargePresenter {


    void errorAlert(String msg);

    void showDialog();

    void dismissDialog();

    void successAlert(String msg);

    void doValidation();

    boolean validation();


    void getLastTransaction();

    void getLastTransaction(ArrayList<LastFiveRechargePojo> latTransaction);

    void doRecharge();

    void rungetOperatorWebservice();

    void decryptRechargeResponse(String stringToDecrypt);

    void decryptOperatorResponse(String stringToDecrypt);


    void LastTransactionResponse(String encryptedResponse);

    void LastTransactionFailureResponse(String encryptedResponse);

    void getRoffers();

    void getViewPlans();

    void viewDTHPlans();

    void ViewCustomerInfo();
}
