package in.omkarpayment.app.recharge.electricity;

/**
 * Created by Krish on 11-Aug-17.
 */

public interface IElectricityBillView {

    void errorAlert(String msg);

    Integer getSelectSpinner();

    String getOperatorCode();

    void successAlert(String msg);

    void SpSelectionError();

    void showPDialog();

    void dismissPDialog();


    String ConsumerName();

    String BillingUnits();

    String PCycle();

    String Amount();

    void editConsumerNumberError();

    void editConsumerNameError();

    void editBillingUnitsError();

    void editPCycleError();

    void optional1Error();

    void optional2Error();

    void editAmountError();

    void viewBillResponse(ViewBillResponsePojo reportResponse);

    String getConsumerNo();

    String getServiceID();

    String getDeviceID();

    String getRechargePin();
String refrenceId();
    String getOptional1();

    String getOptional2();

    String getUserId();
}
