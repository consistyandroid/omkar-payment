package in.omkarpayment.app.recharge.recharge;

import android.app.Dialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.LastRechargeListData;
import in.omkarpayment.app.adapter.NewLastRechargePreReportAdapter;
import in.omkarpayment.app.adapter.SpinnerAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.browsPlan.BrowsePlansActivity;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.FragmentDthBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.DTHCustomerInfoPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.recharge.IRechargeView;
import in.omkarpayment.app.recharge.ItemData;
import in.omkarpayment.app.recharge.Operator;
import in.omkarpayment.app.recharge.RechargePresenterImpl;
import in.omkarpayment.app.report.lasttransaction.ILastTransactionView;
import in.omkarpayment.app.report.lasttransaction.LastTransactionPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by Krish on 10-Jun-17.
 */
public class DTHFragment extends Fragment implements IRechargeView, IAvailableBalanceView, ILastTransactionView {
    View rovView;
    Dialog dialog;
    RechargePresenterImpl rechargePresenter;
    AlertImpl alert;
    ArrayList<ItemData> list = new ArrayList<>();
    ArrayList<LastRechargeListData> DthRechargeList = new ArrayList<>(10);
    CustomProgressDialog progressDialog;
    LastTransactionPresenter lastTransactionPresenter;
    FragmentDthBinding fragmentDthBinding;
    RecyclerView listview;
    Operator objOperator = new Operator();
    EditText edtPin;
    Button btnSubmit;
    Double CurrentBalance;
    //NewLastRechargeReportAdapter adapter;
    NewLastRechargePreReportAdapter adapter;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    LogWriter log = new LogWriter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentDthBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dth, container, false);
        rovView = fragmentDthBinding.getRoot();
        rechargePresenter = new RechargePresenterImpl(this);
        lastTransactionPresenter = new LastTransactionPresenter(this);
        lastTransactionPresenter.getLastTransaction();
        alert = new AlertImpl(getContext());
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        listview = (RecyclerView) rovView.findViewById(R.id.listView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listview.setLayoutManager(mLayoutManager);
        listview.setItemAnimator(new DefaultItemAnimator());
        list = new Operator().getDthOperator();
        progressDialog = new CustomProgressDialog(getContext());
        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),
                R.layout.spinner_layout, R.id.txt, list);
        fragmentDthBinding.spOperator.setAdapter(adapter);

        fragmentDthBinding.btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rechargePresenter.doValidation();
            }
        });
        fragmentDthBinding.btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lastTransactionPresenter.getLastTransaction();
                        iAvailableBalancePresenter.getAvailableBalance();
                        showPDialog();
                    }
                }, 30);
            }
        });
        fragmentDthBinding.btnLastfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });
        fragmentDthBinding.btnViewPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechargePresenter.viewDTHPlans();
            }
        });
        fragmentDthBinding.btnMyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechargePresenter.ViewCustomerInfo();
            }
        });
        return rovView;
    }


    @Override
    public String getNumber() {
        return fragmentDthBinding.editNumber.getText().toString().trim();
    }

    @Override
    public String getAmount() {
        return fragmentDthBinding.editAmonut.getText().toString().trim();
    }

    @Override
    public void edtNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "DTH number"));
        fragmentDthBinding.editNumber.requestFocus();
    }

    @Override
    public void edtAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        fragmentDthBinding.editAmonut.requestFocus();

    }

    @Override
    public Integer getSelectSpinner() {
        return fragmentDthBinding.spOperator.getSelectedItemPosition();
    }

    @Override
    public void SpSelectionError() {
        alert.errorAlert(String.format("Please select operator"));
    }

    @Override
    public String serviceId() {
        return "2";
    }

    @Override
    public String mPin() {
        return "1234";//edtPin.getText().toString().trim();
    }

    @Override
    public Double currentBalance() {
        return CurrentBalance;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        dialog.show();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));

                } else {
                    dialog.dismiss();
                    rechargePresenter.doRecharge();
                    showPDialog();
                }
            }
        });
    }

    @Override
    public String getOperatorName() {
        ItemData operatorAPI = (ItemData) fragmentDthBinding.spOperator.getSelectedItem();
        String opName = "";
        if (operatorAPI.getText().equals("Videocon D2H")) {
            opName = "Videocon";
        } else {
            opName = operatorAPI.getText();
        }
        return opName;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 3 && !data.equals(null)) {
                String requiredValue = data.getStringExtra("amount");
                fragmentDthBinding.editAmonut.setText(requiredValue);
            }
        }
    }


    @Override
    public void errorAlert(String msg) {
        Log.i("Error", msg);
        dismissPDialog();
        alert.errorAlert(msg);
    }

    public void lastTransaction() {
        lastTransactionPresenter.getLastTransaction();
    }

    @Override
    public void successAlert(String msg) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clear();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lastTransactionPresenter.getLastTransaction();
                        // lastTransaction();
                    }
                }, 2000);

            }
        });
        dialog.show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lastTransactionPresenter.getLastTransaction();
                // lastTransaction();
            }
        }, 2000);
    }

    @Override
    public void filterDialog() {

    }

    @Override
    public void rechargeConfirmAlert() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", getNumber(), getAmount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  mPinDialog();
                dialog.dismiss();
                rechargePresenter.doRecharge();
                showPDialog();

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge) {
        dismissPDialog();
        adapter = new NewLastRechargePreReportAdapter(getContext(), lastRecharge);
        fragmentDthBinding.listView.setAdapter(adapter);
    }


    @Override
    public String getOperatorCode() {
        ItemData operatorAPI = (ItemData) fragmentDthBinding.spOperator.getSelectedItem();

        return operatorAPI.getOpcode();
    }


    @Override
    public void clear() {
        fragmentDthBinding.editNumber.setText("");
        fragmentDthBinding.editAmonut.setText("");
        fragmentDthBinding.spOperator.setSelection(0);
        //edtPin.setText("");
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void fetchOperator(String serviceName) {

    }


    @Override
    public void operatorResponse(ArrayList<OperatorPojo> operatorList) {
        Operator.allOperator = operatorList;
        fetchOperator("Dth");
    }

    @Override
    public void lastFiveResponseDialog() {

    }

    @Override
    public void getRoffers(String rofferPojos) {

    }

    @Override
    public void browsPlans(String respo) {
        log.i("response", respo);
        Intent bin = new Intent(getContext(), BrowsePlansActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("plansResponse", respo);
        bundle.putString("btnClick", "DTH");
        bin.putExtras(bundle);
        startActivityForResult(bin, 3);
    }

    @Override
    public void customerInfo(String response) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custominfo_dialog);
        TextView txtName = (TextView) dialog.findViewById(R.id.txtCustomerName);

        TextView txtStatus = (TextView) dialog.findViewById(R.id.txtStatus);
        TextView txtPlanName = (TextView) dialog.findViewById(R.id.txtPlanName);
        TextView txtBalance = (TextView) dialog.findViewById(R.id.txtBalance);
        TextView txtMonthlyPack = (TextView) dialog.findViewById(R.id.txtMonthlyRechargePack);
        TextView txtNextRechDate = (TextView) dialog.findViewById(R.id.txtNextRechargeDate);
        ImageView btnClose = (ImageView) dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

        Gson gson = new Gson();
        Log.i("record", "indialog");
        Log.i("response", response);
        DTHCustomerInfoPojo responseePojo = gson.fromJson(response, DTHCustomerInfoPojo.class);
        List<DTHCustomerInfoPojo.Record> ci = responseePojo.getRecords();
       /* Type listType = new TypeToken<ArrayList<DTHCustomerInfoPojo>>() {
        }.getType();
        List<DTHCustomerInfoPojo> dthrecord = gson.fromJson(response, listType);
        List<DTHCustomerInfoPojo.Record> record = dthrecord.get(0).getRecords();*/
        Log.i("record", ci.toString());
        try {
            txtName.setText(ci.get(0).getCustomerName());
            txtBalance.setText(ci.get(0).getBalance());
            txtNextRechDate.setText(ci.get(0).getNextRechargeDate());
            txtPlanName.setText(ci.get(0).getPlanname());
            txtStatus.setText(ci.get(0).getPlanname());
            txtMonthlyPack.setText(ci.get(0).getMonthlyRecharge());
            txtStatus.setText(ci.get(0).getStatus());
            Log.i("status", (ci.get(0).getStatus()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        dismissPDialog();
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {

            }

        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {
        dismissPDialog();
        for (SaleCommissionPojo obj : lastrasaction) {
            try {
                UserDetails.UserSaleCommission = obj.getDiscountAmount() + (obj.getDiscountRechargeCount());
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
