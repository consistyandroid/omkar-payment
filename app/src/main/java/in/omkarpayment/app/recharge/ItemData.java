package in.omkarpayment.app.recharge;

public class ItemData {

    String text;


    String opcode;
    Integer imageId;

    public ItemData(String text, String opcode, Integer imageId) {
        this.text = text;
        this.imageId = imageId;
        this.opcode=opcode;
    }

    public String getText() {
        return text;
    }

    public Integer getImageId() {
        return imageId;
    }

    public String getOpcode() {
        return opcode;
    }

    public void setOpcode(String opcode) {
        this.opcode = opcode;
    }

}
