package in.omkarpayment.app.recharge.recharge;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.NewLastRechargeReportAdapter;
import in.omkarpayment.app.adapter.SpinnerAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.FragmentPrepaidBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.recharge.IRechargeView;
import in.omkarpayment.app.recharge.ItemData;
import in.omkarpayment.app.recharge.Operator;
import in.omkarpayment.app.recharge.RechargePresenterImpl;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by Krish on 10-Jun-17.
 */

public class DatacardFragment extends Fragment implements IRechargeView, IAvailableBalanceView {

    View rovView;
    RechargePresenterImpl rechargePresenter;
    AlertImpl alert;
    ArrayList<ItemData> list = new ArrayList<>();
    FragmentPrepaidBinding fragmentPrepaidBinding;
    CustomProgressDialog progressDialog;
    Operator objOperator = new Operator();
    EditText edtPin;
    Button btnSubmit;
    Dialog dialog;
    Double CurrentBalance;
    IAvailableBalancePresenter iAvailableBalancePresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentPrepaidBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_prepaid, container, false);
        rovView = fragmentPrepaidBinding.getRoot();
        rechargePresenter = new RechargePresenterImpl(this);
        alert = new AlertImpl(getContext());
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        progressDialog = new CustomProgressDialog(getContext());
        list = new Operator().getDatacardOpertor();

        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),
                R.layout.spinner_layout, R.id.txt, list);
        fragmentPrepaidBinding.spOperator.setAdapter(adapter);

        fragmentPrepaidBinding.btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechargePresenter.doValidation();
            }
        });
  /*      fragmentPrepaidBinding.phoneBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    startActivityForResult(localIntent, 1);
                    *//*Intent i=new Intent(getActivity(),PickContact.class);
                    startActivity(i);*//*
                } catch (Exception e) {
                    Toast.makeText(getContext(), "Number is not in format" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });*/
        return rovView;
    }

    @Override
    public void edtNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "mobile number"));
        fragmentPrepaidBinding.editNumber.requestFocus();

    }

    @Override
    public void edtAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        fragmentPrepaidBinding.editAmonut.requestFocus();

    }

    @Override
    public Integer getSelectSpinner() {
        return fragmentPrepaidBinding.spOperator.getSelectedItemPosition();
    }

    @Override
    public void SpSelectionError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "operator"));
    }


    @Override
    public String serviceId() {
        return "1";
    }

    @Override
    public String mPin() {
        return edtPin.getText().toString().trim();
    }

    @Override
    public Double currentBalance() {
        return CurrentBalance;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        dialog.show();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));

                } else {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent) {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        switch (paramInt1) {
        }
        Cursor localCursor;
        do {
            while (paramInt2 != -1) {
                return;
            }
            localCursor = getActivity().managedQuery(paramIntent.getData(), null, null, null, null);
        } while (!localCursor.moveToFirst());
        String str1 = removeExtraCharacters(localCursor.getString(localCursor.getColumnIndexOrThrow("data1")));
        String str2 = str1.substring(-10 + str1.length());
        fragmentPrepaidBinding.editNumber.setText(str2);
    }

    private String removeExtraCharacters(String paramString) {
        try {
            paramString = paramString.replace("(", "");
            paramString = paramString.replace(")", "");
            paramString = paramString.replace("-", "");
            paramString = paramString.replace(" ", "");
            String str = paramString.replace("+", "");
            return str;
        } catch (Exception localException) {

        }
        return paramString;
    }

    @Override
    public String getOperatorName() {
        ItemData operatorAPI = (ItemData) fragmentPrepaidBinding.spOperator.getSelectedItem();
        return operatorAPI.getOpcode();
    }

    @Override
    public void errorAlert(String msg) {
        dismissPDialog();
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        dismissPDialog();
        clear();
        alert.successAlert(msg);

    }


    @Override
    public String getNumber() {

        return fragmentPrepaidBinding.editNumber.getText().toString().trim();
    }

    @Override
    public String getAmount() {

        return fragmentPrepaidBinding.editAmonut.getText().toString().trim();
    }

    @Override
    public void rechargeConfirmAlert() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", getNumber(), getAmount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechargePresenter.doRecharge();
                dialog.dismiss();
                showPDialog();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge) {
        NewLastRechargeReportAdapter  MobileAdapter  = new NewLastRechargeReportAdapter(getActivity(),lastRecharge);
        fragmentPrepaidBinding.listViewInPrepaid.setAdapter(MobileAdapter);
        MobileAdapter.notifyDataSetChanged();
    }

    @Override
    public String getOperatorCode() {
        ItemData operatorAPI = (ItemData) fragmentPrepaidBinding.spOperator.getSelectedItem();
        return operatorAPI.getOpcode();
    }

    @Override
    public void clear() {
        fragmentPrepaidBinding.editNumber.setText("");
        fragmentPrepaidBinding.editAmonut.setText("");
        fragmentPrepaidBinding.spOperator.setSelection(0);
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void fetchOperator(String serviceName) {
    }

    @Override
    public void operatorResponse(ArrayList<OperatorPojo> operatorList) {
        Operator.allOperator = operatorList;
        fetchOperator("Mobile");
    }

    @Override
    public void lastFiveResponseDialog() {

    }

    @Override
    public void getRoffers(String rofferPojos) {

    }

    @Override
    public void browsPlans(String respo) {

    }

    @Override
    public void customerInfo(String response) {

    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {

            }

        }

    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
