package in.omkarpayment.app.recharge.recharge;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.NewLastRechargePreReportAdapter;
import in.omkarpayment.app.adapter.SpinnerAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.browsPlan.BrowsePlansActivity;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.FragmentPrepaidBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.recharge.IRechargeView;
import in.omkarpayment.app.recharge.ItemData;
import in.omkarpayment.app.recharge.Operator;
import in.omkarpayment.app.recharge.RechargePresenterImpl;
import in.omkarpayment.app.report.lasttransaction.ILastTransactionView;
import in.omkarpayment.app.report.lasttransaction.LastTransactionPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by Krish on 10-Jun-17.
 */

public class PrepaidFragment extends Fragment implements IRechargeView, IAvailableBalanceView, ILastTransactionView {

    View rovView;
    RechargePresenterImpl rechargePresenter;
    LastTransactionPresenter lastTransactionPresenter;
    AlertImpl alert;
    ArrayList<ItemData> list = new ArrayList<>();
    FragmentPrepaidBinding fragmentPrepaidBinding;
    CustomProgressDialog progressDialog;
    Operator objOperator = new Operator();
    EditText edtPin;
    RecyclerView listViewInPrepaid;
    Button btnSubmit;
    Dialog dialog, dialogResponse;
    Double CurrentBalance;
    Animation animVanish;
    NewLastRechargePreReportAdapter adapter;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    ArrayList<LastFiveRechargePojo> PrepaidRecharge = new ArrayList<>(10);
    SwipeRefreshLayout swipeRefresh;
    LogWriter log = new LogWriter();
    String opName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentPrepaidBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_prepaid, container, false);
        rovView = fragmentPrepaidBinding.getRoot();
        animVanish = AnimationUtils.loadAnimation(getContext(), R.anim.blinkbutton);
        rechargePresenter = new RechargePresenterImpl(this);
        lastTransactionPresenter = new LastTransactionPresenter(this);

        alert = new AlertImpl(getContext());
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        listViewInPrepaid = (RecyclerView) rovView.findViewById(R.id.listViewInPrepaid);
        lastTransactionPresenter.getLastTransaction();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listViewInPrepaid.setLayoutManager(mLayoutManager);
        listViewInPrepaid.setItemAnimator(new DefaultItemAnimator());

        progressDialog = new CustomProgressDialog(getContext());
        list = new Operator().getMobileOpertor();

        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),
                R.layout.spinner_layout, R.id.txt, list);

        fragmentPrepaidBinding.spOperator.setAdapter(adapter);


        fragmentPrepaidBinding.btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lastTransactionPresenter.getLastTransaction();
                        iAvailableBalancePresenter.getAvailableBalance();
                        showPDialog();
                    }
                }, 30);
            }
        });
        fragmentPrepaidBinding.btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentPrepaidBinding.editNumber.getText().toString().length() == 10) {
                    rechargePresenter.doValidation();
                } else {
                    edtNumberError();
                }
            }
        });
        fragmentPrepaidBinding.btnROffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                rechargePresenter.getRoffers();

            }
        });
        fragmentPrepaidBinding.btnViewPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animVanish);
                rechargePresenter.getViewPlans();
            }
        });

        fragmentPrepaidBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });

        swipeRefresh = (SwipeRefreshLayout) rovView.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        lastTransactionPresenter.getLastTransaction();
                        showPDialog();
                    }
                }, 3000);
            }
        });
        fragmentPrepaidBinding.phnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    PrepaidFragment.this.startActivityForResult(localIntent, 1);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Number is not format" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return rovView;
    }


    @Override
    public void edtNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "mobile number"));
        fragmentPrepaidBinding.editNumber.requestFocus();

    }

    @Override
    public void edtAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        fragmentPrepaidBinding.editAmonut.requestFocus();
    }

    @Override
    public Integer getSelectSpinner() {
        return fragmentPrepaidBinding.spOperator.getSelectedItemPosition();

    }

    @Override
    public void SpSelectionError() {
        alert.errorAlert(String.format("Please select operator"));
    }

    @Override
    public String serviceId() {
        return "1";
    }

    @Override
    public String mPin() {
        return "1234";
    }

    @Override
    public Double currentBalance() {
        return CurrentBalance;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));
                } else {
                    dialog.dismiss();
                    rechargePresenter.doRecharge();
                    showPDialog();
                }
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == 1) {
                switch (requestCode) {
                }
                Cursor localCursor;
                do {
                    while (resultCode != -1) {
                        return;
                    }
                    localCursor = getActivity().managedQuery(data.getData(), null, null, null, null);
                } while (!localCursor.moveToFirst());
                String str1 = removeExtraCharacters(localCursor.getString(localCursor.getColumnIndexOrThrow("data1")));
                String str2 = str1.substring(-10 + str1.length());
                fragmentPrepaidBinding.editNumber.setText(str2);
            } else if (requestCode == 2 && !data.equals(null)) {
                String requiredValue = data.getStringExtra("amount");
                fragmentPrepaidBinding.editAmonut.setText(requiredValue);
            }
        }
    }

    private String removeExtraCharacters(String paramString) {
        try {
            paramString = paramString.replace("(", "");
            paramString = paramString.replace(")", "");
            paramString = paramString.replace("-", "");
            paramString = paramString.replace(" ", "");
            String str = paramString.replace("+", "");
            return str;
        } catch (Exception localException) {

        }
        return paramString;
    }

    @Override
    public String getOperatorName() {
        ItemData operatorAPI = (ItemData) fragmentPrepaidBinding.spOperator.getSelectedItem();
        if (operatorAPI.getText().equals("BSNL STV") || operatorAPI.getText().equals("BSNL TOPUP")) {
            opName = "BSNL";
        } else if (operatorAPI.getText().equals("TATA DOCOMO") || operatorAPI.getText().equals("DOCOMO SPECIAL")) {
            opName = "Tata Docomo";
        } else if (operatorAPI.getText().equals("TELENOR TOPUP") || operatorAPI.getText().equals("TELENOR STV")) {
            opName = "Telenor";
        } else if (operatorAPI.getText().equals("INDICOM/VIRGIN/T24") || operatorAPI.getText().equals("MTNL MUMBAI")) {
            opName = "Tata Indicome";
        } else if (operatorAPI.getText().equals("VODAFONE")) {
            opName = "Vodafone";
        } else if (operatorAPI.getText().equals("AIRTEL")) {
            opName = "Airtel";
        } else if (operatorAPI.getText().equals("IDEA")) {
            opName = "Idea";
        } else {
            opName = operatorAPI.getText();
        }
        return opName;
    }

    @Override
    public void errorAlert(String msg) {
        dismissPDialog();
        alert.errorAlert(msg);

    }

    @Override
    public void successAlert(String msg) {
        //lastTransaction();
        //alert.successAlert(msg);
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clear();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lastTransactionPresenter.getLastTransaction();
                        // lastTransaction();
                    }
                }, 2000);

            }
        });
        dialog.show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lastTransactionPresenter.getLastTransaction();
                // lastTransaction();
            }
        }, 2000);

    }

    @Override
    public void filterDialog() {
    }

    @Override
    public String getNumber() {

        return fragmentPrepaidBinding.editNumber.getText().toString().trim();
    }

    @Override
    public String getAmount() {

        return fragmentPrepaidBinding.editAmonut.getText().toString().trim();
    }


    @Override
    public void rechargeConfirmAlert() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", getNumber(), getAmount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                rechargePresenter.doRecharge();
                showPDialog();
                //mPinDialog();

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge) {
        dismissPDialog();
        adapter = new NewLastRechargePreReportAdapter(getContext(), lastRecharge);
        listViewInPrepaid.setAdapter(adapter);
    }

    public void lastTransaction() {
        lastTransactionPresenter.getLastTransaction();

    }

    @Override
    public String getOperatorCode() {
        ItemData operatorAPI = (ItemData) fragmentPrepaidBinding.spOperator.getSelectedItem();
        return operatorAPI.getOpcode();
    }

    @Override
    public void clear() {
        fragmentPrepaidBinding.editNumber.setText("");
        fragmentPrepaidBinding.editAmonut.setText("");
        fragmentPrepaidBinding.spOperator.setSelection(0);
//        edtPin.setText("");
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void fetchOperator(String serviceName) {
    }

    @Override
    public void operatorResponse(ArrayList<OperatorPojo> operatorList) {
        Operator.allOperator = operatorList;
        fetchOperator("Mobile");
    }

    @Override
    public void lastFiveResponseDialog() {
        dialogResponse = new Dialog(getActivity(), R.style.ThemeWithCorners);
        dialogResponse.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialogResponse.setContentView(R.layout.activity_lastfiveshow);
        dialogResponse.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialogResponse.setTitle("Last Five Response");
        dialogResponse.show();
        Button btnBack = (Button) dialogResponse.findViewById(R.id.btnBack);
        Button btnRefresh = (Button) dialogResponse.findViewById(R.id.btnRefresh);

        /*listFive = (RecyclerView) dialogResponse.findViewById(R.id.list);
        listFive.setAdapter(adapter);*/

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogResponse.dismiss();
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }

    @Override
    public void getRoffers(String rofferPojos) {
        log.i("rofferPojos", rofferPojos);
        Intent intent = new Intent(getContext(), RofferActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("rofferPojos", rofferPojos);
        intent.putExtras(bundle);
        startActivityForResult(intent, 2);
    }

    @Override
    public void browsPlans(String respo) {
        log.i("respo", respo);
        Intent bin = new Intent(getContext(), BrowsePlansActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("plansResponse", respo);
        bundle.putString("btnClick", "Mobile");
        bin.putExtras(bundle);
        startActivityForResult(bin, 2);
    }

    @Override
    public void customerInfo(String response) {

    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        dismissPDialog();
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();

            }

        }

    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {
        dismissPDialog();
        for (SaleCommissionPojo obj : lastrasaction) {
            try {
                UserDetails.UserSaleCommission = obj.getDiscountAmount() + (obj.getDiscountRechargeCount());
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
