package in.omkarpayment.app.recharge;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;

import in.omkarpayment.app.R;
import in.omkarpayment.app.recharge.recharge.DTHFragment;
import in.omkarpayment.app.recharge.recharge.PostpaidFragment;
import in.omkarpayment.app.recharge.recharge.PrepaidFragment;


public class RechargeActivity extends AppCompatActivity {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Bundle bundle;
    Context context = this;
    String btnPress;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        Bundle extras = getIntent().getExtras();
        btnPress = extras.getString("btnPress");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            if (btnPress.equalsIgnoreCase("Prepaid")) {
                title = "Prepaid Recharge";
            } else if (btnPress.equalsIgnoreCase("Postpaid")) {
                title = "Postpaid Recharge";
            } else if (btnPress.equalsIgnoreCase("DTH")) {
                title = "DTH Recharge";
            } else if (btnPress.equalsIgnoreCase("BalanceReport")) {
                title = "Transfer Report";
            } else if (btnPress.equalsIgnoreCase("SaleSummary")) {
                title = "SaleSummary Report";
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            SpannableString set = new SpannableString(title);

            set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_PRIORITY);
            getSupportActionBar().setTitle(set);
        }

        if (btnPress.equalsIgnoreCase("DTH")) {
            DTHFragment dthFragment = new DTHFragment();
            initfragment();
            dthFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.content_recharge, dthFragment);
            fragmentTransaction.commit();
        }
        if (btnPress.equalsIgnoreCase("Prepaid")) {
            PrepaidFragment prepaidFragment = new PrepaidFragment();
            initfragment();
            prepaidFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.content_recharge, prepaidFragment);
            fragmentTransaction.commit();
        }
        if (btnPress.equalsIgnoreCase("Postpaid")) {
            PostpaidFragment prepaidFragment = new PostpaidFragment();
            initfragment();
            prepaidFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.content_recharge, prepaidFragment);
            fragmentTransaction.commit();
        }
    }

    private void initfragment() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
