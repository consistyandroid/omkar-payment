package in.omkarpayment.app.recharge.electricity;

import in.omkarpayment.app.json.ElectricityBillViewArrayPojo;

import java.util.List;



public interface IElectricityBillPresenter {

    void errorAlert(String msg);

    void successAlert(String msg);

    void showPDialog();

    void dismissPDialog();

    void viewBill();

    void viewBillResponse(List<ElectricityBillViewArrayPojo> viewBillDetails);

    void decryptViewBillFailureResponse(String encryptedResponse);

    void decryptViewBillPaymentResponse(String encryptedResponse);

    void payBill();

    void payMSEBBill();

    void decryptPayBillResponse(String encryptedResponse);

    void decryptPayBillFailureResponse(String encryptedResponse);


}
