package in.omkarpayment.app.recharge.recharge;

import android.app.Dialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.LastRechargeListData;
import in.omkarpayment.app.adapter.NewLastRechargePreReportAdapter;
import in.omkarpayment.app.adapter.SpinnerAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.FragmentPrepaidBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.recharge.IRechargeView;
import in.omkarpayment.app.recharge.ItemData;
import in.omkarpayment.app.recharge.Operator;
import in.omkarpayment.app.recharge.RechargePresenterImpl;
import in.omkarpayment.app.report.lasttransaction.ILastTransactionView;
import in.omkarpayment.app.report.lasttransaction.LastTransactionPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by Krish on 05-Jul-17.
 */

public class PostpaidFragment extends Fragment implements IRechargeView, IAvailableBalanceView, ILastTransactionView {

    View rovView;

    RechargePresenterImpl rechargePresenter;
    AlertImpl alert;
    ArrayList<ItemData> list = new ArrayList<>();
    ArrayList<LastRechargeListData> PostpaidRechargeList = new ArrayList<>(10);

    CustomProgressDialog progressDialog;
    FragmentPrepaidBinding fragmentPrepaidBinding;
    LastTransactionPresenter lastTransactionPresenter;
    Operator objOperator = new Operator();
    EditText edtPin;
    Button btnSubmit;
    RecyclerView listView;
    Double CurrentBalance;
    NewLastRechargePreReportAdapter adapter;
    SwipeRefreshLayout swipeRefresh;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentPrepaidBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_prepaid, container, false);
        rovView = fragmentPrepaidBinding.getRoot();

        rechargePresenter = new RechargePresenterImpl(this);
        lastTransactionPresenter = new LastTransactionPresenter(this);

        lastTransactionPresenter.getLastTransaction();
        alert = new AlertImpl(getContext());
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        listView = (RecyclerView) rovView.findViewById(R.id.listViewInPrepaid);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());

        list = new Operator().getPostpaidOpertor();

        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(),
                R.layout.spinner_layout, R.id.txt, list);
        fragmentPrepaidBinding.spOperator.setAdapter(adapter);

        progressDialog = new CustomProgressDialog(getContext());
        rechargePresenter.getLastTransaction();

        fragmentPrepaidBinding.btnRecharge.setText(R.string.payment);
        fragmentPrepaidBinding.btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentPrepaidBinding.editNumber.getText().toString().length() == 10) {
                    rechargePresenter.doValidation();
                } else {
                    edtNumberError();
                }

            }
        });

        fragmentPrepaidBinding.btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lastTransactionPresenter.getLastTransaction();
                        iAvailableBalancePresenter.getAvailableBalance();
                        showPDialog();
                    }
                }, 30);
            }
        });
        fragmentPrepaidBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });

        fragmentPrepaidBinding.editNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (fragmentPrepaidBinding.editNumber.getText().toString().length() == 10)     //size as per your requirement
                {
                    fragmentPrepaidBinding.editAmonut.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        swipeRefresh = (SwipeRefreshLayout) rovView.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        lastTransactionPresenter.getLastTransaction();
                    }
                }, 3000);
            }
        });

        fragmentPrepaidBinding.phnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    PostpaidFragment.this.startActivityForResult(localIntent, 1);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Number is not format" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return rovView;
    }

    @Override
    public String getNumber() {
        return fragmentPrepaidBinding.editNumber.getText().toString().trim();
    }

    @Override
    public String getAmount() {
        return fragmentPrepaidBinding.editAmonut.getText().toString().trim();
    }

    @Override
    public void edtNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "number"));
        fragmentPrepaidBinding.editNumber.requestFocus();

    }

    @Override
    public void edtAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        fragmentPrepaidBinding.editAmonut.requestFocus();

    }

    @Override
    public Integer getSelectSpinner() {
        return fragmentPrepaidBinding.spOperator.getSelectedItemPosition();
    }

    @Override
    public void SpSelectionError() {
        alert.errorAlert(String.format("Please select operator"));
    }

    @Override
    public String serviceId() {
        return "1";
    }

    @Override
    public String mPin() {
        return "1234";//edtPin.getText().toString().trim();
    }

    @Override
    public Double currentBalance() {
        return CurrentBalance;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        dialog.show();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));

                } else {
                    dialog.dismiss();
                    rechargePresenter.doRecharge();
                    showPDialog();
                }
            }
        });
    }

    @Override
    public String getOperatorName() {
        ItemData operatorAPI = (ItemData) fragmentPrepaidBinding.spOperator.getSelectedItem();

        return operatorAPI.getOpcode();
    }


    @Override
    public void errorAlert(String msg) {
        dismissPDialog();
        alert.errorAlert(msg);

    }

    public void lastTransaction() {
        lastTransactionPresenter.getLastTransaction();

    }

    @Override
    public void successAlert(String msg) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clear();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lastTransactionPresenter.getLastTransaction();
                        // lastTransaction();
                    }
                }, 2000);

            }
        });
        dialog.show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lastTransactionPresenter.getLastTransaction();
                // lastTransaction();
            }
        }, 2000);

    }

    @Override
    public void filterDialog() {

    }

    @Override
    public void rechargeConfirmAlert() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", getNumber(), getAmount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mPinDialog();
                dialog.dismiss();
                rechargePresenter.doRecharge();
                showPDialog();

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge) {
        dismissPDialog();
        adapter = new NewLastRechargePreReportAdapter(getContext(), lastRecharge);
        fragmentPrepaidBinding.listViewInPrepaid.setAdapter(adapter);
    }

    @Override
    public String getOperatorCode() {
        ItemData operatorAPI = (ItemData) fragmentPrepaidBinding.spOperator.getSelectedItem();

        return operatorAPI.getOpcode();
    }

    @Override
    public void clear() {
        fragmentPrepaidBinding.editNumber.setText("");
        fragmentPrepaidBinding.editAmonut.setText("");

        fragmentPrepaidBinding.spOperator.setSelection(0);
        //edtPin.setText("");
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void fetchOperator(String serviceName) {

    }

    @Override
    public void operatorResponse(ArrayList<OperatorPojo> operatorList) {
        Operator.allOperator = operatorList;

        fetchOperator("Postpaid");
    }

    @Override
    public void lastFiveResponseDialog() {

    }

    @Override
    public void getRoffers(String rofferPojos) {

    }

    @Override
    public void browsPlans(String respo) {

    }

    @Override
    public void customerInfo(String response) {

    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        dismissPDialog();
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {
        dismissPDialog();
        for (SaleCommissionPojo obj : lastrasaction) {
            try {
                UserDetails.UserSaleCommission = obj.getDiscountAmount() + (obj.getDiscountRechargeCount());
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
