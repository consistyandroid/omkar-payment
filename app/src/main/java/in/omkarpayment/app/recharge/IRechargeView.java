package in.omkarpayment.app.recharge;


import java.util.ArrayList;

import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.OperatorPojo;

/**
 * Created by Krish on 6/6/17.
 */

public interface IRechargeView {


    String getNumber();

    String getAmount();

    void edtNumberError();

    void edtAmountError();

    Integer getSelectSpinner();

    void SpSelectionError();


    String serviceId();

    String mPin();

    Double currentBalance();

    void mPinDialog();

    String getOperatorName();

    void errorAlert(String msg);

    void successAlert(String msg);

    void rechargeConfirmAlert();

    void getLastTransaction(ArrayList<LastFiveRechargePojo> lastRecharge);

    String getOperatorCode();

    void clear();

    void showPDialog();

    void dismissPDialog();

    void fetchOperator(String serviceName);

    void operatorResponse(ArrayList<OperatorPojo> operatorList);

    void lastFiveResponseDialog();

    void getRoffers(String rofferPojos);

    void browsPlans(String respo);

    void customerInfo(String response);
}
