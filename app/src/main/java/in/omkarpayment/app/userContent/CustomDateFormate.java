package in.omkarpayment.app.userContent;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Krish on 26-May-17.
 */

public class CustomDateFormate {

    public static String getTime(String time, String inFormat, String outFormat) {
        String out = "";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inFormat);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outFormat);
        try {
            //   time = time.replaceAll("\"", "");
            Log.i("In time ", time);
            Date date = inputFormat.parse(time);

            out = outputFormat.format(date);

        } catch (ParseException e) {
            Log.i("ParseException Error ", e.toString());
        } catch (Exception e) {
            Log.i("Error in date", e.toString());
        }
        return out;
    }

    public static String getCurrentDate() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();

        return dateFormat.format(cal.getTime());
    }

    public static String getPreviousDateToShow() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.MONTH, -6);

        return dateFormat.format(cal.getTime());
    }

    public static String getCurrentDateToShow() {

        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        Calendar cal = Calendar.getInstance();

        return dateFormat.format(cal.getTime());
    }

}
