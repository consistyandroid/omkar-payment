package in.omkarpayment.app.userContent;

/**
 * Created by Krish on 11-Jul-17.
 */

public class SpinnerData {
    public static String[] spPurchase = {"Debit","Credit"};
    public static String[] spType = {"Transfer", "Receive"};
    public static String[] electricity = {"Select Operator", "MSEB"};
    public static String[] PayMode = {"Cash", "Bank Transfer", "Cash Deposit in Bank", "Credit"};
    public static String[] spDays = {"Select Day", "Today", "Two Days", "Five Days", "Ten Days"};
    public static String spDaysCode[] = {"-1", "0", "1", "5", "9"};
    public static String[] PayModeForAdmin = {"Cash", "Bank Transfer", "Cash Deposit in Bank"};

}
