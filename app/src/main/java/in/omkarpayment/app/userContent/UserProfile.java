package in.omkarpayment.app.userContent;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.widget.TextView;

import in.omkarpayment.app.R;

public class UserProfile extends AppCompatActivity {

    TextView txtUName, txtMobile, txtRole, txtBalance;

    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtRole = (TextView) findViewById(R.id.txtRole);
        txtUName = (TextView) findViewById(R.id.txtUName);
        txtMobile = (TextView) findViewById(R.id.txtMobile);
        txtBalance = (TextView) findViewById(R.id.txtBalance);

        //  txtUName.setTypeface(myTypeface);
        if (getSupportActionBar() != null) {
            String title = "Profile";
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setIcon(R.mipmap.ic_launcher);
            SpannableString set = new SpannableString(title);
            set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            getSupportActionBar().setTitle(set);
            final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        txtMobile.setText(UserDetails.MobileNumber);
        txtUName.setText(UserDetails.Username);
        txtRole.setText(UserDetails.UserType);
        txtBalance.setText(String.valueOf(UserDetails.UserBalance));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
