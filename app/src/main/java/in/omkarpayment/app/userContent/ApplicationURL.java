package in.omkarpayment.app.userContent;

/**
 * Created by Krish on 30-Jun-17.
 */

public class ApplicationURL {
    public static String base_URL = "https://omkarpayment.in/webservices/";
    public static String PlayStoreURL = "https://play.google.com/store/apps/details?id=in.omkarpayment.app";
    public static String electricityBill_URL = "http://billing.consisty.com/";
    public static String aeps_URL = "https://omkarpayment.in/";
    public static String AMoneyTransfer_URL = "https://omkarpayment.in/ADMR/WebService/";
    public static String allE_MoneyTransfer_URL="https://omkarpayment.in/AlleDMR/";

}
