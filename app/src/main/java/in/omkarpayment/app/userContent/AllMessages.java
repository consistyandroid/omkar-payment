package in.omkarpayment.app.userContent;

/**
 * Created by ${user} on 25/2/17.
 */

public class AllMessages {


    public static String internetError = "Internet connection is not available";
    public static String unableToFetchData = "Unable to fetch data/Restart app";
    public static String currentBalance = "Your current balance is Rs.";
    public static String wrongUsernamePassword = "Please enter valid username or password";
    public static String pleaseEnter = "Please enter %s";
    public static String somethingWrong = "Something wrong";
    public static String responseError = "Response error";
    public static String errorMsgForgotPassword = "Please enter correct lapuid and mobile No";
    public static String pleaseSelect = "Please select %s";
    public static String pleaseWait = "Please Wait....";
    public static String comingSoon = "Coming soon....";
    public static String dataNot = "Data not found";
    public static String PasswordValidationMsg = "Password should contains number,splchar,capslater";
    public static String lowBalanceMessage = "Your current balance is lower than recharge amount";
    public static String minBalanceMessage = "Your current balance is lower than bill amount";
    public static String minBalanceTransferMessage = "Your current balance is lower than tranfer amount";
    public static String minDmrBalanceTransferMessage = "Your current DMR balance is lower than tranfer amount";
    public static String pleaseCheckInternet = "PLease check your internet connection";
    public static String InvalidUser = "Invalid user";
    public static String AppName = "Omkar Payment";
    public static String termsConsitionURL = "";
    public static String pleaseCheckContact = "Please check entered details";


    public static String transferError = "User should be different";
    public static String PageNotFound = "Page not found..!";
    public static String ServerError = "Internal Server Error, means that server cannot process the request for an unknown reason";
    public static String wrongResponce = "Wrong response";
    public String logoutConfirm = "Are you sure.?";
    public String strrole[] = {"Select Role", "RETAILER", "DISTRIBUTER", "API USER"};
    public String[] array_type = {"Select Type", "Receiver", "Transfer"};
    public static String DMRBalancelow = "Your current DMR balance are low";

}
