package in.omkarpayment.app.userContent;

public class Validation {

    public boolean isNullOrEmpty(String data) {
        if (data == null)
            return false;

        if (data.equals(""))
            return false;

        if (data.isEmpty())
            return false;

        if (data.length() <= 0)
            return false;

        return true;
    }

    public boolean isValidSenderMobileNumber(String senderMobileNumber) {

        if (!isNullOrEmpty(senderMobileNumber)) {
            return false;
        }
        if (senderMobileNumber.length() != 10) {
            return false;
        }
        return true;
    }

    public boolean isValidSenderPinCode(String senderPinCode) {

        if (!isNullOrEmpty(senderPinCode)) {
            return false;
        }
        if (senderPinCode.length() != 6) {
            return false;
        }
        return true;
    }

    public boolean isValidAccountNumber(String accountNumber) {

        if (!isNullOrEmpty(accountNumber)) {
            return false;
        }
        if (accountNumber.length() < 4) {
            return false;
        }
        return true;
    }

    public boolean isValidIFSCode(String IFSCode) {

        if (!isNullOrEmpty(IFSCode)) {
            return false;
        }
        if (IFSCode.length() < 9) {
            return false;
        }
        return true;
    }

}
