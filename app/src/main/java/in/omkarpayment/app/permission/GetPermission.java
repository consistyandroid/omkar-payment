package in.omkarpayment.app.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;

import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.Calendar;

import in.omkarpayment.app.currentLocationService.LocationPojo;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by Krish on 01-Jul-17.
 */

public class GetPermission implements IGetPermission {
    Context context;


    public GetPermission(Context context) {
        this.context = context;
    }

    public void CheckPermission() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // READ_PHONE_STATE permission has not been granted.
            requestReadPhoneStatePermission();
        } else {
            // READ_PHONE_STATE permission is already been granted.
            doPermissionGrantedStuffs();
        }
    }

    private void requestReadPhoneStatePermission() {
        int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                Manifest.permission.READ_PHONE_STATE)) {

            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

        } else {
            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }
    }

    public boolean isLocationPermissionGranted() {
        // check location access permission is granted or not
        return (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED);
    }

    public void RequestLocationPermission() {
        int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;
        ActivityCompat.requestPermissions((Activity) context,
                new String[]
                        {
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                        },
                MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
    }
    public void doPermissionGrantedStuffs() {
        getPermission();
    }

    @Override
    public void getPermission() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_PHONE_STATE) ==
                    PackageManager.PERMISSION_GRANTED) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    UserDetails.IMEI = Build.ID;
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (telephonyManager.getImei() != null) {
                        UserDetails.IMEI = telephonyManager.getImei();
                    } else {
                        UserDetails.IMEI = Build.getSerial();
                    }
                } else {
                    if (telephonyManager.getDeviceId() != null) {
                        UserDetails.IMEI = telephonyManager.getDeviceId();
                    } else {
                        UserDetails.IMEI = Build.ID;
                    }
                }
                if (UserDetails.IMEI == null) {
                    UserDetails.IMEI = Build.ID;
                }
                String manufacturer = Build.MANUFACTURER;
                String model = Build.MODEL;
                Log.i("IMEI", UserDetails.IMEI);

                LocationPojo locationPojo = new LocationPojo();
                locationPojo.setLatitude(0.0);
                locationPojo.setLongitude(0.0);
                locationPojo.setAccuracy(Float.valueOf("0"));
                locationPojo.setAddress("");
                locationPojo.setLastUpdated(Calendar.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
