package in.omkarpayment.app.purchaseReport;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.json.PurchaseReportPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by consisty on 9/10/17.
 */

public class PurchaseReportPresenterImpl implements IPurchaseReportPresenter{

    IPurchaseReportView iPurchaseReportView;
    LogWriter log = new LogWriter();

    public PurchaseReportPresenterImpl(IPurchaseReportView iPurchaseReportView) {
        this.iPurchaseReportView = iPurchaseReportView;
    }

    @Override
    public void errorAlert(String msg) {
        iPurchaseReportView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iPurchaseReportView.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        iPurchaseReportView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iPurchaseReportView.dismissPDialog();
    }

    @Override
    public void validate() {
        if (iPurchaseReportView.fromDate().isEmpty() || iPurchaseReportView.fromDate().equals("")) {
            iPurchaseReportView.txtFromDateError();
        } else if (iPurchaseReportView.toDate().isEmpty() || iPurchaseReportView.toDate().equals("")) {
            iPurchaseReportView.txtToDateError();
        } else {
            if (iPurchaseReportView.reportType().equals("Debit")) {
                transferReport();
            } else if (iPurchaseReportView.reportType().equals("Credit")) {
                receiveReport();
            } else {
                transferReport();
            }
        }
    }



    @Override
    public void PurchaseReportResponseFailResponse(String encryptedResponse) {
        String body = "";
        log.i("Report response", encryptedResponse);
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            errorAlert(body);
        } catch (Exception e) {

        }
    }

    @Override
    public void PurchaseReportResponseResponse(String encryptedResponse) {
        String body = "";

        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            log.i("Report body response", body);
            ArrayList<PurchaseReportPojo> reportResponse = gson.fromJson(body, new TypeToken<ArrayList<PurchaseReportPojo>>() {
            }.getType());
            if (reportResponse != null) {
                iPurchaseReportView.getReportResponse(reportResponse);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void transferReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iPurchaseReportView.fromDate());
            dayreportrequest.put("ToDate", iPurchaseReportView.toDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegatePurchaseReport = this;
                model.webserviceMethod(encryptString, keyData, "PurchaseReport");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiveReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iPurchaseReportView.fromDate());
            dayreportrequest.put("ToDate", iPurchaseReportView.toDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegatePurchaseReport = this;
                model.webserviceMethod(encryptString, keyData, "PurchaseReportCredit");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void walletTransferReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iPurchaseReportView.fromDate());
            dayreportrequest.put("ToDate", iPurchaseReportView.toDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegatePurchaseReport = this;
                model.webserviceMethod(encryptString, keyData, "PurchaseReport");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissDialog() {
        iPurchaseReportView.dismissPDialog();
    }
}
