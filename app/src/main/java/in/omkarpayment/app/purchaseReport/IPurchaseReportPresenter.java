package in.omkarpayment.app.purchaseReport;

/**
 * Created by consisty on 9/10/17.
 */

public interface IPurchaseReportPresenter {

    void errorAlert(String msg);

    void successAlert(String msg);


    void showPDialog();

    void dismissPDialog();

    void validate();

    void PurchaseReportResponseFailResponse(String encryptedResponse);

    void PurchaseReportResponseResponse(String encryptedResponse);

    void transferReport();

    void receiveReport();

    void walletTransferReport();

    void dismissDialog();
}
