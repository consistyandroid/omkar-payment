package in.omkarpayment.app.purchaseReport;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.PurchaseAdapterlist;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.PurchaseReportPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.CustomDateFormate;
import in.omkarpayment.app.userContent.SpinnerData;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

/**
 * Created by consisty on 9/10/17.
 */

public class PurchaseReportActivity extends AppCompatActivity implements IPurchaseReportView {
    ArrayAdapter<String> spAdapter;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    String strFromDate = "", strToDate = "", ReportTypeCredit = "Credit", ReportTypeDebit = "Debit";
    AlertImpl alert;
    CustomProgressDialog pDialog;
    IPurchaseReportPresenter iPurchaseReportPresenter;
    PurchaseAdapterlist adapter;
    RecyclerView listView;
    Spinner spType;
    Context context;
    TextView txtFromDate, txtToDate;
    NetworkState ns = new NetworkState();
    Button btnShow;
    LinearLayout LaySpinner, LayReceiverLayout, LaySenderLayout, dateFieldBackground;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_report);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Purchase Report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        listView = (RecyclerView) findViewById(R.id.listv);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(null);

        spType = (Spinner) findViewById(R.id.spType);
        LaySpinner = (LinearLayout) findViewById(R.id.LaySpinner);
        LayReceiverLayout = (LinearLayout) findViewById(R.id.LayReceiveText);
        LaySenderLayout = (LinearLayout) findViewById(R.id.LaySenderText);
        dateFieldBackground = (LinearLayout) findViewById(R.id.dateFieldBackground);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        btnShow = (Button) findViewById(R.id.btnShow);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);


        spAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SpinnerData.spPurchase);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(spAdapter);

        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        iPurchaseReportPresenter = new PurchaseReportPresenterImpl(this);
        if (UserDetails.UserType.equals(UserType.Retailer)) {

            spType.setSelection(1);
            LaySpinner.setVisibility(View.GONE);
            LaySenderLayout.setVisibility(View.GONE);
        } else if (UserDetails.UserType.equals(UserType.Admin)) {

            LaySpinner.setVisibility(View.GONE);
            LaySenderLayout.setVisibility(View.GONE);
            LayReceiverLayout.setVisibility(View.GONE);
        } else {

            LayReceiverLayout.setVisibility(View.GONE);
            LaySenderLayout.setVisibility(View.GONE);
        }

        txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(PurchaseReportActivity.this, Frmdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txtToDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(PurchaseReportActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Frmdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strFromDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                txtFromDate.setText(Date);
                txtFromDate.setError(null);


            }
        };
        todate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strToDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                txtToDate.setText(Date);
                txtToDate.setError(null);


            }
        };

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(context)){
                    iPurchaseReportPresenter.validate();
                }else {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

            }
        });


        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ReportTypeCredit = SpinnerData.spPurchase[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });


    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public String reportType() {
        return ReportTypeCredit;
    }

    @Override
    public String fromDate() {
        return strFromDate;
    }

    @Override
    public String toDate() {
        return strToDate;
    }

    @Override
    public void txtFromDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "From Date"));
    }

    @Override
    public void txtToDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "To Date"));
    }

    @Override
    public void getReportResponse(ArrayList<PurchaseReportPojo> reportResponse) {
        adapter = new PurchaseAdapterlist(reportResponse);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
