package in.omkarpayment.app.purchaseReport;

import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.json.PurchaseReportPojo;

import java.util.ArrayList;

/**
 * Created by consisty on 9/10/17.
 */

public interface IPurchaseReportView {

    void errorAlert(String msg);

    void successAlert(String msg);


    void showPDialog();

    void dismissPDialog();

    String reportType();

    String fromDate();

    String toDate();

    void txtFromDateError();

    void txtToDateError();

    void getReportResponse(ArrayList<PurchaseReportPojo> reportResponse);
}
