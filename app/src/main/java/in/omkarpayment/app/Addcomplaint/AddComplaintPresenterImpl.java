package in.omkarpayment.app.Addcomplaint;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by consisty on 22/1/18.
 */

public class AddComplaintPresenterImpl implements IAddComplaintPresenter {
    IAddComplaintView iAddComplaintView;

    public AddComplaintPresenterImpl(IAddComplaintView iAddComplaintView) {
        this.iAddComplaintView = iAddComplaintView;
    }

    @Override
    public void addComplaintValidation() {
        if (validation()) {

            reportComplaint();
        }
    }

    public boolean validation() {
        if (iAddComplaintView.getSubject().equalsIgnoreCase("Select Subject".toLowerCase())) {
            iAddComplaintView.subjectError();
            return false;
        } else {
            if (iAddComplaintView.getSubject().contains("Other Complaint")) {
                if (iAddComplaintView.edtDescription().isEmpty()) {
                    iAddComplaintView.edtDescriptionError();
                    return false;
                }
            } else if (iAddComplaintView.getSubject().contains("Prepaid Complaint") || iAddComplaintView.getSubject().equalsIgnoreCase("Postpaid Complaint")
                    || iAddComplaintView.getSubject().contains("Dth Complaint") || iAddComplaintView.getSubject().equalsIgnoreCase("Electricity Complaint")) {
                if (iAddComplaintView.edtCustomerNo().isEmpty()) {
                    iAddComplaintView.edtCustomerError();
                    return false;
                }
                if (iAddComplaintView.getOperator().equalsIgnoreCase("SELECT OPERATOR")) {
                    iAddComplaintView.operatorError();
                    return false;
                }
                if (iAddComplaintView.edtTransactionNo().isEmpty()) {
                    iAddComplaintView.edtTransactionError();
                    return false;
                }
                if (iAddComplaintView.edtDescription().isEmpty()) {
                    iAddComplaintView.edtDescriptionError();
                    return false;
                }
            }
        }
       /* } else */
        return true;

    }

    @Override
    public void addOtherComplaintValidation() {

    }

    @Override
    public void decryptAddComplaintResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            if (body != null) {
                iAddComplaintView.getAddSuccessResponse(body);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void decryptAddComplaintFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            if (body != null) {
                iAddComplaintView.getAddFailureResponse(body);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void dismissPDialog() {

    }

    @Override
    public void errorAlert(String messageFailure) {

    }

    private void reportComplaint() {

        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addcomplaint = new JSONObject();
        try {


            addcomplaint.put("Subject", iAddComplaintView.getSubject());
            addcomplaint.put("CustomerNumber", iAddComplaintView.edtCustomerNo());
            addcomplaint.put("Operator", iAddComplaintView.getOperator());
            addcomplaint.put("TransactionNumber", iAddComplaintView.edtTransactionNo());
            addcomplaint.put("Description", iAddComplaintView.edtDescription());
            String jsonrequest = addcomplaint.toString();
            Log.i("add complaint Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateAddComplaint = this;
                model.webserviceMethod(encryptString, keyData, "AddComplaint");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
