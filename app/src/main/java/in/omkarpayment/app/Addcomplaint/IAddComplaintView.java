package in.omkarpayment.app.Addcomplaint;

/**
 * Created by consisty on 12/1/18.
 */

public interface IAddComplaintView {
    String getSubject();

    String getOperator();

    String edtDescription();

    String edtCustomerNo();

    String edtTransactionNo();

    void subjectError();

    void operatorError();

    void edtDescriptionError();

    void edtCustomerError();

    void edtTransactionError();

    void getAddSuccessResponse(String body);

    void getAddFailureResponse(String body);
}
