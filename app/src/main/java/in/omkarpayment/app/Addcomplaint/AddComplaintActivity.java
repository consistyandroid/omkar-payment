package in.omkarpayment.app.Addcomplaint;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;


public class AddComplaintActivity extends AppCompatActivity implements IAddComplaintView {

    Button btnReset, btnAddComplaint;
    EditText edtDescription, edtTransactionNo, edtCustomernumber;
    LinearLayout layoutOperator;
    Spinner spSubject, spOperator;
    Context context;
    AlertImpl alert;
    IAddComplaintPresenter iAddComplaintPresenter;
    String subject, operator, item;
    TextView txtCustomerNo, txtTransactionNo, txtOperator, txtcustno;
    int opPrepaid, opPostpaid, opDth, opElectricity;
    int operatorList;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_complaint);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        iAddComplaintPresenter = new AddComplaintPresenterImpl(this);
        alert = new AlertImpl(this);
        btnAddComplaint = (Button) findViewById(R.id.btnAddComplaint);
        btnReset = (Button) findViewById(R.id.btnReset);
        edtDescription = (EditText) findViewById(R.id.edtDescription);
        edtTransactionNo = (EditText) findViewById(R.id.edtTransactionNo);
        edtCustomernumber = (EditText) findViewById(R.id.edtCustomernumber);
        spSubject = (Spinner) findViewById(R.id.spSubject);
        spOperator = (Spinner) findViewById(R.id.spOperatorr);
        txtCustomerNo = (TextView) findViewById(R.id.txtcustno);
        txtTransactionNo = (TextView) findViewById(R.id.txttranno);
        txtOperator = (TextView) findViewById(R.id.txtoperator);
        txtcustno = (TextView) findViewById(R.id.txtcustno);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        layoutOperator = (LinearLayout) findViewById(R.id.layoutOperator);
        layoutOperator.setVisibility(View.GONE);
        String title = "Add Complaint";
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        ArrayAdapter<String> Subject_adapter = new ArrayAdapter<String>(AddComplaintActivity.this, R.layout.spinner_layout, R.id.txt, getResources().getStringArray(R.array.subject));
        spSubject.setAdapter(Subject_adapter);
        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                subject = getResources().getStringArray(R.array.subject)[position];
                selectOperator();
                item = (String) spSubject.getItemAtPosition(position);
                if (item.equalsIgnoreCase("Other Complaint")) {
                    spOperator.setVisibility(View.GONE);
                    edtTransactionNo.setVisibility(View.GONE);
                    txtOperator.setVisibility(View.GONE);
                    txtTransactionNo.setVisibility(View.GONE);
                    edtCustomernumber.setVisibility(View.GONE);
                    txtcustno.setVisibility(View.GONE);

                } else {
                    spOperator.setVisibility(View.VISIBLE);
                    edtTransactionNo.setVisibility(View.VISIBLE);
                    txtOperator.setVisibility(View.VISIBLE);
                    txtTransactionNo.setVisibility(View.VISIBLE);
                    edtCustomernumber.setVisibility(View.VISIBLE);
                    txtcustno.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });


        btnAddComplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iAddComplaintPresenter.addComplaintValidation();

            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clear();
            }
        });
    }

    private void selectOperator() {
        if (subject.equalsIgnoreCase("Prepaid Complaint")) {
            layoutOperator.setVisibility(View.VISIBLE);
            adapterForOperator(adapter, getResources().getStringArray(R.array.opPrepaid));
        } else if (subject.equalsIgnoreCase("Postpaid Complaint")) {
            layoutOperator.setVisibility(View.VISIBLE);
            adapterForOperator(adapter, getResources().getStringArray(R.array.opPostpaid));
        } else if (subject.equalsIgnoreCase("Dth Complaint")) {
            layoutOperator.setVisibility(View.VISIBLE);
            adapterForOperator(adapter, getResources().getStringArray(R.array.opDth));
        } else if (subject.equalsIgnoreCase("Electricity Complaint")) {
            layoutOperator.setVisibility(View.VISIBLE);
            adapterForOperator(adapter, getResources().getStringArray(R.array.opElectricity));
        } else if (subject.equalsIgnoreCase("Other Complaint")) {
            layoutOperator.setVisibility(View.VISIBLE);
            adapterForOperator(adapter, getResources().getStringArray(R.array.operator));
        }
    }

    private void adapterForOperator(ArrayAdapter<String> adapter, final String[] stringArray) {
        adapter = new ArrayAdapter<String>(AddComplaintActivity.this, R.layout.spinner_layout, R.id.txt, stringArray);
        spOperator.setAdapter(adapter);
        spOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                operator = stringArray[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void clear() {
        edtDescription.setText("");
        edtTransactionNo.setText("");
        edtCustomernumber.setText("");
        spSubject.setSelection(0);
        spOperator.setSelection(0);
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getOperator() {
        return operator;
    }

    @Override
    public String edtDescription() {
        return edtDescription.getText().toString();
    }

    @Override
    public String edtCustomerNo() {
        return edtCustomernumber.getText().toString();
    }

    @Override
    public String edtTransactionNo() {
        return edtTransactionNo.getText().toString();
    }

    @Override
    public void subjectError() {
        alert.errorAlert("Please select subject");
    }

    @Override
    public void operatorError() {
        alert.errorAlert("Please select operator");
    }

    @Override
    public void edtDescriptionError() {
        alert.errorAlert("Please enter description");
    }

    @Override
    public void edtCustomerError() {
        alert.errorAlert("Please enter cunstomer number");
    }

    @Override
    public void edtTransactionError() {
        alert.errorAlert("Please enter transaction number");
    }

    @Override
    public void getAddSuccessResponse(String body) {
        alert.successAlert(body);
        clear();
    }

    @Override
    public void getAddFailureResponse(String body) {
        alert.errorAlert(body);
        clear();
    }
}
