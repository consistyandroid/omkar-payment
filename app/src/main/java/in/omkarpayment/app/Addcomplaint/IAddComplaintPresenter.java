package in.omkarpayment.app.Addcomplaint;

/**
 * Created by consisty on 12/1/18.
 */

public interface IAddComplaintPresenter {
    void addComplaintValidation();

    void addOtherComplaintValidation();

    void decryptAddComplaintResponse(String encryptedResponse);

    void decryptAddComplaintFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void errorAlert(String messageFailure);
}
