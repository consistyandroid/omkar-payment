package in.omkarpayment.app.main;

import android.util.Log;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.DownlineListPojo;
import in.omkarpayment.app.json.Notification;
import in.omkarpayment.app.json.StatusResponcePojo;
import in.omkarpayment.app.json.StatusResponsePojo;
import in.omkarpayment.app.model.WebServiceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by consisty on 6/9/17.
 */

public class MainPresenter implements IMainPresenter {

    IMainView iMainView;

    public MainPresenter(IMainView iMainView) {
        this.iMainView = iMainView;
    }


    @Override
    public void getNotification() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateMainResponse = this;
        model.webserviceMethod("", keyData, "GetNotification");
    }

    @Override
    public void selectNotification(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Body",body.toString());
            ArrayList<Notification> notification = gson.fromJson(body, new TypeToken<ArrayList<Notification>>() {
            }.getType());
            if (notification != null) {
                iMainView.Statusresponse(notification);
                Log.i("Notificatin",notification.toString());
            }
        } catch (Exception e) {

        }
    }


    @Override
    public void selectNotificationFailureResponse(String encryptedResponse) throws Exception {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        body = data.Decrypt(encryptedResponse);
        Log.i("BodyFFNoti",body.toString());
    }
}
