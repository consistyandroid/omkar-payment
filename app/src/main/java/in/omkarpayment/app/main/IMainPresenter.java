package in.omkarpayment.app.main;

/**
 * Created by consisty on 6/9/17.
 */

public interface IMainPresenter  {
    void getNotification();

    void selectNotification(String encryptedResponse);

  /*  void dismissPDialog();
*/
    void selectNotificationFailureResponse(String encryptedResponse) throws Exception;
}
