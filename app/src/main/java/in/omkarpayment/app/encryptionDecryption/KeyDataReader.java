package in.omkarpayment.app.encryptionDecryption;


import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.UserDetails;

import org.json.JSONObject;

/**
 * Created by ${user} on 14/4/17.
 */

public class KeyDataReader {
    public String get() {
        LogWriter log = new LogWriter();
        String keyData = "";
        try {
            Cryptography_Android data = new Cryptography_Android();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserID", UserDetails.UserId);
            jsonObject.put("Token", UserDetails.Token);

            String datatoEncrypt = jsonObject.toString();
            log.i("KEY DATA", datatoEncrypt);
            keyData = data.Encrypt(datatoEncrypt);
            log.i("KEY DATA encrypt", keyData);

        } catch (Exception e) {

        }
        return keyData;
    }
}
