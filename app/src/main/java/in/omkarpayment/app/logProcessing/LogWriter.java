package in.omkarpayment.app.logProcessing;

import android.util.Log;

/**
 * Created by ${user} on 15/4/17.
 */

public class LogWriter {

    static boolean isTesting = true;

    private static boolean isValid(String tag, String message) {
        if (tag.trim().length() <= 0) {
            return false;
        }
        if (message == null) {
            return false;
        }
        if (message.trim().length() <= 0) {
            return false;
        }
        return true;
    }

    public static void i(String tag, String message) {

        if (isTesting && isValid(tag, message)) {
            //System.gc().
            Log.i(tag, message);
        }
    }

    public static void e(String tag, String message) {

        if (isTesting && isValid(tag, message)) {
            //System.gc().
            Log.e(tag, message);
        }
    }

    public static void d(String tag, String message) {

        if (isTesting && isValid(tag, message)) {
            //System.gc().
            Log.d(tag, message);
        }
    }

    public static void w(String tag, String message) {

        if (isTesting && isValid(tag, message)) {
            //System.gc().
            Log.w(tag, message);
        }
    }
}
