package in.omkarpayment.app.alert;

/**
 * Created by Krish on 05-Jun-17.
 */

public interface IAlert {

    void successAlert(String status);

    void errorAlert(String status);

    void warningAlert(String status);
}
