package in.omkarpayment.app.progressDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ProgressBar;

import in.omkarpayment.app.R;

import static android.provider.Settings.Global.getString;


/**
 * Created by Krish on 01-Jul-17.
 */

public class CustomProgressDialog implements IPDialog {
    Context context;
    ProgressDialog pDialog;

    public CustomProgressDialog(Context context) {
        this.context = context;
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please Wait");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
    }

    @Override
    public void showPDialog() {
        if (pDialog != null) {
            pDialog.show();
        }
    }
    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }

    }
}
