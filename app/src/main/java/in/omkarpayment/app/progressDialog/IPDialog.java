package in.omkarpayment.app.progressDialog;

/**
 * Created by Krish on 01-Jul-17.
 */

public interface IPDialog {

    void showPDialog();

    void dismissPDialog();
}
