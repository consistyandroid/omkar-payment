package in.omkarpayment.app.dataBaseProcessing;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String dataBaseName = "FCMNotification.db";
    public static final String tableNotificationHistory = "NotificationHistory";

    public DataBaseHelper(Context context) {
        super(context, dataBaseName, null, 1);
        // SQLiteDatabase db = this.getWritableDatabase();
        /*Log.i("db Constructor", "db Created");
        onCreate(db);*/
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table if not exists NotificationHistory (HistoryID INTEGER PRIMARY KEY AUTOINCREMENT, DOC TEXT, NotificationMessage TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS NotificationHistory");
        onCreate(db);
    }

    public boolean insertNotification(String DOC, String NotificationMessage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("DOC", DOC);
        contentValues.put("NotificationMessage", NotificationMessage);

        long result = db.insert(tableNotificationHistory, null, contentValues);

        if (result != -1) {
            return true;
        }
        return false;
    }

    public ArrayList<HashMap> selectData() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap> notificationList = new ArrayList<>();
        String query = "SELECT HistoryID, DOC, NotificationMessage FROM NotificationHistory";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap<String, String> user = new HashMap<>();
            //user.put("HistoryID", cursor.getString(cursor.getColumnIndex(HistoryID)));
            user.put("DOC", cursor.getString(cursor.getColumnIndex("DOC")));
            user.put("NotificationMessage", cursor.getString(cursor.getColumnIndex("NotificationMessage")));
            notificationList.add(user);
        }
        return notificationList;
    }

    public int DeleteUser() {
        int i;

        SQLiteDatabase db = this.getWritableDatabase();
        i = db.delete(tableNotificationHistory, null, null);
        db.close();

        return i;
    }

}
