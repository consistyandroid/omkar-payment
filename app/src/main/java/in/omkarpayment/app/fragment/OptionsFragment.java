package in.omkarpayment.app.fragment;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.OptionListAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.FragmentMoreBinding;
import in.omkarpayment.app.json.optionData;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

import java.util.ArrayList;

/**
 * Created by ${user} on 13/12/17.
 */

public class OptionsFragment extends Fragment {

    View rovView;
    FragmentMoreBinding fragmentHomeBinding;
    ArrayList<optionData> stringArrayList = new ArrayList<optionData>();
    AlertImpl alert;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false);
        rovView = fragmentHomeBinding.getRoot();
        alert = new AlertImpl(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        fragmentHomeBinding.listView.setLayoutManager(mLayoutManager);
        fragmentHomeBinding.listView.setItemAnimator(new DefaultItemAnimator());
        fillUpList();


        return rovView;
    }

    private void fillUpList() {
        try {
            stringArrayList.add(new optionData("Balance Check", "0", R.mipmap.icon_mobile));
            if (UserDetails.UserType.equals(UserType.Retailer) || UserDetails.UserType.equals(UserType.APIUSER)) {
                stringArrayList.add(new optionData("Last Transaction", "1", R.mipmap.icon_mobile));
                stringArrayList.add(new optionData("Day Report", "2", R.mipmap.icon_mobile));
            } else if (UserDetails.UserType.equals(UserType.Distributor) || UserDetails.UserType.equals(UserType.Admin)) {

                stringArrayList.add(new optionData("Balance Transfer", "4", R.mipmap.icon_mobile));
                stringArrayList.add(new optionData("Balance Reverse", "9", R.mipmap.icon_mobile));
                stringArrayList.add(new optionData("User Activation", "6", R.mipmap.icon_mobile));
                stringArrayList.add(new optionData("DownLine List", "7", R.mipmap.icon_mobile));
            }
        } catch (Exception e) {

        }
        stringArrayList.add(new optionData("Search Number", "3", R.mipmap.icon_mobile));
        stringArrayList.add(new optionData("Change Password", "10", R.mipmap.icon_mobile));
        stringArrayList.add(new optionData("Balance Report", "5", R.mipmap.icon_mobile));
        stringArrayList.add(new optionData("Help", "11", R.mipmap.icon_mobile));
        stringArrayList.add(new optionData("Logout", "8", R.mipmap.icon_mobile));
        OptionListAdapter adapter = new OptionListAdapter(getContext(), stringArrayList);
        fragmentHomeBinding.listView.setAdapter(adapter);
    }


}
