package in.omkarpayment.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.omkarpayment.app.BankDetailsActivity;
import in.omkarpayment.app.HelpActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.balanceTransfer.BalanceReverseActivity;
import in.omkarpayment.app.cashCollection.cashCollection.CashCollectionActivity;
import in.omkarpayment.app.cashCollection.cashCollectionReport.CashCollectionReportActivity;
import in.omkarpayment.app.changeMobileNumber.ChangeMobileNumberActivity;
import in.omkarpayment.app.changePassword.ChangePasswordActivity;
import in.omkarpayment.app.changePin.ChangePinActivity;
import in.omkarpayment.app.databinding.FragmentDistributorBinding;
import in.omkarpayment.app.downlinelist.DownlineListActivity;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.report.MoneyTxCommissionStructure.MoneyTxCommissionStrActivity;
import in.omkarpayment.app.report.StockReport.StockReportActivity;
import in.omkarpayment.app.report.balanceReport.BalanceReportActivity;
import in.omkarpayment.app.report.commissionStructure.ComissionStructure;
import in.omkarpayment.app.report.complaintReport.ComplaintReportActivity;
import in.omkarpayment.app.report.searchMobile.SearchNumberActivity;
import in.omkarpayment.app.stockRequest.StockRequestActivity;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserProfile;
import in.omkarpayment.app.userContent.UserType;

public class DistributorFragment extends Fragment {

    View rovView;
    LinearLayout layoutBalReverse, layoutComplaintRep, layoutSearchNumber, layoutStockReport, layoutStockRequest;
    FragmentDistributorBinding fragmentDistributorBinding;

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hideKeyboard(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("Inside Distributor", "Yes");
        fragmentDistributorBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_distributor, container, false);
        rovView = fragmentDistributorBinding.getRoot();

        layoutBalReverse = (LinearLayout) rovView.findViewById(R.id.layoutBalReverse);
        layoutComplaintRep = (LinearLayout) rovView.findViewById(R.id.layoutComplaintRep);
        layoutSearchNumber = (LinearLayout) rovView.findViewById(R.id.layoutSearchNumber);
        layoutStockReport = (LinearLayout) rovView.findViewById(R.id.layoutStockReport);
        layoutStockRequest = (LinearLayout) rovView.findViewById(R.id.layoutStockRequest);
        layoutStockRequest.setVisibility(View.GONE);


        if (UserDetails.UserType.equals(UserType.SuperDistributor)) {

            layoutBalReverse.setVisibility(View.GONE);
            layoutComplaintRep.setVisibility(View.GONE);
            layoutSearchNumber.setVisibility(View.GONE);
            layoutStockReport.setVisibility(View.VISIBLE);
            layoutStockRequest.setVisibility(View.VISIBLE);

        }
        if (UserDetails.UserType.equals(UserType.Distributor)) {
            layoutBalReverse.setVisibility(View.GONE);
            layoutStockReport.setVisibility(View.VISIBLE);
            layoutStockRequest.setVisibility(View.VISIBLE);
        }

        fragmentDistributorBinding.btnViewCompliant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComplaintReportActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnCashCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CashCollectionActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnCashCollectionReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CashCollectionReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnBalanceReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BalanceReverseActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnBalanceReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BalanceReportActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangePinActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BankDetailsActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnChangeMobileNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeMobileNumberActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnCommiStructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComissionStructure.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnStockRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), StockRequestActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnStockReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StockReportActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnSearchNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchNumberActivity.class);
                startActivity(intent);

            }
        });

        fragmentDistributorBinding.btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), HelpActivity.class);
                startActivity(intent);

            }
        });

        fragmentDistributorBinding.btnMoneyTxCommission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MoneyTxCommissionStrActivity.class);
                startActivity(intent);

            }
        });
        fragmentDistributorBinding.btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        fragmentDistributorBinding.btnUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserProfile.class);
                startActivity(intent);

            }
        });

        fragmentDistributorBinding.btnDownlineList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DownlineListActivity.class);
                startActivity(intent);

            }
        });

        fragmentDistributorBinding.btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.confirm_dialog);
                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                text.setText("             Are you sure to Logout?              ");

                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);

                Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
                btncancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                        dialog.dismiss();
                    }
                });

                dialog.show();


            }
        });

        return rovView;
    }
}
