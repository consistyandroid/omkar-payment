package in.omkarpayment.app.fragment.GetMoneyTransferUI;

public interface IMoneyTransferUIView {
    void showDialog();

    void dismissDialog();

    void successAlert(String msg);

    void errorAlert(String msg);

    void getSuccessResponse(String msg);
    // void loginSuccess(ArrayList<LoginDetailsPojo> loginResult);

}
