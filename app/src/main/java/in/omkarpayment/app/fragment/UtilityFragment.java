package in.omkarpayment.app.fragment;

import android.graphics.Typeface;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.UtilityViewPagerAdapter;
import in.omkarpayment.app.recharge.electricity.ElectricityActivity;

public class UtilityFragment extends Fragment {
    View v;
    ViewPager viewPager;
    TabLayout tabLayout;
    Typeface tf;

    public static UtilityFragment newInstance() {
        Bundle args = new Bundle();

        UtilityFragment fragment = new UtilityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_utility_fragment, container, false);
        String fontPath = "font/Muli.ttf";
        tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        Log.d("Utility : ", "we are at Utility");
        viewPager = (ViewPager) v.findViewById(R.id.viewPagerUtility);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) v.findViewById(R.id.tabs_utility);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        return v;
    }

    private void setupViewPager(ViewPager viewPager) {
        UtilityViewPagerAdapter adapter = new UtilityViewPagerAdapter(getChildFragmentManager());
       adapter.addFragment(new ElectricityActivity(),"Electricity");
        //adapter.addFragment(new LandlineActivity(), "Landline");
       // adapter.addFragment(new GasBillActivity(), "Gas bill");
        // viewPager.setSaveFromParentEnabled(false);
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText("Electricity");
        tabOne.setTypeface(tf);
        tabOne.setTextColor(getResources().getColor(R.color.blue));
        tabOne.setTextSize(12);
        tabLayout.getTabAt(0).setCustomView(tabOne);

       /* TextView tabtwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabtwo.setText("Landline");
        tabtwo.setTypeface(tf);
        tabtwo.setTextColor(getResources().getColor(R.color.blue));
        tabtwo.setTextSize(12);
        tabLayout.getTabAt(1).setCustomView(tabtwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabThree.setText("Gas Bill");
        tabThree.setTypeface(tf);
        tabThree.setTextColor(getResources().getColor(R.color.blue));
        tabThree.setTextSize(12);
        tabLayout.getTabAt(2).setCustomView(tabThree);*/
    }
}

