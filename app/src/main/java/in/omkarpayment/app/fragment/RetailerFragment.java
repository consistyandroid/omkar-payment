package in.omkarpayment.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.recharge.aepsCon.AEPSActivity;
import com.recharge.aepsCon.AEPSFragment;
import com.recharge.aepsCon.aepsReport.AepsReportActivity;

import in.omkarpayment.app.Addcomplaint.AddComplaintActivity;
import in.omkarpayment.app.BankDetailsActivity;
import in.omkarpayment.app.CustomerCareActivity;
import in.omkarpayment.app.HelpActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.cashCollection.cashCollectionReport.CashCollectionReportActivity;
import in.omkarpayment.app.changeMobileNumber.ChangeMobileNumberActivity;
import in.omkarpayment.app.changePassword.ChangePasswordActivity;
import in.omkarpayment.app.changePin.ChangePinActivity;
import in.omkarpayment.app.databinding.FragmentRetailerBinding;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.report.MoneyTxCommissionStructure.MoneyTxCommissionStrActivity;
import in.omkarpayment.app.report.NewMoneyTxReport.NewMoneyTransferReportActivity;
import in.omkarpayment.app.report.balanceReport.BalanceReportActivity;
import in.omkarpayment.app.report.commissionStructure.ComissionStructure;
import in.omkarpayment.app.report.dayreport.DayReportActivity;
import in.omkarpayment.app.report.lasttransaction.LastTransactionActivity;
import in.omkarpayment.app.report.searchMobile.SearchNumberActivity;
import in.omkarpayment.app.stockRequest.StockRequestActivity;
import in.omkarpayment.app.userContent.UserProfile;


public class RetailerFragment extends Fragment {

    View rovView;

    FragmentRetailerBinding fragmentPrepaidBinding;

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hideKeyboard(getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentPrepaidBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_retailer, container, false);
        rovView = fragmentPrepaidBinding.getRoot();
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        /*fragmentPrepaidBinding.btnMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RechargeActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("btnPress", "Prepaid");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });*/

        fragmentPrepaidBinding.btnMoneyTxReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewMoneyTransferReportActivity.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnBankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BankDetailsActivity.class);
                startActivity(intent);
            }
        });
        fragmentPrepaidBinding.btnCashCollectionReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CashCollectionReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentPrepaidBinding.btnChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangePinActivity.class);
                startActivity(intent);
            }
        });
        fragmentPrepaidBinding.btnAEPSReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AepsReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentPrepaidBinding.btnCustomerCare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CustomerCareActivity.class);
                startActivity(intent);
            }
        });
   /*     fragmentPrepaidBinding.btnElectricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ElectricityActivity.class);
                startActivity(intent);
            }
        });*/
       /* fragmentPrepaidBinding.btnInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), InsuranceBillActivity.class);
                startActivity(intent);
            }
        });
        fragmentPrepaidBinding.btnGas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), GasBillActivity.class);
                startActivity(intent);
            }
        });
        fragmentPrepaidBinding.btnMoneyTx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MoneyTransferActivity.class);
                startActivity(intent);
            }
        });*/
        fragmentPrepaidBinding.btnBalanceReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BalanceReportActivity.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnComplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddComplaintActivity.class);
                startActivity(intent);

            }
        });
        fragmentPrepaidBinding.btnAeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AEPSActivity.class);
                startActivity(intent);
            }
        });
       /* fragmentPrepaidBinding.btnLandline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LandlineActivity.class);
                startActivity(intent);
            }
        });*/
        fragmentPrepaidBinding.btnSearchNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchNumberActivity.class);
                startActivity(intent);

            }
        });
        fragmentPrepaidBinding.btnStockRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), StockRequestActivity.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnChangeMobileNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeMobileNumberActivity.class);
                startActivity(intent);

            }
        });

      /*  fragmentPrepaidBinding.btnWalletTransReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WalletTransferReport.class);
                startActivity(intent);
            }
        });
        */
        fragmentPrepaidBinding.btnCommiStructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComissionStructure.class);
                startActivity(intent);


            }
        });
        fragmentPrepaidBinding.btnMoneyTxCommission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MoneyTxCommissionStrActivity.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnDayReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DayReportActivity.class);
                startActivity(intent);

            }
        });


        fragmentPrepaidBinding.btnLastransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LastTransactionActivity.class);
                startActivity(intent);

            }
        });

      /*  fragmentPrepaidBinding.btnBalanceReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BalanceReportActivity.class);
                startActivity(intent);
            }
        });
*/

        fragmentPrepaidBinding.btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserProfile.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), HelpActivity.class);
                startActivity(intent);

            }
        });

        fragmentPrepaidBinding.btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.confirm_dialog);
                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                text.setText("             Are you sure to Logout?              ");
                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
                btncancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        return rovView;
    }
}
