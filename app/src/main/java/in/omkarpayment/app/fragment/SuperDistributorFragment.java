package in.omkarpayment.app.fragment;

import android.app.Dialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import in.omkarpayment.app.HelpActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.changeMobileNumber.ChangeMobileNumberActivity;
import in.omkarpayment.app.databinding.FragmentSuperDistributorBinding;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.report.balanceReport.BalanceReportActivity;
import in.omkarpayment.app.userContent.UserProfile;

/**
 * Created by lenovo on 2/14/2018.
 */

public class SuperDistributorFragment extends Fragment {

    View rovView;

    FragmentSuperDistributorBinding fragmentSuperDistributorBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("Inside Distributor", "Yes");
        fragmentSuperDistributorBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_super_distributor, container, false);
        rovView = fragmentSuperDistributorBinding.getRoot();

       /* fragmentDistributorBinding.btnViewCompliant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComplaintReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnStockReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), StockReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnBalanceReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BalanceReverseActivity.class);
                startActivity(intent);
            }
        });*/
        fragmentSuperDistributorBinding.btnBalanceReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BalanceReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentSuperDistributorBinding.btnChangeMobileNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangeMobileNumberActivity.class);
                startActivity(intent);
            }
        });

       /* fragmentDistributorBinding.btnCommiStructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComissionStructure.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnStockReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), StockReportActivity.class);
                startActivity(intent);
            }
        });
        fragmentDistributorBinding.btnSearchNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchNumberActivity.class);
                startActivity(intent);
            }
        });
*/
        fragmentSuperDistributorBinding.btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), HelpActivity.class);
                startActivity(intent);
            }
        });


        /*fragmentDistributorBinding.btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
*/
        fragmentSuperDistributorBinding.btnUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserProfile.class);
                startActivity(intent);
            }
        });

        /*fragmentDistributorBinding.btnDownlineList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DownlineListActivity.class);
                startActivity(intent);
            }
        });*/

        fragmentSuperDistributorBinding.btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.confirm_dialog);
                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                text.setText("             Are you sure to Logout?              ");

                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);

                Button btncancel = (Button) dialog.findViewById(R.id.btncancel);
                btncancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().finish();
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });

        return rovView;
    }
}
