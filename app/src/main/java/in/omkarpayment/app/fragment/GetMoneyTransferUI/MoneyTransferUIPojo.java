package in.omkarpayment.app.fragment.GetMoneyTransferUI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoneyTransferUIPojo {
    @SerializedName("RechargeProviderID")
    @Expose
    private Integer rechargeProviderID;
    @SerializedName("RechargeProviderName")
    @Expose
    private String rechargeProviderName;
    @SerializedName("MoneyTransferActDact")
    @Expose
    private Boolean moneyTransferActDact;

    public Integer getRechargeProviderID() {
        return rechargeProviderID;
    }

    public void setRechargeProviderID(Integer rechargeProviderID) {
        this.rechargeProviderID = rechargeProviderID;
    }

    public String getRechargeProviderName() {
        return rechargeProviderName;
    }

    public void setRechargeProviderName(String rechargeProviderName) {
        this.rechargeProviderName = rechargeProviderName;
    }

    public Boolean getMoneyTransferActDact() {
        return moneyTransferActDact;
    }

    public void setMoneyTransferActDact(Boolean moneyTransferActDact) {
        this.moneyTransferActDact = moneyTransferActDact;
    }

}
