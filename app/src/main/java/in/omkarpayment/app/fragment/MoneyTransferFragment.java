package in.omkarpayment.app.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.recharge.allEDMT.aGetSenderDetails.AGetSenderDetailsFragment;
import com.recharge.eDMT.eGetSenderDetails.eGetSenderDetailsFragment;

import in.omkarpayment.app.MoneyTransferpkg.validationpkg.BeneficiaryListFragment;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.ViewPagerAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.fragment.GetMoneyTransferUI.IMoneyTransferUIView;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.UserDetails;

public class MoneyTransferFragment extends Fragment implements IMoneyTransferUIView {
    View v;
    ViewPager viewPager;
    TabLayout tabLayout;
    Typeface tf;
    CustomProgressDialog progressDialog;
    AlertImpl alert;


    TextView tabOne, tabtwo, tabThree;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_recharge, container, false);
        String fontPath = "font/Muli.ttf";
        tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        progressDialog = new CustomProgressDialog(getContext());
        alert = new AlertImpl(getContext());

        viewPager = (ViewPager) v.findViewById(R.id.viewpagerrecharge);
        setupViewPager(viewPager);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        tabLayout = (TabLayout) v.findViewById(R.id.tabs_recharge);
        tabLayout.setupWithViewPager(viewPager);


        setupTabIcons();
        return v;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
      //  adapter.addFragment(new BeneficiaryListFragment(), "DMT1");
        if (UserDetails.MoneyTxUI.equalsIgnoreCase("AlleRecharge")) {
            adapter.addFragment(new eGetSenderDetailsFragment(), "DMT2");
        } else if (UserDetails.MoneyTxUI.equalsIgnoreCase("AlleRechargeDMR")) {
            adapter.addFragment(new AGetSenderDetailsFragment(), "DMT3");
        } else {
            adapter.addFragment(new BeneficiaryListFragment(), "DMT1");
        }
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        /*tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText("DMT1");
        tabOne.setTypeface(tf);
        tabOne.setTextColor(getResources().getColor(R.color.blue));
        tabOne.setTextSize(12);
        tabLayout.getTabAt(0).setCustomView(tabOne);*/
        if (UserDetails.MoneyTxUI.equalsIgnoreCase("AlleRecharge")) {

            tabtwo = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
            tabtwo.setText("DMT2");
            tabtwo.setTypeface(tf);
            tabtwo.setTextColor(getResources().getColor(R.color.blue));
            tabtwo.setTextSize(12);
            tabLayout.getTabAt(0).setCustomView(tabtwo);
        } else if (UserDetails.MoneyTxUI.equalsIgnoreCase("AlleRechargeDMR")) {
            tabThree = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
            tabThree.setText("DMT3");
            tabThree.setTypeface(tf);
            tabThree.setTextColor(getResources().getColor(R.color.blue));
            tabThree.setTextSize(12);
            tabLayout.getTabAt(0).setCustomView(tabThree);
        } else {
            tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabOne.setText("DMT1");
            tabOne.setTypeface(tf);
            tabOne.setTextColor(getResources().getColor(R.color.blue));
            tabOne.setTextSize(12);
            tabLayout.getTabAt(0).setCustomView(tabOne);
        }
    }

    @Override
    public void showDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void successAlert(String msg) {

        alert.successAlert(msg);
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void getSuccessResponse(String msg) {
        Log.i("Message UI", msg);
        String ResponseMessage = msg;
        Log.i("ResponseMessage", msg);
    }


}
