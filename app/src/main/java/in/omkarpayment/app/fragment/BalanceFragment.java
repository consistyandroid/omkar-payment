package in.omkarpayment.app.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;


import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.ViewPagerAdapter;
import in.omkarpayment.app.balanceTransfer.BalanceTransferActivity;
import in.omkarpayment.app.userActivation.UserActivationActivity;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;


/**
 * Created by consisty on 29/12/17.
 */

public class BalanceFragment extends Fragment {
    View v;
    ViewPager viewPager;
    TabLayout tabLayout;
    Typeface tf;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_recharge, container, false);
        String fontPath = "font/Muli.ttf";
        tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        viewPager = (ViewPager) v.findViewById(R.id.viewpagerrecharge);
        setupViewPager(viewPager);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        tabLayout = (TabLayout) v.findViewById(R.id.tabs_recharge);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        return v;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new BalanceTransferActivity(), "Balance Transfer");
        adapter.addFragment(new UserActivationActivity(), "Add User");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText("Balance Transfer");
        tabOne.setTypeface(tf);
        tabOne.setTextColor(getResources().getColor(R.color.blue));
        tabOne.setTextSize(12);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        if (UserDetails.UserType.equalsIgnoreCase(UserType.Admin) ||
                UserDetails.UserType.equalsIgnoreCase(UserType.Distributor)) {

            TextView tabtwo = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
            tabtwo.setText("Add User");
            tabtwo.setTypeface(tf);
            tabtwo.setTextColor(getResources().getColor(R.color.blue));
            tabtwo.setTextSize(12);
            tabLayout.getTabAt(1).setCustomView(tabtwo);

        }
    }


}
