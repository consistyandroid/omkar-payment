package in.omkarpayment.app.fragment.GetMoneyTransferUI;

public interface IMoneyTransferUIPresenter {
    void decryptMoneyTransferResponse(String encryptedResponseBody);

    void decryptMoneyTransferFailResponse(String encryptedResponseBody);

    void errorAlert(String msg);

    void successAlert(String msg);

    void getMoneyTransferUI();

}
