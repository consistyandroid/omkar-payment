package in.omkarpayment.app.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.ViewPagerAdapter;
import in.omkarpayment.app.recharge.recharge.DTHFragment;
import in.omkarpayment.app.recharge.recharge.PostpaidFragment;
import in.omkarpayment.app.recharge.recharge.PrepaidFragment;

public class RechargeFragment extends Fragment {
    View v;
    ViewPager viewPager;
    TabLayout tabLayout;
    Typeface tf;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_recharge, container, false);
        String fontPath = "font/Muli.ttf";
        tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
        viewPager = (ViewPager) v.findViewById(R.id.viewpagerrecharge);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) v.findViewById(R.id.tabs_recharge);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        return v;
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new PrepaidFragment(), "Mobile");
        adapter.addFragment(new DTHFragment(), "Dth");
        adapter.addFragment(new PostpaidFragment(), "Postpaid");
        adapter.addFragment(new MoneyTransferFragment(), "Money Transfer");//MoneyTransferFragment
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText("Mobile");
        tabOne.setTypeface(tf);
        tabOne.setTextColor(getResources().getColor(R.color.blue));
        tabOne.setTextSize(11);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabtwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabtwo.setText("Dth");
        tabtwo.setTypeface(tf);
        tabtwo.setTextColor(getResources().getColor(R.color.blue));
        tabtwo.setTextSize(11);
        tabLayout.getTabAt(1).setCustomView(tabtwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabThree.setText("Postpaid");
        tabThree.setTypeface(tf);
        tabThree.setTextColor(getResources().getColor(R.color.blue));
        tabThree.setTextSize(11);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tavFour = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tavFour.setText("Money Transfer");
        tavFour.setTypeface(tf);
        tavFour.setTextColor(getResources().getColor(R.color.blue));
        tavFour.setTextSize(11);
        tabLayout.getTabAt(3).setCustomView(tavFour);

    }


}
