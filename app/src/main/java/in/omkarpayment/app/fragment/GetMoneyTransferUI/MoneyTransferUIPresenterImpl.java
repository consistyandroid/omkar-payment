package in.omkarpayment.app.fragment.GetMoneyTransferUI;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.UserDetails;

public class MoneyTransferUIPresenterImpl implements IMoneyTransferUIPresenter {
    IMoneyTransferUIView iMoneyTransferUIView;

    public MoneyTransferUIPresenterImpl(IMoneyTransferUIView iMoneyTransferUIView) {
        this.iMoneyTransferUIView = iMoneyTransferUIView;
    }

    @Override
    public void getMoneyTransferUI() {
        String keyData = new KeyDataReader().get();
        MoneyTransferUIModel model = new MoneyTransferUIModel();
        model.iMoneyTransferUIPresenter = this;
        model.moneyTransferUI("", keyData);
    }
    @Override
    public void decryptMoneyTransferResponse(String encryptedResponseBody) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(encryptedResponseBody);


            if (body == null || body.isEmpty() || body.equals("[]")) {
                iMoneyTransferUIView.dismissDialog();
                return;
            }
            log.i("moneyTxUi Response", body);
            Gson gson = new Gson();

            List<MoneyTransferUIPojo> messageResponsePojo = gson.fromJson(body, new TypeToken<List<MoneyTransferUIPojo>>(){}.getType());

            UserDetails.MoneyTxUI = messageResponsePojo.get(0).getRechargeProviderName();
            log.i("moneyTxUi Response", UserDetails.MoneyTxUI);
            // iMoneyTransferUIView.getSuccessResponse(messageResponsePojo.getRechargeProviderName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptMoneyTransferFailResponse(String encryptedResponseBody) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(encryptedResponseBody);
            log.i("moneyTxUiFailResponse", body);
            iMoneyTransferUIView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorAlert(String msg) {
        iMoneyTransferUIView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iMoneyTransferUIView.successAlert(msg);
    }


}
