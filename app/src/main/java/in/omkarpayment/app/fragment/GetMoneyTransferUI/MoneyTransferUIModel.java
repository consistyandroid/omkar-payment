package in.omkarpayment.app.fragment.GetMoneyTransferUI;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.IWebServicesModel;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoneyTransferUIModel {
    public IMoneyTransferUIPresenter iMoneyTransferUIPresenter = null;
    LogWriter log = new LogWriter();

    /*public void moneyTransferUI(String request,String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.MoneyTransferUIWebservice( request, keyData);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("onResponseBody UI", response.body().getBody());
                            delegate.decryptMoneyTransferResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.decryptMoneyTransferFailResponse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }*/
    public void moneyTransferUI(String request, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
        try {
            Call<WebserviceResponsePojo> call = webObject.MoneyTransferUIWebservice(request, keyData);
            call.enqueue(new Callback<WebserviceResponsePojo>() {

                @Override
                public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                    try {
                        LogWriter log = new LogWriter();

                        if (response.code() != 200) {
                            iMoneyTransferUIPresenter.errorAlert(AllMessages.wrongResponce);
                            return;
                        }
                        if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("MoneyTx UI onResponseBody", response.body().getBody());
                            iMoneyTransferUIPresenter.decryptMoneyTransferResponse(response.body().getBody());
                        }
                        if (response.body().getStatus().equalsIgnoreCase("FAILUER") && !response.body().getBody().equals("")) {
                            log.i("MoneyTx UI onResponseBody", response.body().getBody());
                            iMoneyTransferUIPresenter.decryptMoneyTransferFailResponse(response.body().getBody());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                    LogWriter log = new LogWriter();
                    log.i("onFailure", t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
