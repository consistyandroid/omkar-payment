package in.omkarpayment.app.fragment;


import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.NewLastRechargePreReportAdapter;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.FragmentHomeBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.report.lasttransaction.ILastTransactionView;
import in.omkarpayment.app.report.lasttransaction.LastTransactionPresenter;
import in.omkarpayment.app.userContent.AllMessages;

/**
 * Created by ${user} on 13/12/17.
 */

public class HomeFragment extends Fragment implements IAvailableBalanceView, ILastTransactionView {

    View rovView;
    Context context;
    FragmentHomeBinding fragmentHomeBinding;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    String currentBalance = "0.00", saleCommission = "0.00";
    LastTransactionPresenter lastTransactionPresenter;
    CustomProgressDialog pDialog;
    NewLastRechargePreReportAdapter adapter;
    Animation animation;
    NetworkState ns = new NetworkState();

    public static HomeFragment newInstance() {
        return (new HomeFragment());

    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        rovView = fragmentHomeBinding.getRoot();

        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        pDialog = new CustomProgressDialog(getActivity());


        lastTransactionPresenter = new LastTransactionPresenter(this);

        AnimUpdate();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        fragmentHomeBinding.listView.setLayoutManager(mLayoutManager);
        fragmentHomeBinding.listView.setItemAnimator(new DefaultItemAnimator());
        fragmentHomeBinding.txtAvailableBal.setText("Account Balance :" + currentBalance + "" + "/-");
        fragmentHomeBinding.txtSaleComm.setText("Sale Comm :" + saleCommission + "" + "/-");
        fragmentHomeBinding.txtRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateBalanceAndList();
                AnimUpdate();
            }
        });
        fragmentHomeBinding.imageRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimUpdate();
            }
        });


        return rovView;
    }


    public void AnimUpdate() {
        updateBalanceAndList();
        fragmentHomeBinding.txtAvailableBal.setText("");
        fragmentHomeBinding.txtSaleComm.setText("");
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        fragmentHomeBinding.imageRefresh.startAnimation(animation);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ns.isInternetAvailable(getActivity())) {
                    iAvailableBalancePresenter.getAvailableBalance();
                    iAvailableBalancePresenter.getSaleCommission();
                }
            }
        }, 2000);
    }


    public void updateBalanceAndList() {
        ((HomeActivity) getActivity()).updateBal();
        if (ns.isInternetAvailable(getActivity())) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragmentHomeBinding.listView.setAdapter(null);
                    lastTransactionPresenter.getLastTransaction();
                }
            }, 2000);
        } else {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        pDialog.dismissPDialog();
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                currentBalance = String.valueOf(obj.getCurrentBalance());
                fragmentHomeBinding.txtAvailableBal.setText("Account Balance :"+"₹ " + currentBalance + "" + "/-");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {
        pDialog.dismissPDialog();
        for (SaleCommissionPojo obj : lastrasaction) {
            try {
                saleCommission = String.valueOf(obj.getDiscountAmount())+"("+obj.getDiscountRechargeCount()+")";
                fragmentHomeBinding.txtSaleComm.setText("Sale Comm          :"+"₹ " + saleCommission + "" + "/-");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> lastTransaction) {
        adapter = new NewLastRechargePreReportAdapter(getContext(), lastTransaction);
        fragmentHomeBinding.listView.setAdapter(adapter);

    }

    @Override
    public void errorAlert(String msg) {

    }

    @Override
    public void successAlert(String msg) {

    }

    @Override
    public void filterDialog() {

    }

    @Override
    public void showPDialog() {

    }

    @Override
    public void dismissPDialog() {

    }
}
