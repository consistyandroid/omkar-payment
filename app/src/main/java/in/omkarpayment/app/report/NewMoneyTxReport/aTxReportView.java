package in.omkarpayment.app.report.NewMoneyTxReport;


import com.recharge.allEDMT.aMoneyTransfer.json.ATransferReportPojo;

import java.util.ArrayList;

public interface aTxReportView {

    void editFromDateError();

    void editToDateError();

    String strFromDate();

    String toDate();

    void getTxReport(ArrayList<ATransferReportPojo> CTransferReportPojos);

    void alert(String status);

    void getReport();

    void unableToFetchError(String message);

}
