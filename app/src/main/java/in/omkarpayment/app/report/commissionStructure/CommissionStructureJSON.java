package in.omkarpayment.app.report.commissionStructure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by consisty on 23/11/17.
 */

public class CommissionStructureJSON {


    @SerializedName("RetailerCommission")
    @Expose
    private String retailerCommission;
    @SerializedName("DistCommission")
    @Expose
    private String distCommission;
    @SerializedName("TotalCommission")
    @Expose
    private String totalCommission;
    @SerializedName("ServiceName")
    @Expose
    private String serviceName;
    @SerializedName("OperatorName")
    @Expose
    private String operatorName;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("OperatorID")
    @Expose
    private String operatorID;
    @SerializedName("Message")
    @Expose
    private Object message;
    @SerializedName("UserTypeID")
    @Expose
    private Integer userTypeID;

    public String getRetailerCommission() {
        return retailerCommission;
    }

    public void setRetailerCommission(String retailerCommission) {
        this.retailerCommission = retailerCommission;
    }

    public String getDistCommission() {
        return distCommission;
    }

    public void setDistCommission(String distCommission) {
        this.distCommission = distCommission;
    }

    public String getTotalCommission() {
        return totalCommission;
    }

    public void setTotalCommission(String totalCommission) {
        this.totalCommission = totalCommission;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

}
