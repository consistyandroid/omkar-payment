package in.omkarpayment.app.report.NewMoneyTxReport.moneyTransferReceipt;

import java.util.ArrayList;

public interface aMoneyTransferReceiptView {

    String getBillNumber();

    void unableToFetchAlert(String message);

    void getReceipt();

    void errorAlert(String status);

    void getReceiptSuccess(ArrayList<MoneyTransferReceiptPojo> moneyTransferReceiptPojo);

    void getReceiptFailAlert(String response);

}
