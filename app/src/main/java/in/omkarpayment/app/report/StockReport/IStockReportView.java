package in.omkarpayment.app.report.StockReport;




import java.util.ArrayList;

import in.omkarpayment.app.json.StockReportPojo;

/**
 * Created by Somnath-Laptop on 29-Jan-2018.
 */

public interface IStockReportView {
    void getStockReport(ArrayList<StockReportPojo> stockreport);

    void errorAlert(String msg);

    void successAlert(String msg);

    void filterDialog();

    void showPDialog();

    void dismissPDialog();

    String StockReportStatus();

    String strFromDate();

    String strToDate();

    void unableToFetchError(String message);

    void acceptRequestAlert(String message);
}
