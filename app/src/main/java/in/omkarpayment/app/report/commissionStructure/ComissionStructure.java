package in.omkarpayment.app.report.commissionStructure;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.CommissionStructureReportAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.report.searchMobile.SearchNumberPresenter;
import in.omkarpayment.app.userContent.AllMessages;

public class ComissionStructure extends AppCompatActivity implements ICommissionSView {
    SwipeRefreshLayout swipeRefresh;
    RecyclerView listView;
    AlertImpl alert;
    Context context = this;
    CustomProgressDialog pDialog;
    CommissionStructureReportAdapter adapter;
    ICommissionSPresenter iCommissionSPresenter;
    SearchNumberPresenter searchNumberPresenter;
    NetworkState ns = new NetworkState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comission_structure);

        iCommissionSPresenter = new CommissionSPresenterImpl(this);
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        listView = (RecyclerView) findViewById(R.id.listView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(null);
        if(ns.isInternetAvailable(context)){
            iCommissionSPresenter.getCommissionStructure();
        }else {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Commission Structure";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)

            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void getCommisionStructureView(ArrayList<CommissionStructureJSON> commission) {
        adapter = new CommissionStructureReportAdapter(commission);
        listView.setAdapter(adapter);

        Log.i("Comision", commission.toString());
    }

    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
    }

    @Override
    public void errorAlert(String somethingWrong) {
        alert.errorAlert(somethingWrong);
    }

    @Override
    public void showDialog() {

        if (pDialog == null) {
            pDialog.showPDialog();
        }
    }
}
