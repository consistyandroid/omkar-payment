package in.omkarpayment.app.report.NewMoneyTxReport;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.allEDMT.aMoneyTransfer.aMoneyTransferModel.AMoneyTransferModel;
import com.recharge.allEDMT.aMoneyTransfer.json.ATransferReportPojo;


import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.Validation;

public class TxReportPresenter implements aTxReportPresenter {
    aTxReportView aTxReportView;
    LogWriter log = new LogWriter();

    public TxReportPresenter(aTxReportView aTxReportView) {
        this.aTxReportView = aTxReportView;
    }

    @Override
    public void validateDate() {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(aTxReportView.strFromDate().trim())) {
            aTxReportView.editFromDateError();
            return;
        }
        if (!validation.isNullOrEmpty(aTxReportView.toDate().trim())) {
            aTxReportView.editToDateError();
            return;
        }
        aTxReportView.getReport();
    }

    @Override
    public void getReport(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getMoneyTransferReport", body);

            if (body == null || body.isEmpty() || body.equals("")) {
                aTxReportView.unableToFetchError("report.");
                return;
            }

            Gson gson = new Gson();
            ArrayList<ATransferReportPojo> ATransferReportPojos = gson.fromJson(body, new TypeToken<ArrayList<ATransferReportPojo>>() {
            }.getType());

            if (ATransferReportPojos == null ||
                    ATransferReportPojos.isEmpty() ||
                    ATransferReportPojos.equals("")) {
                aTxReportView.alert("Data not available.");
                return;
            }

            aTxReportView.getTxReport(ATransferReportPojos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {
        aTxReportView.alert(status);
    }

    @Override
    public void getMoneyTxReport() {

        String keyData = new KeyDataReader().get();
        AMoneyTransferModel model = new AMoneyTransferModel();
        model.aTxReportPresenter = this;
        model.getTransactionReport("", keyData);

        /*String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject reportRequest = new JSONObject();
        try {
            reportRequest.put("FromDate", aTxReportView.strFromDate());
            reportRequest.put("ToDate", aTxReportView.toDate());

            String jsonRequest = reportRequest.toString();
            log.i("JsonRequest", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aTxReportView.unableToFetchError("data.");
                return;
            }
            iMoneyTransferModel model = new iMoneyTransferModel();
            model.aTxReportPresenter = this;
            model.getTransactionReport(encryptString, keyData);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void getReportFail(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getReportFail", body);
            if (body == null ||
                    body.isEmpty() ||
                    body.equals("")) {
                aTxReportView.unableToFetchError("data.");
            }
            aTxReportView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
