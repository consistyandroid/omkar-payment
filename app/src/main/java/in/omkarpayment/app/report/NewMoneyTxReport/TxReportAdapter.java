package in.omkarpayment.app.report.NewMoneyTxReport;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.recharge.allEDMT.aMoneyTransfer.json.ATransferReportPojo;


import java.util.ArrayList;

import in.omkarpayment.app.R;
import in.omkarpayment.app.report.NewMoneyTxReport.moneyTransferReceipt.MoneyTransferReceiptActivity;


public class TxReportAdapter extends RecyclerView.Adapter<TxReportAdapter.MyViewHolder> {
    public static int i;

    Context context;
   NewMoneyTransferReportActivity newMoneyTransferReportActivity;
    private ArrayList<ATransferReportPojo> list;

    public TxReportAdapter(Context context, NewMoneyTransferReportActivity newMoneyTransferReportActivity, ArrayList<ATransferReportPojo> list) {
        this.newMoneyTransferReportActivity = newMoneyTransferReportActivity;
        this.list = list;
        this.context = context;
    }


    @Override
    public TxReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_txreport, parent, false);

        return new TxReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TxReportAdapter.MyViewHolder holder, int position) {
        final ATransferReportPojo pojo = list.get(position);

        holder.txtDate.setText(pojo.getDate());
        holder.txtStatus.setText(pojo.getStatus());
        holder.txtSenderNo.setText(pojo.getSenderMobileNumber());
        holder.txtAcNo.setText(String.valueOf(pojo.getAccountNumber()));
        holder.txtBeneficiaryName.setText(pojo.getBeneficiaryName());
        holder.txtTxId.setText(pojo.getProviderRefNo());
        holder.txtReceipt.setVisibility(View.GONE);
        try {
            holder.txtAmount.setText(String.format("%.02f", Double.parseDouble(String.valueOf(pojo.getAmount()))));
            holder.txtClosingBal.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getUserClosingBalance()))));
            holder.txtOpeningBalance.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getUserOpeningBalance()))));
            holder.txtCommission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getCharge()))));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (pojo.getStatus().startsWith("S")) {
                holder.txtCheckStatus.setVisibility(View.GONE);
                holder.bgShape.setColor(context.getResources().getColor(R.color.green));
                holder.txtAmount.setTextColor(context.getResources().getColor(R.color.green));
                holder.txtRupeeSymbol.setTextColor(context.getResources().getColor(R.color.green));
                holder.txtReceipt.setVisibility(View.VISIBLE);
            } else if (pojo.getStatus().startsWith("F") || (pojo.getStatus().startsWith("R"))) {
                holder.txtCheckStatus.setVisibility(View.GONE);
                holder.bgShape.setColor(context.getResources().getColor(R.color.red));
                holder.txtAmount.setTextColor(context.getResources().getColor(R.color.red));
                holder.txtRupeeSymbol.setTextColor(context.getResources().getColor(R.color.red));
                holder.txtReceipt.setVisibility(View.GONE);
            } else if (pojo.getStatus().startsWith("P") || pojo.getStatus().equals("WAITING")) {
                holder.txtCheckStatus.setVisibility(View.VISIBLE);
                holder.bgShapeStatus.setColor(context.getResources().getColor(R.color.black));
                holder.bgShape.setColor(context.getResources().getColor(R.color.orange));
                holder.txtAmount.setTextColor(context.getResources().getColor(R.color.orange));
                holder.txtRupeeSymbol.setTextColor(context.getResources().getColor(R.color.orange));
                holder.txtReceipt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.txtCheckStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "!!..Coming Soon..!!", Toast.LENGTH_LONG).show();
            }
        });
        holder.txtReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MoneyTransferReceiptActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("ReceiptBillNumber", pojo.getBillNumber());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtStatus, txtDate, txtSenderNo, txtAcNo, txtBeneficiaryName, txtCheckStatus, txtOpeningBalance,
                txtCommission, txtClosingBal, txtAmount, txtTxId, txtRefund, txtRupeeSymbol;
        GradientDrawable bgShape, bgShapeStatus;
        ImageView txtReceipt;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtDate = rowView.findViewById(R.id.txtDate);
            txtTxId = rowView.findViewById(R.id.txtTxId);
            txtStatus = rowView.findViewById(R.id.txtStatus);
            txtSenderNo = rowView.findViewById(R.id.txtSenderNo);
            txtAcNo = rowView.findViewById(R.id.txtAcNo);
            txtBeneficiaryName = rowView.findViewById(R.id.txtBeneficiaryName);
            bgShape = (GradientDrawable) txtStatus.getBackground();
            txtAmount = rowView.findViewById(R.id.txtAmount);
            txtRefund = rowView.findViewById(R.id.txtRefund);
            txtCommission = rowView.findViewById(R.id.txtCommission);
            txtRupeeSymbol = rowView.findViewById(R.id.txtRupeeSymbol);
            txtClosingBal = rowView.findViewById(R.id.txtClosingBal);
            txtReceipt = rowView.findViewById(R.id.txtReceipt);
            txtOpeningBalance = rowView.findViewById(R.id.txtOpeningBalance);
            txtCheckStatus = rowView.findViewById(R.id.txtCheckStatus);
            bgShapeStatus = (GradientDrawable) txtCheckStatus.getBackground();

            txtCheckStatus.setVisibility(View.GONE);
        }
    }
}
