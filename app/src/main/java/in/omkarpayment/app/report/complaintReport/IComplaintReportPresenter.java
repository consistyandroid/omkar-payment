package in.omkarpayment.app.report.complaintReport;

import java.util.ArrayList;

import in.omkarpayment.app.json.ComplaintReportPojo;

/**
 * Created by Somnath-Laptop on 01-Feb-2018.
 */

public interface IComplaintReportPresenter {
    void getComplaintReport();

    void showDialog();

    void dismissDialog();

    void errorAlert(String msg);

    void getComplaintReport(ArrayList<ComplaintReportPojo> complaintreport);

    void decreptedComplaintReportResponse(String encryptedResponse);

    void decreptedComplaintReportFailureResponse(String encryptedResponse);
}
