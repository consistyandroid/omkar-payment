package in.omkarpayment.app.report.complaintReport;

import java.util.ArrayList;

import in.omkarpayment.app.json.ComplaintReportPojo;

/**
 * Created by Somnath-Laptop on 01-Feb-2018.
 */

public interface IComplaintReportView {
    void getComplaintReport(ArrayList<ComplaintReportPojo> complaintreport);

    void errorAlert(String msg);

    void successAlert(String msg);

    void filterDialog();

    void showPDialog();

    void dismissPDialog();
}
