package in.omkarpayment.app.report.dayreport;


import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;

import java.util.ArrayList;

/**
 * Created by Krish on 12-Jul-17.
 */

public interface IDayReportPresenter {

    void validateDate();

    void showDialog();

    void dismissDialog();

    void successAlert(String msg);
    void errorAlert(String msg);

    void getDayWiseReport(ArrayList<DayReportPojo> dayReport);

    void DayReportResponse(String encryptedResponse);

    void DayReportFailureResponse(String encryptedResponse);
}
