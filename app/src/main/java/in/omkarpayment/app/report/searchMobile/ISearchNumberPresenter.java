package in.omkarpayment.app.report.searchMobile;




import java.util.ArrayList;

import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;



public interface ISearchNumberPresenter {


    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);



    void validateNumber();

    void SearchReport();

    void decryptSearchNumberReportListResponse(String encryptedResponse);

    void decryptSearchNumberReportListFailureResponse(String encryptedResponse);




}
