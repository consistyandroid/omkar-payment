package in.omkarpayment.app.report.StockReport;



import java.util.ArrayList;

import in.omkarpayment.app.json.StockReportPojo;

/**
 * Created by Somnath-Laptop on 29-Jan-2018.
 */

public interface IStockReportPresenter {
    void getStockReport();

    void showDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void getStockReport(ArrayList<StockReportPojo> stockreport);

    void StockReportResponse(String encryptedResponse);

    void StockReportFailureResponse(String encryptedResponse);

    void rejectBalanceRequest(Integer requestID);

    void acceptRejectBalanceRequestResponse(String response);

    void acceptRejectBalanceRequestFailResponse(String response);

    void acceptBalanceRequest(StockReportPojo requestReportPojo, String CashCollectionAmount, String Remark);
}
