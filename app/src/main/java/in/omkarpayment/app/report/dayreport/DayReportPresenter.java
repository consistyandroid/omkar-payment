package in.omkarpayment.app.report.dayreport;


import android.annotation.SuppressLint;
import android.util.Log;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Krish on 12-Jul-17.
 */

public class DayReportPresenter implements IDayReportPresenter {
    IDayRportView iDayRportView;
    LogWriter log = new LogWriter();

    public DayReportPresenter(IDayRportView iDayRportView) {
        this.iDayRportView = iDayRportView;
    }

    @Override
    public void validateDate() {
        if (iDayRportView.strFromDate().equals("") || iDayRportView.strFromDate().isEmpty()) {
            iDayRportView.editFromDateError();
        } else if (iDayRportView.strToDate().equals("") || iDayRportView.strToDate().isEmpty()) {
            iDayRportView.editToDateError();
        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject dayreportrequest = new JSONObject();
            try {
                dayreportrequest.put("Date", iDayRportView.strFromDate());
                dayreportrequest.put("ToDate", iDayRportView.strToDate());
                String jsonrequest = dayreportrequest.toString();
                log.i("Report Request", jsonrequest);
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                if (!encryptString.equals("")) {
                    WebServiceModel model = new WebServiceModel();
                    model.delegateDayResponse = this;
                    model.webserviceMethod(encryptString, keyData, "DayReport");
                    showDialog();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showDialog() {
        iDayRportView.showPDialog();
    }

    @Override
    public void dismissDialog() {
        iDayRportView.dismissPDialog();

    }

    @Override
    public void successAlert(String msg) {
        iDayRportView.successAlert(msg);

    }
    @Override
    public void errorAlert(String msg) {
        iDayRportView.errorAlert(msg);

    }

    @Override
    public void getDayWiseReport(ArrayList<DayReportPojo> dayReport) {
iDayRportView.getDayWiseReport(dayReport);
    }

    @SuppressLint("LongLogTag")
    @Override
    public void DayReportResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Decrept response in day report",body.toString());

            ArrayList<DayReportPojo> dayreport = gson.fromJson(body, new TypeToken<ArrayList<DayReportPojo>>() {
            }.getType());

            if (dayreport != null) {
                iDayRportView.getDayWiseReport(dayreport);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void DayReportFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Decrept response",body.toString());
            errorAlert(body);

        } catch (Exception e) {

        }
    }

}
