package in.omkarpayment.app.report.MoneyTransferReport;

/**
 * Created by consisty on 14/10/17.
 */

public interface IMoneyTransferReportPresenter {
    void validateDate();

    void decryptMoneyTransferReportListResponse(String encryptedResponse);

    void decryptMoneyTransferReportListFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void errorAlert(String somethingWrong);

}
