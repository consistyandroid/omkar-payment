package in.omkarpayment.app.report.MoneyTxCommissionStructure;

import java.util.ArrayList;

import in.omkarpayment.app.json.MoneyTxCommissionPojo;


/**
 * Created by lenovo on 8/17/2018.
 */

public interface IMoneyTxCommissionView {

    void getMoneyTxCommisionStructureView(ArrayList<MoneyTxCommissionPojo> commission);

    void dismissPDialog();

    void errorAlert(String somethingWrong);

    void showDialog();
}
