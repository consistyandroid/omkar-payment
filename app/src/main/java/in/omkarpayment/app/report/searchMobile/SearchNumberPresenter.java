package in.omkarpayment.app.report.searchMobile;


import android.util.Log;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by Krish on 25-Jul-17.
 */

public class SearchNumberPresenter implements ISearchNumberPresenter {

    ISearchNumberView iSearchNumberView;

    public SearchNumberPresenter(ISearchNumberView iSearchNumberView) {
        this.iSearchNumberView = iSearchNumberView;
    }
    @Override
    public void showPDialog() {
        iSearchNumberView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iSearchNumberView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iSearchNumberView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iSearchNumberView.successAlert(msg);
    }

/*    @Override
    public void getReport(ArrayList<DayReportPojo> Report) {
        iSearchNumberView.getReport(Report);
    }*/
    @Override
    public void validateNumber() {
        if (iSearchNumberView.getNumber().isEmpty()) {
            iSearchNumberView.editNumberError();
        }else {
            SearchReport();
        }
    }

    @Override
    public void SearchReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("ConsumerNo", iSearchNumberView.getNumber());
            String jsonrequest = dayreportrequest.toString();
            Log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateSearchNumber = this;
                model.webserviceMethod(encryptString, keyData, "SearchNumber");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptSearchNumberReportListResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("response",body);
            ArrayList<SearchNumberPojo> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<SearchNumberPojo>>() {
            }.getType());
            if (lastrasaction != null) {
                iSearchNumberView.getReport(lastrasaction);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void decryptSearchNumberReportListFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("failure response",body);
            errorAlert(body);
        } catch (Exception e) {

        }

    }
}
