package in.omkarpayment.app.report.NewMoneyTxReport.moneyTransferReceipt;

public interface aMoneyTransferReceiptPresenter {

    void getReceipt();

    void validateBillNumber();

    void errorAlert(String status);

    void getReceiptFailResponse(String response);

    void getReceiptResponse(String response);

}
