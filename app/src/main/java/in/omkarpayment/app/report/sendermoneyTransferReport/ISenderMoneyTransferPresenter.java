package in.omkarpayment.app.report.sendermoneyTransferReport;

/**
 * Created by consisty on 7/3/18.
 */

public interface ISenderMoneyTransferPresenter {
    void searchTransactions();

    void decryptSearchNumberReportListResponse(String encryptedResponse);

    void decryptSearchNumberReportListFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void errorAlert(String somethingWrong);

    void showPDialog();

    void successAlert(String msg);
}
