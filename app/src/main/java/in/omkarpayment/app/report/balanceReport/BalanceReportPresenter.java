package in.omkarpayment.app.report.balanceReport;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Krish on 14-Aug-17.
 */

public class BalanceReportPresenter implements IBalanceReportPresenter {
    IBalanceReportView iBalanceReportView;
    LogWriter log = new LogWriter();

    public BalanceReportPresenter(IBalanceReportView iBalanceReportView) {
        this.iBalanceReportView = iBalanceReportView;
    }

    @Override
    public void errorAlert(String msg) {
        iBalanceReportView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iBalanceReportView.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        iBalanceReportView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iBalanceReportView.dismissPDialog();
    }

    @Override
    public void validate() {
        if (iBalanceReportView.fromDate().isEmpty() || iBalanceReportView.fromDate().equals("")) {
            iBalanceReportView.txtFromDateError();
        } else if (iBalanceReportView.toDate().isEmpty() || iBalanceReportView.toDate().equals("")) {
            iBalanceReportView.txtToDateError();
        } else {
            if (iBalanceReportView.reportType().equals("Transfer")) {
                transferReport();
            } else if (iBalanceReportView.reportType().equals("Receive")) {
              receiveReport();
            } else if (iBalanceReportView.reportType().equals("Wallet Report")) {

            }
        }
    }

    @Override
    public void BalanceReportResponseFailResponse(String encryptedResponse) {
        String body = "";
        log.i("Report response", encryptedResponse);
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            errorAlert(body);
        } catch (Exception e) {

        }
    }

    @Override
    public void BalanceReportResponseResponse(String encryptedResponse) {
        String body = "";

        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            log.i("Report body response", body);
            ArrayList<BalanceTransferReportPojo> reportResponse = gson.fromJson(body, new TypeToken<ArrayList<BalanceTransferReportPojo>>() {
            }.getType());
            if (reportResponse != null) {
                iBalanceReportView.getReportResponse(reportResponse);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void transferReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iBalanceReportView.fromDate());
            dayreportrequest.put("ToDate", iBalanceReportView.toDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateBalanceReportResponse = this;
                model.webserviceMethod(encryptString, keyData, "BalanceTransReport");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiveReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iBalanceReportView.fromDate());
            dayreportrequest.put("ToDate", iBalanceReportView.toDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateBalanceReportResponse = this;
                model.webserviceMethod(encryptString, keyData, "BalanceReceiveReport");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void walletTransferReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iBalanceReportView.fromDate());
            dayreportrequest.put("ToDate", iBalanceReportView.toDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateBalanceReportResponse = this;
                model.webserviceMethod(encryptString, keyData, "WalletTranReport");
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
