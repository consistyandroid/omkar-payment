package in.omkarpayment.app.report.MoneyTransferReport;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.omkarpayment.app.R;
import in.omkarpayment.app.TransparentProgressDialog;
import in.omkarpayment.app.adapter.MoneyTransferReportListAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityMoneyTransferReportBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.report.dayreport.DayReportPresenter;
import in.omkarpayment.app.userContent.AllMessages;

/**
 * Created by consisty on 14/10/17.
 */

public class MoneyTransferReportActivity extends AppCompatActivity implements IMoneyTransferReportView {
    ActivityMoneyTransferReportBinding activityMoneyTransferReportBinding;
    IMoneyTransferReportPresenter iMoneyTransferReportPresenter;

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    String strFromDate = "", strToDate = "";
    MoneyTransferReportListAdapter adapter;
    AlertImpl alert;

    RecyclerView listView;
    CustomProgressDialog progressDialog;
    DayReportPresenter dayReportPresenter;
    Context context = this;

    TextView txtFromDate, txtToDate;
    Button btnShow;
    SwipeRefreshLayout swipeRefresh;
    private int selectedDate, selectedMonth, selectedYear;
    private TransparentProgressDialog pd;
    private Handler h;
    private Runnable r;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMoneyTransferReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_money_transfer_report);
        alert = new AlertImpl(this);
        progressDialog = new CustomProgressDialog(this);
        iMoneyTransferReportPresenter = new MoneyTransferReportPresenterImpl(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Money Transfer Report ";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        progress();
        listView = (RecyclerView) findViewById(R.id.listView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());

        btnShow = (Button) findViewById(R.id.btnShow);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtToDate = (TextView) findViewById(R.id.txtToDate);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        setDefaultValue();

        // custom dialog
        txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(MoneyTransferReportActivity.this);
                dialog.setContentView(R.layout.datepickerview);
                dialog.setTitle("");

                DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker1);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                selectedDate = calendar.get(Calendar.DAY_OF_MONTH);
                selectedMonth = calendar.get(Calendar.MONTH);
                selectedYear = calendar.get(Calendar.YEAR);
                datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {

                        if (selectedDate == dayOfMonth && selectedMonth == month && selectedYear == year) {
                            txtToDate.setText(dayOfMonth + "/" + (month + 1) + " /" + year);
                            dialog.dismiss();
                        } else {

                            if (selectedDate != dayOfMonth) {
                                txtToDate.setText(dayOfMonth + "/" + (month + 1) + " /" + year);
                                dialog.dismiss();
                            } else {
                                if (selectedMonth != month) {
                                    txtToDate.setText(dayOfMonth + " /" + (month + 1) + " /" + year);
                                    dialog.dismiss();
                                }
                            }
                        }
                        selectedDate = dayOfMonth;
                        selectedMonth = (month);
                        selectedYear = year;
                    }
                });


                dialog.show();
            }
        });

        txtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(MoneyTransferReportActivity.this);
                dialog.setContentView(R.layout.datepickerview);
                dialog.setTitle("");

                DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker1);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                selectedDate = calendar.get(Calendar.DAY_OF_MONTH);
                selectedMonth = calendar.get(Calendar.MONTH);
                selectedYear = calendar.get(Calendar.YEAR);
                datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {

                        if (selectedDate == dayOfMonth && selectedMonth == month && selectedYear == year) {
                            txtFromDate.setText(dayOfMonth + " /" + (month + 1) + " /" + year);
                            dialog.dismiss();
                        } else {

                            if (selectedDate != dayOfMonth) {
                                txtFromDate.setText(dayOfMonth + " /" + (month + 1) + " /" + year);
                                dialog.dismiss();
                            } else {
                                if (selectedMonth != month) {
                                    txtFromDate.setText(dayOfMonth + " /" + (month + 1) + " /" + year);
                                    dialog.dismiss();
                                }
                            }
                        }
                        selectedDate = dayOfMonth;
                        selectedMonth = (month);
                        selectedYear = year;
                    }
                });


                dialog.show();
            }
        });

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureScreen();
                iMoneyTransferReportPresenter.validateDate();
                listView.setAdapter(null);
            }
        });

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        //swipeRefresh.setColorScheme(R.color.orangenew, R.color.colorPrimaryDark, R.color.red);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        iMoneyTransferReportPresenter.validateDate();
                        listView.setAdapter(null);
                    }
                }, 3000);
            }
        });

    }

    public void progress() {
        h = new Handler();
        pd = new TransparentProgressDialog(this, R.drawable.progress);
        r = new Runnable() {
            @Override
            public void run() {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
        };

    }

    private void setDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;
        txtToDate.setText(date);
        txtToDate.setError(null);
        txtFromDate.setText(date);
        txtFromDate.setError(null);
    }

    private void captureScreen() {
        View v = getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        try {
            FileOutputStream fos = new FileOutputStream(new File(Environment
                    .getExternalStorageDirectory().toString(), "SCREEN"
                    + System.currentTimeMillis() + ".png"));
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String strFromDate() {
        return txtFromDate.getText().toString();
    }

    @Override
    public String getUserId() {
        return null;
    }

    @Override
    public String strToDate() {
        return txtToDate.getText().toString();
    }

    @Override
    public void editFromDateError() {
        txtFromDate.setError(String.format(AllMessages.pleaseEnter, " From Date"));
        txtFromDate.requestFocus();
    }

    @Override
    public void editToDateError() {
        txtToDate.setError(String.format(AllMessages.pleaseEnter, " To Date"));
        txtToDate.requestFocus();
    }

    @Override
    public void showPDialog() {
        pd.show();
    }

    @Override
    public void dismissPDialog() {
        h.removeCallbacks(r);
        if (pd.isShowing()) {
            pd.dismiss();
        }

    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String sttus) {
        alert.successAlert(sttus);
    }


    @Override
    public void getDayWiseReport(ArrayList<MoneyTransferReportPojo> moneytxReport) {
        dismissPDialog();
        adapter = new MoneyTransferReportListAdapter(context, moneytxReport);
        listView.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
