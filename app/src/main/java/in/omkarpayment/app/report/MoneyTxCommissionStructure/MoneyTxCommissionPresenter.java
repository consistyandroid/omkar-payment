package in.omkarpayment.app.report.MoneyTxCommissionStructure;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.MoneyTxCommissionPojo;
import in.omkarpayment.app.model.GetMoneyTxCommissionStrModel;


/**
 * Created by lenovo on 8/17/2018.
 */

public class MoneyTxCommissionPresenter implements IMoneyTxCommissionPresenter {
    IMoneyTxCommissionView iMoneyTxCommissionView;

    public MoneyTxCommissionPresenter(IMoneyTxCommissionView iMoneyTxCommissionView) {
        this.iMoneyTxCommissionView = iMoneyTxCommissionView;
    }

    @Override
    public void getMoneyTxCommissionStructure() {
        String keyData = new KeyDataReader().get();
        GetMoneyTxCommissionStrModel model = new GetMoneyTxCommissionStrModel();
        model.delegate = this;
        model.getMoneyTxCommission(keyData);
        showDialog();
    }

    @Override
    public void decryptMoneyTxCommissionStructureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Commission Structure", body);
            ArrayList<MoneyTxCommissionPojo> commission = gson.fromJson(body, new TypeToken<ArrayList<MoneyTxCommissionPojo>>() {
            }.getType());
            if (commission != null) {
                iMoneyTxCommissionView.getMoneyTxCommisionStructureView(commission);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptMoneyTxCommissionStructureFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Commission Structure", body);
            iMoneyTxCommissionView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissPDialog() {
        iMoneyTxCommissionView.dismissPDialog();
    }

    @Override
    public void showDialog() {
        iMoneyTxCommissionView.showDialog();
    }

    @Override
    public void errorAlert(String somethingWrong) {

    }
}
