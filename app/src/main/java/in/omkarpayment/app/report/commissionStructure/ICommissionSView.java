package in.omkarpayment.app.report.commissionStructure;

import java.util.ArrayList;

import in.omkarpayment.app.json.LastFiveRechargePojo;

/**
 * Created by consisty on 23/11/17.
 */

public interface ICommissionSView {
    void getCommisionStructureView(ArrayList<CommissionStructureJSON> commission);

    void dismissPDialog();

    void errorAlert(String somethingWrong);

    void showDialog();
}
