package in.omkarpayment.app.report.StockReport;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.StockReportAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.StockReportPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.CustomDateFormate;
import in.omkarpayment.app.userContent.EnglishNumberToWords;

import static android.R.layout.simple_spinner_item;

public class StockReportActivity extends AppCompatActivity implements IStockReportView {
    TextView editSearch;
    SwipeRefreshLayout swipeRefresh;
    RecyclerView listView;
    AlertImpl alert;
    Context context = this;
    String Status, strToDate, strFromDate;
    CustomProgressDialog pDialog;
    IStockReportPresenter iStockReportPresenter;
    StockReportAdapter adapter;
    ArrayAdapter<String> StatusForStockReport;
    Spinner spType;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    TextView txtFromDate, txtToDate;
    String cashCollectionAmount = "0", cashCollectionRemark = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_report);

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        editSearch = (TextView) findViewById(R.id.editSearch);
        listView = (RecyclerView) findViewById(R.id.listView);
        spType = (Spinner) findViewById(R.id.spType);


        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);

        iStockReportPresenter = new StockReportPresenter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());

        listView.setAdapter(null);
        txtFromDate = (TextView) findViewById(R.id.txtFromDate);
        txtToDate = (TextView) findViewById(R.id.txtToDate);

        setDateDefaultValue();
        StatusForStockReport = new ArrayAdapter<String>(context, simple_spinner_item, getResources().getStringArray(R.array.StatusForStockReport));
        StatusForStockReport.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(StatusForStockReport);


        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Status = "";
                Status = getResources().getStringArray(R.array.StatusForStockReport)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        Button btnShow = (Button) findViewById(R.id.btnShow);
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissPDialog();
                listView.setAdapter(null);
                iStockReportPresenter.getStockReport();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Stock Report";

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(StockReportActivity.this, Frmdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txtToDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(StockReportActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Frmdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strFromDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                txtFromDate.setText(Date);
                txtFromDate.setError(null);


            }
        };
        todate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strToDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                txtToDate.setText(Date);
                txtToDate.setError(null);

            }
        };


    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;

        txtToDate.setText(date);
        txtToDate.setError(null);

        txtFromDate.setText(date);
        txtFromDate.setError(null);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void getStockReport(ArrayList<StockReportPojo> stockreport) {
        dismissPDialog();

        adapter = new StockReportAdapter(StockReportActivity.this, context, stockreport);
        listView.setAdapter(adapter);
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void filterDialog() {
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public String StockReportStatus() {
        return Status;
    }

    @Override
    public String strFromDate() {
        return strFromDate;
    }

    @Override
    public String strToDate() {
        return strToDate;
    }

    @Override
    public void unableToFetchError(String message) {
        dismissPDialog();
        alert.errorAlert("Unable to fetch/receive " + message);
    }

    @Override
    public void acceptRequestAlert(String message) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(context.getResources().getString(R.string.app_name));
        text.setText(message);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listView.setAdapter(null);
                iStockReportPresenter.getStockReport();
            }
        });
        dialog.show();
    }

    public void rejectBalanceRequest(final Integer requestID) {

        final Dialog dialog = new Dialog(context);
      //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(context.getResources().getString(R.string.app_name));
        text.setText("Do you want to reject the request?");
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        Button btnNo = dialog.findViewById(R.id.btnNo);


        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iStockReportPresenter.rejectBalanceRequest(requestID);
                dialog.dismiss();
                pDialog.showPDialog();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void acceptBalanceRequest(final StockReportPojo requestReportPojo) {
        final Dialog dialog = new Dialog(context);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(context.getResources().getString(R.string.app_name));
        text.setText("Do you want to accept the request?");
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        Button btnNo = dialog.findViewById(R.id.btnNo);
//        ImageView btnClose = dialog.findViewById(R.id.btnClose);
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissPDialog();
                DisplayMetrics metrics = context.getResources().getDisplayMetrics();
                int width = metrics.widthPixels;

                final Dialog dialogBalReq = new Dialog(context);
                dialogBalReq.requestWindowFeature(Window.FEATURE_NO_TITLE);
               // dialogBalReq.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogBalReq.setCancelable(false);
                dialogBalReq.setContentView(R.layout.dialog_accept_balance_request);

                TextView txtBalanceType = dialogBalReq.findViewById(R.id.txtBalanceType);
                TextView txtRequestTo = dialogBalReq.findViewById(R.id.txtRequestTo);
                TextView txtUserID = dialogBalReq.findViewById(R.id.txtUserId);
                TextView txtAmount = dialogBalReq.findViewById(R.id.txtAmount);
                TextView txtAmountInWords = dialogBalReq.findViewById(R.id.txtAmountInWords);
                TextView txtRequestID = dialogBalReq.findViewById(R.id.txtRequestID);
                TextView txtReceiverBalance = dialogBalReq.findViewById(R.id.txtReceiverBalance);
                TextView txtReceiverOS = dialogBalReq.findViewById(R.id.txtReceiverOS);
                final TextView txtCashCollectionAmountInWords = dialogBalReq.findViewById(R.id.txtCashCollectionAmountInWords);
                final EditText editCashCollectionAmount = dialogBalReq.findViewById(R.id.editCashCollectionAmount);
                final EditText editCashCollectionRemark = dialogBalReq.findViewById(R.id.editRemark);

                txtBalanceType.setText(requestReportPojo.getBalanceType());
                txtRequestTo.setText(requestReportPojo.getParentName() + " (" + requestReportPojo.getRequestTo() + ")");
                txtUserID.setText(requestReportPojo.getRequestFrom() + " (" + requestReportPojo.getUserID() + ")");
                txtRequestID.setText(String.valueOf(requestReportPojo.getRequestID()));


               // txtReceiverBalance.setText(String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(requestReportPojo.getCurrentBalance()))));


                //txtReceiverOS.setText(String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(requestReportPojo.getOutstanding()))));

                txtAmount.setText(String.valueOf(requestReportPojo.getAmount()));
                txtAmountInWords.setText("Rs. " + EnglishNumberToWords.amountToWord(Integer.parseInt(String.valueOf(requestReportPojo.getAmount().intValue()))) + " Only");


                Button btnAcceptRequest = dialogBalReq.findViewById(R.id.btnAcceptRequest);

                editCashCollectionAmount.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() <= 0) {
                            txtCashCollectionAmountInWords.setText("");
                            return;
                        }
                        txtCashCollectionAmountInWords.setText("Rs. " + EnglishNumberToWords.amountToWord(Integer.parseInt(s.toString())) + " Only");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });



                btnAcceptRequest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // pDialog.showPDialog();
                        cashCollectionAmount = editCashCollectionAmount.getText().toString().trim();
                        cashCollectionRemark = editCashCollectionRemark.getText().toString().trim();

                        if (cashCollectionAmount.isEmpty()) {
                            cashCollectionAmount = "0";

                        } else if (cashCollectionRemark.isEmpty()) {
                            cashCollectionRemark = "";
                        }
                        dialog.dismiss();
                        iStockReportPresenter.acceptBalanceRequest(requestReportPojo, cashCollectionAmount, cashCollectionRemark);
                        dialogBalReq.dismiss();
                        pDialog.showPDialog();

                    }
                });
                if (dialogBalReq.getWindow() != null) {
                    dialogBalReq.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
                }
                dialogBalReq.show();

               /* iBalanceRequestReportPresenter.acceptBalanceRequest(requestReportPojo);
                dialog.dismiss();*/
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
