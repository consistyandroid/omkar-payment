package in.omkarpayment.app.report.MoneyTransferReport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by consisty on 14/10/17.
 */

public class MoneyTransferReportPojo {

    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("SenderNumber")
    @Expose
    private String senderNumber;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("AccountNo")
    @Expose
    private String accountNo;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("ClosingBal")
    @Expose
    private Double closingBal;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("Bank")
    @Expose
    private String bank;
    @SerializedName("Charges")
    @Expose
    private Double charges;
    @SerializedName("NetAmount")
    @Expose
    private Double netAmount;
    @SerializedName("Username")
    @Expose
    private String username;
    @SerializedName("Through")
    @Expose
    private String through;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("InstapayID")
    @Expose
    private String instapayID;
    @SerializedName("IFSCCode")
    @Expose
    private String iFSCCode;

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Double getClosingBal() {
        return closingBal;
    }

    public void setClosingBal(Double closingBal) {
        this.closingBal = closingBal;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Double getCharges() {
        return charges;
    }

    public void setCharges(Double charges) {
        this.charges = charges;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getThrough() {
        return through;
    }

    public void setThrough(String through) {
        this.through = through;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getInstapayID() {
        return instapayID;
    }

    public void setInstapayID(String instapayID) {
        this.instapayID = instapayID;
    }

    public String getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }
}
