package in.omkarpayment.app.report.MoneyTransferReport;

import java.util.ArrayList;

/**
 * Created by consisty on 14/10/17.
 */

public interface IMoneyTransferReportView {

    String strFromDate();

    String getUserId();

    String strToDate();

    void editFromDateError();

    void editToDateError();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String sttus);

    void getDayWiseReport(ArrayList<MoneyTransferReportPojo> dayReport);
}
