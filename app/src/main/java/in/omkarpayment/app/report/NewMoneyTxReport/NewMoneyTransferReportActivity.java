package in.omkarpayment.app.report.NewMoneyTxReport;


import android.app.DatePickerDialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.recharge.allEDMT.aMoneyTransfer.json.ATransferReportPojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityMoneyTxReportBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.CustomDateFormate;


public class NewMoneyTransferReportActivity extends AppCompatActivity implements aTxReportView {

    ActivityMoneyTxReportBinding txReportBinding;
    aTxReportPresenter aTxReportPresenter;
    String strFromDate = "", strToDate = "";
    AlertImpl alert;
    TextView txtToolbarBalance;
    Context context = this;
    CustomProgressDialog pDialog;
    NetworkState ns = new NetworkState();
    private int mYear, mMonth, mDay, fDate, tDate, fMonth, tMonth, fYear, tYear;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        txReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_money_tx_report);
        aTxReportPresenter = new TxReportPresenter(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        //txtToolbarBalance.setText("RCH : ₹ " + UserDetails.UserBalance);
       // txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));
        String title = "Money Transfer Report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
       /* TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);*/
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        txReportBinding.listView.setLayoutManager(mLayoutManager);
        txReportBinding.listView.setItemAnimator(new DefaultItemAnimator());
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        txReportBinding.txtToDate.setText(CustomDateFormate.getCurrentDateToShow());
        strToDate = CustomDateFormate.getCurrentDate();
        txReportBinding.txtFromDate.setText(CustomDateFormate.getCurrentDateToShow());
        strFromDate = CustomDateFormate.getCurrentDate();

        setDateDefaultValue();

        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
        } else {
            aTxReportPresenter.getMoneyTxReport();
            pDialog.showPDialog();
        }
        txReportBinding.txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(NewMoneyTransferReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        tDate = selectedday;
                        tMonth = selectedmonth + 1;
                        tYear = selectedyear;
                        String dateSelected = padLeftZero(selectedday + "") + "/" + padLeftZero((selectedmonth + 1) + "") + "/" + selectedyear;
                        txReportBinding.txtToDate.setText(dateSelected);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.show();
            }
        });

        txReportBinding.txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(NewMoneyTransferReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        fDate = selectedday;
                        fMonth = selectedmonth + 1;
                        fYear = selectedyear;
                        String dateSelected = padLeftZero(selectedday + "") + "/" + padLeftZero((selectedmonth + 1) + "") + "/" + selectedyear;
                        txReportBinding.txtFromDate.setText(dateSelected);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.show();
            }

        });

        txReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                if (fYear > tYear) {
                    alert.errorAlert("Please select valid from date");
                    return;
                } else {
                    if (fMonth > tMonth) {
                        alert.errorAlert("Please select valid from date");
                        return;
                    } else {
                        if (fMonth == tMonth) {
                            if (fDate > tDate) {
                                alert.errorAlert("Please select valid from date");
                                return;
                            }
                        }
                    }
                }
                aTxReportPresenter.validateDate();
                txReportBinding.listView.setAdapter(null);
                pDialog.showPDialog();
            }
        });

    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;

        String[] splitDate = date.split("/");
        fDate = Integer.parseInt(splitDate[0]);
        tDate = Integer.parseInt(splitDate[0]);
        fMonth = Integer.parseInt(splitDate[1]);
        tMonth = Integer.parseInt(splitDate[1]);
        fYear = Integer.parseInt(splitDate[2]);
        tYear = Integer.parseInt(splitDate[2]);


        txReportBinding.txtToDate.setText(date);
        txReportBinding.txtToDate.setError(null);

        txReportBinding.txtFromDate.setText(date);
        txReportBinding.txtFromDate.setError(null);

    }

    public String padLeftZero(String input) {
        String outPut = String.format("%2s", input).replace(' ', '0');
        return outPut;
    }

    @Override
    public void editFromDateError() {

    }

    @Override
    public void editToDateError() {

    }

    @Override
    public String strFromDate() {
        return txReportBinding.txtFromDate.getText().toString();
    }

    @Override
    public String toDate() {
        return txReportBinding.txtToDate.getText().toString();
    }

    @Override
    public void getTxReport(ArrayList<ATransferReportPojo> iTransferReportPojos) {
        pDialog.dismissPDialog();
        TxReportAdapter adapter = new TxReportAdapter(context, NewMoneyTransferReportActivity.this
                , iTransferReportPojos);
        txReportBinding.listView.setAdapter(adapter);
    }

    @Override
    public void alert(String status) {
        pDialog.dismissPDialog();
        alert.errorAlert(status);
    }

    @Override
    public void getReport() {
        aTxReportPresenter.getMoneyTxReport();
        pDialog.showPDialog();
    }

    @Override
    public void unableToFetchError(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive " + message + " Please restart your app.");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }
}
