package in.omkarpayment.app.report.lasttransaction;

import in.omkarpayment.app.json.LastFiveRechargePojo;

import java.util.ArrayList;

/**
 * Created by Krish on 10-Aug-17.
 */

public interface ILastTransactionPresenter {

    void getLastTransaction();

    void showDialog();

    void dismissDialog();

    void errorAlert(String msg);

    void getLastTransaction(ArrayList<LastFiveRechargePojo> latTransaction);

    void LastTransactionResponse(String encryptedResponse);

    void LastTransactionFailureResponse(String encryptedResponse);
}
