package in.omkarpayment.app.report.balanceReport;

import android.app.DatePickerDialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.BalanceReportAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityBalanceReportBinding;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.CustomDateFormate;
import in.omkarpayment.app.userContent.SpinnerData;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

public class BalanceReportActivity extends AppCompatActivity implements IBalanceReportView {
    ActivityBalanceReportBinding balanceReportBinding;
    ArrayAdapter<String> spAdapter;
    Calendar myCalendar;
    Context context = this;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    String strFromDate = "", strToDate = "", ReportType = "Receive";
    AlertImpl alert;
    CustomProgressDialog pDialog;
    IBalanceReportPresenter iBalanceReportPresenter;
    BalanceReportAdapter adapter;
    RecyclerView listView;
    NetworkState ns = new NetworkState();
    LinearLayout dateFieldBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        balanceReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_balance_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Balance Report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        SpannableString set = new SpannableString(title);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setDefaultValue();


        listView = (RecyclerView) findViewById(R.id.listv);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(null);


        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        spAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SpinnerData.spType);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        balanceReportBinding.spType.setAdapter(spAdapter);
        balanceReportBinding.LayReceiveText.setVisibility(View.GONE);
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        iBalanceReportPresenter = new BalanceReportPresenter(this);
        if (UserDetails.UserType.equals(UserType.Retailer)) {
            balanceReportBinding.spType.setSelection(1);
            balanceReportBinding.LaySpinner.setVisibility(View.GONE);
            balanceReportBinding.LayReceiveText.setVisibility(View.VISIBLE);
        } else {
            balanceReportBinding.LayReceiveText.setVisibility(View.GONE);
        }
        setDateDefaultValue();
        balanceReportBinding.txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(in.omkarpayment.app.report.balanceReport.BalanceReportActivity.this, Frmdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        balanceReportBinding.txtToDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(in.omkarpayment.app.report.balanceReport.BalanceReportActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Frmdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strFromDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                balanceReportBinding.txtFromDate.setText(Date);
                balanceReportBinding.txtFromDate.setError(null);


            }
        };
        todate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strToDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                balanceReportBinding.txtToDate.setText(Date);
                balanceReportBinding.txtToDate.setError(null);

            }
        };
        balanceReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setAdapter(null);
                if (ns.isInternetAvailable(context)) {
                    iBalanceReportPresenter.validate();
                } else {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

            }
        });
        balanceReportBinding.spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ReportType = SpinnerData.spType[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

    }

    private void setDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;
        balanceReportBinding.txtToDate.setText(date);
        balanceReportBinding.txtToDate.setError(null);
        balanceReportBinding.txtFromDate.setText(date);
        balanceReportBinding.txtFromDate.setError(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
         /*   Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public String reportType() {
        return ReportType;
    }

    @Override
    public String fromDate() {
        return strFromDate;
    }

    @Override
    public String toDate() {
        return strToDate;
    }

    @Override
    public void txtFromDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseSelect, "From Date"));
    }

    @Override
    public void txtToDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseSelect, "To Date"));
    }

    @Override
    public void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse) {
        adapter = new BalanceReportAdapter(context, reportResponse);
        listView.setAdapter(adapter);
    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;

        balanceReportBinding.txtToDate.setText(date);
        balanceReportBinding.txtToDate.setError(null);

        balanceReportBinding.txtFromDate.setText(date);
        balanceReportBinding.txtFromDate.setError(null);

    }
}
