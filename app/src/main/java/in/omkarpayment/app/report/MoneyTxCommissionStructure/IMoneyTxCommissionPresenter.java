package in.omkarpayment.app.report.MoneyTxCommissionStructure;

/**
 * Created by lenovo on 8/17/2018.
 */

public interface IMoneyTxCommissionPresenter {
    void getMoneyTxCommissionStructure();

    void decryptMoneyTxCommissionStructureResponse(String encryptedResponse);

    void decryptMoneyTxCommissionStructureFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void showDialog();

    void errorAlert(String somethingWrong);
}
