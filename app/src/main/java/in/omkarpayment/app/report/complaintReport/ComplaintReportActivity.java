package in.omkarpayment.app.report.complaintReport;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.ComplaintReportAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.ComplaintReportPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;

public class ComplaintReportActivity extends AppCompatActivity implements IComplaintReportView {
    TextView editSearch;
    SwipeRefreshLayout swipeRefresh;
    RecyclerView listView;
    AlertImpl alert;
    Context context = this;
    CustomProgressDialog pDialog;
    ComplaintReportAdapter adapter;
    IComplaintReportPresenter iComplaintReportPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_report);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        editSearch = (TextView) findViewById(R.id.editSearch);
        listView = (RecyclerView) findViewById(R.id.listView);
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        iComplaintReportPresenter = new ComplaintReportPresenter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        iComplaintReportPresenter.getComplaintReport();
        listView.setAdapter(null);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Complaint View";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
           finish(); // close this activity and return to preview activity (if there is any)
       /*     Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getComplaintReport(ArrayList<ComplaintReportPojo> complaintreport) {
        dismissPDialog();
        adapter = new ComplaintReportAdapter(context, complaintreport);
        listView.setAdapter(adapter);
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void filterDialog() {

    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }
}
