package in.omkarpayment.app.report.balanceReport;

import android.app.DatePickerDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityWalletTransferReportBinding;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.CustomDateFormate;

import java.util.ArrayList;
import java.util.Calendar;

public class WalletTransferReport extends AppCompatActivity implements IBalanceReportView {
    AlertImpl alert;
    CustomProgressDialog pDialog;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    String strFromDate = "", strToDate = "";
    ActivityWalletTransferReportBinding walletTransferReportBinding;
    IBalanceReportPresenter iBalanceReportPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        walletTransferReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_wallet_transfer_report);

        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        iBalanceReportPresenter = new BalanceReportPresenter(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Wallet Transfer report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);


        walletTransferReportBinding.txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(WalletTransferReport.this, Frmdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        walletTransferReportBinding.txtToDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(WalletTransferReport.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Frmdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strFromDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                walletTransferReportBinding.txtFromDate.setText(Date);
                walletTransferReportBinding.txtFromDate.setError(null);


            }
        };
        todate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strToDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                walletTransferReportBinding.txtToDate.setText(Date);
                walletTransferReportBinding.txtToDate.setError(null);


            }
        };

        walletTransferReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iBalanceReportPresenter.validate();
            }
        });

    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public String reportType() {
        return "Wallet Report";
    }

    @Override
    public String fromDate() {
        return strFromDate;
    }

    @Override
    public String toDate() {
        return strToDate;
    }

    @Override
    public void txtFromDateError() {
        walletTransferReportBinding.txtFromDate.setError(String.format(AllMessages.pleaseSelect, "From Date"));
        walletTransferReportBinding.txtFromDate.requestFocus();
    }

    @Override
    public void txtToDateError() {
        walletTransferReportBinding.txtToDate.setError(String.format(AllMessages.pleaseSelect, "To Date"));
        walletTransferReportBinding.txtToDate.requestFocus();
    }

    @Override
    public void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
