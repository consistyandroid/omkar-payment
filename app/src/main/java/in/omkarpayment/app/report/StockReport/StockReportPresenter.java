package in.omkarpayment.app.report.StockReport;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.StockReportPojo;
import in.omkarpayment.app.model.StockRequestReportModel;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.Validation;

/**
 * Created by Somnath-Laptop on 29-Jan-2018.
 */

public class StockReportPresenter implements IStockReportPresenter {
    IStockReportView iStockReportView;

    public StockReportPresenter(IStockReportView iStockReportView) {
        this.iStockReportView = iStockReportView;
    }
    @Override
    public void getStockReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iStockReportView.strFromDate());
            dayreportrequest.put("ToDate", iStockReportView.strToDate());
            dayreportrequest.put("Status", iStockReportView.StockReportStatus());
            String jsonrequest = dayreportrequest.toString();
            Log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateStockReportResponse = this;
                model.webserviceMethod(encryptString, keyData, "StockReport");
                showDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //String keyData = new KeyDataReader().get();

    }

    @Override
    public void showDialog() {
        iStockReportView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iStockReportView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iStockReportView.errorAlert(msg);
    }

    @Override
    public void getStockReport(ArrayList<StockReportPojo> stockreport) {
        iStockReportView.getStockReport(stockreport);

    }

    @Override
    public void StockReportResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("StockReportSRes:", body);
            ArrayList<StockReportPojo> stockreport = gson.fromJson(body, new TypeToken<ArrayList<StockReportPojo>>() {
            }.getType());
            if (stockreport != null) {
                iStockReportView.getStockReport(stockreport);

            }
        } catch (Exception e) {

        }

    }

    @Override
    public void StockReportFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("StockReportFRes:", body);
            errorAlert(body);

        } catch (Exception e) {

        }

    }
    @Override
    public void rejectBalanceRequest(Integer requestID) {
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject request = new JSONObject();
        try {
            request.put("RequestID", requestID);
            request.put("Reason", "Reject");

            String jsonRequest = request.toString();
            Log.i("RejectBalanceRequest", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("Encrypted String", encryptString);

            Validation validation = new Validation();
            if (!validation.isNullOrEmpty(encryptString)) {
                iStockReportView.unableToFetchError("data");
                iStockReportView.unableToFetchError("data");
                return;
            }
            StockRequestReportModel model = new StockRequestReportModel();
            model.delegateBalanceRequestReport = this;
            model.rejectBalanceRequestWebService(encryptString, keyData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void acceptRejectBalanceRequestResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            Validation validation = new Validation();
            if (!validation.isNullOrEmpty(body)) {
                iStockReportView.unableToFetchError("report");
                return;
            }
            iStockReportView.acceptRequestAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void acceptRejectBalanceRequestFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            iStockReportView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void acceptBalanceRequest(StockReportPojo requestReportPojo, String cashCollectionAmount, String cashCollectionRemark) {
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject request = new JSONObject();
        try {
            request.put("BalanceType", requestReportPojo.getBalanceType());
            request.put("SenderID", requestReportPojo.getRequestTo());
            request.put("ReceiverID", requestReportPojo.getUserID());
            request.put("Amount", requestReportPojo.getAmount());
            request.put("Type", "TRANSFER");
            request.put("RequestID", requestReportPojo.getRequestID());
            request.put("RechargePin", "1234");
            request.put("Remark", requestReportPojo.getRemark());
            request.put("CashCollected", cashCollectionAmount);
            request.put("CashCollectionRemark", cashCollectionRemark);

            String jsonRequest = request.toString();
            Log.i("accept Balance Request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("Encrypted String", encryptString);

            Validation validation = new Validation();
            if (!validation.isNullOrEmpty(encryptString)) {
                iStockReportView.unableToFetchError("data");
                return;
            }
            StockRequestReportModel model = new StockRequestReportModel();
            model.delegateBalanceRequestReport = this;
            model.acceptBalanceRequestWebService(encryptString, keyData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
