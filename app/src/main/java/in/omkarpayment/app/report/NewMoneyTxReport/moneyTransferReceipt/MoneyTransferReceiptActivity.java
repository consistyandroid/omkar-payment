package in.omkarpayment.app.report.NewMoneyTxReport.moneyTransferReceipt;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.omkarpayment.app.BuildConfig;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityReportReceiptBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;


public class MoneyTransferReceiptActivity extends AppCompatActivity implements aMoneyTransferReceiptView {

    ActivityReportReceiptBinding activityReportReceiptBinding;
    aMoneyTransferReceiptPresenter aMoneyTransferReceiptPresenter;
    AlertImpl alert;
    Context context = this;
    CustomProgressDialog pDialog;
    TextView txtToolbarBalance;
    NetworkState ns = new NetworkState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityReportReceiptBinding = DataBindingUtil.setContentView(this, R.layout.activity_report_receipt);
        aMoneyTransferReceiptPresenter = new NewMoneyTransferReportPresenter(this);
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
       /* txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        //txtToolbarBalance.setText("RCH : ₹ " + UserDetails.UserBalance);
        txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));*/
        String title = "Money Transfer Receipt";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        /*TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);*/

        setBundle();

        aMoneyTransferReceiptPresenter.validateBillNumber();

        activityReportReceiptBinding.btnSavePdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityReportReceiptBinding.btnSavePdf.setVisibility(View.INVISIBLE);
                activityReportReceiptBinding.layoutToolbar.setVisibility(View.INVISIBLE);
                takeScreenshot();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        activityReportReceiptBinding.btnSavePdf.setVisibility(View.VISIBLE);
        activityReportReceiptBinding.layoutToolbar.setVisibility(View.VISIBLE);
        //txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));
        //txtToolbarBalance.setText("RCH : ₹ " + UserDetails.UserBalance + "\nDMR : ₹ " + UserDetails.DMRBalance);
    }

    private void setBundle() {
        Bundle bundle = getIntent().getExtras();
        try {
            if (bundle.getString("ReceiptBillNumber") == null) {
                AMoneyTransferUserDetails.receiptBillNumber = "";
                return;
            }
            AMoneyTransferUserDetails.receiptBillNumber = bundle.getString("ReceiptBillNumber");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takeScreenshot() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        try {

            String mPath = Environment.getExternalStorageDirectory().toString() + "/MoneyTransferReceipt(" + currentDateandTime + ").jpg";
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Uri uri;
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            uri = Uri.fromFile(imageFile);
        } else {
            uri = FileProvider.getUriForFile(MoneyTransferReceiptActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider", imageFile);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getBillNumber() {
        return AMoneyTransferUserDetails.receiptBillNumber;
    }

    @Override
    public void unableToFetchAlert(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive " + message + " Please restart your app.");
    }

    @Override
    public void getReceipt() {

        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }

        activityReportReceiptBinding.txtTransferType.setText("Funds Transfer Through : ....");
        activityReportReceiptBinding.txtPayDate.setText("Please wait....");
        activityReportReceiptBinding.txtShopName.setText("Please wait....");
        activityReportReceiptBinding.txtRetailerNumber.setText("Please wait....");
        activityReportReceiptBinding.txtSenderName.setText("Please wait....");
        activityReportReceiptBinding.txtSenderMobileNumber.setText("Please wait....");
        activityReportReceiptBinding.txtBeneficiaryName.setText("Please wait....");
        activityReportReceiptBinding.txtAccountNumber.setText("Please wait....");
        activityReportReceiptBinding.txtBankName.setText("Please wait....");
        activityReportReceiptBinding.txtOriginalAmount.setText("Please wait....");
        activityReportReceiptBinding.txtTransactionID.setText("Please wait....");
        activityReportReceiptBinding.txtStatus.setText("Please wait....");
        activityReportReceiptBinding.txtTransactionAmount.setText("Please wait....");

        pDialog.showPDialog();
        aMoneyTransferReceiptPresenter.getReceipt();
    }

    @Override
    public void errorAlert(String status) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert(status);
    }

    @Override
    public void getReceiptSuccess(ArrayList<MoneyTransferReceiptPojo> moneyTransferReceiptPojo) {

        activityReportReceiptBinding.txtTransferType.setText("Funds Transfer Through : " + moneyTransferReceiptPojo.get(0).getPaymentType());
        activityReportReceiptBinding.txtPayDate.setText(moneyTransferReceiptPojo.get(0).getDate());
        activityReportReceiptBinding.txtShopName.setText(moneyTransferReceiptPojo.get(0).getShopeName());
        activityReportReceiptBinding.txtRetailerNumber.setText(moneyTransferReceiptPojo.get(0).getMobileNumber());
        activityReportReceiptBinding.txtSenderName.setText(moneyTransferReceiptPojo.get(0).getSenderName());
        activityReportReceiptBinding.txtSenderMobileNumber.setText(moneyTransferReceiptPojo.get(0).getSenderMobileNumber());
        activityReportReceiptBinding.txtBeneficiaryName.setText(moneyTransferReceiptPojo.get(0).getBeneficiaryName());
        activityReportReceiptBinding.txtAccountNumber.setText(moneyTransferReceiptPojo.get(0).getAccountNumber());
        activityReportReceiptBinding.txtBankName.setText(moneyTransferReceiptPojo.get(0).getBank());
        activityReportReceiptBinding.txtOriginalAmount.setText(String.valueOf(moneyTransferReceiptPojo.get(0).getNetAmount()));
        activityReportReceiptBinding.txtTransactionID.setText(moneyTransferReceiptPojo.get(0).getProviderOprID());
        activityReportReceiptBinding.txtStatus.setText(String.valueOf(moneyTransferReceiptPojo.get(0).getStatus()));
        activityReportReceiptBinding.txtTransactionAmount.setText(moneyTransferReceiptPojo.get(0).getAmountList());

        pDialog.dismissPDialog();

    }

    @Override
    public void getReceiptFailAlert(String response) {

        activityReportReceiptBinding.txtTransferType.setText("Funds Transfer Through : N/A");
        activityReportReceiptBinding.txtPayDate.setText("N/A");
        activityReportReceiptBinding.txtShopName.setText("N/A");
        activityReportReceiptBinding.txtRetailerNumber.setText("N/A");
        activityReportReceiptBinding.txtSenderName.setText("N/A");
        activityReportReceiptBinding.txtSenderMobileNumber.setText("N/A");
        activityReportReceiptBinding.txtBeneficiaryName.setText("N/A");
        activityReportReceiptBinding.txtAccountNumber.setText("N/A");
        activityReportReceiptBinding.txtBankName.setText("N/A");
        activityReportReceiptBinding.txtOriginalAmount.setText("N/A");
        activityReportReceiptBinding.txtTransactionID.setText("N/A");
        activityReportReceiptBinding.txtStatus.setText("N/A");
        activityReportReceiptBinding.txtTransactionAmount.setText("N/A");

        pDialog.dismissPDialog();
        alert.errorAlert(response);

    }
}
