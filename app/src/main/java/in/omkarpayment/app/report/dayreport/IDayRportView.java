package in.omkarpayment.app.report.dayreport;


import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;

import java.util.ArrayList;

/**
 * Created by Krish on 12-Jul-17.
 */

public interface IDayRportView {
    String strFromDate();

    String strToDate();

    void editFromDateError();
    void editToDateError();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void getDayWiseReport(ArrayList<DayReportPojo> dayReport);
}


