package in.omkarpayment.app.report.commissionStructure;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by consisty on 23/11/17.
 */

public class CommissionSPresenterImpl implements ICommissionSPresenter {

    ICommissionSView iCommissionSView;

    public CommissionSPresenterImpl(ICommissionSView iCommissionSView) {
        this.iCommissionSView = iCommissionSView;
    }


    @Override
    public void getCommissionStructure() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateCommissionStructure = this;
        model.webserviceMethod("", keyData, "CommmissionStructure");
        showDialog();
    }

    @Override
    public void decryptCommissionStructureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Commission Structure",body);
            ArrayList<CommissionStructureJSON> commission = gson.fromJson(body, new TypeToken<ArrayList<CommissionStructureJSON>>() {
            }.getType());
            if (commission != null) {
                iCommissionSView.getCommisionStructureView(commission);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void decryptCommissionStructureFailureResponse(String encryptedResponse) {


    }

    @Override
    public void dismissPDialog() {
        iCommissionSView.dismissPDialog();
    }

    @Override
    public void errorAlert(String somethingWrong) {
        iCommissionSView.errorAlert(somethingWrong);


    }

    private void showDialog() {

        iCommissionSView.showDialog();
    }
}
