package in.omkarpayment.app.report.sendermoneyTransferReport;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.SenderMoneyTransferPojoModel;
import in.omkarpayment.app.model.WebServiceModel;


/**
 * Created by consisty on 7/3/18.
 */

public class SenderMoneyTransferPresenterImpl implements ISenderMoneyTransferPresenter {
    ISenderMoneyTransferView iSenderMoneyTransferView;

    public SenderMoneyTransferPresenterImpl(ISenderMoneyTransferView iSenderMoneyTransferView) {
        this.iSenderMoneyTransferView = iSenderMoneyTransferView;
    }

    @Override
    public void searchTransactions() {
        if (iSenderMoneyTransferView.getNumber().isEmpty()) {
            iSenderMoneyTransferView.editNumberError();
        } else {
            SearchReport();
        }
    }

    @Override
    public void showPDialog() {
        iSenderMoneyTransferView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iSenderMoneyTransferView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iSenderMoneyTransferView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iSenderMoneyTransferView.successAlert(msg);
    }

    public void SearchReport() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("SenderMobileNumber", iSenderMoneyTransferView.getNumber());
            String jsonrequest = dayreportrequest.toString();
            Log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
      /*          WebServiceModel model = new WebServiceModel();
                model.delegateSearchSenderNumber = this;
                model.webserviceMethod(encryptString, keyData, "SearchSenderTransactions");
                showPDialog();*/

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptSearchNumberReportListResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("SenderList--++>", body);
            ArrayList<SenderMoneyTransferPojoModel> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<SenderMoneyTransferPojoModel>>() {
            }.getType());
            if (lastrasaction != null) {
                 iSenderMoneyTransferView.getReport(lastrasaction);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void decryptSearchNumberReportListFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("SenderListF--++>", body);
            errorAlert(body);
        } catch (Exception e) {

        }

    }
}
