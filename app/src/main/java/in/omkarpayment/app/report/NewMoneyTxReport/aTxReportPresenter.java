package in.omkarpayment.app.report.NewMoneyTxReport;


public interface aTxReportPresenter {

    void validateDate();

    void getReport(String response);

    void alert(String status);

    void getMoneyTxReport();

    void getReportFail(String response);

}
