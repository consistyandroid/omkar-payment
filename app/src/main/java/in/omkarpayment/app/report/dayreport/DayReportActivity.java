package in.omkarpayment.app.report.dayreport;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.DayReportAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityDayReportBinding;
import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.CustomDateFormate;

public class DayReportActivity extends AppCompatActivity implements IDayRportView {

    ActivityDayReportBinding dayReportBinding;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    String strFromDate = "", strToDate = "";
    DayReportAdapter adapter;
    AlertImpl alert;

    CustomProgressDialog progressDialog;
    CollapsingToolbarLayout collapsingToolbarLayout;
    DayReportPresenter dayReportPresenter;
    Context context = this;
    NetworkState ns = new NetworkState();
    private int selectedDate, selectedMonth, selectedYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dayReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_day_report);
        progressDialog = new CustomProgressDialog(this);
        dayReportPresenter = new DayReportPresenter(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Day Report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        dayReportBinding.listView.setLayoutManager(mLayoutManager);
        dayReportBinding.listView.setItemAnimator(new DefaultItemAnimator());
        alert = new AlertImpl(this);

        setDateDefaultValue();
        dayReportBinding.txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(DayReportActivity.this, Frmdate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


        dayReportBinding.txtToDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                new DatePickerDialog(DayReportActivity.this, todate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dayReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ns.isInternetAvailable(context)) {
                    dayReportPresenter.validateDate();
                } else {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

                dayReportBinding.listView.setAdapter(null);
            }
        });
        Frmdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strFromDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                dayReportBinding.txtFromDate.setText(Date);
                dayReportBinding.txtFromDate.setError(null);


            }
        };
        todate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strToDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                dayReportBinding.txtToDate.setText(Date);
                dayReportBinding.txtToDate.setError(null);


            }
        };
    }

    @Override
    public String strFromDate() {
        return dayReportBinding.txtFromDate.getText().toString();
    }

    @Override
    public String strToDate() {
        return dayReportBinding.txtToDate.getText().toString();
    }

    @Override
    public void editFromDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "From Date"));
        dayReportBinding.txtFromDate.requestFocus();
    }

    @Override
    public void editToDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "To Date"));
        dayReportBinding.txtToDate.requestFocus();
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String sttus) {
        alert.successAlert(sttus);
    }


    @Override
    public void getDayWiseReport(ArrayList<DayReportPojo> dayReport) {
        adapter = new DayReportAdapter(context, dayReport);
        dayReportBinding.listView.setAdapter(adapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);

            // finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;

        dayReportBinding.txtToDate.setText(date);
        dayReportBinding.txtToDate.setError(null);

        dayReportBinding.txtFromDate.setText(date);
        dayReportBinding.txtFromDate.setError(null);

    }
}
