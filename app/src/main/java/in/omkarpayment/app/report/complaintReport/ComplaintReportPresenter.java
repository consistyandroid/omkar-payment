package in.omkarpayment.app.report.complaintReport;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.ComplaintReportPojo;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by Somnath-Laptop on 01-Feb-2018.
 */

public class ComplaintReportPresenter implements IComplaintReportPresenter {
    IComplaintReportView iComplaintReportView;

    public ComplaintReportPresenter(IComplaintReportView iComplaintReportView) {
        this.iComplaintReportView = iComplaintReportView;
    }

    @Override
    public void getComplaintReport() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateComplaintReportResponse = this;
        model.webserviceMethod("", keyData, "ViewComplaint");
        showDialog();
    }

    @Override
    public void showDialog() {
        iComplaintReportView.showPDialog();
    }

    @Override
    public void dismissDialog() {
        iComplaintReportView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iComplaintReportView.errorAlert(msg);
    }

    @Override
    public void getComplaintReport(ArrayList<ComplaintReportPojo> complaintreport) {
        iComplaintReportView.getComplaintReport(complaintreport);
    }

    @Override
    public void decreptedComplaintReportResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("ComplaintReport", body);
            ArrayList<ComplaintReportPojo> complaintreport = gson.fromJson(body, new TypeToken<ArrayList<ComplaintReportPojo>>() {
            }.getType());
            if (complaintreport != null) {
                iComplaintReportView.getComplaintReport(complaintreport);

            }
        } catch (Exception e) {

        }

    }

    @Override
    public void decreptedComplaintReportFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            errorAlert(body);

        } catch (Exception e) {

        }

    }


}
