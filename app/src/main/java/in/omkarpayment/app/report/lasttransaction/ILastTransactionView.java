package in.omkarpayment.app.report.lasttransaction;

import in.omkarpayment.app.json.LastFiveRechargePojo;

import java.util.ArrayList;

/**
 * Created by Krish on 10-Aug-17.
 */

public interface ILastTransactionView {

    void getLastTransaction(ArrayList<LastFiveRechargePojo> lastTransaction);

    void errorAlert(String msg);

    void successAlert(String msg);

    void filterDialog();

    void showPDialog();

    void dismissPDialog();
}
