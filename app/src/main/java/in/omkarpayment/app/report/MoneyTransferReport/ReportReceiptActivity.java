package in.omkarpayment.app.report.MoneyTransferReport;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;



import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityReportReceiptBinding;


public class ReportReceiptActivity extends AppCompatActivity {

    public static int REQUEST_PERMISSIONS = 1;
    ActivityReportReceiptBinding activityReportReceiptBinding;
    boolean boolean_permission;
    boolean boolean_save;
    Bitmap bitmap;
    AlertImpl alert;
    Context context = this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityReportReceiptBinding = DataBindingUtil.setContentView(this, R.layout.activity_report_receipt);
        alert = new AlertImpl(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setSupportActionBar(toolbar);
        String title = "Report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        fn_permission();

        Bundle bundle = getIntent().getExtras();
        String txtTransType = bundle.getString("TransType");
        String txtTxId = bundle.getString("TxId");
        String txtAccountNo = bundle.getString("AccountNo");
        String Amount = bundle.getString("Amount");
        String txtBenefName = bundle.getString("BenifName");
        String txtBankName = bundle.getString("BankName");
        String txtSenderName = bundle.getString("SenderName");
        String txtPayDate = bundle.getString("PayDate");
        String txtIfscCode = bundle.getString("IFSCCode");

       /* activityReportReceiptBinding.txtTransType.setText(String.format("Funds Transfer Through –: %s",
                txtTransType));
        activityReportReceiptBinding.txtTransType2.setText(String.format("Your %s request has been initiated. To confirm transaction\n" +
                "Transaction Ref No is %s", txtTransType, txtTxId));

        activityReportReceiptBinding.txtAccountNo.setText(txtAccountNo);
        activityReportReceiptBinding.txtAmount.setText(Amount);
        activityReportReceiptBinding.txtBenefName.setText(txtBenefName);
        activityReportReceiptBinding.txtBankName.setText(txtBankName);
        activityReportReceiptBinding.txtSenderNo.setText(txtSenderName);
        activityReportReceiptBinding.txtPayDate.setText(txtPayDate);
        activityReportReceiptBinding.txtIFSCCode.setText(txtIfscCode);*/
        //     listener();
        activityReportReceiptBinding.btnSavePdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityReportReceiptBinding.btnSavePdf.setVisibility(View.GONE);
                takeScreenshot();
            }
        });
    }
    private void takeScreenshot() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + "Money Transfer Receipt" + "(" + currentDateandTime + ") " + ".jpg";
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }

    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }



/*
    private void createPdf() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels;
        float width = displaymetrics.widthPixels;

        int convertHighet = (int) hight, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);


        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();


        Paint paint = new Paint();
        canvas.drawPaint(paint);


        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        document.finishPage(page);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        // write the document content
        final String targetPdf = "/sdcard/MoneyTransferReceipt(" + currentDateandTime + ").pdf";
        final File filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));

            SweetAlertDialog sweetAlertDialogView;
            sweetAlertDialogView = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialogView.setTitleText("Message");
            sweetAlertDialogView.setContentText("PDF Generated Successfully");
            sweetAlertDialogView.setCancelText("View");
            sweetAlertDialogView.setConfirmText("Ok");
            sweetAlertDialogView.showCancelButton(true);


            sweetAlertDialogView.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismiss();
                    try {
                        openFile(context, filePath, targetPdf);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            sweetAlertDialogView.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    sweetAlertDialog.dismiss();


                }
            })
                    .show();
            Button viewGroup = (Button) sweetAlertDialogView.findViewById(cn.pedant.SweetAlert.R.id.confirm_button);
            Button viewForCan = (Button) sweetAlertDialogView.findViewById(cn.pedant.SweetAlert.R.id.cancel_button);
            if (viewGroup != null) {

                viewGroup.setBackgroundColor(viewGroup.getResources().getColor(R.color.blue));
            }
            if (viewForCan != null) {

                viewForCan.setBackgroundColor(viewGroup.getResources().getColor(R.color.red));
            }
            activityReportReceiptBinding.btnSavePdf.setVisibility(View.INVISIBLE);
            boolean_save = true;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();
    }*/

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(ReportReceiptActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {
            } else {
                ActivityCompat.requestPermissions(ReportReceiptActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);

            }

            if ((ActivityCompat.shouldShowRequestPermissionRationale(ReportReceiptActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
            } else {
                ActivityCompat.requestPermissions(ReportReceiptActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);

            }
        } else {
            boolean_permission = true;


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
