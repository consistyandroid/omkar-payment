package in.omkarpayment.app.report.sendermoneyTransferReport;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.SenderMoneyTransferAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivitySenderMoneyTransferReportBinding;
import in.omkarpayment.app.json.SenderMoneyTransferPojoModel;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;


public class SenderMoneyTransferReport extends AppCompatActivity implements ISenderMoneyTransferView {

    ActivitySenderMoneyTransferReportBinding activitySenderMoneyTransferReportBinding;
    ISenderMoneyTransferPresenter iSenderMoneyTransferPresenter;
    CustomProgressDialog progressDialog;
    Context context = this;
    SenderMoneyTransferAdapter adapter;
    AlertImpl alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySenderMoneyTransferReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_sender_money_transfer_report);
        iSenderMoneyTransferPresenter = new SenderMoneyTransferPresenterImpl(this);

        progressDialog = new CustomProgressDialog(this);
        alert = new AlertImpl(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Search Sender Transactions";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        activitySenderMoneyTransferReportBinding.listView.setLayoutManager(mLayoutManager);
        activitySenderMoneyTransferReportBinding.listView.setItemAnimator(new DefaultItemAnimator());
        activitySenderMoneyTransferReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchSenderNumber();
            }
        });


    }

    private void searchSenderNumber() {
        iSenderMoneyTransferPresenter.searchTransactions();
    }

    @Override
    public String getNumber() {
        return activitySenderMoneyTransferReportBinding.editSercheNumber.getText().toString();
    }

    @Override
    public void editNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Number"));
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void getReport(ArrayList<SenderMoneyTransferPojoModel> Report) {
        adapter = new SenderMoneyTransferAdapter(context, Report);
        activitySenderMoneyTransferReportBinding.listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
