package in.omkarpayment.app.report.commissionStructure;

/**
 * Created by consisty on 23/11/17.
 */

public interface ICommissionSPresenter {
    void getCommissionStructure();

    void decryptCommissionStructureResponse(String encryptedResponse);

    void decryptCommissionStructureFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void errorAlert(String somethingWrong);
}
