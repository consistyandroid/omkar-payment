package in.omkarpayment.app.report.balanceReport;

import in.omkarpayment.app.json.BalanceTransferReportPojo;

import java.util.ArrayList;

/**
 * Created by Krish on 14-Aug-17.
 */

public interface IBalanceReportView {


    void errorAlert(String msg);

    void successAlert(String msg);


    void showPDialog();

    void dismissPDialog();

    String reportType();

    String fromDate();

    String toDate();

    void txtFromDateError();

    void txtToDateError();

    void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse);

}
