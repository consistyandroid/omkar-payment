package in.omkarpayment.app.report.balanceReport;

/**
 * Created by Krish on 14-Aug-17.
 */

public interface IBalanceReportPresenter {

    void errorAlert(String msg);

    void successAlert(String msg);


    void showPDialog();

    void dismissPDialog();

    void validate();

    void BalanceReportResponseFailResponse(String encryptedResponse);

    void BalanceReportResponseResponse(String encryptedResponse);

    void transferReport();

    void receiveReport();

    void walletTransferReport();
}
