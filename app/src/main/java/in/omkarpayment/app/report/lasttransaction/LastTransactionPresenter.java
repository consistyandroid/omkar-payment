package in.omkarpayment.app.report.lasttransaction;

import android.util.Log;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.LastFiveRechargePojo;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.recharge.IRechargeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by Krish on 10-Aug-17.
 */

public class LastTransactionPresenter implements ILastTransactionPresenter {

    ILastTransactionView iLastTransactionView;

    public LastTransactionPresenter(ILastTransactionView iLastTransactionView) {
        this.iLastTransactionView = iLastTransactionView;
    }

    @Override
    public void getLastTransaction() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateLastTransactionResponse = this;
        model.webserviceMethod("", keyData, "LastTransaction");
        showDialog();
    }

    @Override
    public void showDialog() {
       // iLastTransactionView.showPDialog();
    }

    @Override
    public void dismissDialog() {
       // iLastTransactionView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iLastTransactionView.errorAlert(msg);
    }

    @Override
    public void getLastTransaction(ArrayList<LastFiveRechargePojo> latTransaction) {
        iLastTransactionView.getLastTransaction(latTransaction);

    }

    @Override
    public void LastTransactionResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Last 10 Report",body);
            ArrayList<LastFiveRechargePojo> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<LastFiveRechargePojo>>() {
            }.getType());
            if (lastrasaction != null) {
                iLastTransactionView.getLastTransaction(lastrasaction);

            }
        } catch (Exception e) {

        }

    }

    @Override
    public void LastTransactionFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
           // errorAlert(body);

        } catch (Exception e) {

        }

    }
}
