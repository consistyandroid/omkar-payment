package in.omkarpayment.app.report.searchMobile;





import java.util.ArrayList;

import in.omkarpayment.app.json.DayReportPojo;
import in.omkarpayment.app.json.LastFiveRechargePojo;

/**
 * Created by Krish on 25-Jul-17.
 */

public interface ISearchNumberView {


    String getNumber();

    void editNumberError();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void getReport(ArrayList<SearchNumberPojo> Report);
}
