package in.omkarpayment.app.report.MoneyTransferReport.moneyTransferReceipt;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.allEDMT.aMoneyTransfer.aMoneyTransferModel.AMoneyTransferModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.userContent.Validation;

public class MoneyTransferReportPresenter implements aMoneyTransferReceiptPresenter {

    aMoneyTransferReceiptView aMoneyTransferReceiptView;

    public MoneyTransferReportPresenter(aMoneyTransferReceiptView aMoneyTransferReceiptView) {
        this.aMoneyTransferReceiptView = aMoneyTransferReceiptView;
    }

    @Override
    public void getReceipt() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject receiptRequest = new JSONObject();

        try {

            receiptRequest.put("BillNumber", aMoneyTransferReceiptView.getBillNumber());

            String jsonRequest = receiptRequest.toString();
            Log.i("receipt request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aMoneyTransferReceiptView.unableToFetchAlert("data.");
                return;
            }

           /* AMoneyTransferModel model = new AMoneyTransferModel();
            model.newMoneyTransferReportPresenter = this;
            model.getReceiptWebservice(encryptString, keyData);*/

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void validateBillNumber() {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(aMoneyTransferReceiptView.getBillNumber().trim())) {
            aMoneyTransferReceiptView.unableToFetchAlert("bill number.");
            return;
        }
        aMoneyTransferReceiptView.getReceipt();
    }

    @Override
    public void errorAlert(String status) {
        aMoneyTransferReceiptView.errorAlert(status);
    }

    @Override
    public void getReceiptFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            Log.i("MoneyTransferReceiptF", body);

            Validation validation = new Validation();
            if (!validation.isNullOrEmpty(body)) {
                aMoneyTransferReceiptView.unableToFetchAlert("data.");
                return;
            }

            aMoneyTransferReceiptView.getReceiptFailAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getReceiptResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            Log.i("getMoneyTransferReport", body);

            if (body.equals("[]") || body.isEmpty()) {
                aMoneyTransferReceiptView.getReceiptFailAlert(body);
                return;
            }
            Gson gson = new Gson();
            ArrayList<MoneyTransferReceiptPojo> moneyTransferReceiptPojo = gson.fromJson(body, new TypeToken<ArrayList<MoneyTransferReceiptPojo>>() {
            }.getType());

            if (moneyTransferReceiptPojo == null ||
                    moneyTransferReceiptPojo.isEmpty() ||
                    moneyTransferReceiptPojo.equals("")) {
                aMoneyTransferReceiptView.unableToFetchAlert("data.");
            }

            aMoneyTransferReceiptView.getReceiptSuccess(moneyTransferReceiptPojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
