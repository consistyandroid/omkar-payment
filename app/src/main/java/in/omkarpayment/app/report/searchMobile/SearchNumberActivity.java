package in.omkarpayment.app.report.searchMobile;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.SearchNumberAdapte;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivitySearchNumberBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

public class SearchNumberActivity extends AppCompatActivity implements ISearchNumberView {

    ActivitySearchNumberBinding searchNumberBinding;
    CustomProgressDialog progressDialog;
    AlertImpl alert;
    Context context = this;
    NetworkState ns = new NetworkState();
    SearchNumberPresenter searchNumberPresenter;
    SearchNumberAdapte adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchNumberBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_number);
        searchNumberPresenter = new SearchNumberPresenter(this);
        progressDialog = new CustomProgressDialog(this);
        alert = new AlertImpl(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Search Number";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        searchNumberBinding.listView.setLayoutManager(mLayoutManager);
        searchNumberBinding.listView.setItemAnimator(new DefaultItemAnimator());
        searchNumberBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchNumberBinding.editSercheNumber.getText().toString().length() < 4) {

                    editNumberError();
                } else {
                    if (ns.isInternetAvailable(context)) {
                        keyboard();
                        searchNumber();
                    } else {
                        Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }
    public void keyboard(){
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public void searchNumber() {
        searchNumberBinding.listView.setAdapter(null);
        searchNumberPresenter.validateNumber();
    }

    @Override
    public String getNumber() {
        return searchNumberBinding.editSercheNumber.getText().toString();
    }

    @Override
    public void editNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "valid number"));
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void getReport(ArrayList<SearchNumberPojo> Report) {
        adapter = new SearchNumberAdapte(context, Report);
        searchNumberBinding.listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
            /*Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);*/
        }
        return super.onOptionsItemSelected(item);
    }
}
