package in.omkarpayment.app.report.sendermoneyTransferReport;

import java.util.ArrayList;

import in.omkarpayment.app.json.SenderMoneyTransferPojoModel;

/**
 * Created by consisty on 7/3/18.
 */

public interface ISenderMoneyTransferView {
    String getNumber();

    void editNumberError();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void getReport(ArrayList<SenderMoneyTransferPojoModel> Report);
}
