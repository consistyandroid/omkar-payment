package in.omkarpayment.app.report.MoneyTransferReport;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by consisty on 14/10/17.
 */

public class MoneyTransferReportPresenterImpl implements IMoneyTransferReportPresenter {

    IMoneyTransferReportView iMoneyTransferReportView;
    LogWriter log = new LogWriter();

    public MoneyTransferReportPresenterImpl(IMoneyTransferReportView iMoneyTransferReportView) {
        this.iMoneyTransferReportView = iMoneyTransferReportView;
    }

    @Override
    public void validateDate() {
        if (iMoneyTransferReportView.strFromDate().equals("") || iMoneyTransferReportView.strFromDate().isEmpty()) {
            iMoneyTransferReportView.editFromDateError();
        } else if (iMoneyTransferReportView.strToDate().equals("") || iMoneyTransferReportView.strToDate().isEmpty()) {
            iMoneyTransferReportView.editToDateError();
        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject dayreportrequest = new JSONObject();
            try {

                dayreportrequest.put("Date", iMoneyTransferReportView.strFromDate());
                dayreportrequest.put("ToDate", iMoneyTransferReportView.strToDate());
                String jsonrequest = dayreportrequest.toString();
                log.i("Report Request", jsonrequest);
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                if (!encryptString.equals("")) {
                    WebServiceModel model = new WebServiceModel();
                    model.delegateMoneyTransferReport = this;
                    model.webserviceMethod(encryptString, keyData, "MoneyTransferReport");
                    showDialog();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void decryptMoneyTransferReportListResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i(" MoneyRPS body", body);
            ArrayList<MoneyTransferReportPojo> lastrasaction = gson.fromJson(body, new TypeToken<ArrayList<MoneyTransferReportPojo>>() {
            }.getType());
            if (lastrasaction != null) {
                iMoneyTransferReportView.getDayWiseReport(lastrasaction);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptMoneyTransferReportListFailureResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i(" MOneyRPF body", body);
            errorAlert(body);
        } catch (Exception e) {
            errorAlert(body);
        }
    }

    @Override
    public void dismissPDialog() {
        iMoneyTransferReportView.dismissPDialog();
    }

    @Override
    public void errorAlert(String somethingWrong) {
        iMoneyTransferReportView.errorAlert(somethingWrong);
    }

    private void showDialog() {
        iMoneyTransferReportView.showPDialog();
    }

}
