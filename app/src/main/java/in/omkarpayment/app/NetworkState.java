package in.omkarpayment.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Vinod on 28/3/17.
 */

public class NetworkState {

    public boolean isInternetAvailable(Context context) {
       /* boolean result = false;
        ConnectivityManager check = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (check != null) {
            NetworkInfo.State mobile = check.getNetworkInfo(0).getState();
            //wifi
            NetworkInfo.State wifi = check.getNetworkInfo(1).getState();
            if (mobile == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTED) {
                result = true;
            } else {
                return false;
            }
        }

        return result;*/


        if (context != null) {
            ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            //mobile
            NetworkInfo.State mobile = conMan.getNetworkInfo(0).getState();
            //wifi
            NetworkInfo.State wifi = conMan.getNetworkInfo(1).getState();

            if (mobile == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTED) {

                return true;

            } else {

                return false;
            }

        }
        return false;

    }
}
