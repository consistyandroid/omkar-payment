package in.omkarpayment.app.changePassword;

import android.util.Log;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.UserDetails;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krish on 10-Jul-17.
 */

public class ChangePasswordPrasenter implements IChangePasswordPresenter {
    IChangePasswordView iChangePasswordView;
    LogWriter log = new LogWriter();

    public ChangePasswordPrasenter(IChangePasswordView iChangePasswordView) {
        this.iChangePasswordView = iChangePasswordView;
    }

    @Override
    public void validateField() {
        if (iChangePasswordView.getOldPassword().isEmpty()) {
            iChangePasswordView.editOldPasswordError();
        } else if (iChangePasswordView.getNewPassword().isEmpty()) {
            iChangePasswordView.editNewPasswordError();
        } else if (iChangePasswordView.getConfirmPassword().isEmpty()) {
            iChangePasswordView.editConfirmPasswordError();
        } else if (!iChangePasswordView.getConfirmPassword().equals(iChangePasswordView.getNewPassword())) {
            iChangePasswordView.errorAlert("New Password And Confirm Password Should be Same");
        } else {
            iChangePasswordView.changePassword();
        }
    }

    @Override
    public void changePassword() {

        String encryptString = "";
        String keyData = new KeyDataReader().get();

        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("OldPassword", iChangePasswordView.getOldPassword());
            loginrequest.put("Password", iChangePasswordView.getNewPassword());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateChangePassResponse = this;
                model.webserviceMethod(encryptString, keyData, "ChangePassword");
                showPDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @Override
    public void showPDialog() {
        iChangePasswordView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iChangePasswordView.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        iChangePasswordView.errorAlert(errorMsg);

    }

    @Override
    public void successAlert(String msg) {
        iChangePasswordView.successAlert(msg);

    }

    @Override
    public void moveToLogin() {
        iChangePasswordView.moveToLogin();
    }

    @Override
    public void ChangePasswordResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("body", body);
            iChangePasswordView.successAlert(body);
        } catch (Exception e) {
            iChangePasswordView.errorAlert(body);
        }

    }

    @Override
    public void ChangePasswordFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            log.i("ChangePasswordFailureResponse", body);
            iChangePasswordView.errorAlert(body);
        } catch (Exception e) {
            iChangePasswordView.errorAlert(body);
        }

    }

    @Override
    public void changePin() {

        String encryptString = "";
        String keyData = new KeyDataReader().get();

        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("OldPassword", iChangePasswordView.getOldPassword());
            loginrequest.put("Password", iChangePasswordView.getNewPassword());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);

            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateChangePassResponse = this;
                model.webserviceMethod(encryptString, keyData, "ChangePin");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void resetPin(String Mobileno) {
        String encryptString = "";
        String keyData = new KeyDataReader().get();

        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("MobileNumber", Mobileno);
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
