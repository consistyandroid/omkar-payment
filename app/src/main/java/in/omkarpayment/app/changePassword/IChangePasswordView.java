package in.omkarpayment.app.changePassword;

/**
 * Created by Krish on 10-Jul-17.
 */

public interface IChangePasswordView {

    void editOldPasswordError();

    String getOldPassword();

    String getNewPassword();

    String getConfirmPassword();

    void showPDialog();

    void validate();

    void dismissPDialog();

    void errorAlert(String errorMsg);

    void successAlert(String msg);

    void moveToLogin();

    void editNewPasswordError();

    void editConfirmPasswordError();

    void changePassword();

    void editConfirmPasswordSameError();

    String initialCode();

}
