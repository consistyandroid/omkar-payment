package in.omkarpayment.app.changePassword;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Validator;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityChangePasswordBinding;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

public class ChangePasswordActivity extends AppCompatActivity implements IChangePasswordView {
    ActivityChangePasswordBinding changePasswordBinding;
    CustomProgressDialog progressDialog;
    Validator validator;
    IChangePasswordPresenter changePasswordPrasenter;
    AlertImpl alert;
    EditText editNewPassword, editOldPassword, editConfirmPassword;
    Context context = this;
    NetworkState ns = new NetworkState();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        changePasswordBinding = DataBindingUtil.setContentView(ChangePasswordActivity.this, R.layout.activity_change_password);

        progressDialog = new CustomProgressDialog(this);

        changePasswordPrasenter = new ChangePasswordPrasenter(this);

        alert = new AlertImpl(this);


        editNewPassword = (EditText) findViewById(R.id.editNewPassword);
        editOldPassword = (EditText) findViewById(R.id.editOldPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
        );
        changePasswordBinding.show.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        changePasswordBinding.editOldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        changePasswordBinding.editOldPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });
        changePasswordBinding.shownew.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        changePasswordBinding.editNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        changePasswordBinding.editNewPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });
        changePasswordBinding.showconfirm.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        changePasswordBinding.editConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        changePasswordBinding.editConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            String title = "Change Password";
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            SpannableString set = new SpannableString(title);
            set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            getSupportActionBar().setTitle(set);
            getSupportActionBar().setIcon(R.mipmap.ic_launcher);
            final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);


        }
        changePasswordBinding.btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(context)) {
                    changePasswordPrasenter.validateField();
                } else {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
            );
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onValidationSucceeded() {
        changePasswordPrasenter.changePassword();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }*/

    @Override
    public void editOldPasswordError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "old password"));
        changePasswordBinding.editOldPassword.requestFocus();

    }

    @Override
    public String getOldPassword() {
        return changePasswordBinding.editOldPassword.getText().toString();
    }

    @Override
    public String getNewPassword() {
        return editNewPassword.getText().toString().trim();
    }

    @Override
    public String getConfirmPassword() {
        return editConfirmPassword.getText().toString();
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void validate() {
        validator.validate();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        alert.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                changePasswordPrasenter.moveToLogin();
            }
        });
        dialog.show();
        /*alert.successAlert(msg);
        changePasswordPrasenter.moveToLogin();*/
    }

    @Override
    public void moveToLogin() {
        Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void editNewPasswordError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "New password"));

    }

    @Override
    public void editConfirmPasswordError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Confirm password"));

    }

    @Override
    public void changePassword() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText("Please confirm you want\nto change password...");

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordPrasenter.changePassword();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void editConfirmPasswordSameError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Confirm password as same as new password"));

    }



    @Override
    public String initialCode() {
        return "1";
    }


}
