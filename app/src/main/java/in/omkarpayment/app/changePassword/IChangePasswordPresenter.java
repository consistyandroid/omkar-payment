package in.omkarpayment.app.changePassword;

/**
 * Created by Krish on 10-Jul-17.
 */

public interface IChangePasswordPresenter {

    void validateField();

    void changePassword();

    void showPDialog();


    void dismissPDialog();

    void errorAlert(String errorMsg);

    void successAlert(String msg);

    void moveToLogin();

    void ChangePasswordResponse(String encryptedResponse);

    void ChangePasswordFailureResponse(String encryptedResponse);

    void changePin();

    void resetPin(String Mobileno);

}
