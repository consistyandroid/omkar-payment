package in.omkarpayment.app.changePassword;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableString;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Validator;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityChangePasswordBinding;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

/**
 * Created by consisty on 23/2/18.
 */

public class ChangeInitialPassword extends AppCompatActivity implements IChangePasswordView {
    ActivityChangePasswordBinding changePasswordBinding;
    CustomProgressDialog progressDialog;
    Validator validator;
    IChangePasswordPresenter changePasswordPrasenter;
    AlertImpl alert;
    Context context = this;
    boolean doubleBackToExitPressedOnce = false;
    EditText editNewPassword, editOldPassword, editConfirmPassword;
    NetworkState nu = new NetworkState();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        changePasswordBinding = DataBindingUtil.setContentView(ChangeInitialPassword.this, R.layout.activity_change_password);

        progressDialog = new CustomProgressDialog(this);

        changePasswordPrasenter = new ChangePasswordPrasenter(this);

        alert = new AlertImpl(this);


        editNewPassword = (EditText) findViewById(R.id.editNewPassword);
        editOldPassword = (EditText) findViewById(R.id.editOldPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);

        changePasswordBinding.show.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        changePasswordBinding.editOldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        changePasswordBinding.editOldPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });
        changePasswordBinding.shownew.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        changePasswordBinding.editNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        changePasswordBinding.editNewPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });
        changePasswordBinding.showconfirm.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_UP:
                        changePasswordBinding.editConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        changePasswordBinding.editConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            String title = "Change Password";
            getSupportActionBar().setTitle(title);
            SpannableString set = new SpannableString(title);
            getSupportActionBar().setTitle(set);
        }
        changePasswordBinding.btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nu.isInternetAvailable(context)) {
                    changePasswordPrasenter.validateField();
                } else {
                    alert.errorAlert(String.format(AllMessages.internetError));
                }

            }
        });

    }

    @Override
    public void editOldPasswordError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "OldPassword"));
       /* changePasswordBinding.editOldPassword.setError(String.format(AllMessages.pleaseEnter, "OldPassword"));
        changePasswordBinding.editOldPassword.requestFocus();*/
    }

    @Override
    public String getOldPassword() {
        return changePasswordBinding.editOldPassword.getText().toString();
    }

    @Override
    public String getNewPassword() {
        return editNewPassword.getText().toString().trim();
    }

    @Override
    public String getConfirmPassword() {
        return editConfirmPassword.getText().toString();
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void validate() {
        validator.validate();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        alert.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                changePasswordPrasenter.moveToLogin();
            }
        });
        dialog.show();
        /*alert.successAlert(msg);
        changePasswordPrasenter.moveToLogin();*/
    }

    @Override
    public void moveToLogin() {
        Intent intent = new Intent(ChangeInitialPassword.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void editNewPasswordError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "New password"));
    }

    @Override
    public void editConfirmPasswordError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Confirm password"));
    }

    @Override
    public void changePassword() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText("Please confirm you want\nto change password...");

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordPrasenter.changePassword();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /*
        @Override
        public void changePassword() {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Message")
                    .setContentText("Please confirm you want\nto change password...")
                    .setCustomImage(R.mipmap.ic_launcher)
                    .showCancelButton(true)
                    .setCancelText("Cancel")
                    .setConfirmText("Confirm")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            changePasswordPrasenter.changePassword();
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                        }
                    })
                    .show();
        }
    */
    @Override
    public void editConfirmPasswordSameError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Confirm password as same as new password"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public String initialCode() {
        return "1";
    }


}
