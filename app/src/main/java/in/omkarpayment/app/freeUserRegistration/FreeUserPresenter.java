package in.omkarpayment.app.freeUserRegistration;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Krish on 04-Aug-17.
 */

public class FreeUserPresenter implements IFreUserPresenter {

    IFreeUserView iFreeUserView;
    LogWriter log = new LogWriter();

    public FreeUserPresenter(IFreeUserView iFreeUserView) {
        this.iFreeUserView = iFreeUserView;
    }


    @Override
    public void showProgressDialog() {
        iFreeUserView.showProgressDialog();
    }

    @Override
    public void dismissPDialog() {
        iFreeUserView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iFreeUserView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iFreeUserView.successAlert(msg);
    }

    @Override
    public void validateRegistration() {
        if (validation()) {
            iFreeUserView.confirmRegistration();
        }
    }

    @Override
    public void registration(String Name, String EmailId, String Dob, String MobileNo, String Password, String Gender, String Pin) {
        String encryptString = "";
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("Name", Name);
            loginrequest.put("EmailId", EmailId);
            loginrequest.put("DateOfBirth", Dob);
            loginrequest.put("MobileNo", MobileNo);
            loginrequest.put("Password", Password);
            loginrequest.put("Gender", Gender);
            loginrequest.put("RechargePin", Pin);
            String jsonrequest = loginrequest.toString();
            log.i("Activation request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);

            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateFreeUserResponse = this;
                model.webserviceMethod(encryptString, "", "FreeUserRegistration");
                //   showProgressDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

/*
    @Override
    public boolean panValidation() {
        str_Pan_Number = iFreeUserView.e.getText().toString().trim();

        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");

        Matcher matcher = pattern.matcher(str_Pan_Number);

        if (matcher.matches()) {

        } else {
            edtpannumber.setError(String.format(msg.pleaseEnter, " Valid Pan number"));
            edtpannumber.requestFocus();
            return false;
        }
        return true;
    }
*/

    @Override
    public boolean validation() {
        if (iFreeUserView.FirstName().isEmpty()) {
            iFreeUserView.editFirstNameError();
            return false;
        }
        if (iFreeUserView.LastName().isEmpty()) {
            iFreeUserView.editLastNameError();
            return false;
        }
        if (iFreeUserView.radioButton() == false) {
            iFreeUserView.radioButtonError();
            return false;
        }
        if (iFreeUserView.MobileNumber().isEmpty()) {
            iFreeUserView.editMobileNumberError();
            return false;
        }
        if (iFreeUserView.EmailAddress().isEmpty()) {
            iFreeUserView.editEmailAddressError();
            return false;
        }
        if (!isValidEmail(iFreeUserView.EmailAddress())) {
            iFreeUserView.editEmailAddressError();
            return false;
        }
        if (iFreeUserView.getSendDate().equals("")) {
            iFreeUserView.editDateError();
            return false;
        }
       /* if (iFreeUserView.mPin().isEmpty()) {
            iFreeUserView.editmPinError();
            return false;
        }*/
        if (iFreeUserView.Password().isEmpty()) {
            iFreeUserView.editPasswordError();
            return false;
        }
        if (iFreeUserView.CheckBox() == false) {
            iFreeUserView.checkBoxError();
            return false;
        }
        return true;
    }

    @Override
    public void decryptFreeUserResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        log.i("bodyText : ", encryptedResponse);
        try {
            body = data.Decrypt(encryptedResponse);
            log.i("Decrypted body", body);
            iFreeUserView.successAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
