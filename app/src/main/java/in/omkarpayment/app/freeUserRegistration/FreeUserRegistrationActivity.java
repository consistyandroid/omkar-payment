package in.omkarpayment.app.freeUserRegistration;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Toast;

import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityFreeUserRagistrationBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;



public class FreeUserRegistrationActivity extends AppCompatActivity implements IFreeUserView {

    ActivityFreeUserRagistrationBinding registrationBinding;

    AlertImpl alert;
    CustomProgressDialog dialog;
    boolean checkRadioBtn = false, checkBox = false;
    String Gender, SendDate = "";
    Context context = this;
    IFreUserPresenter iFreUserPresenter;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registrationBinding = DataBindingUtil.setContentView(this, R.layout.activity_free_user_ragistration);
        iFreUserPresenter = new FreeUserPresenter(this);
        alert = new AlertImpl(this);
        dialog = new CustomProgressDialog(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            String title = "Free User";
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            SpannableString set = new SpannableString(title);
            set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            getSupportActionBar().setTitle(set);
        }

        registrationBinding.rgGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioBtnMale:
                        Gender = "Male";
                        checkRadioBtn = true;
                        break;
                    case R.id.radioBtnFemale:
                        Gender = "Female";
                        checkRadioBtn = true;
                        break;
                }
            }
        });
        registrationBinding.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBox = true;
                } else {
                    checkBox = false;
                }
            }
        });

        registrationBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iFreUserPresenter.validateRegistration();
            }
        });

        registrationBinding.txtTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(AllMessages.termsConsitionURL); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                if (isValidAge(year, monthOfYear, dayOfMonth)) {
                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    registrationBinding.txtDoB.setText(sdf.format(userAge.getTime()));
                    // log.i("Age in ", sdf.format(userAge.getTime()));
                    SendDate = sdf.format(userAge.getTime());
                    registrationBinding.txtDoB.setError(null);
                } else {
                    Toast.makeText(getApplicationContext(), "Your age should be more than 18+ year(s)", Toast.LENGTH_SHORT).show();
                    registrationBinding.txtDoB.setText("");

                }
            }
        };

        registrationBinding.txtDoB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(FreeUserRegistrationActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    @Override
    public void showProgressDialog() {
        dialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        dialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public String FirstName() {
        return registrationBinding.editFirstName.getText().toString();
    }

    @Override
    public String LastName() {
        return registrationBinding.editLastName.getText().toString();
    }

    @Override
    public String EmailAddress() {
        return registrationBinding.editEmail.getText().toString();
    }

    @Override
    public String MobileNumber() {
        return registrationBinding.editMobileNo.getText().toString();
    }

    @Override
    public String mPin() {
        return registrationBinding.editPin.getText().toString();
    }

    @Override
    public String Password() {
        return registrationBinding.editPassword.getText().toString();
    }

    @Override
    public String getSendDate() {
        return SendDate;
    }

    @Override
    public String Gender() {
        return Gender;
    }

    @Override
    public void editFirstNameError() {
        registrationBinding.editFirstName.setError(String.format(AllMessages.pleaseEnter, "First Name"));
        registrationBinding.editFirstName.requestFocus();
    }

    @Override
    public void editLastNameError() {
        registrationBinding.editLastName.setError(String.format(AllMessages.pleaseEnter, "Last Name"));
        registrationBinding.editLastName.requestFocus();
    }

    @Override
    public void editEmailAddressError() {
        registrationBinding.editEmail.setError(String.format(AllMessages.pleaseEnter, "Valid Email Id"));
        registrationBinding.editEmail.requestFocus();
    }

    @Override
    public void editMobileNumberError() {
        registrationBinding.editMobileNo.setError(String.format(AllMessages.pleaseEnter, "Mobile Number"));
        registrationBinding.editMobileNo.requestFocus();
    }

    @Override
    public void editmPinError() {
        registrationBinding.editPin.setError(String.format(AllMessages.pleaseEnter, "Pin"));
        registrationBinding.editPin.requestFocus();
    }

    @Override
    public void editPasswordError() {
        registrationBinding.editPassword.setError(String.format(AllMessages.pleaseEnter, "Password"));
        registrationBinding.editPassword.requestFocus();
    }

    @Override
    public void editDateError() {
        registrationBinding.txtDoB.setError(String.format(AllMessages.pleaseSelect, "Date"));
        registrationBinding.txtDoB.requestFocus();
    }

    @Override
    public void checkBoxError() {
        Toast.makeText(context, String.format(AllMessages.pleaseSelect, "Terms & Condition"), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void radioButtonError() {
        Toast.makeText(context, String.format(AllMessages.pleaseSelect, "Gender"), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean CheckBox() {
        return checkBox;
    }


    @Override
    public boolean radioButton() {
        return checkRadioBtn;
    }

    @Override
    public void confirmRegistration() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValidAge(int year, int monthOfYear, int dayOfMonth) {
        Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        //log.i("Age", userAge.toString());
        if (minAdultAge.before(userAge)) {
            return false;
        } else {
            return true;
        }
    }

    private Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private int getDiffYears(Date first) {
        Calendar userDate = toCalendar(first);

        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, 0);

        Calendar currentDate = minAdultAge;
        int diff = currentDate.get(Calendar.YEAR) - userDate.get(Calendar.YEAR);
        if (userDate.get(Calendar.MONTH) > currentDate.get(Calendar.MONTH) ||
                (userDate.get(Calendar.MONTH) == currentDate.get(Calendar.MONTH) && userDate.get(Calendar.DATE) > currentDate.get(Calendar.DATE))) {
            diff--;
        }
        // log.i("Diff", diff + "");
        return diff;
    }

    private boolean isValidAge(String dob) {
        try {
            //log.i("Age Typed ", dob);
            DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sourceFormat.parse(dob);
            int diff = getDiffYears(date);
            //  log.i("Diff", diff + "");
            if (diff < 18) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }
}
