package in.omkarpayment.app.freeUserRegistration;

/**
 * Created by Krish on 04-Aug-17.
 */

public interface IFreUserPresenter {


    void showProgressDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void validateRegistration();
    void registration(String Name, String EmailId,String Dob,String MobileNo,String Password,String Gender,String Pin);
   // boolean panValidation();
    boolean isValidEmail(String target);
    boolean validation();


    void decryptFreeUserResponse(String encryptedResponse);

}