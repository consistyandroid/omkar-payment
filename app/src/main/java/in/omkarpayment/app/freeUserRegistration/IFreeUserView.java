package in.omkarpayment.app.freeUserRegistration;

/**
 * Created by Krish on 04-Aug-17.
 */

public interface IFreeUserView {

    void showProgressDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    String FirstName();

    String LastName();

    String EmailAddress();

    String MobileNumber();

    String mPin();

    String Password();

    String getSendDate();

    String Gender();

    void editFirstNameError();

    void editLastNameError();

    void editEmailAddressError();

    void editMobileNumberError();

    void editmPinError();

    void editPasswordError();

    void editDateError();


    void checkBoxError();

    void radioButtonError();


    boolean CheckBox();

    boolean radioButton();

    void confirmRegistration();


}
