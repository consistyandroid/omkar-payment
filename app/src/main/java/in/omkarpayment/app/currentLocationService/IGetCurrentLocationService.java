package in.omkarpayment.app.currentLocationService;

public interface IGetCurrentLocationService {

    void GetDeviceCurrentLocation(LocationPojo currentLocationPojo);

    void showPDialog();

}