package in.omkarpayment.app.currentLocationService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;

public class LocationPojo {

    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("accuracy")
    @Expose
    private Float accuracy;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("dateInString")
    @Expose
    private String dateInString;

    @SerializedName("lastUpdated")
    @Expose
    private Calendar lastUpdated;

    public Double getLatitude() {
        return latitude == null ? 0.0 : latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude == null ? 0.0 : latitude;
    }

    public Double getLongitude() {
        return longitude == null ? 0.0 : longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude == null ? 0.0 : longitude;
    }

    public Float getAccuracy() {
        return accuracy == null ? 0 : accuracy;
    }

    public void setAccuracy(Float accuracy) {
        this.accuracy = accuracy == null ? 0 : accuracy;
    }

    public String getAddress() {
        return address == null ? "" : address;
    }

    public void setAddress(String address) {
        this.address = address == null ? "" : address;
    }

    public String getDateInString() {
        return dateInString == null ? "" : dateInString;
    }

    public void setDateInString(String dateInString) {
        this.dateInString = dateInString == null ? "" : dateInString;
    }

    public Calendar getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Calendar lastUpdated) {
        this.lastUpdated = lastUpdated == null ? Calendar.getInstance() : lastUpdated;
    }

}