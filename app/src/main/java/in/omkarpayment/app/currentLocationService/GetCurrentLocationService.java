package in.omkarpayment.app.currentLocationService;

import android.Manifest;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;


import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import in.omkarpayment.app.R;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.permission.GetPermission;
import in.omkarpayment.app.userContent.UserDetails;


public class GetCurrentLocationService extends Service implements LocationListener {

    private Context context;
    private IGetCurrentLocationService iGetCurrentLocationService;
    private GetPermission getPermission;
    private LogWriter log;

    private static final String TAG = GetCurrentLocationService.class.getSimpleName();

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for Network status
    boolean isNetworkEnabled = false;

    // flag for isLocationFoundOnce
    boolean isLocationFoundOnce = false;

    // the minimum distance to change updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 1 meters

    // the minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10; // 5 seconds

    // location manager
    private LocationManager locationManager;

    // fastest updates interval - 15 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_TIME_INTERVAL_IN_UPDATES = 1000 * 15; // 15 seconds

    public GetCurrentLocationService(Context context,
                                     IGetCurrentLocationService iGetCurrentLocationService) {
        this.context = context;
        this.iGetCurrentLocationService = iGetCurrentLocationService;
        getPermission = new GetPermission(context);
        log = new LogWriter();
    }

    public boolean isLocationEnabled() {

        // check GPS location is Enabled or not
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(),
                        Settings.Secure.LOCATION_MODE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //setLocationMode(locationMode);
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        }

        locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        return !TextUtils.isEmpty(locationProviders);
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void getLocation() {
        isLocationFoundOnce = false;

        if (!getPermission.isLocationPermissionGranted()) {
            getPermission.RequestLocationPermission();
            return;
        }

        if (!isLocationEnabled()) {
            forceEnableLocation();
            return;
        }

        if (iGetCurrentLocationService != null) {
            iGetCurrentLocationService.showPDialog();
        }

        // Store LocationManager.GPS_PROVIDER or LocationManager.NETWORK_PROVIDER information
        String provider_info;
        try {
            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

            //getting GPS status
            if (hasGPSDevice(context)) {
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            }

            //getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSEnabled) {
                log.i(TAG, "Using GPS Service");

                provider_info = LocationManager.GPS_PROVIDER;
            } else if (isNetworkEnabled) {
                log.i(TAG, "Using NETWORK Based Service");

                provider_info = LocationManager.NETWORK_PROVIDER;
            } else {
                log.i(TAG, "Using NO Service");

                provider_info = "";
            }

            if (provider_info.isEmpty()) {
                iGetCurrentLocationService.GetDeviceCurrentLocation(UserDetails.currentLocationPojo);
                return;
            }

            if (ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context,
                            Manifest.permission.ACCESS_FINE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(provider_info, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (locationManager != null) {
                            log.i(TAG, "Using NETWORK Based Service");
                            if (ActivityCompat.checkSelfPermission(context,
                                    Manifest.permission.ACCESS_COARSE_LOCATION) ==
                                    PackageManager.PERMISSION_GRANTED ||
                                    ActivityCompat.checkSelfPermission(context,
                                            Manifest.permission.ACCESS_FINE_LOCATION) ==
                                            PackageManager.PERMISSION_GRANTED) {
                                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (location != null) {
                                    onLocationChanged(location);
                                }
                            }
                        }
                    }
                }, 10000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void forceEnableLocation() {

        final Dialog dialog = new Dialog(context, R.style.transparentBackground);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText("In order to proceed securely, " + context.getResources().getString(R.string.app_name) +
                " need to enable your 'Location' service. Do you want to enable?");

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setText("Enable");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });

        Button btnNo = dialog.findViewById(R.id.btnNo);
        btnNo.setText("Cancel");
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (isLocationFoundOnce) {
            return;
        }
        isLocationFoundOnce = true;
        // get accuracy of location
        Float accuracy = location.getAccuracy();

        LocationPojo locationPojo = new LocationPojo();

        locationPojo.setLatitude(location.getLatitude());
        locationPojo.setLongitude(location.getLongitude());
        locationPojo.setAccuracy(accuracy);

        log.i("Current Lat", String.valueOf(location.getLatitude()));
        log.i("Current Long", String.valueOf(location.getLongitude()));

        String currentAddress = "";
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(locationPojo.getLatitude(), locationPojo.getLongitude(), 1);

            if (addresses.get(0).getAddressLine(0) != null) {
                currentAddress += addresses.get(0).getAddressLine(0);
            }
            if (addresses.get(0).getAddressLine(1) != null) {
                currentAddress += ", " + addresses.get(0).getAddressLine(1);
            }
            if (addresses.get(0).getAddressLine(2) != null) {
                currentAddress += ", " + addresses.get(0).getAddressLine(2);
            }
            Log.i("current address", currentAddress);
            locationPojo.setAddress(currentAddress);
            locationPojo.setLastUpdated(Calendar.getInstance());

            UserDetails.currentLocationPojo = locationPojo;

            if (iGetCurrentLocationService != null) {
                stopGettingLocationUpdates();
                iGetCurrentLocationService.GetDeviceCurrentLocation(locationPojo);
            }
        } catch (Exception e) {
            e.printStackTrace();

            locationPojo.setAddress("");
            locationPojo.setLastUpdated(Calendar.getInstance());

            UserDetails.currentLocationPojo = locationPojo;

            if (iGetCurrentLocationService != null) {
                stopGettingLocationUpdates();
                iGetCurrentLocationService.GetDeviceCurrentLocation(locationPojo);
            }
        }
    }

    private void stopGettingLocationUpdates() {
        if (locationManager != null) {
            locationManager.removeUpdates(GetCurrentLocationService.this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        log.i("onProviderEnabled", provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        log.i("onProviderEnabled", provider);
    }

}