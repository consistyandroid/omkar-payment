package in.omkarpayment.app.login;

import java.util.ArrayList;

import in.omkarpayment.app.json.ForgetPasswordPojo;
import in.omkarpayment.app.json.LoginDetailsPojo;

/**
 * Created by Krish on 30-May-17.
 */

public interface ILoginView {

    String getUserName();

    String getPassword();

    void UserNameError();

    void PasswordError();

    void showProgressDialog();

    void showUpdateVersionDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void warningAlert();

    void saveCredentials();

    void commitClear();

    void checkButtonError();

    void loginSuccess(ArrayList<LoginDetailsPojo> LoginDetails);

    void forgotPass(ArrayList<ForgetPasswordPojo> loginResult);

    void showForgotPasswordDialog();

    String getForgotUserName();

    String getForgotMobileNo();

    String getUserType();

    void ForgotUserNameError();

    void ForgotMobileNameError();

    String MemberId();

    String MobileNo();

    void ForgotSuccess(String body);

    void ConfirmDialog();

    void openOtpScreen();

    void edtOTPError();

}
