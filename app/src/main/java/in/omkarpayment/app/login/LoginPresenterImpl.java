package in.omkarpayment.app.login;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.fragment.GetMoneyTransferUI.IMoneyTransferUIPresenter;
import in.omkarpayment.app.fragment.GetMoneyTransferUI.MoneyTransferUIPojo;
import in.omkarpayment.app.json.LoginDetailsPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.ILoginService;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by Krish on 31-May-17.
 */

public class LoginPresenterImpl implements ILoginPresenter, ILoginService,IMoneyTransferUIPresenter {

    ILoginView loginView;
    String username, password;
    LogWriter log = new LogWriter();
    Cryptography_Android data;

    public LoginPresenterImpl(ILoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void validateCredentials() {
        username = loginView.getUserName();
        password = loginView.getPassword();
        if (username.isEmpty()) {
            loginView.UserNameError();
        } else if (password.isEmpty()) {
            loginView.PasswordError();
        } else {
            sendToLogin(loginView.getUserName(), loginView.getPassword(), UserDetails.IMEI, loginView.getUserType());
        }
    }

    @Override
    public void SaveCredential() {
        loginView.saveCredentials();
    }

    @Override
    public void commitclearCredentials() {
        loginView.commitClear();
    }

    @Override
    public void validateForgotPassword() {
        if (loginView.getForgotMobileNo().isEmpty()) {
            loginView.ForgotMobileNameError();
        } else {
            loginView.ConfirmDialog();
        }
    }

    @Override
    public void showDialogForgotPass() {
        loginView.showForgotPasswordDialog();
    }

    @Override
    public void showDialog() {
        loginView.showProgressDialog();
    }

    @Override
    public void dismissDialog() {
        loginView.dismissPDialog();
    }

    @Override
    public void successAlert(String msg) {
        loginView.successAlert(msg);
    }

    @Override
    public void getLoginResponse(String response) {

    }

    @Override
    public void FailResponse(String response) {
        dismissDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(response);
            log.i("FailResponseDe", body);
            loginView.successAlert(body);
        } catch (Exception e) {
        }
    }

    @Override
    public void decryptMoneyTransferResponse(String encryptedResponseBody) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(encryptedResponseBody);
            log.i("getMoneyTx UI Resp.", body);

            Gson gson = new Gson();
            MoneyTransferUIPojo messageResponsePojo = gson.fromJson(body, new TypeToken<MoneyTransferUIPojo>() {
            }.getType());
            AMoneyTransferUserDetails.MoneyTxUI=messageResponsePojo.getRechargeProviderName();
            log.i("MoneyTxUIString",AMoneyTransferUserDetails.MoneyTxUI);
            // iMoneyTransferUIView.getSuccessResponse(messageResponsePojo.getRechargeProviderName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptMoneyTransferFailResponse(String encryptedResponseBody) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(encryptedResponseBody);
            log.i("verifyBeneAccountFailResponse", body);
            loginView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorAlert(String msg) {

        loginView.errorAlert(msg);
    }

    @Override
    public void forgotPasswordResponse(String response) {
        dismissDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(response);
            log.i("forgotPasswordResponseDe", body);
            successAlert(body);
        } catch (Exception e) {
        }
    }

    @Override
    public void submitOTP(String otp) {
        if (otp.isEmpty()) {
            loginView.edtOTPError();
        } else {
            String encryptString = "";
            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            int index = 0;
            try {
                loginrequest.put("UserName", loginView.getUserName());
                loginrequest.put("Password", loginView.getPassword());
                loginrequest.put("IpAddress", UserDetails.IMEI);
                loginrequest.put("OTP", otp);
                loginrequest.put("AndroidVersion", UserDetails.AndroidVersion);
                loginrequest.put("AndroidToken", UserDetails.AndroidToken);
                String jsonrequest = loginrequest.toString();
                log.i("jsonrequest", jsonrequest);
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                if (encryptString != null && !encryptString.equals("")) {
                    WebServiceModel model = new WebServiceModel();
                    model.inject = this;
                    model.webserviceMethod(encryptString, "", "LoginWithOTP");
                    showDialog();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }

    @Override
    public void loginSuccess(ArrayList<LoginDetailsPojo> loginResult) {
        loginView.loginSuccess(loginResult);
    }

    @Override
    public void updateDialog() {
        loginView.showUpdateVersionDialog();
    }

    @Override
    public boolean sendToLogin(String Username, String Password, String IpAddress, String UserType) {
        String encryptString = "";
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        int index = 0;
        try {
            loginrequest.put("UserName", Username);
            loginrequest.put("Password", Password);
            loginrequest.put("IpAddress", IpAddress);
            loginrequest.put("UserType", UserType);
            loginrequest.put("AndroidVersion", UserDetails.AndroidVersion);
            String jsonrequest = loginrequest.toString();
            log.i("jsonrequest", jsonrequest);

            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (encryptString != null && !encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.inject = this;
                model.webserviceMethod(encryptString, "", "Login");
                showDialog();
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void decryptString(String encryptedResponseBody) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponseBody);
            Log.i("body:", body.toString());
            if (body.equalsIgnoreCase("\"OTP send on registered mobile number\"")) {
                otpScreen();
            } else {
                Gson gson = new Gson();
                log.i("DecryptedS body", body);
                ArrayList<LoginDetailsPojo> loginResult = gson.fromJson(body, new TypeToken<ArrayList<LoginDetailsPojo>>() {
                }.getType());
                loginSuccess(loginResult);
            }

        } catch (Exception e) {
        }
    }
    @Override
    public void getMoneyTransferUI() {

        /*String keyData = new KeyDataReader().get();
        MoneyTransferUIModel model = new MoneyTransferUIModel();
        model.delegate = this;
        model.moneyTransferUI("", keyData);*/
    }

    private void otpScreen() {
        loginView.openOtpScreen();
    }

    @Override
    public void LoginFailureResponse(String encryptedResponse) {
        String body = "";
        data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);

            log.i("Login Failuer Response", body);

            if (body.contains("Please update android version")) {
                updateDialog();
            } else {
                loginView.errorAlert(body);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void forgotPassword() {
        String encryptString = "";
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("MobileNo", loginView.getForgotMobileNo());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (encryptString != null && !encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.inject = this;
                model.webserviceMethod(encryptString, "", "ForgotPassword");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ForgetFailuerResponse(String encryptedResponse) {
        log.i("Login FDetails", encryptedResponse);
        String body = "";
        loginView.errorAlert(encryptedResponse);
    }
}
