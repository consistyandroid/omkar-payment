package in.omkarpayment.app.login;

import java.util.ArrayList;

import in.omkarpayment.app.json.LoginDetailsPojo;

/**
 * Created by Krish on 31-May-17.
 */

public interface ILoginPresenter {


    void validateCredentials();

    void SaveCredential();

    void commitclearCredentials();

    void validateForgotPassword();

    void getMoneyTransferUI();

    void showDialogForgotPass();

    void showDialog();

    void dismissDialog();

    void successAlert(String msg);

    void errorAlert(String msg);

    void loginSuccess(ArrayList<LoginDetailsPojo> loginResult);

    void updateDialog();

    boolean sendToLogin(String Username, String Password, String IpAddress, String UserType);


    void decryptString(String encryptedResponseBody);

    void LoginFailureResponse(String encryptedResponse);

    void forgotPassword();

    void ForgetFailuerResponse(String encryptedResponse);

    void forgotPasswordResponse(String encryptedResponse);

    void submitOTP(String s);
}
