package in.omkarpayment.app.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.omkarpayment.app.Home.HomeActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityLoginBinding;
import in.omkarpayment.app.json.ForgetPasswordPojo;
import in.omkarpayment.app.json.LoginDetailsPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.ApplicationURL;
import in.omkarpayment.app.userContent.UserDetails;

public class LoginActivity extends AppCompatActivity implements ILoginView, CompoundButton.OnCheckedChangeListener {

    public static SharedPreferences loginPreference;
    public static SharedPreferences.Editor loginEditor;
    public Boolean saveLogin = false, logoutStatus;
    AlertImpl alert;
    ILoginPresenter loginPresenter;
    CustomProgressDialog progressDialog;
    ActivityLoginBinding activityLoginBinding;

    Context context = this;
    EditText editMemberId, editMobileNo, edtOTP;
    Dialog dialog, dialogOTP;
    String UserType = "Partner";
    String password;
    NetworkState ns = new NetworkState();
    Boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginPreference = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        saveLogin = loginPreference.getBoolean("saveLogin", false);
        logoutStatus = loginPreference.getBoolean("logoutStatus", false);
        loginEditor = loginPreference.edit();
        activityLoginBinding.showPassword.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        activityLoginBinding.editPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                    case MotionEvent.ACTION_DOWN:
                        activityLoginBinding.editPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                }
                return true;
            }
        });


        UserDetails.IMEI = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        alert = new AlertImpl(this);
        password = activityLoginBinding.editPassword.getText().toString();
        UserDetails.password = password;
        if (saveLogin == true) {
            activityLoginBinding.editUserName.setText(loginPreference.getString("username", ""));
            activityLoginBinding.editPassword.setText(loginPreference.getString("password", ""));
            loginEditor.putBoolean("logoutStatus", true);
            activityLoginBinding.checkBox.setChecked(true);
            password = activityLoginBinding.editPassword.getText().toString();
            UserDetails.password = password;
        }
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            UserDetails.AndroidVersion = pInfo.versionName;
            activityLoginBinding.txtVersion.setText(AllMessages.AppName + "Version :" + UserDetails.AndroidVersion);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loginPresenter = new LoginPresenterImpl(this);
        progressDialog = new CustomProgressDialog(this);
        //loginPresenter.getMoneyTransferUI();
        activityLoginBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activityLoginBinding.checkBox.isChecked()) {
                    saveCredentials();
                } else {
                    commitClear();
                }
                if (ns.isInternetAvailable(context)) {
                    loginPresenter.validateCredentials();
                } else {
                    alert.errorAlert(AllMessages.internetError);
                }

            }
        });
        activityLoginBinding.txtForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPasswordDialog();
            }
        });
        activityLoginBinding.txtVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationURL.PlayStoreURL));
                startActivity(myIntent);
            }
        });
  /*      activityLoginBinding.imgUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationURL.PlayStoreURL));
                startActivity(myIntent);
            }
        });*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        //  activityLoginBinding.editPassword.setText("");
    }

    @Override
    public String getUserName() {
        return activityLoginBinding.editUserName.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return activityLoginBinding.editPassword.getText().toString();
    }

    @Override
    public void UserNameError() {
        activityLoginBinding.editUserName.setError(String.format(AllMessages.pleaseEnter, "Username"));
        activityLoginBinding.editUserName.requestFocus();
    }

    @Override
    public void PasswordError() {
        activityLoginBinding.editPassword.setError(String.format(AllMessages.pleaseEnter, "Password"));
        activityLoginBinding.editPassword.requestFocus();
    }

    @Override
    public void showProgressDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void showUpdateVersionDialog() {
        dialog = new Dialog(context);
        //dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog);
        //dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        //dialog.setTitle("Message");

        Button btn_Ok = (Button) dialog.findViewById(R.id.btn_dialog);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.a);

        TextView txtversion = (TextView) dialog.findViewById(R.id.text_dialog);
        txtversion.setText("    Please Update latest version app   ");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);


        btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("inokbtn", ApplicationURL.PlayStoreURL);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationURL.PlayStoreURL));
                startActivity(intent);
            }
        });
        dialog.show();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        if (msg.equals("Invalid User") || msg.equals("FAILURE")) {
            alert.errorAlert(AllMessages.wrongUsernamePassword);
        } else {
            alert.errorAlert(msg);
        }
//        dialog.dismiss();
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void warningAlert() {

    }

    @Override
    public void saveCredentials() {
        loginEditor.putBoolean("saveLogin", true);
        loginEditor.putString("username", activityLoginBinding.editUserName.getText().toString());
        loginEditor.putString("password", activityLoginBinding.editPassword.getText().toString());
        loginEditor.commit();
    }

    @Override
    public void commitClear() {
        loginEditor.clear();
        loginEditor.commit();
    }

    @Override
    public void checkButtonError() {
        alert.errorAlert("Please Select User Type");
    }

    @Override
    public void loginSuccess(ArrayList<LoginDetailsPojo> LoginDetails) {
        if (dialogOTP != null) {
            dialogOTP.dismiss();
        }
        for (LoginDetailsPojo obj : LoginDetails) {
            UserDetails.Username = obj.getUserName();
            UserDetails.UserType = obj.getUserType();
            UserDetails.ParentName = obj.getParentName();
            UserDetails.UserBalance = obj.getCurrentBalance();
            UserDetails.EmailAddress = obj.getEmailID();
            UserDetails.Token = obj.getToken();
            UserDetails.MobileNumber = obj.getMobileNumber();
            UserDetails.UserId = obj.getUserID();
            UserDetails.ParentId = obj.getParentID();
            try {
                Log.i("Inside Home :", "HOme");
                Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);//MainActivity
                Bundle bundle = new Bundle();
                homeIntent.putExtras(bundle);
                startActivity(homeIntent);
                finish();
               /* if (obj.getIsInitialPassword().equals(false)) {
                    Log.i("Inside Change :", "Change");
                    Intent homeIntent = new Intent(LoginActivity.this, ChangeInitialPassword.class);//change initial pass
                    Bundle bundle = new Bundle();
                    homeIntent.putExtras(bundle);
                    startActivity(homeIntent);
                    finish();
                } else if (obj.getIsInitialPassword().equals(true)) {
                    Log.i("Inside Home :", "HOme");
                    Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);//MainActivity
                    Bundle bundle = new Bundle();
                    homeIntent.putExtras(bundle);
                    startActivity(homeIntent);
                    finish();
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void forgotPass(ArrayList<ForgetPasswordPojo> loginResult) {
        dialog.dismiss();
    }

    @Override
    public void showForgotPasswordDialog() {
        dialog = new Dialog(context, R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.activity_forgot_password);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Reset Password");
        Button btn_Reset = (Button) dialog.findViewById(R.id.btn_forget);
        Button btn_Clear = (Button) dialog.findViewById(R.id.btn_cancel);
        editMemberId = (EditText) dialog.findViewById(R.id.editMemberId);
        editMobileNo = (EditText) dialog.findViewById(R.id.editMobileNo);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        dialog.show();
        btn_Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(context)) {
                    loginPresenter.validateForgotPassword();
                }

                dialog.dismiss();
            }
        });

        btn_Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public String getForgotUserName() {
        return editMemberId.getText().toString().trim();
    }

    @Override
    public String getForgotMobileNo() {
        return editMobileNo.getText().toString().trim();
    }

    @Override
    public String getUserType() {
        return UserType;
    }

    @Override
    public void ForgotUserNameError() {

        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Member Id"));
        editMemberId.requestFocus();
        /*editMemberId.setError(String.format(AllMessages.pleaseEnter, "Username"));
         */
    }

    @Override
    public void ForgotMobileNameError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Mobile No"));
        editMobileNo.requestFocus();
    }

    @Override
    public String MemberId() {
        return editMemberId.getText().toString().trim();
    }

    @Override
    public String MobileNo() {
        return editMobileNo.getText().toString().trim();
    }


    private void dialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void ForgotSuccess(String body) {
        editMobileNo.setText("");
        dialog();
        alert.successAlert(body);
    }

    @Override
    public void ConfirmDialog() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Are you sure to reset password?"));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPresenter.forgotPassword();
                dialog.dismiss();
                progressDialog.showPDialog();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void openOtpScreen() {
        dialogOTP = new Dialog(LoginActivity.this);
        dialogOTP.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogOTP.setCancelable(false);
        dialogOTP.setContentView(R.layout.login_otp_screen);
        edtOTP = (EditText) dialogOTP.findViewById(R.id.edtOTP);
        Button dialogButton = (Button) dialogOTP.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialogOTP.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginPresenter.submitOTP(edtOTP.getText().toString());
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOTP.dismiss();
            }
        });
        dialogOTP.show();
    }

    @Override
    public void edtOTPError() {
        alert.errorAlert("Please enter OTP here");
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            activityLoginBinding.editPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            activityLoginBinding.editPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        }
    }
}
