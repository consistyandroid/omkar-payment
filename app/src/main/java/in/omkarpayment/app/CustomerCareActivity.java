package in.omkarpayment.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import in.omkarpayment.app.databinding.ActivityCustomerCareBinding;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class CustomerCareActivity extends AppCompatActivity {
    ActivityCustomerCareBinding activityCustomerCareBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityCustomerCareBinding = DataBindingUtil.setContentView(this, R.layout.activity_customer_care);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Customer Care";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        activityCustomerCareBinding.txtDishtvno1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtDishtvno1.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtDishtvno2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtDishtvno2.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtBigtvno1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtBigtvno1.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtBigtvno2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtBigtvno2.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtsundirectno1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtsundirectno1.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtsundirectno2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtsundirectno2.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtVideoconno1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtVideoconno1.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtVideoconno2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtVideoconno2.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtAirtelno1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtAirtelno1.getText().toString().trim());

            }
        });
        activityCustomerCareBinding.txtAirtelno2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling(activityCustomerCareBinding.txtAirtelno2.getText().toString().trim());

            }
        });

    }

    public void calling(String phonenumber) {
        Log.i("phonenumber", activityCustomerCareBinding.txtDishtvno1.getText().toString().trim());
        try {
            Intent my_callIntent = new Intent(Intent.ACTION_DIAL);
            my_callIntent.setData(Uri.parse("tel:" + phonenumber));
            //here the word 'tel' is important for making a call...

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(my_callIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Error in your phone call" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
