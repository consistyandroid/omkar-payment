package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.UserDetails;

import static in.omkarpayment.app.MoneyTransferpkg.validationpkg.BeneficiaryListFragment.SenderMob;


/**
 * Created by consisty on 12/10/17.
 */

public class BeneficiaryListPresenterImpl implements IBeneficiaryListPresenter {

    IBeneficiaryListView iBeneficiaryListView;

    LogWriter log = new LogWriter();

    public BeneficiaryListPresenterImpl(IBeneficiaryListView iBeneficiaryListView) {
        this.iBeneficiaryListView = iBeneficiaryListView;
    }

    @Override
    public void doSearchList() {
        if (iBeneficiaryListView.getMobileNumber().equals("") || iBeneficiaryListView.getMobileNumber().length() != 10) {
            iBeneficiaryListView.edtMobileNumberError();
        } else {
            iBeneficiaryListView.searchBeneficiaries();
        }
    }

    @Override
    public void searchBeneficiaries() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("MobileNumber", iBeneficiaryListView.getMobileNumber());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("BeneficiaryList Request", jsonrequest);


            WebServiceModel model = new WebServiceModel();
            model.delegateBeneficiaryList = this;
            model.webserviceMethod(encryptString, keyData, "BeneficiaryList");

            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptBeneficiaryListResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("BeneficiaryList", body);

            ArrayList<BeneficiaryListpojo> beneficiaryList = gson.fromJson(body, new TypeToken<ArrayList<BeneficiaryListpojo>>() {
            }.getType());
            if (beneficiaryList != null) {

                beneficiaryListShow(beneficiaryList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptBeneficiaryListFailureResponse(String encryptedResponse) {

        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("BeneficiaryList", body);
            errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void dismissPDialog() {
        iBeneficiaryListView.dissmissDialog();
    }

    @Override
    public void CheckSenderOtp() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("SenderID", UserDetails.SenderRegID);
            loginrequest.put("MobileNumber", SenderMob);
            loginrequest.put("OTP", iBeneficiaryListView.getOtp());


            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("Otp Request", jsonrequest);
            Log.i("Sender R ID", UserDetails.SenderRegID);

            CheckSenderOtpModel model = new CheckSenderOtpModel();
            model.iBeneficiaryListPresenter = this;
            model.verifyOTPForAddSender(encryptString, keyData);

            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void errorAlert(String body) {
        iBeneficiaryListView.errorAlert(body);
    }


    @Override
    public void getSenderDetails() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("MobileNumber", iBeneficiaryListView.getMobileNumber());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("BeneficiaryList Request", jsonrequest);
            WebServiceModel model = new WebServiceModel();
            model.delegateBeneficiaryList = this;
            model.webserviceMethod(encryptString, keyData, "SenderDetails");
            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void SenderDetailsResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("SenderDetails", body);
            ArrayList<SenderDetailsPojo> details = gson.fromJson(body, new TypeToken<ArrayList<SenderDetailsPojo>>() {
            }.getType());
            if (details != null) {
                senderDetails(details);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void senderDetails(ArrayList<SenderDetailsPojo> details) {
        iBeneficiaryListView.showdetails(details);
    }

    @Override
    public void SenderDetailsFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("SenderDetails", body);
            errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddSenderVerifyOTPFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verify Add Sender FailResponse", body);

            if (body == null) {
                iBeneficiaryListView.unableToFetchError("verify sender.");
                return;
            }
            iBeneficiaryListView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddSenderVerifyOTPResponse(String response) {
        iBeneficiaryListView.dissmissDialog();
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verify Add Sender FailResponse", body);

            if (body == null) {
                iBeneficiaryListView.unableToFetchError("verify sender.");
                return;
            }
            iBeneficiaryListView.SenderSuccessRespaonse(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void beneficiaryListShow(ArrayList<BeneficiaryListpojo> beneficiaryList) {
        iBeneficiaryListView.beneficiaryListShow(beneficiaryList);
    }

    private void showDialog() {
        //iBeneficiaryListView.showPdialog();
    }
}
