package in.omkarpayment.app.MoneyTransferpkg.Registrationpkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SenderIDDetailsPojo {
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("SenderID")
    @Expose
    private String senderID;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }
}
