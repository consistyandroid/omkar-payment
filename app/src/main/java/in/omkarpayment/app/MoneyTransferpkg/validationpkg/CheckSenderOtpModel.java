package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.IWebServicesModel;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckSenderOtpModel {
    IBeneficiaryListPresenter iBeneficiaryListPresenter;
    public void verifyOTPForAddSender(String encryptString, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.SenderRegistrationWithOTP(encryptString, keyData);//OTP
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        iBeneficiaryListPresenter.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        iBeneficiaryListPresenter.getAddSenderVerifyOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        iBeneficiaryListPresenter.getAddSenderVerifyOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        iBeneficiaryListPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                iBeneficiaryListPresenter.errorAlert(AllMessages.internetError);
            }
        });
    }
}
