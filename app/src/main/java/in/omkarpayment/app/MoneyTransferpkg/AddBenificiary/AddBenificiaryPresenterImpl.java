package in.omkarpayment.app.MoneyTransferpkg.AddBenificiary;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.GetBankNamesPojo;
import in.omkarpayment.app.json.moneyTx.AddBeneficiaryPojo;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by consisty on 12/10/17.
 */

public class AddBenificiaryPresenterImpl implements IAddBenificiaryPresenter {


    IAddBenificiaryView iAddBenificiaryView;

    public AddBenificiaryPresenterImpl(IAddBenificiaryView iAddBenificiaryView) {
        this.iAddBenificiaryView = iAddBenificiaryView;
    }


    @Override
    public void validateAddBeneficiaryFields() {
        if (iAddBenificiaryView.getSenderMobileNumber().equals("") || iAddBenificiaryView.getSenderMobileNumber().length() != 10) {
            iAddBenificiaryView.edtMobileNumberError();
        } else if (iAddBenificiaryView.getBankName().equals("")) {
            iAddBenificiaryView.edtBankError();
        } else if (iAddBenificiaryView.getIFSCCode().equals("")) {
            iAddBenificiaryView.edtIFSCError();
        } else if (iAddBenificiaryView.getAccountNumber().equals("")) {
            iAddBenificiaryView.edtAccountNumberError();
        } else if (iAddBenificiaryView.getName().equals("")) {
            iAddBenificiaryView.edtNameError();
        } else {
            doBeneficiaryRegistration();
        }
    }


    @Override
    public void doBeneficiaryRegistration() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addBeneficiary = new JSONObject();
        try {

            addBeneficiary.put("SenderID", UserDetails.SenderID);
            addBeneficiary.put("Name", iAddBenificiaryView.getName());
            //addBeneficiary.put("LastName", iCAddSenderBeneficiaryView.beneficiaryLastName().trim());
            addBeneficiary.put("Bank", iAddBenificiaryView.getBankName());
            addBeneficiary.put("MobileNumber", iAddBenificiaryView.getSenderMobileNumber());
            addBeneficiary.put("AccountNo",  iAddBenificiaryView.getAccountNumber());
            addBeneficiary.put("IFSC", iAddBenificiaryView.getIFSCCode());

            String jsonrequest = addBeneficiary.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("Add B Request", jsonrequest);
            WebServiceModel model = new WebServiceModel();
            model.delegateAddBeneficiary = this;
            model.webserviceMethod(encryptString, keyData, "AddBeneficiary");
            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptAddBeneficiaryResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("AddBeneficiaryRes -->", body);
            AddBeneficiaryPojo addBeneficiary = gson.fromJson(body, new TypeToken<AddBeneficiaryPojo>() {
            }.getType());

            if (addBeneficiary.getMessage().equals("SUCCESS")) {
                UserDetails.SenderID = addBeneficiary.getSenderID();
                UserDetails.BeneficiaryID = addBeneficiary.getBenficiaryID();
                iAddBenificiaryView.addBeneSuccessResponse(addBeneficiary);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addBeneficiarySuccess(AddBeneficiaryPojo addBeneficiary) {

        iAddBenificiaryView.addBeneficiarySuccess(addBeneficiary);
    }

    @Override
    public void decryptAddBeneficiaryFailureResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("AddBeneficiaryRes -->", body);

            errorAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }


/*
        String body = "";
        Log.i("AddBenefiFailResponse", encryptedResponse);
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            iAddBenificiaryView.beneficiaryFailureMessage(body);
            Log.i("Decrypted body", body);
        } catch (Exception e) {
            return;
        }*/
    }

    @Override
    public void dismissPDialog() {
        iAddBenificiaryView.dismissPDialog();
    }

    /*SenderID,BenFiciaryID,OTP*/
    @Override
    public void checkOtp() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("SenderID", UserDetails.SenderID);
            loginrequest.put("BenficiaryID", UserDetails.BeneficiaryID);
            loginrequest.put("OTP", iAddBenificiaryView.getOtp());


            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("Otp Request", jsonrequest);


            WebServiceModel model = new WebServiceModel();
            model.delegateAddBeneficiary = this;
            model.webserviceMethod(encryptString, keyData, "OTP");

            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchBank() {

    }

    @Override
    public void decryptOTPResponse(String encryptedResponse) {
        Log.i("OTP", encryptedResponse);
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Decrypted Success body", body);
            if (body != null) {
                iAddBenificiaryView.successAlert(body);
            }
        } catch (Exception e) {
            //iAddBenificiaryView.successAlert(body);
        }
    }

    @Override
    public void decryptOTPFailureResponse(String encryptedResponse) {
        Log.i("OTP", encryptedResponse);
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            iAddBenificiaryView.errorAlert(body);
            Log.i("Decrypted Failure body", body);
        } catch (Exception e) {
            //iAddBenificiaryView.errorAlert(body);
        }
    }

    @Override
    public void errorAlert(String somethingWrong) {
        iAddBenificiaryView.errorAlert(somethingWrong);
    }

    @Override
    public void decryptGetBankNameResponse(String encryptedResponse) {

        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("getBankNamesResponse", body);
            Gson gson = new Gson();
            ArrayList<GetBankNamesPojo> bankNames = gson.fromJson(body, new TypeToken<ArrayList<GetBankNamesPojo>>() {
            }.getType());
            iAddBenificiaryView.sendBankNamesResponse(bankNames);

        } catch (Exception e) {

        }
    }

    @Override
    public void decryptGetBankNameFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            iAddBenificiaryView.errorAlert(body);
            Log.i("getBankNamesFailRespons", body);
        } catch (Exception e) {
        }
    }

    @Override
    public void getBankNames() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateAddBeneficiary = this;
        model.webserviceMethod("", keyData, "GetBank");

    }

    @Override
    public void getDefaultIFSCCode(String ifsc) {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addBenif = new JSONObject();
        try {
            addBenif.put("ID", String.valueOf(ifsc));
            String jsonrequest = addBenif.toString();
            Log.i("request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            WebServiceModel model = new WebServiceModel();
            model.delegateAddBeneficiary = this;
            model.webserviceMethod(encryptString, keyData, "DefaultIFSC");


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void validateUserVerification(String bankName, String ifsc, String accountNumber, String senderNumber, String deviceID) {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verify = new JSONObject();
        try {

            /*{"MobileNumber":"9595215859", "AccountNo":"20356390959","IFSC":"SBIN0016845","Bank":"SBI","IPAddress":"12145454","SenderID":"13"}*/
            verify.put("MobileNumber", senderNumber);
            verify.put("Bank", String.valueOf(bankName));
            verify.put("IFSC", String.valueOf(ifsc));
            verify.put("AccountNo", String.valueOf(accountNumber));
            verify.put("SenderID", String.valueOf(UserDetails.SenderID));
            verify.put("IPAddress", String.valueOf(deviceID));

            String jsonrequest = verify.toString();
            Log.i("Verification request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            WebServiceModel model = new WebServiceModel();
            model.delegateAddBeneficiary = this;
            model.webserviceMethod(encryptString, keyData, "Verification");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void VerificationResponseResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            UserDetails.name = body;
            String name = String.valueOf(body);
            iAddBenificiaryView.userName(name);
            Log.i("verifiedUserName", body);
        } catch (Exception e) {
        }
    }

    @Override
    public void VerificationFailureResponseResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            iAddBenificiaryView.errorAlertVarification(body);
            Log.i("FalureVerified name", body);
        } catch (Exception e) {
        }
    }

    @Override
    public void ResendOtp() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("SenderID", UserDetails.SenderID);
            loginrequest.put("BenficiaryID", UserDetails.BeneficiaryID);

            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("ResendOtp Request", jsonrequest);

            WebServiceModel model = new WebServiceModel();
            model.delegateAddBeneficiary = this;
            model.webserviceMethod(encryptString, keyData, "ResendOTP");
            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog() {
        iAddBenificiaryView.showPDialog();
    }
}
