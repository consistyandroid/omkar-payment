package in.omkarpayment.app.MoneyTransferpkg.AddBenificiary;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.AutocompleteForBankNameAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.GetBankNamesPojo;
import in.omkarpayment.app.json.moneyTx.AddBeneficiaryPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by consisty on 12/10/17.
 */

public class AddBenificiaryActivity extends AppCompatActivity implements IAddBenificiaryView {

    IAddBenificiaryPresenter iAddBenificiaryPresenter;
    Context context = this;
    AlertImpl alert;
    CustomProgressDialog progressDialog;
    EditText edtName, edtIFSC, edtAccountNumber, edtSenderID, edtOtp;
    TextView edtSenderMobileNumber;
    Button btnAddBeneficiary, btnSubmit, btnSearch, btnResendOtp, btnVerify;
    Dialog dialog;
    String SenderMob;
    AutoCompleteTextView edtBankName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moneytransfer_addbeneficiary);
        Bundle extras = getIntent().getExtras();
        SenderMob = extras.getString("sendermobno");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Add New Beneficiary";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        alert = new AlertImpl(context);
        iAddBenificiaryPresenter = new AddBenificiaryPresenterImpl(this);
        progressDialog = new CustomProgressDialog(context);
        iAddBenificiaryPresenter.getBankNames();
        edtSenderMobileNumber = (TextView) findViewById(R.id.edtSenderMobileNumber);
        edtName = (EditText) findViewById(R.id.edtBeneficiaryName);
        edtIFSC = (EditText) findViewById(R.id.edtIFSC);
        edtBankName = (AutoCompleteTextView) findViewById(R.id.spBank);
        edtAccountNumber = (EditText) findViewById(R.id.edtAccountNumber);
        btnAddBeneficiary = (Button) findViewById(R.id.btnAddBeneficiary);

        edtSenderMobileNumber.setText(SenderMob);

        //edtSenderID = (EditText) findViewById(R.id.edtSenderId);
        edtBankName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GetBankNamesPojo object = (GetBankNamesPojo) parent.getItemAtPosition(position);
                String IfscCode = object.getIFSC();
                edtIFSC.setText(IfscCode);
            }
        });


        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAddBenificiaryPresenter.searchBank();
            }
        });

        btnVerify = (Button) findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtBankName.getText().toString().equalsIgnoreCase("") || edtAccountNumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "First select bank name and enter account number", Toast.LENGTH_SHORT).show();
                } else {
                    btnVerify.setVisibility(View.GONE);
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please Confirm")
                            .setCustomImage(R.mipmap.ic_launcher)
                            .showCancelButton(true)
                            .setCancelText("Cancel")
                            .setContentText("Do you want to verify? ")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    iAddBenificiaryPresenter.validateUserVerification
                                            (edtBankName.getText().toString(), edtIFSC.getText().toString(),
                                                    edtAccountNumber.getText().toString(), SenderMob, UserDetails.IPCOnst);
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    btnVerify.setVisibility(View.VISIBLE);
                                }
                            })
                            .show();
                }
            }
        });

        btnAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAddBenificiaryPresenter.validateAddBeneficiaryFields();
            }
        });


    }

    private void onResultIntent() {
        Intent intent = getIntent();
        intent.putExtra("SenderNo", SenderMob);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public void forOtp() {
        iAddBenificiaryPresenter.validateAddBeneficiaryFields();
    }


    @Override
    public void edtNameError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "beneficiary name"));
    }

    @Override
    public void edtIFSCError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "IFSC code"));

    }

    @Override
    public void edtAccountNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "account number"));

    }

    @Override
    public void edtSenderIDError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "sender id"));

    }

    @Override
    public void edtBankError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "bank name"));

    }

    @Override
    public void edtMobileNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "mobile number"));

    }


    @Override
    public String getSenderMobileNumber() {
        return edtSenderMobileNumber.getText().toString().trim();

    }

    @Override
    public String getName() {
        return edtName.getText().toString().trim();
    }

    @Override
    public String getIFSCCode() {
        return edtIFSC.getText().toString().trim();
    }

    @Override
    public String getBankName() {
        return edtBankName.getText().toString().trim();
    }

    @Override
    public String getAccountNumber() {
        return edtAccountNumber.getText().toString().trim();
    }

    @Override
    public String getSenderID() {
        return edtSenderID.getText().toString().trim();
    }

    @Override
    public void doBeneficiaryRegistration() {
    }


    public void otpPopup() {
        dialog = new Dialog(context, R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_newotp);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Enter OTP");

        edtOtp = (EditText) dialog.findViewById(R.id.editOtp);
        edtOtp.setHint("Enter OTP");
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
       /* btnResendOtp = (Button) dialog.findViewById(R.id.btnResendOtp);
        btnResendOtp.setVisibility(View.VISIBLE);

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAddBenificiaryPresenter.ResendOtp();
                            }
        });*/
        dialog.show();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOtp.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "otp"));
                } else {
                    iAddBenificiaryPresenter.checkOtp();
                    dialogDissmiss();
                }
            }
        });
    }

    @Override
    public void sendBankNamesResponse(ArrayList<GetBankNamesPojo> bankNames) {
        AutocompleteForBankNameAdapter autocompleteTextAdapter = new AutocompleteForBankNameAdapter(context, R.layout.adapter_autocomplete_text, bankNames);
        edtBankName.setThreshold(1);
        edtBankName.setAdapter(autocompleteTextAdapter);

    }

    @Override
    public void userName(String body) {

    }

    @Override
    public void beneficiaryFailureMessage(String body) {

    }

    @Override
    public void errorAlertVarification(String body) {

    }

    private void dialogDissmiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    @Override
    public void successAlert(String body) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(body);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResultIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void addBeneSuccessResponse(AddBeneficiaryPojo addBeneficiaryPojo) {
        if (addBeneficiaryPojo.getMessage().equals("SUCCESS")) {
            alert.successAlert("Beneficiary added successfuly");
        }
        clear();
    }

    public void clear() {
        edtName.setText("");
        edtIFSC.setText("");
        edtAccountNumber.setText("");
        edtBankName.setText("");
        edtBankName.requestFocus();
    }

    @Override
    public String getOtp() {

        return edtOtp.getText().toString().trim();
    }

    @Override
    public void errorAlert(String body) {
        alert.errorAlert(body);
        clear();
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void addBeneficiarySuccess(AddBeneficiaryPojo addBeneficiary) {
        //UserDetails.SenderID = addBeneficiary.getSenderID();
        // UserDetails.BeneficiaryID = addBeneficiary.getBenficiaryID();
    }

}
