package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

/**
 * Created by consisty on 12/10/17.
 */

public interface IBeneficiaryListPresenter {
    void doSearchList();

    void searchBeneficiaries();

    void decryptBeneficiaryListResponse(String encryptedResponse);

    void decryptBeneficiaryListFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void CheckSenderOtp();



    void errorAlert(String somethingWrong);

    void getSenderDetails();

    void SenderDetailsResponse(String encryptedResponse);

    void SenderDetailsFailureResponse(String encryptedResponse);
    void getAddSenderVerifyOTPFailResponse(String response);

    void getAddSenderVerifyOTPResponse(String response);
}
