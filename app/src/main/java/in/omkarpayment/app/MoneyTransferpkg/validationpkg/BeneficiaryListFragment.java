package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.omkarpayment.app.MoneyTransferpkg.AddBenificiary.AddBenificiaryActivity;
import in.omkarpayment.app.MoneyTransferpkg.AddBenificiary.AddBenificiaryPresenterImpl;
import in.omkarpayment.app.MoneyTransferpkg.AddBenificiary.IAddBenificiaryPresenter;
import in.omkarpayment.app.MoneyTransferpkg.AddBenificiary.IAddBenificiaryView;
import in.omkarpayment.app.MoneyTransferpkg.Registrationpkg.ISenderRegistrationPresenter;
import in.omkarpayment.app.MoneyTransferpkg.Registrationpkg.ISenderRegistrationView;
import in.omkarpayment.app.MoneyTransferpkg.Registrationpkg.SenderIDDetailsPojo;
import in.omkarpayment.app.MoneyTransferpkg.Registrationpkg.SenderRegistrationPresenterImpl;
import in.omkarpayment.app.R;
import in.omkarpayment.app.TransparentProgressDialog;
import in.omkarpayment.app.adapter.AutocompleteForBankNameAdapter;
import in.omkarpayment.app.adapter.SenderMoneyTransferAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.GetBankNamesPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.json.SenderMoneyTransferPojoModel;
import in.omkarpayment.app.json.moneyTx.AddBeneficiaryPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.report.sendermoneyTransferReport.ISenderMoneyTransferPresenter;
import in.omkarpayment.app.report.sendermoneyTransferReport.ISenderMoneyTransferView;
import in.omkarpayment.app.report.sendermoneyTransferReport.SenderMoneyTransferPresenterImpl;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import static android.app.Activity.RESULT_OK;


public class BeneficiaryListFragment extends Fragment implements IBeneficiaryListView,
        IAvailableBalanceView, IAddBenificiaryView, ISenderRegistrationView, IRefreshInterface, ISenderMoneyTransferView {
    public static String SenderMob;
    AlertImpl alert;
    BeneFiciaryListAdapter beneFiciaryListAdapter;
    ISenderMoneyTransferPresenter iSenderMoneyTransferPresenter;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    CustomProgressDialog progressDialog;
    IAddBenificiaryPresenter iAddBenificiaryPresenter;
    IBeneficiaryListPresenter iBeneficiaryListPresenter;
    ISenderRegistrationPresenter iSenderRegistrationPresenter;
    RecyclerView BeneficiaryListView;
    EditText edtMobileNumber, edtSenderAddMobileNumber;
    Button btnSearchList, btnAddBen, btnVerify, btnContinue;
    Dialog AddBenefeciaryDialog, AddSenderDialog, dialog, customDialog;
    AutoCompleteTextView edtBankName;
    EditText edtName, spBank, edtIFSC, edtAccountNumber, edtSenderID, edtOtp, edtSenderMobileNumber, edtLastName, edtFirstName, edtAddress, edtPincode;
    TextView txtSenderName, txtMobileNo, txtRemainig, txtTotalLimit, txtUtilized, txtAvailable;
    Button btnAddBeneficiary, btnSubmit, dialogCancelButton, dialogButton, btnSenderRegistration, btnClear, btnResendOtp, btnTxList;
    SwipeRefreshLayout swipeRefreshLayout;
    Typeface roboto;
    String message = "";
    LinearLayout fragment_container, layoutDetails, layoutinput;
    BeneFiciaryListAdapter adapter;
    SenderMoneyTransferAdapter adapterSender;
    ArrayList<String> opList = new ArrayList<>();
    View view;
    private TransparentProgressDialog pd;
    private Handler h;
    private Runnable r;

    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    /* @Override
     public void onResume() {
         super.onResume();
         SenderMob = "";
         BeneficiaryListView.setAdapter(null);
         iBeneficiaryListPresenter.getSenderDetails();

     }
 */
    /* @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.moneytransfervalidation);*/
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("Inside Distributor", "Yes");
        view = inflater.inflate(R.layout.moneytransfervalidation, container, false);
        progressDialog = new CustomProgressDialog(getActivity());

        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iBeneficiaryListPresenter = new BeneficiaryListPresenterImpl(this);
        iAddBenificiaryPresenter = new AddBenificiaryPresenterImpl(this);
        iSenderRegistrationPresenter = new SenderRegistrationPresenterImpl(this);
        beneFiciaryListAdapter = new BeneFiciaryListAdapter(this);
        iSenderMoneyTransferPresenter = new SenderMoneyTransferPresenterImpl(this);
/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            String title = "Money Transfer";
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);

        }*/
        alert = new AlertImpl(getActivity());
        BeneficiaryListView = (RecyclerView) view.findViewById(R.id.BeneficiarylistView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        BeneficiaryListView.setLayoutManager(mLayoutManager);
        BeneficiaryListView.setItemAnimator(new DefaultItemAnimator());
        BeneficiaryListView.setAdapter(null);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        fragment_container = (LinearLayout) view.findViewById(R.id.fragment_container);

        txtSenderName = (TextView) view.findViewById(R.id.txtSenderName);
        txtMobileNo = (TextView) view.findViewById(R.id.txtMobileNumber);
        txtUtilized = (TextView) view.findViewById(R.id.txtUtilized);
        txtRemainig = (TextView) view.findViewById(R.id.txtRemaining);

        btnTxList = (Button) view.findViewById(R.id.btnTxList);

        //txtSenderPincode = (TextView) findViewById(R.id.txtSenderPincode);
        //txtUtilized = (TextView) findViewById(R.id.txtUtilized);
        txtAvailable = (TextView) view.findViewById(R.id.txtAvailable);
        layoutDetails = (LinearLayout) view.findViewById(R.id.layoutDetails);
        layoutDetails.setVisibility(View.GONE);

        swipeRefreshLayout.setColorScheme(R.color.orange, R.color.red, R.color.blue);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        doSearchBenifeciary();
                    }
                }, 4000);
            }
        });
        // btnSearchList = (Button) findViewById(R.id.btnSearchList);

        btnAddBen = (Button) view.findViewById(R.id.btnAddBen);
        btnAddBeneficiary = (Button) view.findViewById(R.id.btnAddBeneficiary);
        btnAddBeneficiary.setVisibility(View.GONE);
        edtMobileNumber = (EditText) view.findViewById(R.id.edtMobileNumber);
        btnContinue = (Button) view.findViewById(R.id.btnContinue);
        layoutinput = (LinearLayout) view.findViewById(R.id.layoutinput);

        btnTxList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnTxList.getText().toString().equals("TX.L")) {
                    if (edtMobileNumber.getText().toString().length() == 10) {

                        btnTxList.setText("Ben.L");
                        btnTxList.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_normal));
                        BeneficiaryListView.setAdapter(null);

                        iSenderMoneyTransferPresenter.searchTransactions();

                    }
                } else if (btnTxList.getText().toString().equals("Ben.L")) {
                    if (edtMobileNumber.getText().toString().length() == 10) {
                        btnTxList.setText("TX.L");
                        btnTxList.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_normal));
                        BeneficiaryListView.setAdapter(null);
                        layoutDetails.setVisibility(View.VISIBLE);
                        doSearchBenifeciary();
                    }
                }
            }
        });

        btnAddBen.setVisibility(View.GONE);
        btnTxList.setVisibility(View.GONE);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        edtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 9) {

                    layoutinput.setVisibility(View.VISIBLE);

                    progressD();

                    iBeneficiaryListPresenter.getSenderDetails();
                    layoutDetails.setVisibility(View.VISIBLE);
                    btnAddBeneficiary.setVisibility(View.VISIBLE);
                    SenderMob = edtMobileNumber.getText().toString();
                }
            }
        });
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMobileNumber.getText().toString().length() != 10) {

                    edtMobileNumber.setError(String.format(AllMessages.pleaseEnter, "10 digit mobile no"));
                    edtMobileNumber.requestFocus();

                } else {
                    layoutinput.setVisibility(View.VISIBLE);
                    progressD();
                    // btnTxList.setVisibility(View.GONE);
                    //closeKeyboard(context, edtMobileNumber.getWindowToken());
                    iBeneficiaryListPresenter.getSenderDetails();
                    layoutDetails.setVisibility(View.VISIBLE);
                    btnAddBeneficiary.setVisibility(View.VISIBLE);
                    SenderMob = edtMobileNumber.getText().toString();
                }
            }
        });


        /*edtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                if (edtMobileNumber.getText().length() == 10) {

                    btnContinue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            progressD();
                            // btnTxList.setVisibility(View.GONE);
                            closeKeyboard(context, edtMobileNumber.getWindowToken());
                            btnAddBen.setVisibility(View.VISIBLE);
                            layoutDetails.setVisibility(View.VISIBLE);
                            iBeneficiaryListPresenter.getSenderDetails();
                            SenderMob = edtMobileNumber.getText().toString();
                        }
                    });
                    layoutDetails.setVisibility(View.VISIBLE);
                    iBeneficiaryListPresenter.getSenderDetails();
                    SenderMob = edtMobileNumber.getText().toString();
                }
                if (edtMobileNumber.getText().length() != 10) {
                    BeneficiaryListView.setAdapter(null);
                    //  btnTxList.setText("TX.L");
                    // btnTxList.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.solid_button_curv_orange));
                    btnTxList.setVisibility(View.GONE);
                    btnAddBen.setVisibility(View.GONE);
                    layoutDetails.setVisibility(View.GONE);
                    clearDetails();
                    SenderMob = "";
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });*/


        btnAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddBenificiaryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("sendermobno", SenderMob);
                intent.putExtras(bundle);
                startActivityForResult(intent, 2);
                //addBeneficiary();
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == RESULT_OK || requestCode == 2) {
                String No = data.getStringExtra("SenderNo");
                if (No != null) {
                    edtMobileNumber.setText(No);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void clear() {
        try {
            //edtMobileNumber.setText("");
            layoutinput.setVisibility(View.VISIBLE);
            layoutDetails.setVisibility(View.GONE);
            btnAddBeneficiary.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            clear();
        }
    }

    public void progressD() {
        progressDialog.showPDialog();
    }

    public void progressDialogDismiss() {
        progressDialog.dismissPDialog();
    }

    private void clearDetails() {
        txtSenderName.setText(null);

        txtAvailable.setText(null);
    }

    public void doSearchBenifeciary() {

        iBeneficiaryListPresenter.doSearchList();
    }


    public void addBeneficiary() {

        AddBenefeciaryDialog = new Dialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        AddBenefeciaryDialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        AddBenefeciaryDialog.setContentView(R.layout.dialog_addbenefeciary);
        AddBenefeciaryDialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        AddBenefeciaryDialog.setTitle("Add Beneficiary");
        iAddBenificiaryPresenter.getBankNames();
        AddBenefeciaryDialog.setCanceledOnTouchOutside(false);
        edtSenderMobileNumber = (EditText) AddBenefeciaryDialog.findViewById(R.id.edtSenderMobileNumber);
        edtName = (EditText) AddBenefeciaryDialog.findViewById(R.id.edtBeneficiaryName);
        edtIFSC = (EditText) AddBenefeciaryDialog.findViewById(R.id.edtIFSC);
        edtBankName = (AutoCompleteTextView) AddBenefeciaryDialog.findViewById(R.id.spBank);
        edtAccountNumber = (EditText) AddBenefeciaryDialog.findViewById(R.id.edtAccountNumber);
        edtSenderID = (EditText) AddBenefeciaryDialog.findViewById(R.id.edtSenderId);
        Button btnClear = (Button) AddBenefeciaryDialog.findViewById(R.id.btnClear);
        edtSenderMobileNumber.setText(SenderMob);
        //  edtBankName.requestFocus();
        edtSenderMobileNumber.setEnabled(false);

        edtBankName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GetBankNamesPojo object = (GetBankNamesPojo) parent.getItemAtPosition(position);
                String IfscCode = object.getIFSC();
                edtIFSC.setText(IfscCode);
                Log.i("IFSC : ", object.getIFSC());
            }
        });
        btnAddBeneficiary = (Button) AddBenefeciaryDialog.findViewById(R.id.btnAddBeneficiary);
        btnVerify = (Button) AddBenefeciaryDialog.findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtBankName.getText().toString().equalsIgnoreCase("") || edtAccountNumber.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "First select bank name and enter account number", Toast.LENGTH_SHORT).show();
                } else {
                    btnVerify.setVisibility(View.GONE);
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Please Confirm")
                            .setCustomImage(R.mipmap.ic_launcher)
                            .showCancelButton(true)
                            .setCancelText("Cancel")
                            .setContentText("Do you want to verify? ")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    iAddBenificiaryPresenter.validateUserVerification(edtBankName.getText().toString(),
                                            edtIFSC.getText().toString(), edtAccountNumber.getText().toString(), SenderMob, UserDetails.IPCOnst);
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    btnVerify.setVisibility(View.VISIBLE);
                                }
                            })
                            .show();
                }
            }
        });

        btnAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtSenderMobileNumber.getText().toString().equalsIgnoreCase("") || edtSenderMobileNumber.getText().toString().length() < 10) {
                    edtSenderMobileNumber.setError(String.format(AllMessages.pleaseEnter, "mobile number"));
                } else {
                    //progressD();
                    iAddBenificiaryPresenter.validateAddBeneficiaryFields();
                }
            }
        });
        AddBenefeciaryDialog.show();
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtName.setText("");
                edtIFSC.setText("");
                // edtBankName.setText("");
                edtAccountNumber.setText("");
                edtName.setError(null);
                edtAccountNumber.setError(null);
                //  edtBankName.setError(null);
                edtIFSC.setError(null);
            }
        });

    }


    private void addSender() {
        AddSenderDialog = new Dialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        AddSenderDialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        AddSenderDialog.setContentView(R.layout.dialog_sender_registration);
        AddSenderDialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        AddSenderDialog.setTitle("Sender Registration");
        AddSenderDialog.setCancelable(false);
        edtSenderAddMobileNumber = (EditText) AddSenderDialog.findViewById(R.id.edtSenderMobileNumber);
        edtFirstName = (EditText) AddSenderDialog.findViewById(R.id.edtFirstName);
        edtLastName = (EditText) AddSenderDialog.findViewById(R.id.edtLastName);
        edtAddress = (EditText) AddSenderDialog.findViewById(R.id.edtAddress);
        // edtPincode = (EditText) AddSenderDialog.findViewById(R.id.edtPincode);
        btnClear = (Button) AddSenderDialog.findViewById(R.id.btnClear);
        edtSenderAddMobileNumber.setText(SenderMob);
        edtSenderAddMobileNumber.setEnabled(false);
        edtFirstName.requestFocus();
        btnSenderRegistration = (Button) AddSenderDialog.findViewById(R.id.btnSenderRegistration);
        AddSenderDialog.show();
        btnSenderRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iSenderRegistrationPresenter.validateSenderDetails();
                AddSenderDialog.dismiss();
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtFirstName.setText("");
//                edtLastName.setText("");
                //  edtAddress.setText("");
                clear();
                AddSenderDialog.dismiss();
                edtFirstName.setError(null);
                // edtLastName.setError(null);
                //  edtAddress.setError(null);

            }
        });

    }

    @Override
    public String getMobileNumber() {
        edtMobileNumber.setTypeface(roboto);
        return edtMobileNumber.getText().toString();
    }

    @Override
    public String getFirstName() {

        return edtFirstName.getText().toString().trim();
    }

    @Override
    public String getLastName() {


        return edtFirstName.getText().toString().trim();
    }

    @Override
    public String getAddress() {

        return edtFirstName.getText().toString().trim();
    }

    @Override
    public String getPincode() {
        // edtPincode.setTypeface(roboto);
        return null;//edtPincode.getText().toString().trim();
    }

    @Override
    public String getName() {

        return edtName.getText().toString().trim();
    }

    @Override
    public String getIFSCCode() {
        edtIFSC.setTypeface(roboto);
        return edtIFSC.getText().toString().trim();
    }

    @Override
    public String getBankName() {

        return edtBankName.getText().toString();
    }

    @Override
    public String getAccountNumber() {

        return edtAccountNumber.getText().toString().trim();
    }

    @Override
    public String getSenderID() {

        return edtSenderID.getText().toString().trim();
    }

    @Override
    public void doBeneficiaryRegistration() {
        iAddBenificiaryPresenter.doBeneficiaryRegistration();
    }

    @Override
    public void successAlert(String body) {
        progressDialogDismiss();
        alert.successAlert(body);
        if (AddSenderDialog != null) {
            AddSenderDialog.dismiss();
        }
        if (AddBenefeciaryDialog != null) {
            AddBenefeciaryDialog.dismiss();
            BeneficiaryListView.setAdapter(null);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doSearchBenifeciary();
                }
            }, 2000);
        }

    }

    @Override
    public void addBeneSuccessResponse(AddBeneficiaryPojo addBeneficiaryPojo) {
       /* if (addBeneficiaryPojo.getMessage().equals("SUCCESS")) {
            alert.successAlert("Beneficiary added successfuly");
        }*/
    }

    @Override
    public void SenderSuccessRespaonse(String msg) {
        dialogDissmiss();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* edtMobileNumber.setText(SenderMob);
                iBeneficiaryListPresenter.getSenderDetails();*/
                Intent intent = new Intent(getActivity(), AddBenificiaryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("sendermobno", SenderMob);
                intent.putExtras(bundle);
                startActivity(intent);
                dialog.dismiss();

            }
        });
        dialog.show();
        clear();
    }

    @Override
    public void senderSucessResponsebody(SenderIDDetailsPojo senderIDDetailsPojo) {
        UserDetails.SenderRegID = senderIDDetailsPojo.getSenderID();
        Toast.makeText(getContext(), senderIDDetailsPojo.getMessage(), Toast.LENGTH_SHORT).show();
        otpPopup();
    }


    @Override
    public String getOtp() {
        return edtOtp.getText().toString().trim();
    }

    @Override
    public void edtNameError() {
        edtName.setError(String.format(AllMessages.pleaseEnter, "beneficiary name"));
    }

    @Override
    public void edtIFSCError() {
        edtIFSC.setError(String.format(AllMessages.pleaseEnter, "IFSC code"));

    }

    @Override
    public void edtAccountNumberError() {
        edtAccountNumber.setError(String.format(AllMessages.pleaseEnter, "account number"));

    }

    @Override
    public void edtSenderIDError() {
        edtSenderID.setError(String.format(AllMessages.pleaseEnter, "sender ID"));

    }

    @Override
    public void edtBankError() {
        edtBankName.setError(String.format(AllMessages.pleaseEnter, "bank name"));

    }

    @Override
    public void edtFirstNameError() {
        edtFirstName.setError(String.format(AllMessages.pleaseEnter, "first name"));

    }

    @Override
    public void edtLastNameError() {
        edtLastName.setError(String.format(AllMessages.pleaseEnter, "last name"));

    }

    @Override
    public void edtAddressError() {
        edtAddress.setError(String.format(AllMessages.pleaseEnter, "address"));

    }

    @Override
    public void edtPincodeError() {
        // edtPincode.setError(String.format(AllMessages.pleaseEnter, "valid pincode"));

    }

    @Override
    public void edtMobileNumberError() {
        edtMobileNumber.setError(String.format(AllMessages.pleaseEnter, "mobile number"));
    }

    @Override
    public String getSenderMobileNumber() {

        edtSenderMobileNumber.setTypeface(roboto);
        return SenderMob;
    }

    @Override
    public String getSenderAddMobileNumber() {
        edtSenderAddMobileNumber.setTypeface(roboto);
        return edtSenderAddMobileNumber.getText().toString();
    }

    @Override
    public void searchBeneficiaries() {
        iBeneficiaryListPresenter.searchBeneficiaries();
    }

    @Override
    public void SenderFailuerResponse(SenderDetailsPojo senderDetailsPojo) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(senderDetailsPojo.getMessage());

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////Listbinding
    @Override
    public void beneficiaryListShow(ArrayList<BeneficiaryListpojo> beneficiaryList) {

        for (BeneficiaryListpojo pojo : beneficiaryList) {
            UserDetails.SenderID = pojo.getSenderID();
        }
        if (beneficiaryList != null) {
            adapter = new BeneFiciaryListAdapter(getActivity(), this, beneficiaryList);
            BeneficiaryListView.setAdapter(adapter);
        }
    }

    @Override
    public void getReport(ArrayList<SenderMoneyTransferPojoModel> Report) {
        adapterSender = new SenderMoneyTransferAdapter(getActivity(), Report);
        BeneficiaryListView.setAdapter(adapterSender);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void initToolBar() {
        doSearchBenifeciary();
    }

    @Override
    public void errorAlertSender(ArrayList<SenderDetailsPojo> details) {

        for (SenderDetailsPojo pojo : details) {
            message = pojo.getMessage();
        }
        if (message.contains("Remitter")) {
            btnAddBeneficiary.setVisibility(View.GONE);
            SenderMob = edtMobileNumber.getText().toString();
            addSender();
        }
    }

    @Override
    public void errorAlert(final String body) {
        progressDialogDismiss();
        if (body.contains("Data Not Found")) {

            SenderMob = edtMobileNumber.getText().toString();
            addBeneficiary();
        }
        if (body.contains("Remitter")) {
            btnAddBeneficiary.setVisibility(View.GONE);
            SenderMob = edtMobileNumber.getText().toString();
            addSender();
        }

    }

    @Override
    public String getNumber() {
        return edtMobileNumber.getText().toString();
    }

    @Override
    public void editNumberError() {

    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void doSenderRegistration() {
        iSenderRegistrationPresenter.doSenderRegistration();
    }

    @Override
    public void addBeneficiarySuccess(AddBeneficiaryPojo addBeneficiary) {
    }

    @Override
    public void otpPopup() {
        progressDialogDismiss();
        dialog = new Dialog(getActivity(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Enter OTP");
        dialog.setCanceledOnTouchOutside(false);
        edtOtp = (EditText) dialog.findViewById(R.id.editPin);
        edtOtp.setHint("Enter OTP");
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        btnResendOtp = (Button) dialog.findViewById(R.id.btnResendOtp);
        btnResendOtp.setVisibility(View.VISIBLE);
        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iAddBenificiaryPresenter.ResendOtp();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOtp.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "otp"));
                } else {
                    //iAddBenificiaryPresenter.checkOtp();
                    iBeneficiaryListPresenter.CheckSenderOtp();
                    progressD();
                    dialogDissmiss();
                }
            }
        });
        dialog.show();
    }

    private void dialogDissmiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void unableToFetchError(String message) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive " + message + " Please restart your app.");
    }

    @Override
    public void sendBankNamesResponse(ArrayList<GetBankNamesPojo> bankNames) {
        AutocompleteForBankNameAdapter autocompleteTextAdapter;
        autocompleteTextAdapter = new AutocompleteForBankNameAdapter(getActivity(), R.layout.adapter_autocomplete_text, bankNames);
        edtBankName.setThreshold(1);
        edtBankName.setAdapter(autocompleteTextAdapter);
    }

    @Override
    public void userName(String body) {
        String result = UserDetails.name.replaceAll("[\"]", "");
        edtName.setText(result);
    }

    @Override
    public void beneficiaryFailureMessage(String body) {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .showCancelButton(true)
                .setContentText(body)
                .setCancelText("Cancel")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void errorAlertVarification(String body) {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .showCancelButton(true)
                .setContentText(body)
                .setCancelText("Cancel")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        btnVerify.setVisibility(View.VISIBLE);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void dissmissDialog() {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
    }

    @Override
    public void showPdialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void showdetails(ArrayList<SenderDetailsPojo> details) {
        progressDialogDismiss();
        for (SenderDetailsPojo pojo : details) {
            if (pojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                doSearchBenifeciary();
                txtSenderName.setText(pojo.getFirstName() + "");
                txtMobileNo.setText(pojo.getMobileNumber() + "");
                txtUtilized.setText("₹ " + pojo.getConsumedlimit());
                txtRemainig.setText("₹ " + pojo.getRemaininglimit());
            } else if (pojo.getStatus().equalsIgnoreCase("FAILURE")) {
                if (pojo.getMessage().contains("Remitter")) {
                    SenderMob = edtMobileNumber.getText().toString();
                    errorAlert(pojo.getMessage());
                }
            } else if (pojo.getStatus().equalsIgnoreCase("FAILURE")) {
                if (pojo.getMessage().contains("OTP limit reached")) {
                    errorAlert(pojo.getMessage());
                }
            }
        }
    }


    @Override
    public void refreshBalance() {
        iBeneficiaryListPresenter.getSenderDetails();
    }

    @Override
    public void initToolbal() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                iAvailableBalancePresenter.getAvailableBalance();
            }
        }, 2000);


    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                // CurrentBalance = obj.getCurrentBalance();
                // txtAccountBalance.setText("₹ " + String.valueOf(obj.getCurrentBalance()));
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
