package in.omkarpayment.app.MoneyTransferpkg.AddBenificiary;


import java.util.ArrayList;

import in.omkarpayment.app.json.GetBankNamesPojo;
import in.omkarpayment.app.json.moneyTx.AddBeneficiaryPojo;

/**
 * Created by consisty on 12/10/17.
 */

public interface IAddBenificiaryView {

    void edtNameError();

    void edtIFSCError();

    void edtAccountNumberError();


    void edtSenderIDError();

    void edtBankError();

    void edtMobileNumberError();

    String getSenderMobileNumber();

    String getName();

    String getIFSCCode();

    String getBankName();

    String getAccountNumber();

    String getSenderID();

    void doBeneficiaryRegistration();

    void successAlert(String body);

    void addBeneSuccessResponse(AddBeneficiaryPojo addBeneficiaryPojo);


    String getOtp();

    void errorAlert(String body);

    void showPDialog();

    void dismissPDialog();

    void addBeneficiarySuccess(AddBeneficiaryPojo addBeneficiary);

    void otpPopup();


    void sendBankNamesResponse(ArrayList<GetBankNamesPojo> bankNames);

    void userName(String body);

    void beneficiaryFailureMessage(String body);

    void errorAlertVarification(String body);
}
