package in.omkarpayment.app.MoneyTransferpkg.Registrationpkg;


import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.omkarpayment.app.MoneyTransferpkg.validationpkg.SenderDetailsPojo;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

;


public class SenderRegistrationFragment extends Fragment implements ISenderRegistrationView {

    static String mobilenumber;
    ISenderRegistrationPresenter iSenderRegistrationPresenter;
    View rovView;
    AlertImpl alert;
    CustomProgressDialog progressDialog;
    Button btnSenderRegistration;
    EditText edtMobileNumber,edtLastName,edtFirstName,edtAddress,edtPincode;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        rovView = inflater.inflate(R.layout.moneytransferregistration, container, false);

        alert = new AlertImpl(getContext());
        iSenderRegistrationPresenter = new SenderRegistrationPresenterImpl(this);
        progressDialog = new CustomProgressDialog(getContext());

        edtMobileNumber = (EditText) rovView.findViewById(R.id.edtSenderMobileNumber);
        edtLastName = (EditText) rovView.findViewById(R.id.edtLastName);
        edtFirstName = (EditText) rovView.findViewById(R.id.edtFirstName);
        edtAddress = (EditText) rovView.findViewById(R.id.edtAddress);
        //edtPincode = (EditText) rovView.findViewById(R.id.edtPincode);

        if (mobilenumber!=null){
            mobilenumber=getArguments().getString("mobilenumber");
            edtMobileNumber.setText(mobilenumber);
        }
       btnSenderRegistration = (Button)rovView.findViewById(R.id.btnSenderR);
        btnSenderRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iSenderRegistrationPresenter.validateSenderDetails();
            }
        });

        return rovView;
    }
    @Override
    public void edtFirstNameError() { 
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "first name"));

    }

    @Override
    public void SenderFailuerResponse(SenderDetailsPojo senderDetailsPojo) {

    }

    @Override
    public void edtLastNameError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "last name"));

    }
    @Override
    public void edtMobileNumberError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "mobile number"));

    }
    @Override
    public void edtAddressError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "address"));

    }
    @Override
    public void edtPincodeError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "valid pincode"));

    }
    private void clear(){
        edtMobileNumber.setText("");
        edtFirstName.setText("");
        edtLastName.setText("");
        edtAddress.setText("");
        edtPincode.setText("");
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
        clear();
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
        clear();
    }

    @Override
    public void SenderSuccessRespaonse(String msg) {
        Log.i("sender message",msg);
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void senderSucessResponsebody(SenderIDDetailsPojo senderIDDetailsPojo) {

    }



    @Override
    public String getSenderAddMobileNumber() {
        Typeface roboto = Typeface.createFromAsset(getActivity().getAssets(),
                "font/RobotoCondensed-Light.ttf"); //use this.getAssets if you are calling from an Activity
        edtMobileNumber.setTypeface(roboto);
        return edtMobileNumber.getText().toString().trim();
    }

    @Override
    public String getFirstName() {
        Typeface roboto = Typeface.createFromAsset(getActivity().getAssets(),
                "font/RobotoCondensed-Light.ttf"); //use this.getAssets if you are calling from an Activity
        edtFirstName.setTypeface(roboto);
        return edtFirstName.getText().toString().trim();
    }
    @Override
    public String getLastName() {
        Typeface roboto = Typeface.createFromAsset(getActivity().getAssets(),
                "font/RobotoCondensed-Light.ttf"); //use this.getAssets if you are calling from an Activity
        edtFirstName.setTypeface(roboto);
        return edtFirstName.getText().toString().trim();
    }
    @Override
    public String getAddress() {
        Typeface roboto = Typeface.createFromAsset(getActivity().getAssets(),
                "font/RobotoCondensed-Light.ttf"); //use this.getAssets if you are calling from an Activity
        edtFirstName.setTypeface(roboto);
        return edtFirstName.getText().toString().trim();
    }
    @Override
    public String getPincode() {
      String Pincode = UserDetails.Pincode;
        return Pincode;
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void doSenderRegistration() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Please Confirm")
                .setCustomImage(R.mipmap.ic_launcher)
                .showCancelButton(true)
                .setCancelText("Cancel")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        iSenderRegistrationPresenter.doSenderRegistration();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

}
