package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.omkarpayment.app.MoneyTransferpkg.AddBenificiary.AddBenificiaryActivity;
import in.omkarpayment.app.MoneyTransferpkg.MoneyTransfer.IMoneyTransfer;
import in.omkarpayment.app.R;
import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.model.CheckOtp;
import in.omkarpayment.app.model.DeleteBenefeciaryAsync;
import in.omkarpayment.app.model.DeleteBenefeciaryForOtpRequestAsync;
import in.omkarpayment.app.model.DeleteBenefeciaryWithOtpAsync;
import in.omkarpayment.app.model.IResendOTP;
import in.omkarpayment.app.model.MoneyTransferAsync;
import in.omkarpayment.app.model.ResendOtp;
import in.omkarpayment.app.userContent.AllMessages;


/**
 * Created by ${user} on 12/9/17.
 */

public class BeneFiciaryListAdapter extends RecyclerView.Adapter<BeneFiciaryListAdapter.MyViewHolder> implements IMoneyTransfer, IResendOTP {
    public static int i;
    public static String[] spTypeMoneyTransfer = {"IMPS", "NEFT"};
    public static String SenderIDNumber, BenefeciaryIDNumber;
    public static String BenID, SendID;
    public ProgressDialog pDialog;
    ArrayAdapter<String> spAdapter;
    Context context;
    EditText edtOtp;
    Button btnSubmit, btnResendOtp, btnReseOtp;
    BeneficiaryListFragment beneficiaryListFragment;
    AddBenificiaryActivity addBenificiaryFragment;
    Cryptography_Android data = new Cryptography_Android();
    String Type, Rupees, BenName, BenBank;
    EditText edtAmount, editConfirmAmount, editMPin;
    TextView txtBank, txtReceiver, txtIfsc;
    Spinner spType;
    Dialog dialog;
    IRefreshInterface iRefreshInterface;
    private ArrayList<BeneficiaryListpojo> list;

    public BeneFiciaryListAdapter(Context context, BeneficiaryListFragment beneficiaryListFragment, ArrayList<BeneficiaryListpojo> list) {
        this.list = list;
        this.context = context;
        this.beneficiaryListFragment = beneficiaryListFragment;
    }

    public BeneFiciaryListAdapter(BeneficiaryListFragment beneficiaryListFragment) {
        this.beneficiaryListFragment = beneficiaryListFragment;
    }

    public BeneFiciaryListAdapter(AddBenificiaryActivity addBenificiaryFragment) {
        this.addBenificiaryFragment = addBenificiaryFragment;
    }

    @Override
    public BeneFiciaryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beneficiary_list_adapter, parent, false);
        return new BeneFiciaryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BeneFiciaryListAdapter.MyViewHolder holder, final int position) {
        final BeneficiaryListpojo pojo = list.get(position);
        holder.txtName.setText(pojo.getName());
        holder.txtBankName.setText(pojo.getBank());
        holder.txtAccountNumber.setText(pojo.getAccountNo());
        holder.txtIfscCode.setText(pojo.getIFSC());
        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pojo.getIsValidate().equalsIgnoreCase("0")) {
                 /*   new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Message")
                            .setCustomImage(R.mipmap.ic_launcher)
                            .setContentText("Verify OTP first")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();*/
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.warning_dialog);
                    TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                    text.setText("Verify OTP first");
                    Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                    Button close = (Button) dialog.findViewById(R.id.btnNo);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            resendOtpRequest();
                            dialog.dismiss();
                        }
                    });
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    transferMoney(pojo.getAccountNo(), pojo.getBank(), pojo.getSenderID(),
                            pojo.getName(), pojo.getBenficiaryID(), pojo.getMobileNumber(), pojo.getIFSC());
                    BenID = pojo.getBenficiaryID();
                    SendID = pojo.getSenderID();
                }
            }


        });
        holder.imgVerfied.setVisibility(View.GONE);
        holder.imgNotVerfied.setVisibility(View.GONE);
        if (pojo.getIsValidate() != null) {
            if (pojo.getIsValidate().equals("1")) {
                holder.imgVerfied.setVisibility(View.VISIBLE);
            } else if (pojo.getIsValidate().equals("0")) {
                holder.imgNotVerfied.setVisibility(View.VISIBLE);
                Animation animation = new AlphaAnimation(1, 0);
                animation.setDuration(500);
                animation.setInterpolator(new LinearInterpolator());
                animation.setRepeatCount(Animation.INFINITE);
                animation.setRepeatMode(Animation.REVERSE);
                holder.imgNotVerfied.startAnimation(animation);
                holder.imgNotVerfied.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BenID = pojo.getBenficiaryID();
                        SendID = pojo.getSenderID();
                        resendotpFromList();
                    }
                });
            }
        }

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pojo.getIsValidate().equals(null) && pojo.getIsValidate().equals("0")) {
                    deleteWithoutValidate(pojo.getSenderID(), pojo.getBenficiaryID());
                } else if (!pojo.getIsValidate().equalsIgnoreCase(null) && pojo.getIsValidate().equals("1")) {
                    BenID = pojo.getBenficiaryID();
                    SendID = pojo.getSenderID();
                    otpScreen(pojo.getSenderID(), pojo.getBenficiaryID());
                }
            }


        });
    }

    private void otpScreen(String senderID, String benficiaryID) {
        SenderIDNumber = senderID;
        BenefeciaryIDNumber = benficiaryID;
        String[] request = {senderID, benficiaryID};
        runDeleteForOtp(request);

    }

    private void runDeleteForOtp(String[] request) {
        DeleteBenefeciaryForOtpRequestAsync deleteBenefeciaryForotpAsync = new DeleteBenefeciaryForOtpRequestAsync();
        deleteBenefeciaryForotpAsync.delegate = this;
        deleteBenefeciaryForotpAsync.execute(request);
    }

    private void checkOtp(String senderID, String benficiaryID, String otp) {
        String[] request = {senderID, benficiaryID, otp};
        runDeleteWithOtpAsync(request);

    }

    private void runDeleteWithOtpAsync(String[] request) {
        DeleteBenefeciaryWithOtpAsync deleteBenefeciaryWithotpAsync = new DeleteBenefeciaryWithOtpAsync();
        deleteBenefeciaryWithotpAsync.delegate = this;
        deleteBenefeciaryWithotpAsync.execute(request);
    }

    private void deleteWithoutValidate(String senderID, String benficiaryID) {
        String[] request = {senderID, benficiaryID};
        runDeleteWithoutValidateAsync(request);
    }

    private void runDeleteWithoutValidateAsync(String[] request) {
        DeleteBenefeciaryAsync deleteBenefeciaryAsync = new DeleteBenefeciaryAsync();
        deleteBenefeciaryAsync.delegate = this;
        deleteBenefeciaryAsync.execute(request);
    }

    @SuppressLint("NewApi")
    private void transferMoney(final String accountNumber, final String bankname, final String senderId, final String Name, final String BeneficiaryId, String senderMobileNumber, final String Ifsc) {

        dialog = new Dialog(context, android.R.style.Theme_Holo_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.money_transfer_screen);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Money Transfer");
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        //text.setText(status);
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please Wait....");
        pDialog.setIndeterminate(true);
        pDialog.setIndeterminateDrawable(context.getDrawable(R.mipmap.ic_launcher));
        pDialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        pDialog.dismiss();
        dialog.show();
        edtAmount = (EditText) dialog.findViewById(R.id.edtAmount);
        editMPin = (EditText) dialog.findViewById(R.id.editMPin);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnMoneyTrans);
        TextView txtAccount = (TextView) dialog.findViewById(R.id.txtDialogAccount);
        txtBank = (TextView) dialog.findViewById(R.id.txtDialogBank);
        txtBank.setSelected(true);
        txtBank.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txtBank.setMarqueeRepeatLimit(5);
        TextView txtSenderName = (TextView) dialog.findViewById(R.id.txtDSender);
        txtReceiver = (TextView) dialog.findViewById(R.id.txtDReceiver);
        editConfirmAmount = (EditText) dialog.findViewById(R.id.editConfirmAmount);
        txtReceiver.setSelected(true);
        txtReceiver.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txtReceiver.setMarqueeRepeatLimit(5);
        txtIfsc = (TextView) dialog.findViewById(R.id.txtDialogIfsc);
        spType = (Spinner) dialog.findViewById(R.id.spTypeMoneyTransfer);
        spAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spTypeMoneyTransfer);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(spAdapter);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Type = spTypeMoneyTransfer[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
        txtAccount.setText(accountNumber);
        txtBank.setText(bankname);
        txtSenderName.setText(senderId);
        txtReceiver.setText(Name);
        txtIfsc.setText(Ifsc);
        BenName = txtReceiver.getText().toString();
        BenBank = txtBank.getText().toString();
        final String MobNo = senderMobileNumber;
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtAmount.getText().toString().isEmpty()) {
                    edtAmount.setError("Please enter amount");
                } else if (editConfirmAmount.getText().toString().isEmpty()) {
                    editConfirmAmount.setError("Please enter confirm amount");
                } else if (editMPin.getText().toString().isEmpty()) {
                    editMPin.setError("Please enter m pin");
                } else if (!(edtAmount.getText().toString().trim()).equals(editConfirmAmount.getText().toString().trim())) {
                    editConfirmAmount.setError("please enter amount and confirm amount should be same");
                } else {
                    Double amount = Double.parseDouble(edtAmount.getText().toString());
                    String amt = String.valueOf(amount);
                    String[] parts = amt.split("\\.");
                    Rupees = parts[0];
                    if (amount <= 25000) {
                        String[] request = {BeneficiaryId, getType(), Rupees, BeneficiaryListFragment.SenderMob,RechargePin()};
                        runMoneyTransferAsync(request);
                        pDialog.show();
                    } else {
                        edtAmount.setError("Please enter valid amount");
                    }

                }
            }

        });
    }


    boolean isValidIFSC(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{4}[0-9]{7}");

        Matcher matcher = pattern.matcher(target);

        if (matcher.matches()) {
            return true;
        } else {
            txtIfsc.setText("Please enter valid IFSC code");
            return false;
        }
    }

    private void resendotpFromList() {
      /*  new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText("Beneficiary not verified.\nDo you want to resend OTP?")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        resendOtpRequest();
                        sweetAlertDialog.dismiss();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();*/
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText("Beneficiary not verified.\\nDo you want to resend OTP?");
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button close = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtpRequest();
                dialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void resendOtpRequest() {
        otpPopup();
        //String[] request = {SendID, BenID};
        ResendOtp resendOtp = new ResendOtp();
        resendOtp.delegate = (IResendOTP) this;
        resendOtp.execute(SendID, BenID);
    }

    private void runMoneyTransferAsync(String[] request) {
        MoneyTransferAsync moneyTransfer = new MoneyTransferAsync();
        moneyTransfer.delegate = this;
        moneyTransfer.execute(request);
    }

    public String getType() {
        return Type;
    }

    public String RechargePin() {
        return editMPin.getText().toString().trim();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void getSuccessMessageTransfer(String body) {
        progressDialogDismiss();
        DialogBoxDissmiss();
        String message = null;

        try {
            message = data.Decrypt(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (message.contains("Money transfer successful")) {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog);
            TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
            text.setText(message + "\n" + "Amount : ₹ " + Rupees);
            Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    beneficiaryListFragment.doSearchBenifeciary();
                    beneficiaryListFragment.refreshBalance();
                    DialogBoxDissmiss();
                    dialog.dismiss();
                }
            });

            dialog.show();
        } else {
           /* new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Message")
                    .setCustomImage(R.mipmap.ic_launcher)
                    .setContentText(message)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            beneficiaryListFragment.doSearchBenifeciary();
                            ((BeneficiaryListFragment) context).initToolBar();
                            beneficiaryListFragment.refreshBalance();
                            DialogBoxDissmiss();
                        }
                    })
                    .show();*/
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog);
            TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
            text.setText(message);
            Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    beneficiaryListFragment.doSearchBenifeciary();
                    // ((BeneficiaryListFragment) context).initToolBar();
                    // beneficiaryListFragment.refreshBalance();
                    DialogBoxDissmiss();
                    dialog.dismiss();
                }
            });

            dialog.show();

        }


    }

    private void progressBarShow() {
        if (pDialog == null) {
            pDialog.show();
        }
    }

    private void DialogBoxDissmiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void progressDialogDismiss() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    @Override
    public void getErrorMessage(final String body) {
        String Response = "";

        try {
            Response = data.Decrypt(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        progressDialogDismiss();
        DialogBoxDissmiss();

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(Response);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                beneficiaryListFragment.doSearchBenifeciary();
            }
        });
        dialog.show();

    }

    public void otpPopup() {
        progressDialogDismiss();


        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please Wait....");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        //dialog.setCanceledOnTouchOutside(false);
        pDialog.dismiss();


        dialog = new Dialog(context, R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Enter OTP");
        edtOtp = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        btnResendOtp = (Button) dialog.findViewById(R.id.btnResendOtp);
        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogBoxDissmiss();
                resendOtpRequest();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOtp.getText().toString().equals("")) {
                    edtOtp.setError(String.format(AllMessages.pleaseEnter, "otp"));
                } else {
                    dialog.dismiss();
                    String[] request = {edtOtp.getText().toString()};
                    ckeckOtp(request);
                }
            }
        });
        dialog.show();
    }


    public void ckeckOtp(String[] request) {
        //progressBarShow();
        Log.i("In Checking OTP", "here");
        CheckOtp chkotp = new CheckOtp();
        chkotp.delegate = this;
        chkotp.execute(request);
        //progressDialogDismiss();
        Log.i("In Checking OTP", "Bye");
    }

    @Override
    public void getOtpForDeletBen(String body) {
        dialog = new Dialog(context, R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Enter OTP");
        dialog.setCanceledOnTouchOutside(false);
        edtOtp = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        btnReseOtp = (Button) dialog.findViewById(R.id.btnResendOtp);
        dialog.show();
        btnReseOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogBoxDissmiss();
                resendOtpRequest();

            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOtp.getText().toString().equals("")) {
                    edtOtp.setError("Enter otp first");
                } else {
                    checkOtp(SenderIDNumber, BenefeciaryIDNumber, edtOtp.getText().toString());
                    DialogBoxDissmiss();
                }
            }
        });

    }

    @Override
    public void getRequestForOtp(String body) {
        progressDialogDismiss();
     /*   new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(body + "\nDo you want to resend OTP?")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        resendOtpRequest();
                        otpPopup();
                        sweetAlertDialog.dismiss();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();*/
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(body + "\nDo you want to resend OTP?");
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button close = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtpRequest();
                otpPopup();
                dialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void getResendOtp() {
        Log.i("getResendOtp ", "here");
    }

    @Override
    public void getSuccessOtpMsg(String body) {
      /*  new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(body)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        beneficiaryListFragment.doSearchBenifeciary();
                        if (dialog != null) {
                            dialog.dismiss();
                            ((BeneficiaryListFragment) context).initToolBar();
                        }
                    }
                })
                .show();*/
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(body);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beneficiaryListFragment.doSearchBenifeciary();
                if (dialog != null) {
                    dialog.dismiss();
                    //((BeneficiaryListFragment) context).initToolBar();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void getErrorResendOtp(String body) {
      /*  new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(body)
                .setCustomImage(R.mipmap.ic_launcher)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();

                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                })
                .show();*/
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(body);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button close = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtBankName, txtName, txtAccountNumber, txtIfscCode, btnAction, btnDelete;
        public Button btnTransfer;
        ImageView imgVerfied, imgNotVerfied;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtName = (TextView) rowView.findViewById(R.id.txtName);
            txtBankName = (TextView) rowView.findViewById(R.id.txtBankName);
            txtAccountNumber = (TextView) rowView.findViewById(R.id.txtAccountNumber);
            txtIfscCode = (TextView) rowView.findViewById(R.id.txtIfscCode);
            btnAction = (TextView) rowView.findViewById(R.id.txtAction);
            btnDelete = (TextView) rowView.findViewById(R.id.txtdelete);
            imgVerfied = (ImageView) rowView.findViewById(R.id.imgVerfied);
            imgNotVerfied = (ImageView) rowView.findViewById(R.id.imgNotVerfied);
        }
    }
}