package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by consisty on 3/1/18.
 */

public class SenderDetailsPojo {
    @SerializedName("FirstName")
    @Expose
    private Object firstName;
    @SerializedName("LastName")
    @Expose
    private Object lastName;
    @SerializedName("EasyWalletId")
    @Expose
    private Object easyWalletId;
    @SerializedName("EasyWalletType")
    @Expose
    private Object easyWalletType;
    @SerializedName("State")
    @Expose
    private Object state;
    @SerializedName("City")
    @Expose
    private Object city;
    @SerializedName("Pincode")
    @Expose
    private Object pincode;
    @SerializedName("MobileNo")
    @Expose
    private Object mobileNo;
    @SerializedName("UserID")
    @Expose
    private Object userID;
    @SerializedName("Statuscode")
    @Expose
    private Object statuscode;
    @SerializedName("BenficiaryID")
    @Expose
    private String benficiaryID;
    @SerializedName("SenderID")
    @Expose
    private Object senderID;
    @SerializedName("Address")
    @Expose
    private Object address;
    @SerializedName("MobileNumber")
    @Expose
    private Object mobileNumber;
    @SerializedName("Consumedlimit")
    @Expose
    private Object consumedlimit;
    @SerializedName("Remaininglimit")
    @Expose
    private Object remaininglimit;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("IsVerified")
    @Expose
    private Object isVerified;

    public Object getFirstName() {
        return firstName;
    }

    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Object getEasyWalletId() {
        return easyWalletId;
    }

    public void setEasyWalletId(Object easyWalletId) {
        this.easyWalletId = easyWalletId;
    }

    public Object getEasyWalletType() {
        return easyWalletType;
    }

    public void setEasyWalletType(Object easyWalletType) {
        this.easyWalletType = easyWalletType;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getPincode() {
        return pincode;
    }

    public void setPincode(Object pincode) {
        this.pincode = pincode;
    }

    public Object getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(Object mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Object getUserID() {
        return userID;
    }

    public void setUserID(Object userID) {
        this.userID = userID;
    }

    public Object getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(Object statuscode) {
        this.statuscode = statuscode;
    }

    public String getBenficiaryID() {
        return benficiaryID;
    }

    public void setBenficiaryID(String benficiaryID) {
        this.benficiaryID = benficiaryID;
    }

    public Object getSenderID() {
        return senderID;
    }

    public void setSenderID(Object senderID) {
        this.senderID = senderID;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Object mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Object getConsumedlimit() {
        return consumedlimit;
    }

    public void setConsumedlimit(Object consumedlimit) {
        this.consumedlimit = consumedlimit;
    }

    public Object getRemaininglimit() {
        return remaininglimit;
    }

    public void setRemaininglimit(Object remaininglimit) {
        this.remaininglimit = remaininglimit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public Object getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Object isVerified) {
        this.isVerified = isVerified;
    }
}
