package in.omkarpayment.app.MoneyTransferpkg.Registrationpkg;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import in.omkarpayment.app.MoneyTransferpkg.validationpkg.SenderDetailsPojo;
import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by consisty on 12/10/17.
 */

public class SenderRegistrationPresenterImpl implements ISenderRegistrationPresenter {

    ISenderRegistrationView iSenderRegistrationView;

    public SenderRegistrationPresenterImpl(ISenderRegistrationView iSenderRegistrationView) {
        this.iSenderRegistrationView = iSenderRegistrationView;
    }

    @Override
    public void validateSenderDetails() {
        if (iSenderRegistrationView.getFirstName().equals("")) {
            iSenderRegistrationView.edtFirstNameError();
        }/* else if (iSenderRegistrationView.getLastName().equals("")) {
            iSenderRegistrationView.edtLastNameError();
        }*//*else if (iSenderRegistrationView.getMobileNumber().equals("")||iSenderRegistrationView.getMobileNumber().length()!=10){
            iSenderRegistrationView.edtMobileNumberError();
        }*//* else if (iSenderRegistrationView.getAddress().equals("")) {
            iSenderRegistrationView.edtAddressError();
        } else if (iSenderRegistrationView.getPincode().equals("")) {
            iSenderRegistrationView.edtPincodeError();
        } else*/
        {
            iSenderRegistrationView.doSenderRegistration();
        }

    }

    public void doSenderRegistration() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("FirstName", iSenderRegistrationView.getFirstName());
            //loginrequest.put("LastName", "Nihal");
            loginrequest.put("MobileNumber", iSenderRegistrationView.getSenderAddMobileNumber());
            loginrequest.put("Address", "Nagpur");
            loginrequest.put("Pincode", "413213");


            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            Log.i("Sender R Request", jsonrequest);
            WebServiceModel model = new WebServiceModel();
            model.delegateSenderRegistration = this;
            model.webserviceMethod(encryptString, keyData, "SenderRegistration");


            showDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decryptSenderRegistrationResponse(String encryptedResponse) {
        Log.i("SenderRResponse", encryptedResponse);
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            // iSenderRegistrationView.successAlert(body);
            Gson gson = new Gson();
            SenderIDDetailsPojo senderIDDetailsPojo = gson.fromJson(body, SenderIDDetailsPojo.class);
            if (body.contains("OTP sent successfully")) {

                iSenderRegistrationView.senderSucessResponsebody(senderIDDetailsPojo);
                return;
            }
            iSenderRegistrationView.SenderSuccessRespaonse(body);
            Log.i("Decrypted body", body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void SenderRegistrationFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Sender Fail Respo", body);
            Gson gson = new Gson();
            SenderDetailsPojo senderDetailsPojo = gson.fromJson(body, SenderDetailsPojo.class);
            iSenderRegistrationView.SenderFailuerResponse(senderDetailsPojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissPDialog() {
        iSenderRegistrationView.dismissPDialog();
    }

    @Override
    public void errorAlert(String somethingWrong) {
        iSenderRegistrationView.errorAlert(somethingWrong);
    }

    private void showDialog() {
        iSenderRegistrationView.showPDialog();
    }
}
