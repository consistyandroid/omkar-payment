package in.omkarpayment.app.MoneyTransferpkg.validationpkg;

import java.util.ArrayList;

/**
 * Created by consisty on 12/10/17.
 */

public interface IBeneficiaryListView {


    String getMobileNumber();

    void edtMobileNumberError();

    void searchBeneficiaries();

    void SenderFailuerResponse(SenderDetailsPojo senderDetailsPojo);

    void beneficiaryListShow(ArrayList<BeneficiaryListpojo> beneficiaryList);

    void errorAlert(String body);

    void dissmissDialog();

    String getOtp();

    void showPdialog();

    void unableToFetchError(String message);

    void SenderSuccessRespaonse(String msg);

    void showdetails(ArrayList<SenderDetailsPojo> details);

    void errorAlertSender(ArrayList<SenderDetailsPojo> details);
}
