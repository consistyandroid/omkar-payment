package in.omkarpayment.app.MoneyTransferpkg.AddBenificiary;

/**
 * Created by consisty on 12/10/17.
 */

public interface IAddBenificiaryPresenter {
    void validateAddBeneficiaryFields();

    void doBeneficiaryRegistration();

    void decryptAddBeneficiaryResponse(String encryptedResponse);

    void decryptAddBeneficiaryFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void checkOtp();

    void searchBank();

    void decryptOTPResponse(String encryptedResponse);

    void decryptOTPFailureResponse(String encryptedResponse);

    void errorAlert(String somethingWrong);


    void decryptGetBankNameResponse(String encryptedResponse);

    void decryptGetBankNameFailureResponse(String encryptedResponse);

    void getBankNames();

    void getDefaultIFSCCode(String ifsc);

    void validateUserVerification(String bankName, String ifsc, String accountNumber, String SenderID, String DeviceID);

    void VerificationResponseResponse(String encryptedResponse);

    void VerificationFailureResponseResponse(String encryptedResponse);

    void ResendOtp();
}
