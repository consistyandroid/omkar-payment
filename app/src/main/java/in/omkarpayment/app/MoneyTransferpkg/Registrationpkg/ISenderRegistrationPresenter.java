package in.omkarpayment.app.MoneyTransferpkg.Registrationpkg;

/**
 * Created by consisty on 12/10/17.
 */

public interface ISenderRegistrationPresenter {
    void validateSenderDetails();

    void doSenderRegistration();

    void decryptSenderRegistrationResponse(String encryptedResponse);

    void SenderRegistrationFailureResponse(String encryptedResponse);

    void dismissPDialog();

    void errorAlert(String somethingWrong);
}
