package in.omkarpayment.app.MoneyTransferpkg.MoneyTransfer;

/**
 * Created by consisty on 13/10/17.
 */

public interface IMoneyTransfer {


    void getSuccessMessageTransfer(String body) ;

    void getErrorMessage(String body);

    void getOtpForDeletBen(String body);

    void getRequestForOtp(String body);


}
