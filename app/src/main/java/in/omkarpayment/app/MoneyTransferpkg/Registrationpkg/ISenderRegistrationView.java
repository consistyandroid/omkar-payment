package in.omkarpayment.app.MoneyTransferpkg.Registrationpkg;

import in.omkarpayment.app.MoneyTransferpkg.validationpkg.SenderDetailsPojo;

/**
 * Created by consisty on 12/10/17.
 */

public interface ISenderRegistrationView {
    void edtLastNameError();

    void edtMobileNumberError();

    void edtFirstNameError();
void SenderFailuerResponse(SenderDetailsPojo senderDetailsPojo);
    void edtAddressError();

    void edtPincodeError();

    void errorAlert(String msg);

    void successAlert(String msg);

    void SenderSuccessRespaonse(String msg);

    void senderSucessResponsebody(SenderIDDetailsPojo senderIDDetailsPojo);

    String getSenderAddMobileNumber();

    String getFirstName();

    String getLastName();

    String getAddress();

    String getPincode();

    void showPDialog();

    void dismissPDialog();

    void doSenderRegistration();
}
