package in.omkarpayment.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.fragment.OptionsFragment;
import in.omkarpayment.app.fragment.RechargeFragment;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.Notification;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.main.IMainPresenter;
import in.omkarpayment.app.main.IMainView;
import in.omkarpayment.app.main.MainPresenter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

public class MainActivity extends AppCompatActivity implements IMainView, IAvailableBalanceView {
    CustomProgressDialog pDialog;
    ImageView btnAutoRefresh;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    IMainPresenter iMainPresenter;
    TextView txtUsername, txtBalance, txtMobNo, txtnotification;
    boolean doubleBackToExitPressedOnce = false;
    Context context = this;
    Double CurrentBalance;
    ViewPager viewPager;
    AlertImpl alert;
    private TabLayout tabLayout;
    private Toolbar toolbar;

    public void refreshBalance() {
        txtBalance = (TextView) findViewById(R.id.txtBalance);
        txtBalance.setText(String.valueOf(UserDetails.UserBalance));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iMainPresenter = new MainPresenter(this);
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        txtnotification = (TextView) findViewById(R.id.txtNotification);
        alert = new AlertImpl(this);

        btnAutoRefresh = (ImageView) findViewById(R.id.btnAutoRefresh);

        txtUsername = (TextView) findViewById(R.id.txtUserName);
        txtBalance = (TextView) findViewById(R.id.txtBalance);
        txtUsername.setText(UserDetails.Username);
/*        txtMobNo = (TextView) findViewById(R.id.txtNo);
       // iAvailableBalancePresenter.getAvailableBalance();
       // txtMobNo.setText(UserDetails.MobileNumber);
        //   txtBalance.setText(String.valueOf(UserDetails.UserBalance));

        //  txtToolbarBalance.setText(String.valueOf(UserDetails.UserBalance));*/
        txtnotification = (TextView) findViewById(R.id.txtNotification);
        txtnotification.setSelected(true);
        txtnotification.setEllipsize(TextUtils.TruncateAt.MARQUEE);

        txtnotification.setSingleLine(true);

        txtnotification.setSelected(true);

        iMainPresenter.getNotification();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        btnAutoRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate);
                btnAutoRefresh.startAnimation(animation);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // iAvailableBalancePresenter.getAvailableBalance();
                    }
                }, 2000);
            }
        });
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void Statusresponse(ArrayList<Notification> notification) {
        String ntification = "";
        Log.i("Main", "received");
        for (Notification obj : notification) {
            try {
                ntification = String.valueOf(obj.getNotificationText());
                txtnotification.setText(ntification);
            } catch (Exception e) {

            }

        }
    }

    private void setupTabIcons() {
        try {
            if (UserDetails.UserType.equals(UserType.Retailer)) {
                TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabTwo.setText("Recharge");
                tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.mobile_icon, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabTwo);

          /*      TextView tab = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tab.setText("Money Tx");
                tab.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.more_icon, 0, 0);
                tabLayout.getTabAt(1).setCustomView(tab);
*/
                TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabThree.setText("Options Menu");
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.more_icon, 0, 0);
                tabLayout.getTabAt(1).setCustomView(tabThree);
            } else if (UserDetails.UserType.equals(UserType.Distributor)) {
                TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabThree.setText("Options Menu");
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.more_icon, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabThree);
            } else if (UserDetails.UserType.equals(UserType.Admin)) {
                TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabThree.setText("Options Menu");
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.more_icon, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabThree);
            } else if (UserDetails.UserType.equals(UserType.APIUSER)) {
                TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabThree.setText("Options Menu");
                tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.more_icon, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabThree);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        try {
            if (UserDetails.UserType.equals(UserType.Retailer)) {
                adapter.addFrag(new RechargeFragment(), "");
              //  adapter.addFrag(new BeneficiaryListFragment(), "");
                adapter.addFrag(new OptionsFragment(), "");
            } else if (UserDetails.UserType.equals(UserType.Distributor)) {
                adapter.addFrag(new OptionsFragment(), "");
            } else if (UserDetails.UserType.equals(UserType.Admin)) {
                adapter.addFrag(new OptionsFragment(), "");
            } else if (UserDetails.UserType.equals(UserType.APIUSER)) {
                adapter.addFrag(new OptionsFragment(), "");
            }

        } catch (Exception e) {

        }
        //


        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                txtBalance.setText(String.valueOf(CurrentBalance));
                alert.successAlert(String.format("Your Current Balance is: %s", String.valueOf(obj.getCurrentBalance())));
            } catch (Exception e) {
                Log.i("Exception Output", CurrentBalance.toString());

            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }

    public void updateBal() {

    }

    public void initToolBar() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                iAvailableBalancePresenter.getAvailableBalance();
            }
        }, 2000);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
