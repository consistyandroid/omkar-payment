package in.omkarpayment.app.changeMobileNumber;

/**
 * Created by lenovo on 2/14/2018.
 */

public interface IChangeMobileNumberPresenter {
    void validateField();

    void changeMobileNumber();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String errorMsg);

    void successAlert(String msg);

    void moveToLogin();

    boolean validation();

    void decryptedChangeMobileNumberResponse(String encryptedResponse);

    void decryptedChangeMobileNumberFailureResponse(String encryptedResponse);


}
