package in.omkarpayment.app.changeMobileNumber;

/**
 * Created by lenovo on 2/14/2018.
 */

public interface IChangeMobileNumberView {
    void editOldMobileNoError();

    String getOldMobileNo();

    String getNewMobileNo();

    String getConfirmMobileNo();

    void showPDialog();

    void validate();

    void dismissPDialog();

    void errorAlert(String errorMsg);

    void successAlert(String msg);

    void moveToLogin();

    void editNewMobileNoError();

    void editConfirmMobileNoError();

    void changeMobileNumber();

    void editConfirmMobileNoSameError();
}
