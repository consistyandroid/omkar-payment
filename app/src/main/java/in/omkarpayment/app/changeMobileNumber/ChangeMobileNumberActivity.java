package in.omkarpayment.app.changeMobileNumber;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import javax.xml.validation.Validator;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

public class ChangeMobileNumberActivity extends AppCompatActivity implements IChangeMobileNumberView {
    EditText edtOldMobileNo, edtNewMobileNo, edtConfirmMobileNo;
    Button btnChange;
    Context context = this;
    IChangeMobileNumberPresenter changeMobileNumberPresenter;
    CustomProgressDialog progressDialog;
    AlertImpl alert;
    Validator validator;
    LogWriter log = new LogWriter();

    NetworkState ns = new NetworkState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mobile_number);
        edtOldMobileNo = (EditText) findViewById(R.id.editOldMobilNo);
        edtOldMobileNo.setFocusable(false);
        edtOldMobileNo.setText(UserDetails.MobileNumber);
        edtNewMobileNo = (EditText) findViewById(R.id.editNewMobilNo);
        edtConfirmMobileNo = (EditText) findViewById(R.id.editConfirmMobilNo);
        btnChange = (Button) findViewById(R.id.btnChange);
        alert = new AlertImpl(this);
        changeMobileNumberPresenter = new ChangeMobileNumberPresenter(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            String title = "Change Mobile Number";
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            SpannableString set = new SpannableString(title);
            set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            getSupportActionBar().setTitle(set);
            getSupportActionBar().setIcon(R.mipmap.ic_launcher);
            final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);


        }
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(context)){
                    changeMobileNumberPresenter.validateField();
                }else {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void changeMobileNumber() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText("Please confirm you want\nto change mobile number...");

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMobileNumberPresenter.changeMobileNumber();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void editConfirmMobileNoSameError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Confirm mobile no as same as new mobile no"));

    }

    @Override
    public void editOldMobileNoError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Old mobile no"));
    }

    @Override
    public String getOldMobileNo() {
        return edtOldMobileNo.getText().toString().trim();
    }

    @Override
    public void validate() {
        // validator.validate();
    }

    @Override
    public String getNewMobileNo() {
        return edtNewMobileNo.getText().toString().trim();
    }

    @Override
    public String getConfirmMobileNo() {
        return edtConfirmMobileNo.getText().toString();
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }


    @Override
    public void dismissPDialog() {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }

    }

    @Override
    public void errorAlert(String errorMsg) {
        alert.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void moveToLogin() {

    }

    @Override
    public void editNewMobileNoError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "New mobile no"));
    }

    @Override
    public void editConfirmMobileNoError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Confirm mobile no"));
    }


}
