package in.omkarpayment.app.changeMobileNumber;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by lenovo on 2/14/2018.
 */

public class ChangeMobileNumberPresenter implements IChangeMobileNumberPresenter {
    IChangeMobileNumberView iChangeMobileNumberView;
    LogWriter log = new LogWriter();

    public ChangeMobileNumberPresenter(IChangeMobileNumberView iChangeMobileNumberView) {
        this.iChangeMobileNumberView = iChangeMobileNumberView;
    }

    @Override
    public void validateField() {
        if (validation()) {
            iChangeMobileNumberView.changeMobileNumber();
        }
    }

    public boolean validation() {
        if (iChangeMobileNumberView.getOldMobileNo().isEmpty()) {
            log.i("old no", iChangeMobileNumberView.getOldMobileNo());

            iChangeMobileNumberView.editOldMobileNoError();
            return false;
        }
        if (iChangeMobileNumberView.getNewMobileNo().isEmpty()) {
            iChangeMobileNumberView.editNewMobileNoError();
            return false;
        }
        if (iChangeMobileNumberView.getConfirmMobileNo().isEmpty()) {
            iChangeMobileNumberView.editConfirmMobileNoError();
            return false;
        }
        if (!iChangeMobileNumberView.getConfirmMobileNo().equals(iChangeMobileNumberView.getNewMobileNo())) {
            iChangeMobileNumberView.errorAlert("New Mobile No And Confirm Mobile No Should be Same");
            return false;
        }
        return true;
    }

    @Override
    public void changeMobileNumber() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();

        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            //loginrequest.put("OldPassword", iChangeMobileNumberView.getOldMobileNo());
            loginrequest.put("MobileNo", iChangeMobileNumberView.getNewMobileNo());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);


            WebServiceModel model = new WebServiceModel();
            model.delegateChangeMobileNoResponse = this;
            model.webserviceMethod(encryptString, keyData, "ChangeMobileNo");
            showPDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @Override
    public void showPDialog() {
        iChangeMobileNumberView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iChangeMobileNumberView.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        iChangeMobileNumberView.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {
        iChangeMobileNumberView.successAlert(msg);
    }

    @Override
    public void moveToLogin() {

    }

    @Override
    public void decryptedChangeMobileNumberResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("body", body);
            iChangeMobileNumberView.successAlert(body);
        } catch (Exception e) {
            iChangeMobileNumberView.errorAlert(body);
        }

    }

    @Override
    public void decryptedChangeMobileNumberFailureResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            iChangeMobileNumberView.errorAlert(body);

            log.i("ChangePasswordFailureResponse", body.toString());

        } catch (Exception e) {
            iChangeMobileNumberView.errorAlert(body);
        }
    }


}
