package in.omkarpayment.app.balanceTransfer;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;
import in.omkarpayment.app.userContent.AllMessages;

/**
 * Created by Krish on 12-Jul-17.
 */

public class TransferPresenter implements ITransferPresenter {
    ITransferView iBalanceTransferView;
    LogWriter log = new LogWriter();
    Double amount = null, senderOpBalance = null, dmrBalance = null;

    public TransferPresenter(ITransferView iBalanceTransferView) {
        this.iBalanceTransferView = iBalanceTransferView;
    }

    @Override
    public void balanceTransfer() {

        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("SenderID", iBalanceTransferView.SenderId());
            loginrequest.put("ReceiverID", iBalanceTransferView.ReceiverId());
            loginrequest.put("Amount", iBalanceTransferView.Amount());
            loginrequest.put("Type", iBalanceTransferView.Type());
            loginrequest.put("BalanceType", iBalanceTransferView.getBalanceTransferType());
            //loginrequest.put("RequestID", iBalanceTransferView.RequestId());
            loginrequest.put("SenderOpeningBalance", iBalanceTransferView.SenderOpBalance());
            loginrequest.put("RechargePin", iBalanceTransferView.RechargePin());
            loginrequest.put("RequestID", "0");
            String jsonrequest = loginrequest.toString();
            log.i("Bal transfer request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateBalTransferResponse = this;
                model.webserviceMethod(encryptString, keyData, "BalanceTransfer");
                showPDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void balanceReverse() {

        if (iBalanceTransferView.ReceiverId().equals("") || iBalanceTransferView.ReceiverId().isEmpty()) {
            iBalanceTransferView.downlineMemError();
        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();
            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            try {
                loginrequest.put("SenderID", iBalanceTransferView.SenderId());
                loginrequest.put("ReceiverID", iBalanceTransferView.ReceiverId());
                loginrequest.put("Amount", iBalanceTransferView.Amount());
                loginrequest.put("Type", iBalanceTransferView.Type());
                loginrequest.put("BalanceType", iBalanceTransferView.getBalanceTransferType());
                //loginrequest.put("RequestID", iBalanceTransferView.RequestId());
                loginrequest.put("SenderOpeningBalance", iBalanceTransferView.SenderOpBalance());
                loginrequest.put("RechargePin", iBalanceTransferView.RechargePin());
                loginrequest.put("ParentID", iBalanceTransferView.ParentID());
                loginrequest.put("RequestID", "0");
                String jsonrequest = loginrequest.toString();
                log.i("Bal transfer request", jsonrequest);
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                if (!encryptString.equals("")) {
                    WebServiceModel model = new WebServiceModel();
                    model.delegateBalTransferResponse = this;
                    model.webserviceMethod(encryptString, keyData, "BalanceReverse");
                    showPDialog();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void showPDialog() {
        iBalanceTransferView.showPDialog();
    }

    @Override
    public void getDownLineList() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegateBalTransferResponse = this;
        model.webserviceMethod("", keyData, "DownLineForDropDown");
        showPDialog();
    }


    @Override
    public void getLastTenTrasaction() {

        String encryptString = "";
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {
            dayreportrequest.put("Date", iBalanceTransferView.getFromDate());
            dayreportrequest.put("ToDate", iBalanceTransferView.getToDate());
            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                Last10BalanceReportModel model = new Last10BalanceReportModel();
                model.delegate = this;
                model.Last10BalanceTxReport(encryptString, keyData);
                showPDialog();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void Last10BalanceReportValidation() {
        if (iBalanceTransferView.reportType().equalsIgnoreCase("Transfer")) {
            getLastTenTrasaction();
        }

    }

    @Override
    public void decryptLast10BalanceTxReportResponsce(String response) {
        String body = "";

        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(response);
            log.i("Report body response", body);
            ArrayList<BalanceTransferReportPojo> reportResponse = gson.fromJson(body, new TypeToken<ArrayList<BalanceTransferReportPojo>>() {
            }.getType());
            if (reportResponse != null) {
                iBalanceTransferView.getReportResponse(reportResponse);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void decryptLast10BalanceTxFailReportResponsce(String response) {
        String body = "";
        log.i("Report response", response);
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(response);
            errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void viewUser() {
        String keyData = new KeyDataReader().get();
        String encryptString = "";
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("MobileNumber", iBalanceTransferView.contactNumber());
            String jsonrequest = loginrequest.toString();
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);

            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateBalTransferResponse = this;
                model.webserviceMethod(encryptString, keyData, "ViewUser");
                showPDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissPDialog() {
        iBalanceTransferView.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        iBalanceTransferView.errorAlert(errorMsg);
    }

    @Override
    public void validateBalanceTransfer() {
        if (dovalidation()) {
            amount = Double.parseDouble(iBalanceTransferView.Amount());
            senderOpBalance = Double.parseDouble(iBalanceTransferView.SenderOpBalance());
            //dmrBalance = Double.parseDouble(iBalanceTransferView.DmrOpBalance());
            if ((amount > senderOpBalance)) {
                errorAlert(AllMessages.minBalanceTransferMessage);
            } else if (iBalanceTransferView.RechargePin().isEmpty()) {
                errorAlert(String.format(AllMessages.pleaseEnter, "m pin"));
            } else {
                iBalanceTransferView.confirmClick();
            }
            /*if (iBalanceTransferView.getBalanceTransferType().contains("Recharge")) {

            } else if (iBalanceTransferView.getBalanceTransferType().contains("DMT")) {
                if ((amount > dmrBalance)) {
                    errorAlert(AllMessages.minDmrBalanceTransferMessage);
                } else {
                    iBalanceTransferView.confirmClick();
                }
            }*/
        }
    }

    @Override
    public void validateBalanceReverse() {
        if (balrevvalidation()) {
            amount = Double.parseDouble(iBalanceTransferView.Amount());
            senderOpBalance = Double.parseDouble(iBalanceTransferView.SenderOpBalance());
            // dmrBalance = Double.parseDouble(iBalanceTransferView.DmrOpBalance());
            if ((amount > senderOpBalance)) {
                errorAlert(AllMessages.minBalanceTransferMessage);
            } else if (iBalanceTransferView.RechargePin().isEmpty()) {
                errorAlert(String.format(AllMessages.pleaseEnter, "m pin"));
            } else {
                iBalanceTransferView.confirmClick();
            }
            /*if (iBalanceTransferView.getBalanceTransferType().contains("Recharge")) {

            } else if (iBalanceTransferView.getBalanceTransferType().contains("DMT")) {
                if ((amount > dmrBalance)) {
                    errorAlert(AllMessages.minDmrBalanceTransferMessage);
                } else {
                    iBalanceTransferView.confirmClick();
                }
            }*/
        }
    }

    @Override
    public boolean dovalidation() {
        if (iBalanceTransferView.ReceiverId().equals("") || iBalanceTransferView.ReceiverId().isEmpty()) {
            iBalanceTransferView.downlineMemError();
            return false;
        }
        if (iBalanceTransferView.Amount().isEmpty()) {
            iBalanceTransferView.editAmountError();
            return false;
        }
        return true;
    }

    @Override
    public boolean balrevvalidation() {
        if (iBalanceTransferView.SenderId().equals("") || iBalanceTransferView.SenderId().isEmpty()) {
            iBalanceTransferView.downlineMemError();
            return false;
        }
        if (iBalanceTransferView.Amount().isEmpty()) {
            iBalanceTransferView.editAmountError();
            return false;
        }
        return true;
    }


    @Override
    public void successAlert(String msg) {
        iBalanceTransferView.successAlert(msg);
    }

    @Override
    public void BalanceTransferFailureResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {

            if (body.equalsIgnoreCase("Unknown Error") || body.contains("Data not available")) {
                errorAlert("Please select member from downline.");
            } else {
                body = data.Decrypt(encryptedResponse);
                Log.i("body", body);
                errorAlert(body);
            }
           /* body = data.Decrypt(encryptedResponse);
            Log.i("Failure Body",body);
            if (body.contains("Data not available")) {
                errorAlert(AllMessages.pleaseCheckContact);
            } else {
                errorAlert(body);
            }*/

        } catch (Exception e) {
        }
    }

    @Override
    public void BalanceTransferResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();

        try {

            if (body.equalsIgnoreCase("Unknown Error")) {
                successAlert("Please select member from downline.");
            } else {
                body = data.Decrypt(encryptedResponse);
                Log.i("body", body);
                successAlert(body);
            }

        } catch (Exception e) {

        }
    }

    @Override
    public void DownLineForDropDown(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("Downline :", body);
            ArrayList<BalTransDownlineListPojo> downlineList = gson.fromJson(body, new TypeToken<ArrayList<BalTransDownlineListPojo>>() {
            }.getType());
            if (downlineList != null) {
                iBalanceTransferView.getBalanceTransferDownLineList(downlineList);
            }
        } catch (Exception e) {

        }
    }

}
