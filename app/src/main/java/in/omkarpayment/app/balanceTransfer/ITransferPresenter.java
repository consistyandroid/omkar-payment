package in.omkarpayment.app.balanceTransfer;


/**
 * Created by Krish on 12-Jul-17.
 */

public interface ITransferPresenter {

    void balanceTransfer();

    void balanceReverse();

    void showPDialog();

    void getDownLineList();

    void getLastTenTrasaction();

    void Last10BalanceReportValidation();

    void decryptLast10BalanceTxReportResponsce(String response);

    void decryptLast10BalanceTxFailReportResponsce(String response);

    void viewUser();

    void dismissPDialog();

    void errorAlert(String errorMsg);

    void validateBalanceTransfer();

    void validateBalanceReverse();

    boolean dovalidation();

    boolean balrevvalidation();

    void successAlert(String msg);

    void BalanceTransferFailureResponse(String encryptedResponse);

    void BalanceTransferResponse(String encryptedResponse);

    void DownLineForDropDown(String encryptedResponse);

}
