package in.omkarpayment.app.balanceTransfer;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.ActivityWalletToWalletTransferBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import java.util.ArrayList;



public class WalletToWalletTransferActivity extends AppCompatActivity implements ITransferView, IAvailableBalanceView {
    ActivityWalletToWalletTransferBinding walletTransferBinding;
    AlertImpl alert;
    CustomProgressDialog pDialog;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    String retid = "", CurrentBalalnce, pin = "";
    Dialog dialog;
    Button btnSubmit;
    ITransferPresenter iTransferPresenter;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        walletTransferBinding = DataBindingUtil.setContentView(this, R.layout.activity_wallet_to_wallet_transfer);
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);

        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        iTransferPresenter = new TransferPresenter(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Wallet Transfer";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        walletTransferBinding.layAmount.setVisibility(View.GONE);
        walletTransferBinding.layButton.setVisibility(View.GONE);
        walletTransferBinding.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (walletTransferBinding.editContactNo.getText().toString().equals("") ||
                        walletTransferBinding.editContactNo.getText().toString().length() < 10) {
                    walletTransferBinding.editContactNo.setError(String.format(AllMessages.pleaseEnter, "Contact No"));
                } else {
                    iTransferPresenter.viewUser();
                }
            }
        });
        walletTransferBinding.btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Amount().isEmpty()) {
                    editAmountError();
                } else {
                    confirmClick();
                }
            }
        });
    }

    @Override
    public void editMemberIdError() {

    }

    @Override
    public void editAmountError() {
        walletTransferBinding.editAmount.setError(String.format(AllMessages.pleaseEnter, "Amount"));
        walletTransferBinding.editAmount.requestFocus();
    }

    @Override
    public void editPinError() {

    }

    @Override
    public void getBalanceTransferDownLineList(ArrayList<BalTransDownlineListPojo> downlineList) {
        for (BalTransDownlineListPojo obj : downlineList) {
            if (!UserDetails.MobileNumber.equals(obj.getMobileNumber())) {
                walletTransferBinding.editContactNo.setText(obj.getMobileNumber());
                retid = String.valueOf(obj.getUserID());
                walletTransferBinding.editContactNo.setEnabled(false);
                walletTransferBinding.txtUsername.setText(obj.getUserName() + "-" + "(" + obj.getUserID() + ")");
                walletTransferBinding.layAmount.setVisibility(View.VISIBLE);
                walletTransferBinding.layButton.setVisibility(View.VISIBLE);
                walletTransferBinding.layViewButton.setVisibility(View.GONE);
            } else {
                errorAlert(AllMessages.transferError);
            }
        }
    }

    @Override
    public void confirmClick() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("MemberId: %s\nAmount: %s", ReceiverId(), Amount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iTransferPresenter.balanceTransfer();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public String SenderId() {
        return String.valueOf(UserDetails.UserId);
    }

    @Override
    public String ReceiverId() {
        return retid;
    }

    @Override
    public String Amount() {
        return walletTransferBinding.editAmount.getText().toString();
    }

    @Override
    public String Type() {
        return "W";
    }

    @Override
    public String RequestId() {
        return "0";
    }

    @Override
    public String SenderOpBalance() {
        return CurrentBalalnce;
    }

    @Override
    public String DmrOpBalance() {
        return null;
    }

    @Override
    public String RechargePin() {
        return pin;
    }

    @Override
    public String getToDate() {
        return null;
    }

    @Override
    public String getFromDate() {
        return null;
    }

    @Override
    public String reportType() {
        return null;
    }

    @Override
    public void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse) {

    }

    @Override
    public String ParentID() {
        return null;
    }

    @Override
    public String getBalanceTransferType() {
        return null;
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(WalletToWalletTransferActivity.this, R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        final EditText edtPin = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        dialog.show();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    edtPin.setError(String.format(AllMessages.pleaseEnter, "mPin"));
                } else {
                    pin = edtPin.getText().toString();
                  /*  iTransferPresenter.balanceTransfer();*/
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public String contactNumber() {
        return walletTransferBinding.editContactNo.getText().toString();
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public void downlineMemError() {

    }

    @Override
    public void errorAlert(String errorMsg) {
        alert.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalalnce = String.valueOf(obj.getCurrentBalance());
                walletTransferBinding.txtBalance.setText(String.valueOf(obj.getCurrentBalance()));
            } catch (Exception e) {

            }

        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
