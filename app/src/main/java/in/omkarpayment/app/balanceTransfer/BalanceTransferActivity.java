package in.omkarpayment.app.balanceTransfer;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.AutocompleteTextAdapter;
import in.omkarpayment.app.adapter.BalanceReportAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.ActivityBalanceTransferBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.CustomDateFormate;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

import static android.R.layout.simple_spinner_item;

public class BalanceTransferActivity extends Fragment implements ITransferView, IAvailableBalanceView {
    ActivityBalanceTransferBinding balanceTransferBinding;
    CustomProgressDialog progressDialog;
    AlertImpl alert;
    String retid = "", CurrentBalance, DmrBalance;
    ITransferPresenter balancePresenter;
    ArrayList<BalTransDownlineListPojo> MemberList;
    Dialog dialog;
    EditText edtPin;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    Button btnSubmit;
    String senderID = "", Type;
    ArrayAdapter<String> spAdapterBalancetype;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    View rowView;
    NetworkState ns = new NetworkState();
    String strFromDate = "", strToDate = "", ReportType = "Transfer";
    RecyclerView listView;
    BalanceReportAdapter adapter;
    Double currentBalance;
    String balance;
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        balanceTransferBinding = DataBindingUtil.inflate(inflater, R.layout.activity_balance_transfer, container, false);
        rowView = balanceTransferBinding.getRoot();
        progressDialog = new CustomProgressDialog(getContext());
        alert = new AlertImpl(getContext());
        balancePresenter = new TransferPresenter(this);
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        if (ns.isInternetAvailable(getActivity())) {
            balancePresenter.getDownLineList();
        }

        setDateDefaultValue();
        balancePresenter.Last10BalanceReportValidation();

        listView = (RecyclerView) rowView.findViewById(R.id.listv);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(null);

        spAdapterBalancetype = new ArrayAdapter<String>(getActivity(), simple_spinner_item, getResources().getStringArray(R.array.spBalanceTransferType));
        spAdapterBalancetype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        balanceTransferBinding.spType.setAdapter(spAdapterBalancetype);

        balanceTransferBinding.spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Type = "";
                Type = getResources().getStringArray(R.array.spBalanceTransferType)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
        balanceTransferBinding.editMemberId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                BalTransDownlineListPojo op = (BalTransDownlineListPojo) parent.getItemAtPosition(pos);
                balanceTransferBinding.editCurrentBalance.setVisibility(View.VISIBLE);
                balanceTransferBinding.txtCurrentBal.setVisibility(View.VISIBLE);
                  currentBalance=op.getCurrentBalance();
                   balance= String.valueOf(currentBalance);
                  Log.i("selected memebr bal",balance);
                balanceTransferBinding.editCurrentBalance.setText( balance);
                if (UserDetails.UserType.equals(UserType.Admin)) {
                    if (op.getUserType().equalsIgnoreCase(UserType.Distributor) || UserDetails.UserType.equalsIgnoreCase(UserType.SuperDistributor)) {
                        senderID = String.valueOf(UserDetails.UserId);
                    } else if (op.getUserType().equalsIgnoreCase(UserType.Retailer)) {
                        senderID = String.valueOf(op.getParentID());
                    }
                } else if (UserDetails.UserType.equals(UserType.Admin)) {
                    senderID = String.valueOf(UserDetails.UserId);
                }
                if (UserDetails.UserType.equalsIgnoreCase(UserType.Distributor) || UserDetails.UserType.equalsIgnoreCase(UserType.SuperDistributor)) {
                    senderID = String.valueOf(UserDetails.UserId);
                }
                retid = String.valueOf(op.getUserID().toString());

            }
        });


        balanceTransferBinding.btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ns.isInternetAvailable(getContext())) {
                    balancePresenter.validateBalanceTransfer();
                } else {
                    alert.errorAlert(AllMessages.internetError);
                }
            }
        });
        Frmdate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strFromDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
               /* balanceReportBinding.txtFromDate.setText(Date);
                balanceReportBinding.txtFromDate.setError(null);*/


            }
        };
        todate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String str = String.format("%02d%02d%d", dayOfMonth, monthOfYear, year);
                strToDate = CustomDateFormate.getTime(str, "ddMMyyyy", "dd/MM/yyyy");
                String Date = CustomDateFormate.getTime(str, "ddMMyyyy", "dd MMMM yyyy");
                /*balanceReportBinding.txtToDate.setText(Date);
                balanceReportBinding.txtToDate.setError(null);*/

            }
        };
        return rowView;
    }

    @Override
    public void editMemberIdError() {
        alert.errorAlert(String.format("Please select member ID "));
    }

    @Override
    public void downlineMemError() {
        alert.errorAlert(String.format("Please select member from downline"));
        balanceTransferBinding.editMemberId.requestFocus();

    }

    @Override
    public void editAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        balanceTransferBinding.editAmount.requestFocus();
    }

    @Override
    public void editPinError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Pin"));
    }

    @Override
    public void getBalanceTransferDownLineList(ArrayList<BalTransDownlineListPojo> downlineList) {
        for (BalTransDownlineListPojo pojo : downlineList) {
            Log.i("Downline:", pojo.getMobileNumber().toString() + pojo.getUserID().toString() + pojo.getUserName().toString());
        }
        // MemberList = downlineList;
        AutocompleteTextAdapter adapter;
        adapter = new AutocompleteTextAdapter(getContext(), R.layout.adapter_autocomplete_text, downlineList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        balanceTransferBinding.editMemberId.setAdapter(adapter);
        balanceTransferBinding.editMemberId.setThreshold(1);
    }

    @Override
    public void confirmClick() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Member ID : %s\nAmount : %s", ReceiverId(), Amount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                balancePresenter.balanceTransfer();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public String SenderId() {

        return senderID;
    }

    @Override
    public String ReceiverId() {

        return retid;
    }

    @Override
    public String Amount() {
        return balanceTransferBinding.editAmount.getText().toString();
    }

    @Override
    public String Type() {
        return "T";
    }

    @Override
    public String RequestId() {
        return "0";
    }

    @Override
    public String SenderOpBalance() {
        return CurrentBalance;
    }

    @Override
    public String DmrOpBalance() {
        return DmrBalance;
    }

    @Override
    public String RechargePin() {
        return balanceTransferBinding.editMpin.getText().toString().trim();
    }

    @Override
    public String getToDate() {
        return strToDate;
    }

    @Override
    public String getFromDate() {
        return strFromDate;
    }

    @Override
    public String reportType() {
        return "Transfer";
    }

    @Override
    public void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse) {
        adapter = new BalanceReportAdapter(getContext(), reportResponse);
        listView.setAdapter(adapter);
    }

    @Override
    public String ParentID() {
        return "";
    }

    @Override
    public String getBalanceTransferType() {

        return "Recharge";
    }

    @Override
    public void mPinDialog() {
        dialog = new Dialog(getContext(), R.style.ThemeWithCorners);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_pin);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("mPin");
        edtPin = (EditText) dialog.findViewById(R.id.editPin);
        btnSubmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        dialog.show();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPin.getText().toString().equals("")) {
                    alert.errorAlert(String.format(AllMessages.pleaseEnter, "mPin"));
                    /*edtPin.setError(String.format(AllMessages.pleaseEnter, "mPin"));*/
                } else {
                    // rechargePresenter.doRecharge();
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public String contactNumber() {
        return null;
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        dismissPDialog();
        alert.errorAlert(errorMsg);
        clear();
    }

    @Override
    public void successAlert(String msg) {
        dismissPDialog();
        clear();
        alert.successAlert(msg);
        //((HomeActivity) getActivity()).updateBal();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                iAvailableBalancePresenter.getAvailableBalance();
            }
        }, 2000);

    }

    private void clear() {
        retid = "";
        balanceTransferBinding.editMemberId.setText("");
        balanceTransferBinding.editAmount.setText("");
        balanceTransferBinding.editMpin.setText("");
        balanceTransferBinding.editCurrentBalance.setText("");
        balanceTransferBinding.editMemberId.requestFocus();
    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;
        Log.i("To Date", strToDate);
        Log.i("From Date", strFromDate);

       /* balanceReportBinding.txtToDate.setText(date);
        balanceReportBinding.txtToDate.setError(null);

        balanceReportBinding.txtFromDate.setText(date);
        balanceReportBinding.txtFromDate.setError(null);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = String.valueOf(obj.getCurrentBalance());
                DmrBalance = String.valueOf(obj.getDMRBalance());
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {

            }

        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
