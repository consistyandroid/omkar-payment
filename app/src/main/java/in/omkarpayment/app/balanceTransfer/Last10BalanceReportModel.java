package in.omkarpayment.app.balanceTransfer;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.IWebServicesModel;
import in.omkarpayment.app.report.balanceReport.IBalanceReportPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 10/13/2018.
 */

public class Last10BalanceReportModel {
    public ITransferPresenter delegate = null;
    LogWriter log = new LogWriter();

    public void Last10BalanceTxReport(String encrypted, String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.BalanceTransferReport(encrypted, keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            //  log.i("onResponseBody", response.body().getBody());
                            delegate.decryptLast10BalanceTxReportResponsce(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.decryptLast10BalanceTxFailReportResponsce(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }
}
