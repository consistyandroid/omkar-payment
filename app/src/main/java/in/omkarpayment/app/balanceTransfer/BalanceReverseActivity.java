package in.omkarpayment.app.balanceTransfer;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.AutocompleteTextAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.ActivityReverseBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.json.BalanceTransferReportPojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import static android.R.layout.simple_spinner_item;

public class BalanceReverseActivity extends AppCompatActivity implements ITransferView, IAvailableBalanceView {
    ActivityReverseBinding balanceReverse;
    CustomProgressDialog progressDialog;
    AlertImpl alert;
    Context context = this;
    ArrayAdapter<String> spAdapterBalancetype;
    String retid = "", CurrentBalalnce, parentID, Type, senderID, DmrBalance;
    ITransferPresenter balancePresenter;
    ArrayList<BalTransDownlineListPojo> MemberList;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    NetworkState ns = new NetworkState();
    Double currentBalance;
    String balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        balanceReverse = DataBindingUtil.setContentView(this, R.layout.activity_reverse);
        progressDialog = new CustomProgressDialog(this);
        alert = new AlertImpl(this);
        balancePresenter = new TransferPresenter(this);
        balancePresenter.getDownLineList();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Balance Reverse";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        balanceReverse.editMemberId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                BalTransDownlineListPojo op = (BalTransDownlineListPojo) parent.getItemAtPosition(pos);
                balanceReverse.editCurrentBalance.setVisibility(View.VISIBLE);
                balanceReverse.txtCurrentBal.setVisibility(View.VISIBLE);
                currentBalance = op.getCurrentBalance();
                balance = String.valueOf(currentBalance);
                Log.i("selected memebr bal", balance);
                balanceReverse.editCurrentBalance.setText(balance);
                retid = String.valueOf(op.getUserID().toString());
                parentID = String.valueOf(op.getParentID());
            }
        });
        spAdapterBalancetype = new ArrayAdapter<String>(context, simple_spinner_item, getResources().getStringArray(R.array.spBalanceTransferType));
        spAdapterBalancetype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        balanceReverse.spType.setAdapter(spAdapterBalancetype);
        balanceReverse.spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Type = "";
                Type = getResources().getStringArray(R.array.spBalanceTransferType)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        balanceReverse.btnReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(context)) {
                    balancePresenter.validateBalanceReverse();
                } else {
                    alert.errorAlert(AllMessages.internetError);
                }
            }
        });
    }

    @Override
    public void getBalanceTransferDownLineList(ArrayList<BalTransDownlineListPojo> downlineList) {
        // MemberList = downlineList;
        AutocompleteTextAdapter adapter;
        adapter = new AutocompleteTextAdapter(context, R.layout.adapter_autocomplete_text, downlineList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        balanceReverse.editMemberId.setAdapter(adapter);
        balanceReverse.editMemberId.setThreshold(1);
    }

    @Override
    public void editMemberIdError() {
        alert.errorAlert(String.format("Please select memberID"));
        balanceReverse.editMemberId.requestFocus();
    }

    @Override
    public void downlineMemError() {
        alert.errorAlert(String.format("Please select member from downline"));
    }

    @Override
    public void editAmountError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "amount"));
        balanceReverse.editAmount.requestFocus();
    }

    @Override
    public void editPinError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "Pin"));
    }

    @Override
    public void confirmClick() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Member ID: %s\nAmount: %s", ReceiverId(), Amount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                balancePresenter.balanceReverse();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public String SenderId() {
        return retid;
    }

    @Override
    public String ReceiverId() {
        return parentID;
    }

    @Override
    public String Amount() {
        return balanceReverse.editAmount.getText().toString().trim();
    }

    @Override
    public String Type() {
        return "RE";
    }

    @Override
    public String RequestId() {
        return "0";
    }

    @Override
    public String SenderOpBalance() {
        return CurrentBalalnce;
    }

    @Override
    public String DmrOpBalance() {
        return DmrBalance;
    }

    @Override
    public String RechargePin() {
        return balanceReverse.editMpin.getText().toString().trim();
    }

    @Override
    public String getToDate() {
        return null;
    }

    @Override
    public String getFromDate() {
        return null;
    }

    @Override
    public String reportType() {
        return null;
    }

    @Override
    public void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse) {

    }

    @Override
    public String ParentID() {
        return parentID;
    }

    @Override
    public String getBalanceTransferType() {
        //Type
        return "Recharge";
    }

    @Override
    public void mPinDialog() {
    }

    @Override
    public String contactNumber() {
        return null;
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        alert.errorAlert(errorMsg);
        clear();
    }

    @Override
    public void successAlert(String msg) {
        dismissPDialog();
        clear();
        alert.successAlert(msg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                iAvailableBalancePresenter.getAvailableBalance();
            }
        }, 2000);
    }

    private void clear() {
        retid = "";
        parentID = "";
        balanceReverse.editMemberId.setText("");
        balanceReverse.editAmount.setText("");
        balanceReverse.editMpin.setText("");
        balanceReverse.editCurrentBalance.setText("");
        balanceReverse.editMemberId.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalalnce = String.valueOf(obj.getCurrentBalance());
                DmrBalance = String.valueOf(obj.getDMRBalance());
                UserDetails.UserBalance = obj.getCurrentBalance();
                ((MainActivity) context).refreshBalance();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
