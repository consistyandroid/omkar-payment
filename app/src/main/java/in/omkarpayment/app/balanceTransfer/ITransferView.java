package in.omkarpayment.app.balanceTransfer;

import java.util.ArrayList;

import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.json.BalanceTransferReportPojo;

/**
 * Created by Krish on 12-Jul-17.
 */

public interface ITransferView {

    void editMemberIdError();

    void editAmountError();

    void editPinError();


    void getBalanceTransferDownLineList(ArrayList<BalTransDownlineListPojo> downlineList);

    void confirmClick();

    String SenderId();

    String ReceiverId();

    String Amount();

    String Type();

    String RequestId();

    String SenderOpBalance();

    String DmrOpBalance();

    String RechargePin();

    String getToDate();

    String getFromDate();

    String reportType();

    void getReportResponse(ArrayList<BalanceTransferReportPojo> reportResponse);

    String ParentID();

    String getBalanceTransferType();

    void mPinDialog();


    String contactNumber();

    void showPDialog();

    void dismissPDialog();

    void downlineMemError();

    void errorAlert(String errorMsg);

    void successAlert(String msg);


}
