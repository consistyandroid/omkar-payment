package in.omkarpayment.app.json.moneyTx;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddBeneficiaryPojo {
    @SerializedName("BenficiaryID")
    @Expose
    private String benficiaryID;
    @SerializedName("SenderID")
    @Expose
    private String senderID;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("AccountNo")
    @Expose
    private String accountNo;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Bank")
    @Expose
    private String bank;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;
    @SerializedName("Message")
    @Expose
    private Object message;
    @SerializedName("IsValidate")
    @Expose
    private Object isValidate;
    @SerializedName("BenFiciaryID")
    @Expose
    private Object benFiciaryID;
    @SerializedName("OTP")
    @Expose
    private Object oTP;

    public String getBenficiaryID() {
        return benficiaryID;
    }

    public void setBenficiaryID(String benficiaryID) {
        this.benficiaryID = benficiaryID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getIFSC() {
        return iFSC;
    }

    public void setIFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Object getIsValidate() {
        return isValidate;
    }

    public void setIsValidate(Object isValidate) {
        this.isValidate = isValidate;
    }

    public Object getBenFiciaryID() {
        return benFiciaryID;
    }

    public void setBenFiciaryID(Object benFiciaryID) {
        this.benFiciaryID = benFiciaryID;
    }

    public Object getOTP() {
        return oTP;
    }

    public void setOTP(Object oTP) {
        this.oTP = oTP;
    }
}
