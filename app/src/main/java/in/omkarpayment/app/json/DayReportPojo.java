package in.omkarpayment.app.json;

/**
 * Created by Somnath-Laptop on 10-Jan-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DayReportPojo {
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("OperatorID")
    @Expose
    private Integer operatorID;
    @SerializedName("ServiceName")
    @Expose
    private String serviceName;
    @SerializedName("OperatorName")
    @Expose
    private String operatorName;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("OpeningBalance")
    @Expose
    private Double openingBalance;
    @SerializedName("ClosingBalance")
    @Expose
    private Double closingBalance;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("UserTypeID")
    @Expose
    private Integer userTypeID;
    @SerializedName("DUserTypeID")
    @Expose
    private Integer dUserTypeID;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("RechargeID")
    @Expose
    private Integer rechargeID;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("ParentName")
    @Expose
    private String parentName;
    @SerializedName("ConsumerNumber")
    @Expose
    private String consumerNumber;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("IsDispute")
    @Expose
    private Integer isDispute;
    @SerializedName("DiscountAmount")
    @Expose
    private Double discountAmount;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("RechargeDescription")
    @Expose
    private String rechargeDescription;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public Double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(Double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

    public Integer getDUserTypeID() {
        return dUserTypeID;
    }

    public void setDUserTypeID(Integer dUserTypeID) {
        this.dUserTypeID = dUserTypeID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRechargeID() {
        return rechargeID;
    }

    public void setRechargeID(Integer rechargeID) {
        this.rechargeID = rechargeID;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getConsumerNumber() {
        return consumerNumber;
    }

    public void setConsumerNumber(String consumerNumber) {
        this.consumerNumber = consumerNumber;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public Integer getIsDispute() {
        return isDispute;
    }

    public void setIsDispute(Integer isDispute) {
        this.isDispute = isDispute;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getRechargeDescription() {
        return rechargeDescription;
    }

    public void setRechargeDescription(String rechargeDescription) {
        this.rechargeDescription = rechargeDescription;
    }

}
