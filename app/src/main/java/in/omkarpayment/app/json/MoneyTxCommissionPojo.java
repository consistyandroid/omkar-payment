package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lenovo on 8/17/2018.
 */

public class MoneyTxCommissionPojo {
    @SerializedName("FromAmount")
    @Expose
    private Double fromAmount;
    @SerializedName("ToAmount")
    @Expose
    private Double toAmount;
    @SerializedName("RetailerFixRs")
    @Expose
    private Double retailerFixRs;
    @SerializedName("RetailerFlexi")
    @Expose
    private Double retailerFlexi;
    @SerializedName("DistributorFixRs")
    @Expose
    private Double distributorFixRs;
    @SerializedName("DistributorFlexi")
    @Expose
    private Double distributorFlexi;
    @SerializedName("SuperDistributorFixRs")
    @Expose
    private Double superDistributorFixRs;
    @SerializedName("SuperDistributorFlexi")
    @Expose
    private Double superDistributorFlexi;
    @SerializedName("ID")
    @Expose
    private Integer iD;

    public Double getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(Double fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Double getToAmount() {
        return toAmount;
    }

    public void setToAmount(Double toAmount) {
        this.toAmount = toAmount;
    }

    public Double getRetailerFixRs() {
        return retailerFixRs;
    }

    public void setRetailerFixRs(Double retailerFixRs) {
        this.retailerFixRs = retailerFixRs;
    }

    public Double getRetailerFlexi() {
        return retailerFlexi;
    }

    public void setRetailerFlexi(Double retailerFlexi) {
        this.retailerFlexi = retailerFlexi;
    }

    public Double getDistributorFixRs() {
        return distributorFixRs;
    }

    public void setDistributorFixRs(Double distributorFixRs) {
        this.distributorFixRs = distributorFixRs;
    }

    public Double getDistributorFlexi() {
        return distributorFlexi;
    }

    public void setDistributorFlexi(Double distributorFlexi) {
        this.distributorFlexi = distributorFlexi;
    }

    public Double getSuperDistributorFixRs() {
        return superDistributorFixRs;
    }

    public void setSuperDistributorFixRs(Double superDistributorFixRs) {
        this.superDistributorFixRs = superDistributorFixRs;
    }

    public Double getSuperDistributorFlexi() {
        return superDistributorFlexi;
    }

    public void setSuperDistributorFlexi(Double superDistributorFlexi) {
        this.superDistributorFlexi = superDistributorFlexi;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

}


