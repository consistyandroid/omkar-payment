package in.omkarpayment.app.json;

/**
 * Created by Somnath-Laptop on 09-Jan-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LastFiveRechargePojo {
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("RechargeID")
    @Expose
    private Integer rechargeID;
    @SerializedName("serviceID")
    @Expose
    private Integer serviceID;
    @SerializedName("ConsumerNumber")
    @Expose
    private String consumerNumber;
    @SerializedName("DOCType")
    @Expose
    private String dOCType;
    @SerializedName("OpeningBalance")
    @Expose
    private Double openingBalance;
    @SerializedName("ClosingBalance")
    @Expose
    private Double closingBalance;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("DiscountAmount")
    @Expose
    private Double discountAmount;
    @SerializedName("AmountCreditDebit")
    @Expose
    private String amountCreditDebit;
    @SerializedName("CommissionCreditDebit")
    @Expose
    private String commissionCreditDebit;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("IsDispute")
    @Expose
    private Boolean isDispute;
    @SerializedName("OperatorID")
    @Expose
    private Integer operatorID;
    @SerializedName("ServiceName")
    @Expose
    private String serviceName;
    @SerializedName("OperatorName")
    @Expose
    private String operatorName;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("UserTypeID")
    @Expose
    private Integer userTypeID;
    @SerializedName("status")
    @Expose
    private String sTATUS;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("ParentName")
    @Expose
    private String parentName;
    @SerializedName("DUserTypeID")
    @Expose
    private Integer dUserTypeID;
    @SerializedName("SR")
    @Expose
    private Integer sR;

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Integer getRechargeID() {
        return rechargeID;
    }

    public void setRechargeID(Integer rechargeID) {
        this.rechargeID = rechargeID;
    }

    public Integer getServiceID() {
        return serviceID;
    }

    public void setServiceID(Integer serviceID) {
        this.serviceID = serviceID;
    }

    public String getConsumerNumber() {
        return consumerNumber;
    }

    public void setConsumerNumber(String consumerNumber) {
        this.consumerNumber = consumerNumber;
    }

    public String getDOCType() {
        return dOCType;
    }

    public void setDOCType(String dOCType) {
        this.dOCType = dOCType;
    }

    public Double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public Double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(Double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getAmountCreditDebit() {
        return amountCreditDebit;
    }

    public void setAmountCreditDebit(String amountCreditDebit) {
        this.amountCreditDebit = amountCreditDebit;
    }

    public String getCommissionCreditDebit() {
        return commissionCreditDebit;
    }

    public void setCommissionCreditDebit(String commissionCreditDebit) {
        this.commissionCreditDebit = commissionCreditDebit;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public Boolean getIsDispute() {
        return isDispute;
    }

    public void setIsDispute(Boolean isDispute) {
        this.isDispute = isDispute;
    }

    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getDUserTypeID() {
        return dUserTypeID;
    }

    public void setDUserTypeID(Integer dUserTypeID) {
        this.dUserTypeID = dUserTypeID;
    }

    public Integer getSR() {
        return sR;
    }

    public void setSR(Integer sR) {
        this.sR = sR;
    }

}
