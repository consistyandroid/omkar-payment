package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lenovo on 7/26/2018.
 */

public class DTHCustomerInfoPojo {
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("records")
    @Expose
    private List<Record> records = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private Object message;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }


    public class Record {

        @SerializedName("MonthlyRecharge")
        @Expose
        private String monthlyRecharge;
        @SerializedName("Balance")
        @Expose
        private String balance;
        @SerializedName("customerName")
        @Expose
        private String customerName;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("NextRechargeDate")
        @Expose
        private String nextRechargeDate;
        @SerializedName("planname")
        @Expose
        private String planname;

        public String getMonthlyRecharge() {
            return monthlyRecharge;
        }

        public void setMonthlyRecharge(String monthlyRecharge) {
            this.monthlyRecharge = monthlyRecharge;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getNextRechargeDate() {
            return nextRechargeDate;
        }

        public void setNextRechargeDate(String nextRechargeDate) {
            this.nextRechargeDate = nextRechargeDate;
        }

        public String getPlanname() {
            return planname;
        }

        public void setPlanname(String planname) {
            this.planname = planname;
        }


    }
}
