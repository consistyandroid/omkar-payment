package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Somnath-Laptop on 01-Feb-2018.
 */

public class ComplaintReportPojo {
    @SerializedName("RefundID")
    @Expose
    private Integer refundID;
    @SerializedName("RechargeID")
    @Expose
    private Integer rechargeID;
    @SerializedName("DOR")
    @Expose
    private String dOR;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("ConsumerNumber")
    @Expose
    private String consumerNumber;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("RMStatus")
    @Expose
    private String rMStatus;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("Sender")
    @Expose
    private String sender;
    @SerializedName("UserTypeID")
    @Expose
    private Integer userTypeID;

    public Integer getRefundID() {
        return refundID;
    }

    public void setRefundID(Integer refundID) {
        this.refundID = refundID;
    }

    public Integer getRechargeID() {
        return rechargeID;
    }

    public void setRechargeID(Integer rechargeID) {
        this.rechargeID = rechargeID;
    }

    public String getDOR() {
        return dOR;
    }

    public void setDOR(String dOR) {
        this.dOR = dOR;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getConsumerNumber() {
        return consumerNumber;
    }

    public void setConsumerNumber(String consumerNumber) {
        this.consumerNumber = consumerNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRMStatus() {
        return rMStatus;
    }

    public void setRMStatus(String rMStatus) {
        this.rMStatus = rMStatus;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

}
