package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaleCommissionPojo {

    @SerializedName("DiscountAmount")
    @Expose
    private Double discountAmount;
    @SerializedName("DiscountRechargeCount")
    @Expose
    private Integer discountRechargeCount;

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getDiscountRechargeCount() {
        return discountRechargeCount;
    }

    public void setDiscountRechargeCount(Integer discountRechargeCount) {
        this.discountRechargeCount = discountRechargeCount;
    }

}
