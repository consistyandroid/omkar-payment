package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ${user} on 5/2/18.
 */

public class DTHPlansMainPojo {
    @SerializedName("Add-On Pack")
    @Expose
    private List<DTHPlansAddOnPojo> addOnPack = null;
    @SerializedName("Plan")
    @Expose
    private List<DTHPlansPojo> plan = null;


    public List<DTHPlansAddOnPojo> getAddOnPack() {
        return addOnPack;
    }
    public void setAddOnPack(List<DTHPlansAddOnPojo> addOnPack) {
        this.addOnPack = addOnPack;
    }

    public List<DTHPlansPojo> getPlan() {
        return plan;
    }

    public void setPlan(List<DTHPlansPojo> plan) {
        this.plan = plan;
    }

}
