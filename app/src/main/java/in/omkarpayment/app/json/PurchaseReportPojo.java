package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by consisty on 12/4/17.
 */

public class PurchaseReportPojo {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Date")
    @Expose
    private Object date;
    @SerializedName("ToDate")
    @Expose
    private Object toDate;
    @SerializedName("UserTypeID")
    @Expose
    private Integer userTypeID;
    @SerializedName("DType")
    @Expose
    private String dType;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("DEmailID")
    @Expose
    private String dEmailID;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("RechargePin")
    @Expose
    private String rechargePin;
    @SerializedName("RUserTypeID")
    @Expose
    private Integer rUserTypeID;
    @SerializedName("DUserTypeID")
    @Expose
    private Integer dUserTypeID;
    @SerializedName("ParentMobileNumber")
    @Expose
    private String parentMobileNumber;
    @SerializedName("UTRNo")
    @Expose
    private String uTRNo;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("IsDeleted")
    @Expose
    private Integer isDeleted;
    @SerializedName("CreditID")
    @Expose
    private Integer creditID;
    @SerializedName("CreditTo")
    @Expose
    private String creditTo;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("PayMode")
    @Expose
    private String payMode;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("RequestID")
    @Expose
    private Integer requestID;
    @SerializedName("SenderName")
    @Expose
    private String senderName;
    @SerializedName("BalanceTransferID")
    @Expose
    private Integer balanceTransferID;
    @SerializedName("SenderID")
    @Expose
    private Integer senderID;
    @SerializedName("ReceiverName")
    @Expose
    private String receiverName;
    @SerializedName("SenderClosingBalance")
    @Expose
    private Double senderClosingBalance;
    @SerializedName("SenderOpeningBalance")
    @Expose
    private Double senderOpeningBalance;
    @SerializedName("ReceiverID")
    @Expose
    private Integer receiverID;
    @SerializedName("MobileNumber")
    @Expose
    private Object mobileNumber;
    @SerializedName("RecOpeBal")
    @Expose
    private Double recOpeBal;
    @SerializedName("Type")
    @Expose
    private Object type;
    @SerializedName("RecCloseBal")
    @Expose
    private Double recCloseBal;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("Sender")
    @Expose
    private String sender;
    @SerializedName("Receiver")
    @Expose
    private String receiver;
    @SerializedName("ClosingBalance")
    @Expose
    private String closingBalance;
    @SerializedName("OpeningBalance")
    @Expose
    private String openingBalance;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public Object getToDate() {
        return toDate;
    }

    public void setToDate(Object toDate) {
        this.toDate = toDate;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

    public String getDType() {
        return dType;
    }

    public void setDType(String dType) {
        this.dType = dType;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getDEmailID() {
        return dEmailID;
    }

    public void setDEmailID(String dEmailID) {
        this.dEmailID = dEmailID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRechargePin() {
        return rechargePin;
    }

    public void setRechargePin(String rechargePin) {
        this.rechargePin = rechargePin;
    }

    public Integer getRUserTypeID() {
        return rUserTypeID;
    }

    public void setRUserTypeID(Integer rUserTypeID) {
        this.rUserTypeID = rUserTypeID;
    }

    public Integer getDUserTypeID() {
        return dUserTypeID;
    }

    public void setDUserTypeID(Integer dUserTypeID) {
        this.dUserTypeID = dUserTypeID;
    }

    public String getParentMobileNumber() {
        return parentMobileNumber;
    }

    public void setParentMobileNumber(String parentMobileNumber) {
        this.parentMobileNumber = parentMobileNumber;
    }

    public String getUTRNo() {
        return uTRNo;
    }

    public void setUTRNo(String uTRNo) {
        this.uTRNo = uTRNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getCreditID() {
        return creditID;
    }

    public void setCreditID(Integer creditID) {
        this.creditID = creditID;
    }

    public String getCreditTo() {
        return creditTo;
    }

    public void setCreditTo(String creditTo) {
        this.creditTo = creditTo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public Integer getRequestID() {
        return requestID;
    }

    public void setRequestID(Integer requestID) {
        this.requestID = requestID;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Integer getBalanceTransferID() {
        return balanceTransferID;
    }

    public void setBalanceTransferID(Integer balanceTransferID) {
        this.balanceTransferID = balanceTransferID;
    }

    public Integer getSenderID() {
        return senderID;
    }

    public void setSenderID(Integer senderID) {
        this.senderID = senderID;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public Double getSenderClosingBalance() {
        return senderClosingBalance;
    }

    public void setSenderClosingBalance(Double senderClosingBalance) {
        this.senderClosingBalance = senderClosingBalance;
    }

    public Double getSenderOpeningBalance() {
        return senderOpeningBalance;
    }

    public void setSenderOpeningBalance(Double senderOpeningBalance) {
        this.senderOpeningBalance = senderOpeningBalance;
    }

    public Integer getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(Integer receiverID) {
        this.receiverID = receiverID;
    }

    public Object getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Object mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Double getRecOpeBal() {
        return recOpeBal;
    }

    public void setRecOpeBal(Double recOpeBal) {
        this.recOpeBal = recOpeBal;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Double getRecCloseBal() {
        return recCloseBal;
    }

    public void setRecCloseBal(Double recCloseBal) {
        this.recCloseBal = recCloseBal;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(String closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }
}
