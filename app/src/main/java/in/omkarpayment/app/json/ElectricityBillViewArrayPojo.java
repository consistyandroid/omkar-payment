package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 11-Aug-17.
 */

public class ElectricityBillViewArrayPojo {

    @SerializedName("consumerNumber")
    @Expose
    private String consumerNumber;
    @SerializedName("ConsumerName")
    @Expose
    private String consumerName;
    @SerializedName("billingUnit")
    @Expose
    private String billingUnit;
    @SerializedName("buDesc")
    @Expose
    private String buDesc;
    @SerializedName("processiongCycle")
    @Expose
    private String processiongCycle;
    @SerializedName("billDueDate")
    @Expose
    private String billDueDate;
    @SerializedName("billAmounts")
    @Expose
    private String billAmounts;
    @SerializedName("desc")
    @Expose
    private String desc;

    public String getConsumerNumber() {
        return consumerNumber;
    }

    public void setConsumerNumber(String consumerNumber) {
        this.consumerNumber = consumerNumber;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getBillingUnit() {
        return billingUnit;
    }

    public void setBillingUnit(String billingUnit) {
        this.billingUnit = billingUnit;
    }

    public String getBuDesc() {
        return buDesc;
    }

    public void setBuDesc(String buDesc) {
        this.buDesc = buDesc;
    }

    public String getProcessiongCycle() {
        return processiongCycle;
    }

    public void setProcessiongCycle(String processiongCycle) {
        this.processiongCycle = processiongCycle;
    }

    public String getBillDueDate() {
        return billDueDate;
    }

    public void setBillDueDate(String billDueDate) {
        this.billDueDate = billDueDate;
    }

    public String getBillAmounts() {
        return billAmounts;
    }

    public void setBillAmounts(String billAmounts) {
        this.billAmounts = billAmounts;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
