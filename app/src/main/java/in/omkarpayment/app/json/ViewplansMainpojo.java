package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ${user} on 27/1/18.
 */

public class ViewplansMainpojo {
    @SerializedName("FULLTT")
    @Expose
    private List<ViewPlansDecPojo> fULLTT = null;
    @SerializedName("TOPUP")
    @Expose
    private List<ViewPlansDecPojo> tOPUP = null;
    @SerializedName("3G/4G")
    @Expose
    private List<ViewPlansDecPojo> _3G4G = null;
    @SerializedName("RATE CUTTER")
    @Expose
    private List<ViewPlansDecPojo> rATECUTTER = null;
    @SerializedName("2G")
    @Expose
    private List<ViewPlansDecPojo> _2G = null;
    @SerializedName("SMS")
    @Expose
    private List<ViewPlansDecPojo> sMS = null;
    @SerializedName("Romaing")
    @Expose
    private List<ViewPlansDecPojo> romaing = null;
    @SerializedName("COMBO")
    @Expose
    private List<ViewPlansDecPojo> cOMBO = null;
    @SerializedName("FRC")
    @Expose
    private List<ViewPlansDecPojo> fRC = null;

    public List<ViewPlansDecPojo> getfULLTT() {
        return fULLTT;
    }

    public void setfULLTT(List<ViewPlansDecPojo> fULLTT) {
        this.fULLTT = fULLTT;
    }

    public List<ViewPlansDecPojo> gettOPUP() {
        return tOPUP;
    }

    public void settOPUP(List<ViewPlansDecPojo> tOPUP) {
        this.tOPUP = tOPUP;
    }

    public List<ViewPlansDecPojo> get_3G4G() {
        return _3G4G;
    }

    public void set_3G4G(List<ViewPlansDecPojo> _3G4G) {
        this._3G4G = _3G4G;
    }

    public List<ViewPlansDecPojo> getrATECUTTER() {
        return rATECUTTER;
    }

    public void setrATECUTTER(List<ViewPlansDecPojo> rATECUTTER) {
        this.rATECUTTER = rATECUTTER;
    }

    public List<ViewPlansDecPojo> get_2G() {
        return _2G;
    }

    public void set_2G(List<ViewPlansDecPojo> _2G) {
        this._2G = _2G;
    }

    public List<ViewPlansDecPojo> getsMS() {
        return sMS;
    }

    public void setsMS(List<ViewPlansDecPojo> sMS) {
        this.sMS = sMS;
    }

    public List<ViewPlansDecPojo> getRomaing() {
        return romaing;
    }

    public void setRomaing(List<ViewPlansDecPojo> romaing) {
        this.romaing = romaing;
    }

    public List<ViewPlansDecPojo> getcOMBO() {
        return cOMBO;
    }

    public void setcOMBO(List<ViewPlansDecPojo> cOMBO) {
        this.cOMBO = cOMBO;
    }

    public List<ViewPlansDecPojo> getfRC() {
        return fRC;
    }

    public void setfRC(List<ViewPlansDecPojo> fRC) {
        this.fRC = fRC;
    }
}
