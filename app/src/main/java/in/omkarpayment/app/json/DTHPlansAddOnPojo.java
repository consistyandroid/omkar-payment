package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lenovo on 7/11/2018.
 */

public class DTHPlansAddOnPojo {

    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;
    @SerializedName("plan_name")
    @Expose
    private String planName;
    @SerializedName("rs")
    @Expose
    private Rs rs;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Rs getRs() {
        return rs;
    }

    public void setRs(Rs rs) {
        this.rs = rs;
    }

}

