package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Somnath-Laptop on 29-Jan-2018.
 */

public class StockReportPojo {
    @SerializedName("RequestID")
    @Expose
    private Integer requestID;
    @SerializedName("BalanceType")
    @Expose
    private String balanceType;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("PayMode")
    @Expose
    private String payMode;
    @SerializedName("BillNumber")
    @Expose
    private String billNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("RequestFrom")
    @Expose
    private String requestFrom;
    @SerializedName("RUserTypeID")
    @Expose
    private Integer rUserTypeID;
    @SerializedName("RequestTo")
    @Expose
    private String requestTo;
    @SerializedName("ParentName")
    @Expose
    private String parentName;
    @SerializedName("DUserTypeID")
    @Expose
    private Integer dUserTypeID;
    @SerializedName("UTRNo")
    @Expose
    private String uTRNo;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("BankName")
    @Expose
    private String bankName;

    public Integer getRequestID() {
        return requestID;
    }

    public void setRequestID(Integer requestID) {
        this.requestID = requestID;
    }

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public Integer getRUserTypeID() {
        return rUserTypeID;
    }

    public void setRUserTypeID(Integer rUserTypeID) {
        this.rUserTypeID = rUserTypeID;
    }

    public String getRequestTo() {
        return requestTo;
    }

    public void setRequestTo(String requestTo) {
        this.requestTo = requestTo;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getDUserTypeID() {
        return dUserTypeID;
    }

    public void setDUserTypeID(Integer dUserTypeID) {
        this.dUserTypeID = dUserTypeID;
    }

    public String getUTRNo() {
        return uTRNo;
    }

    public void setUTRNo(String uTRNo) {
        this.uTRNo = uTRNo;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

}
