package in.omkarpayment.app.json;

/**
 * Created by Somnath-Laptop on 12-Jan-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownlinListSummaryPojo {
    @SerializedName("downlineCount")
    @Expose
    private String downlineCount;
    @SerializedName("sumOfBalance")
    @Expose
    private String sumOfBalance;

    public String getDownlineCount() {
        return downlineCount;
    }

    public void setDownlineCount(String downlineCount) {
        this.downlineCount = downlineCount;
    }

    public String getSumOfBalance() {
        return sumOfBalance;
    }

    public void setSumOfBalance(String sumOfBalance) {
        this.sumOfBalance = sumOfBalance;
    }

}

