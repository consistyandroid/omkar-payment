package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Consisty on 01-Sep-17.
 */

public class GetBankNamesPojo {
    @SerializedName("BankName")
    @Expose
    private Object bankName;
    @SerializedName("AccountNo")
    @Expose
    private Object accountNo;
    @SerializedName("IFSCCode")
    @Expose
    private Object iFSCCode;
    @SerializedName("AccountName")
    @Expose
    private Object accountName;
    @SerializedName("BranchName")
    @Expose
    private Object branchName;
    @SerializedName("BankID")
    @Expose
    private Integer bankID;
    @SerializedName("Bank")
    @Expose
    private String bank;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public Object getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Object accountNo) {
        this.accountNo = accountNo;
    }

    public Object getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(Object iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public Object getAccountName() {
        return accountName;
    }

    public void setAccountName(Object accountName) {
        this.accountName = accountName;
    }

    public Object getBranchName() {
        return branchName;
    }

    public void setBranchName(Object branchName) {
        this.branchName = branchName;
    }

    public Integer getBankID() {
        return bankID;
    }

    public void setBankID(Integer bankID) {
        this.bankID = bankID;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getIFSC() {
        return iFSC;
    }

    public void setIFSC(String iFSC) {
        this.iFSC = iFSC;
    }
}
