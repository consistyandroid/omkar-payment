package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 11-Aug-17.
 */

public class AvailableBalancePojo {
    @SerializedName("CurrentBalance")
    @Expose
    private Double currentBalance;
    @SerializedName("DMRBalance")
    @Expose
    private Double dMRBalance;
    @SerializedName("MinimumBalance")
    @Expose
    private Double minimumBalance;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Double getDMRBalance() {
        return dMRBalance;
    }

    public void setDMRBalance(Double dMRBalance) {
        this.dMRBalance = dMRBalance;
    }

    public Double getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(Double minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}
