package in.omkarpayment.app.json;

/**
 * Created by krish on 05-Jan-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDetailsPojo {


    @SerializedName("ENDSUBSCRIPTION")
    @Expose
    private Integer eNDSUBSCRIPTION;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("CurrentBalance")
    @Expose
    private Double currentBalance;
    @SerializedName("UserType")
    @Expose
    private String userType;
    @SerializedName("UserTypeID")
    @Expose
    private Integer userTypeID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ParentName")
    @Expose
    private String parentName;
    @SerializedName("IP")
    @Expose
    private String iP;
    @SerializedName("IPActive")
    @Expose
    private Object iPActive;
    @SerializedName("Token")
    @Expose
    private String token;

    public Integer getENDSUBSCRIPTION() {
        return eNDSUBSCRIPTION;
    }

    public void setENDSUBSCRIPTION(Integer eNDSUBSCRIPTION) {
        this.eNDSUBSCRIPTION = eNDSUBSCRIPTION;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Integer getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(Integer userTypeID) {
        this.userTypeID = userTypeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getIP() {
        return iP;
    }

    public void setIP(String iP) {
        this.iP = iP;
    }

    public Object getIPActive() {
        return iPActive;
    }

    public void setIPActive(Object iPActive) {
        this.iPActive = iPActive;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
