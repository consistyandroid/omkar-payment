package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 05-Apr-17.
 */

public class ViewStockpojo {

    @SerializedName("RequestId")
    @Expose
    private String requestId;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("UplineId")
    @Expose
    private String uplineId;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("PayMode")
    @Expose
    private String payMode;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("UtrNo")
    @Expose
    private String utrNo;
    @SerializedName("OldBal")
    @Expose
    private String oldBal;
    @SerializedName("CloseBal")
    @Expose
    private String closeBal;
    @SerializedName("STATUS")
    @Expose
    private String sTATUS;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("BillNo")
    @Expose
    private String billNo;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("DOCTime")
    @Expose
    private String dOCTime;
    @SerializedName("UpdateDate")
    @Expose
    private String updateDate;
    @SerializedName("UpdateTime")
    @Expose
    private String updateTime;
    @SerializedName("CreditTo")
    @Expose
    private String creditTo;
    @SerializedName("NAME")
    @Expose
    private String nAME;
    @SerializedName("Mobile_Number")
    @Expose
    private String mobileNumber;
    @SerializedName("UserType")
    @Expose
    private String userType;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUplineId() {
        return uplineId;
    }

    public void setUplineId(String uplineId) {
        this.uplineId = uplineId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getUtrNo() {
        return utrNo;
    }

    public void setUtrNo(String utrNo) {
        this.utrNo = utrNo;
    }

    public String getOldBal() {
        return oldBal;
    }

    public void setOldBal(String oldBal) {
        this.oldBal = oldBal;
    }

    public String getCloseBal() {
        return closeBal;
    }

    public void setCloseBal(String closeBal) {
        this.closeBal = closeBal;
    }

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getDOCTime() {
        return dOCTime;
    }

    public void setDOCTime(String dOCTime) {
        this.dOCTime = dOCTime;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreditTo() {
        return creditTo;
    }

    public void setCreditTo(String creditTo) {
        this.creditTo = creditTo;
    }

    public String getNAME() {
        return nAME;
    }

    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

}
