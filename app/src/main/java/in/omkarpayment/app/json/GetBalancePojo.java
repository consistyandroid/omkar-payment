package in.omkarpayment.app.json;

/**
 * Created by Somnath-Laptop on 06-Jan-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBalancePojo {

    @SerializedName("CurrentBalance")
    @Expose
    private Double currentBalance;
    @SerializedName("MinimumBalance")
    @Expose
    private Double minimumBalance;

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Double getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(Double minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

}

