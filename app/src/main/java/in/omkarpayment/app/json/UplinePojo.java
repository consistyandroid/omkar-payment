package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 04-Apr-17.
 */

public class UplinePojo {

    @SerializedName("UplineName")
    @Expose
    private String uplineName;
    @SerializedName("UplineId")
    @Expose
    private String uplineId;

    public String getUplineName() {
        return uplineName;
    }

    public void setUplineName(String uplineName) {
        this.uplineName = uplineName;
    }

    public String getUplineId() {
        return uplineId;
    }

    public void setUplineId(String uplineId) {
        this.uplineId = uplineId;
    }
}
