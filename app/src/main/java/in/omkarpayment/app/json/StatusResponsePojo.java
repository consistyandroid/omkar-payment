package in.omkarpayment.app.json;

/**
 * Created by Admin on 07 Dec 2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StatusResponsePojo {

    @SerializedName("Status")
    @Expose
    private String status;

    public String getStatus(ArrayList<StatusResponsePojo> statuslist) {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }


}
