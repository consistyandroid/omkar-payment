package in.omkarpayment.app.json;

/**
 * Created by ${user} on 19/12/17.
 */

public class optionData {

    String ItemName;
    String ItemId;
    int image;


    public optionData(String ItemName, String ItemId, int image) {
        this.ItemId = ItemId;
        this.ItemName = ItemName;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }
}
