package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AEPSKeyResponsePojo {
    @SerializedName("SecretKey")
    @Expose
    private String secretKey;
    @SerializedName("SecretKeyTimestamp")
    @Expose
    private String secretKeyTimestamp;
    @SerializedName("Hash")
    @Expose
    private String hash;
    @SerializedName("developerkey")
    @Expose
    private String developerkey;
    @SerializedName("initiatorid")
    @Expose
    private String initiatorid;
    @SerializedName("usercode")
    @Expose
    private String usercode;
    @SerializedName("LogoURL")
    @Expose
    private String logoURL;

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSecretKeyTimestamp() {
        return secretKeyTimestamp;
    }

    public void setSecretKeyTimestamp(String secretKeyTimestamp) {
        this.secretKeyTimestamp = secretKeyTimestamp;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getDeveloperkey() {
        return developerkey;
    }

    public void setDeveloperkey(String developerkey) {
        this.developerkey = developerkey;
    }

    public String getInitiatorid() {
        return initiatorid;
    }

    public void setInitiatorid(String initiatorid) {
        this.initiatorid = initiatorid;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }
}
