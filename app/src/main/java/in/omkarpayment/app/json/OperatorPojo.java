package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 05-Aug-17.
 */

public class OperatorPojo {

    @SerializedName("OperatorName")
    @Expose
    private String operatorName;

    @SerializedName("ServiceName")
    @Expose
    private String serviceName;

    @SerializedName("OperatorID")
    @Expose
    private Integer operatorID;
    @SerializedName("ServiceID")
    @Expose
    private Integer serviceID;

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    public Integer getServiceID() {
        return serviceID;
    }

    public void setServiceID(Integer serviceID) {
        this.serviceID = serviceID;
    }
}
