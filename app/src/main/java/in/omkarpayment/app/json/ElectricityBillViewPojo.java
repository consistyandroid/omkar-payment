package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Krish on 11-Aug-17.
 */

public class ElectricityBillViewPojo {

    @SerializedName("Recharge")
    @Expose
    private List<ElectricityBillViewArrayPojo> recharge = null;

    public List<ElectricityBillViewArrayPojo> getRecharge() {
        return recharge;
    }

    public void setRecharge(List<ElectricityBillViewArrayPojo> recharge) {
        this.recharge = recharge;
    }
}
