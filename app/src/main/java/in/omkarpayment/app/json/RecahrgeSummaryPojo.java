package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 26-May-17.
 */

public class RecahrgeSummaryPojo {

    @SerializedName("RechargeCount")
    @Expose
    private String rechargeCount;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("ServiceType")
    @Expose
    private String serviceType;

    public String getRechargeCount() {
        return rechargeCount;
    }

    public void setRechargeCount(String rechargeCount) {
        this.rechargeCount = rechargeCount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }


}
