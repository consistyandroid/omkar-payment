package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ${user} on 5/2/18.
 */

public class DTHPlansPojo {
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;
    @SerializedName("plan_name")
    @Expose
    private String planName;
    @SerializedName("1Month")
    @Expose
    private String _1Month;
    @SerializedName("3Month")
    @Expose
    private String _3Month;
    @SerializedName("6Month")
    @Expose
    private String _6Month;
    @SerializedName("1Year")
    @Expose
    private String _1Year;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String get1Month() {
        return _1Month;
    }

    public void set1Month(String _1Month) {
        this._1Month = _1Month;
    }

    public String get3Month() {
        return _3Month;
    }

    public void set3Month(String _3Month) {
        this._3Month = _3Month;
    }

    public String get6Month() {
        return _6Month;
    }

    public void set6Month(String _6Month) {
        this._6Month = _6Month;
    }

    public String get1Year() {
        return _1Year;
    }

    public void set1Year(String _1Year) {
        this._1Year = _1Year;
    }

}
