package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 29-May-17.
 */

public class ViewComplaintPojo {


    @SerializedName("SENDER")
    @Expose
    private String sENDER;
    @SerializedName("REGID")
    @Expose
    private String rEGID;
    @SerializedName("COMPLAINTTYPE")
    @Expose
    private String cOMPLAINTTYPE;
    @SerializedName("OPERATOR_ID")
    @Expose
    private String oPERATORID;
    @SerializedName("OPERATOR_NAME")
    @Expose
    private String oPERATORNAME;
    @SerializedName("MOBILENO_VCNO")
    @Expose
    private String mOBILENOVCNO;
    @SerializedName("RDOC")
    @Expose
    private String rDOC;
    @SerializedName("RDOCTime")
    @Expose
    private String rDOCTime;
    @SerializedName("AMOUNT")
    @Expose
    private String aMOUNT;
    @SerializedName("STATUS")
    @Expose
    private String sTATUS;
    @SerializedName("DOA")
    @Expose
    private String dOA;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("REMARKS")
    @Expose
    private String rEMARKS;
    @SerializedName("TRANS_ID")
    @Expose
    private String tRANSID;
    @SerializedName("MOBILE_NUMBER")
    @Expose
    private String mOBILENUMBER;
    @SerializedName("REFUND_ID")
    @Expose
    private String rEFUNDID;
    @SerializedName("DOR")
    @Expose
    private String dOR;
    @SerializedName("THROUGH")
    @Expose
    private String tHROUGH;
    @SerializedName("SERVICETYPE")
    @Expose
    private String sERVICETYPE;
    @SerializedName("API_NAME")
    @Expose
    private String aPINAME;
    @SerializedName("DOC1")
    @Expose
    private String dOC1;
    @SerializedName("DOCTime")
    @Expose
    private String dOCTime;
    @SerializedName("OP_BAL")
    @Expose
    private String oPBAL;
    @SerializedName("SMS_SENT")
    @Expose
    private String sMSSENT;
    @SerializedName("ClosingBAL")
    @Expose
    private String closingBAL;
    @SerializedName("RefundAMT")
    @Expose
    private String refundAMT;
    @SerializedName("RefundStatus")
    @Expose
    private String refundStatus;
    @SerializedName("APIUrl")
    @Expose
    private String aPIUrl;
    @SerializedName("ServiceId")
    @Expose
    private String serviceId;

    public String getSENDER() {
        return sENDER;
    }

    public void setSENDER(String sENDER) {
        this.sENDER = sENDER;
    }

    public String getREGID() {
        return rEGID;
    }

    public void setREGID(String rEGID) {
        this.rEGID = rEGID;
    }

    public String getCOMPLAINTTYPE() {
        return cOMPLAINTTYPE;
    }

    public void setCOMPLAINTTYPE(String cOMPLAINTTYPE) {
        this.cOMPLAINTTYPE = cOMPLAINTTYPE;
    }

    public String getOPERATORID() {
        return oPERATORID;
    }

    public void setOPERATORID(String oPERATORID) {
        this.oPERATORID = oPERATORID;
    }

    public String getOPERATORNAME() {
        return oPERATORNAME;
    }

    public void setOPERATORNAME(String oPERATORNAME) {
        this.oPERATORNAME = oPERATORNAME;
    }

    public String getMOBILENOVCNO() {
        return mOBILENOVCNO;
    }

    public void setMOBILENOVCNO(String mOBILENOVCNO) {
        this.mOBILENOVCNO = mOBILENOVCNO;
    }

    public String getRDOC() {
        return rDOC;
    }

    public void setRDOC(String rDOC) {
        this.rDOC = rDOC;
    }

    public String getRDOCTime() {
        return rDOCTime;
    }

    public void setRDOCTime(String rDOCTime) {
        this.rDOCTime = rDOCTime;
    }

    public String getAMOUNT() {
        return aMOUNT;
    }

    public void setAMOUNT(String aMOUNT) {
        this.aMOUNT = aMOUNT;
    }

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public String getDOA() {
        return dOA;
    }

    public void setDOA(String dOA) {
        this.dOA = dOA;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getREMARKS() {
        return rEMARKS;
    }

    public void setREMARKS(String rEMARKS) {
        this.rEMARKS = rEMARKS;
    }

    public String getTRANSID() {
        return tRANSID;
    }

    public void setTRANSID(String tRANSID) {
        this.tRANSID = tRANSID;
    }

    public String getMOBILENUMBER() {
        return mOBILENUMBER;
    }

    public void setMOBILENUMBER(String mOBILENUMBER) {
        this.mOBILENUMBER = mOBILENUMBER;
    }

    public String getREFUNDID() {
        return rEFUNDID;
    }

    public void setREFUNDID(String rEFUNDID) {
        this.rEFUNDID = rEFUNDID;
    }

    public String getDOR() {
        return dOR;
    }

    public void setDOR(String dOR) {
        this.dOR = dOR;
    }

    public String getTHROUGH() {
        return tHROUGH;
    }

    public void setTHROUGH(String tHROUGH) {
        this.tHROUGH = tHROUGH;
    }

    public String getSERVICETYPE() {
        return sERVICETYPE;
    }

    public void setSERVICETYPE(String sERVICETYPE) {
        this.sERVICETYPE = sERVICETYPE;
    }

    public String getAPINAME() {
        return aPINAME;
    }

    public void setAPINAME(String aPINAME) {
        this.aPINAME = aPINAME;
    }

    public String getDOC1() {
        return dOC1;
    }

    public void setDOC1(String dOC1) {
        this.dOC1 = dOC1;
    }

    public String getDOCTime() {
        return dOCTime;
    }

    public void setDOCTime(String dOCTime) {
        this.dOCTime = dOCTime;
    }

    public String getOPBAL() {
        return oPBAL;
    }

    public void setOPBAL(String oPBAL) {
        this.oPBAL = oPBAL;
    }

    public String getSMSSENT() {
        return sMSSENT;
    }

    public void setSMSSENT(String sMSSENT) {
        this.sMSSENT = sMSSENT;
    }

    public String getClosingBAL() {
        return closingBAL;
    }

    public void setClosingBAL(String closingBAL) {
        this.closingBAL = closingBAL;
    }

    public String getRefundAMT() {
        return refundAMT;
    }

    public void setRefundAMT(String refundAMT) {
        this.refundAMT = refundAMT;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getAPIUrl() {
        return aPIUrl;
    }

    public void setAPIUrl(String aPIUrl) {
        this.aPIUrl = aPIUrl;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
