package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ${user} on 5/2/18.
 */

public class Rs {
    @SerializedName("1 MONTHS")
    @Expose
    private String _1MONTHS;

    public String get1MONTHS() {
        return _1MONTHS;
    }

    public void set1MONTHS(String _1MONTHS) {
        this._1MONTHS = _1MONTHS;
    }
}
