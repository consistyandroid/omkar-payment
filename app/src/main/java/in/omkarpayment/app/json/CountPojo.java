package in.omkarpayment.app.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Krish on 22-Apr-17.
 */

public class CountPojo {

    @SerializedName("Column1")
    @Expose
    private String Column1;

    public String getColumn1() {
        return Column1;
    }

    public void setColumn1(String column1) {
        Column1 = column1;
    }
}
