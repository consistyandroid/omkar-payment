package in.omkarpayment.app.model;


import java.util.concurrent.TimeUnit;

import in.omkarpayment.app.json.ElectricityBillViewPojo;
import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.userContent.ApplicationURL;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface IWebServicesModel {

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(80, TimeUnit.SECONDS)
            .writeTimeout(80, TimeUnit.SECONDS)
            .readTimeout(80, TimeUnit.SECONDS)
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ApplicationURL.base_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    Retrofit aepsRetrofit = new Retrofit.Builder()
            .baseUrl(ApplicationURL.aeps_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    Retrofit retrofit2 = new Retrofit.Builder()

            .baseUrl(ApplicationURL.electricityBill_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    @GET("LoginUser.asmx/SelectUser")
    Call<WebserviceResponsePojo> LoginWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("LoginUser.asmx/SelectUserWithOTP")
    Call<WebserviceResponsePojo> LoginWithOTPWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );
    @GET("Loginuser.asmx/SelectMoneyTransferUI")
    Call<WebserviceResponsePojo> MoneyTransferUIWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("androidrecharge.asmx/AllOperator")
    Call<WebserviceResponsePojo> GetOperatorWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("LoginUser.asmx/SelectBalance")
    Call<WebserviceResponsePojo> GetAvailableBalance(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AndroidRecharge.ASMX/Recharge")
    Call<WebserviceResponsePojo> RechargeWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("FreeUserRegistration.asmx/UserRegistration")
    Call<WebserviceResponsePojo> FreeUserWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("UserActivation.asmx/UserRegistration")
    Call<WebserviceResponsePojo> UserActivationWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata);

    @GET("LastTenRechargeDetails.asmx/LastTenRechargeReport")
    Call<WebserviceResponsePojo> LastTransactionWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("ViewMESB.asmx/ViewElectricityBill")
    Call<ElectricityBillViewPojo> ElectricityBillView(
            @Query("consumerNumber") String consumerNumber
    );

    @GET("DateWiseReport.asmx/RechargeReport")
    Call<WebserviceResponsePojo> DayReportWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("ChangePassword.asmx/UpdatePassword")
    Call<WebserviceResponsePojo> ChangePasswordWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceTransfer.asmx/BalTransfer")
    Call<WebserviceResponsePojo> BalanceTransferWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    /*
        @GET("BalanceTransfer.asmx/BalanceReceiver")
        Call<WebserviceResponsePojo> DownLineForDropWebservice(
                @Query("Request") String Request,
                @Query("Keydata") String Keydata
        );*/
    @GET("DownlineReport.asmx/DownlineMember")
    Call<WebserviceResponsePojo> DownLineForDropWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DownlineReport.asmx/DownlineMember")
    Call<WebserviceResponsePojo> DownLineListWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceTransferReport.asmx/BalanceTransfer")
    Call<WebserviceResponsePojo> BalanceTransferReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceTransferReport.asmx/BalanceReceive")
    Call<WebserviceResponsePojo> BalanceReceiveReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceRequest.asmx/BalRequest")
    Call<WebserviceResponsePojo> StockRequest(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceTransfer.asmx/WalletReceiver")
    Call<WebserviceResponsePojo> ViewUserWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("ChangePin.asmx/UpdatePin")
    Call<WebserviceResponsePojo> ChangePinWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceReverse.asmx/BalReverse")
    Call<WebserviceResponsePojo> BalanceReverseWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("Balancetransferreport.asmx/WalletReceiver")
    Call<WebserviceResponsePojo> WalletTransReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("Balancetransferreport.asmx/WalletReceiver")
    Call<WebserviceResponsePojo> SearchNumberr(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("selectNotification.asmx/selectNotification")
//selectNotification.asmx/selectNotification
    Call<WebserviceResponsePojo> selectNotification(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceTransferReport.asmx/BalanceTransfer")
    Call<WebserviceResponsePojo> PurchaseReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("BalanceTransferReport.asmx/BalanceReceive")
    Call<WebserviceResponsePojo> PurchaseReportCredit(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("ForgotPassword.asmx/ResetPassword")
    Call<WebserviceResponsePojo> ForgotWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("QuickSearch.asmx/SelectRechargeReport")
    Call<WebserviceResponsePojo> SearchNumber(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("ElectricityViewPay.asmx/ViewBill")
    Call<WebserviceResponsePojo> vewBill(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("ElectricityViewPay.asmx/PayBill")
    Call<WebserviceResponsePojo> payElectricityBill(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("CommisionStructure.asmx/SelectCommissionReport")
    Call<WebserviceResponsePojo> commissionStructure(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("Complaint.asmx/ComplaintInsert")
    Call<WebserviceResponsePojo> AddComplaint(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("Complaint.asmx/ComplaintReport")
    Call<WebserviceResponsePojo> ViewComplaint(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceRequest.asmx/RequestReport")
    Call<WebserviceResponsePojo> StockReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("ChangeMobileNo.asmx/UpdateMobileNo")
    Call<WebserviceResponsePojo> ChangeMobileNo(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("MoneyTransfer.asmx/CheckDefaultIFSC")
    Call<WebserviceRespoPojo2> checkForDefaultIFSC(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    //Money Transfer Web services

    @GET("DMT/SenderRegistration.asmx/SenderRegister")
    Call<WebserviceResponsePojo> SenderRegistration(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );
    @GET("DMT/SenderRegistration.asmx/SenderRegistrationSubmitOTP")
    Call<WebserviceResponsePojo> SenderRegistrationWithOTP(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/AddBenificiary.asmx/BenificiaryRegistrationWithoutVerification")
    Call<WebserviceResponsePojo> AddBeneficiary(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/AddBenificiary.asmx/PassOTP")
    Call<WebserviceResponsePojo> OTP(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/AddBenificiary.asmx/ResendOTP")
    Call<WebserviceResponsePojo> ResendOTP(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/SelectBenificiaryList.asmx/BenificiaryList")
    Call<WebserviceResponsePojo> BeneficiaryList(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/SelectBenificiaryList.asmx/SelectSender")
    Call<WebserviceResponsePojo> SelectSender(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/MoneyTransferReport.asmx/DMRReport")
    Call<WebserviceResponsePojo> MoneyTransferReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("DMT/GetIFSC.asmx/GetBank")
    Call<WebserviceResponsePojo> GetBankNamesWebservice(
            @Query("Keydata") String Keydata
    );

    @GET("DMT/GetIFSC.asmx/GetBank")
    Call<WebserviceResponsePojo> Get(
            @Query("Keydata") String Keydata
    );

    @GET("DMT/GetIFSC.asmx/GetBank")
    Call<WebserviceResponsePojo> GetDefaultIfscCode(
            @Query("Keydata") String Keydata
    );

    @GET("DMT/ValidateBenificiary.asmx/IsValidBenificiary")
    Call<WebserviceResponsePojo> VerificationRequest(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    //Call<WebserviceResponsePojo> LoginWithOTPWebservice(String encryptedString, String s);
    //Browse Plan And R Offer
    @GET("ROffers.asmx/Select")
    Call<WebserviceResponsePojo> RofferWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("SimplePlans.asmx/Select")
    Call<WebserviceResponsePojo> BrowesPlansWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("SimplePlans.asmx/SelectDTHPlans")
    Call<WebserviceResponsePojo> DTHPlansWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("ViewCustomerInfo.asmx/SelectDTHUserInformation")
    Call<WebserviceResponsePojo> CustomerInfoWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("SlabCommissionReport.asmx/SelectUser")
    Call<WebserviceResponsePojo> GetMoneyTxCommissionWebservice(
            @Query("Keydata") String Keydata
    );
    @GET("loginuser.asmx/SelectCommission")
    Call<WebserviceResponsePojo> SaleCommissionWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );
    @GET("CashCollection.asmx/Insert")
    Call<WebserviceResponsePojo> CashCollectionWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("CashCollection.asmx/selectCashCollectionReport")
    Call<WebserviceResponsePojo> CashCollectionReportWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );
    @GET("AEPSKeyResponce.asmx/Select")
    Call<WebserviceResponsePojo> AepsKeyResponseWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AEPS/AEPSAndroid.asmx/InitiateTransaction")
    Call<WebserviceResponsePojo> getAEPSTransactionId(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AEPS/Services.asmx/EnableDisable")
    Call<WebserviceResponsePojo> enableUserService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AEPSTransactionInit.asmx/AEPSTransferReport")
    Call<WebserviceResponsePojo> AEPSTransactionReport(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceRequest.asmx/RequestReject")
    Call<WebserviceResponsePojo> RejectBalanceRequestWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("BalanceRequest.asmx/RequestAccept")
    Call<WebserviceResponsePojo> AcceptBalanceRequestWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );


}
