package in.omkarpayment.app.model;

import in.omkarpayment.app.Addcomplaint.IAddComplaintPresenter;
import in.omkarpayment.app.MoneyTransferpkg.AddBenificiary.IAddBenificiaryPresenter;
import in.omkarpayment.app.MoneyTransferpkg.Registrationpkg.ISenderRegistrationPresenter;
import in.omkarpayment.app.MoneyTransferpkg.validationpkg.IBeneficiaryListPresenter;
import in.omkarpayment.app.balanceTransfer.ITransferPresenter;
import in.omkarpayment.app.changeMobileNumber.IChangeMobileNumberPresenter;
import in.omkarpayment.app.changePassword.IChangePasswordPresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.downlinelist.IDownlineListPresenter;
import in.omkarpayment.app.freeUserRegistration.IFreUserPresenter;
import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.login.ILoginPresenter;
import in.omkarpayment.app.main.IMainPresenter;
import in.omkarpayment.app.purchaseReport.IPurchaseReportPresenter;
import in.omkarpayment.app.recharge.IRechargePresenter;
import in.omkarpayment.app.recharge.electricity.IElectricityBillPresenter;
import in.omkarpayment.app.report.MoneyTransferReport.IMoneyTransferReportPresenter;
import in.omkarpayment.app.report.StockReport.IStockReportPresenter;
import in.omkarpayment.app.report.balanceReport.IBalanceReportPresenter;
import in.omkarpayment.app.report.commissionStructure.ICommissionSPresenter;
import in.omkarpayment.app.report.complaintReport.IComplaintReportPresenter;
import in.omkarpayment.app.report.dayreport.IDayReportPresenter;
import in.omkarpayment.app.report.lasttransaction.ILastTransactionPresenter;
import in.omkarpayment.app.report.searchMobile.ISearchNumberPresenter;
import in.omkarpayment.app.stockRequest.IStockRequestPresenter;
import in.omkarpayment.app.userActivation.IActivationPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Krish on 28-Jul-17.
 */

public class WebServiceModel {
    public ILoginPresenter inject = null;
    public IMoneyTransferReportPresenter delegateMoneyTransferReport = null;
    public IRechargePresenter delegateRechargeResponse = null;
    public IFreUserPresenter delegateFreeUserResponse = null;
    public IPurchaseReportPresenter delegatePurchaseReport = null;
    public IActivationPresenter delegateActivationResponse = null;
    public ILastTransactionPresenter delegateLastTransactionResponse = null;
    public IComplaintReportPresenter delegateComplaintReportResponse = null;
    public IChangePasswordPresenter delegateChangePassResponse = null;
    public ISearchNumberPresenter delegateSearchNumber = null;
    public IDayReportPresenter delegateDayResponse = null;
    public ITransferPresenter delegateBalTransferResponse = null;
    public IAvailableBalancePresenter delegateAvailableBalanceResponse = null;
    public IDownlineListPresenter delegatDownlineListResponse = null;
    public IBalanceReportPresenter delegateBalanceReportResponse = null;
    public IStockRequestPresenter delegateStockRequestResponse = null;
    public IElectricityBillPresenter delegateElectricity = null;
    public IAddComplaintPresenter delegateAddComplaint = null;
    public ICommissionSPresenter delegateCommissionStructure = null;
    public IMainPresenter delegateMainResponse = null;
    public IStockReportPresenter delegateStockReportResponse = null;
    public IChangeMobileNumberPresenter delegateChangeMobileNoResponse = null;


    public ISenderRegistrationPresenter delegateSenderRegistration = null;
    public IAddBenificiaryPresenter delegateAddBeneficiary = null;
    public IBeneficiaryListPresenter delegateBeneficiaryList = null;

    IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
    Call<WebserviceResponsePojo> call;
    LogWriter log = new LogWriter();

    public void webserviceMethod(final String encryptedString, final String data, final String Tag) {
        switch (Tag) {
            case "Login":
                call = webObject.LoginWebservice(encryptedString, "");
                break;
            case "LoginWithOTP":
                call = webObject.LoginWithOTPWebservice(encryptedString, "");
                break;

            case "ForgotPassword":
                call = webObject.ForgotWebservice(encryptedString, "");
                break;


            case "Operator":
                call = webObject.GetOperatorWebservice(encryptedString, data);
                break;
            case "Recharge":
                call = webObject.RechargeWebservice(encryptedString, data);
                break;
            case "FreeUserRegistration":
                call = webObject.FreeUserWebservice(encryptedString, "");
                break;
            case "UserActivation":
                call = webObject.UserActivationWebservice(encryptedString, data);
                break;
            case "LastTransaction":
                call = webObject.LastTransactionWebservice(encryptedString, data);
                break;
            case "LastTransactionInRecharge":
                call = webObject.LastTransactionWebservice(encryptedString, data);
                break;
            case "DayReport":
                call = webObject.DayReportWebservice(encryptedString, data);
                break;
            case "PurchaseReport":
                call = webObject.PurchaseReport(encryptedString, data);
                break;
            case "PurchaseReportCredit":
                call = webObject.PurchaseReportCredit(encryptedString, data);
                break;
            case "ChangePassword":
                call = webObject.ChangePasswordWebservice(encryptedString, data);
                break;
            case "BalanceTransfer":
                call = webObject.BalanceTransferWebservice(encryptedString, data);
                break;
            case "DownLineForDropDown":
                call = webObject.DownLineForDropWebservice(encryptedString, data);
                break;
            case "CheckAvailableBalance":
                call = webObject.GetAvailableBalance(encryptedString, data);
                break;
            case "DownLineList":
                call = webObject.DownLineListWebservice("", data);
                break;
            case "BalanceTransReport":
                call = webObject.BalanceTransferReport(encryptedString, data);
                break;
            case "BalanceReceiveReport":
                call = webObject.BalanceReceiveReport(encryptedString, data);
                break;
            case "WalletTranReport":
                call = webObject.WalletTransReport(encryptedString, data);
                break;
            case "StockRequest":
                call = webObject.StockRequest(encryptedString, data);
                break;
            case "StockReport":
                call = webObject.StockReport(encryptedString, data);
                break;

            case "ViewUser":
                call = webObject.ViewUserWebservice(encryptedString, data);
                break;
            case "ChangePin":
                call = webObject.ChangePinWebservice(encryptedString, data);
                break;
            case "BalanceReverse":
                call = webObject.BalanceReverseWebservice(encryptedString, data);
                break;
            case "SearchNumber":
                call = webObject.SearchNumber(encryptedString, data);
                break;
            case "GetNotification":
                call = webObject.selectNotification("", data);
                break;
            case "Electricity":
                call = webObject.vewBill(encryptedString, data);
                break;
            case "ElectricityBillPay":
                call = webObject.payElectricityBill(encryptedString, data);
                break;
            case "CommmissionStructure":
                call = webObject.commissionStructure("", data);
                break;
            case "AddComplaint":
                call = webObject.AddComplaint(encryptedString, data);
                break;
            case "ViewComplaint":
                call = webObject.ViewComplaint(encryptedString, data);
                break;
            case "ChangeMobileNo":
                call = webObject.ChangeMobileNo(encryptedString, data);
                break;

            //Money Transfer

            case "SenderRegistration":
                call = webObject.SenderRegistration(encryptedString, data);
                break;
            case "AddBeneficiary":
                call = webObject.AddBeneficiary(encryptedString, data);
                break;
            case "GetBank":
                call = webObject.GetBankNamesWebservice(data);
                break;
            case "OTP":
                call = webObject.OTP(encryptedString, data);
                break;
            case "OTPADDB":
                call = webObject.OTP(encryptedString, data);
                break;
            case "ResendOTP":
                call = webObject.ResendOTP(encryptedString, data);
                break;
            case "BeneficiaryList":
                call = webObject.BeneficiaryList(encryptedString, data);
                break;
            case "MoneyTransferReport":
                call = webObject.MoneyTransferReport(encryptedString, data);
                break;

            case "SenderDetails":
                call = webObject.SelectSender(encryptedString, data);
                break;
            case "Verification":
                call = webObject.VerificationRequest(encryptedString, data);
                break;
            /////////////////////////////

        }
        call.enqueue(new Callback<WebserviceResponsePojo>() {
            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                dismissDialog(Tag);
                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equalsIgnoreCase("")) {
                            log.i("onResponseBody", response.body().getBody());
                            log.i("onResponseSStatus", response.body().getStatus());
                            sendSuccessResponse(response.body().getBody(), Tag);
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            log.i("onResponseFBody", response.body().getBody());
                            log.i("onResponseFStatus", response.body().getStatus());
                            sendFailureResponse(response.body().getBody(), Tag);
                        } else {
                            errorMsg(Tag, response.body().getStatus());
                        }
                    } else if (response.code() == 500) {
                        errorMsg(Tag, AllMessages.ServerError);
                    } else if (response.code() == 404) {
                        errorMsg(Tag, AllMessages.PageNotFound);
                    } else {
                        somethingWrongMsg(Tag);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                dismissDialog(Tag);
                log.i("onFailure", t.toString());
                internetErrorMsg(Tag);
            }
        });
    }


    private void sendSuccessResponse(String encryptedResponse, String tag) {
        switch (tag) {
            case "Login":
                inject.decryptString(encryptedResponse);
                break;

            case "LoginWithOTP":
                inject.decryptString(encryptedResponse);
                break;

            case "Operator":
                delegateRechargeResponse.decryptOperatorResponse(encryptedResponse);
                break;
            case "Recharge":
                delegateRechargeResponse.decryptRechargeResponse(encryptedResponse);
                break;
            case "FreeUserRegistration":
                delegateFreeUserResponse.decryptFreeUserResponse(encryptedResponse);
                break;
            case "UserActivation":
                delegateActivationResponse.decryptActivationResponse(encryptedResponse);
                break;
            case "LastTransaction":
                delegateLastTransactionResponse.LastTransactionResponse(encryptedResponse);
                break;
            case "LastTransactionInRecharge":
                delegateRechargeResponse.LastTransactionResponse(encryptedResponse);
                break;
            case "DayReport":
                delegateDayResponse.DayReportResponse(encryptedResponse);
                break;
            case "PurchaseReport":
                delegatePurchaseReport.PurchaseReportResponseResponse(encryptedResponse);
                break;
            case "PurchaseReportCredit":
                delegatePurchaseReport.PurchaseReportResponseResponse(encryptedResponse);
                break;
            case "ChangePassword":
                delegateChangePassResponse.ChangePasswordResponse(encryptedResponse);
                break;
            case "ChangePin":
                delegateChangePassResponse.ChangePasswordResponse(encryptedResponse);
                break;
            case "BalanceTransfer":
                delegateBalTransferResponse.BalanceTransferResponse(encryptedResponse);
                break;
            case "BalanceReverse":
                delegateBalTransferResponse.BalanceTransferResponse(encryptedResponse);
                break;
            case "DownLineForDropDown":
                delegateBalTransferResponse.DownLineForDropDown(encryptedResponse);
                break;
            case "CheckAvailableBalance":
                delegateAvailableBalanceResponse.AvailableBalanceResponse(encryptedResponse);
                break;
            case "DownLineList":
                delegatDownlineListResponse.DownlineListResponse(encryptedResponse);
                break;
            case "BalanceTransReport":
                delegateBalanceReportResponse.BalanceReportResponseResponse(encryptedResponse);
                break;
            case "BalanceReceiveReport":
                delegateBalanceReportResponse.BalanceReportResponseResponse(encryptedResponse);
                break;
            case "WalletTranReport":
                delegateBalanceReportResponse.BalanceReportResponseResponse(encryptedResponse);
                break;
            case "StockRequest":
                delegateStockRequestResponse.StockReqResponseResponse(encryptedResponse);
                break;
            case "StockReport":
                delegateStockReportResponse.StockReportResponse(encryptedResponse);
                break;
            case "ViewUser":
                delegateBalTransferResponse.DownLineForDropDown(encryptedResponse);
                break;
            case "GetNotification":
                delegateMainResponse.selectNotification(encryptedResponse);
                break;
            case "SearchNumber":
                delegateSearchNumber.decryptSearchNumberReportListResponse(encryptedResponse);
                break;
            case "Electricity":
                delegateElectricity.decryptViewBillPaymentResponse(encryptedResponse);
                break;

            case "ElectricityBillPay":
                delegateElectricity.decryptPayBillResponse(encryptedResponse);
                break;
            case "CommmissionStructure":
                delegateCommissionStructure.decryptCommissionStructureResponse(encryptedResponse);
                break;
            case "ViewComplaint":
                delegateComplaintReportResponse.decreptedComplaintReportResponse(encryptedResponse);
                break;
            case "ChangeMobileNo":
                delegateChangeMobileNoResponse.decryptedChangeMobileNumberResponse(encryptedResponse);
                break;
            case "ForgotPassword":
                inject.forgotPasswordResponse(encryptedResponse);
                break;


            //Money Transfer
            case "MoneyTransferReport":
                delegateMoneyTransferReport.decryptMoneyTransferReportListResponse(encryptedResponse);
                break;
            case "SenderRegistration":
                delegateSenderRegistration.decryptSenderRegistrationResponse(encryptedResponse);
                break;
            case "BeneficiaryList":
                delegateBeneficiaryList.decryptBeneficiaryListResponse(encryptedResponse);
                break;
            case "OTP":
                delegateAddBeneficiary.decryptOTPResponse(encryptedResponse);
                break;
            case "OTPADDB":
                delegateAddBeneficiary.decryptOTPResponse(encryptedResponse);
                break;
            case "AddBeneficiary":
                delegateAddBeneficiary.decryptAddBeneficiaryResponse(encryptedResponse);
                break;
            case "GetBank":
                delegateAddBeneficiary.decryptGetBankNameResponse(encryptedResponse);
                break;


            case "SenderDetails":
                delegateBeneficiaryList.SenderDetailsResponse(encryptedResponse);
                break;
            case "Verification":
                delegateAddBeneficiary.VerificationResponseResponse(encryptedResponse);
                break;
        }
    }

    private void sendFailureResponse(String encryptedResponse, String tag) throws Exception {
        switch (tag) {
            case "Login":
                inject.LoginFailureResponse(encryptedResponse);
                break;

            case "LoginWithOTP":
                inject.LoginFailureResponse(encryptedResponse);
                break;

            case "Operator":
                delegateRechargeResponse.decryptOperatorResponse(encryptedResponse);
                break;
            case "Recharge":
                delegateRechargeResponse.decryptRechargeResponse(encryptedResponse);
                break;
            case "FreeUserRegistration":
                delegateFreeUserResponse.decryptFreeUserResponse(encryptedResponse);
                break;
            case "UserActivation":
                delegateActivationResponse.decryptActivationResponse(encryptedResponse);
                break;
            case "LastTransaction":
                delegateLastTransactionResponse.LastTransactionFailureResponse(encryptedResponse);
                break;
            case "LastTransactionInRecharge":
                delegateRechargeResponse.LastTransactionFailureResponse(encryptedResponse);
                break;
            case "DayReport":
                delegateDayResponse.DayReportFailureResponse(encryptedResponse);
                break;
            case "PurchaseReport":
                delegatePurchaseReport.PurchaseReportResponseFailResponse(encryptedResponse);
                break;
            case "PurchaseReportCredit":
                delegatePurchaseReport.PurchaseReportResponseFailResponse(encryptedResponse);
                break;
            case "ChangePassword":
                delegateChangePassResponse.ChangePasswordFailureResponse(encryptedResponse);
                break;
            case "ChangePin":
                delegateChangePassResponse.ChangePasswordFailureResponse(encryptedResponse);
                break;
            case "BalanceTransfer":
                delegateBalTransferResponse.BalanceTransferFailureResponse(encryptedResponse);
                break;
            case "BalanceReverse":
                delegateBalTransferResponse.BalanceTransferFailureResponse(encryptedResponse);
                break;
            case "DownLineForDropDown":
                delegateBalTransferResponse.BalanceTransferFailureResponse(encryptedResponse);
                break;
            case "CheckAvailableBalance":
                delegateAvailableBalanceResponse.AvailableBalanceFailResponse(encryptedResponse);
                break;
            case "DownLineList":
                delegatDownlineListResponse.DownlineListFailResponse(encryptedResponse);
                break;
            case "BalanceTransReport":
                delegateBalanceReportResponse.BalanceReportResponseFailResponse(encryptedResponse);
                break;
            case "BalanceReceiveReport":
                delegateBalanceReportResponse.BalanceReportResponseFailResponse(encryptedResponse);
                break;
            case "WalletTranReport":
                delegateBalanceReportResponse.BalanceReportResponseFailResponse(encryptedResponse);
                break;
            case "StockRequest":
                delegateStockRequestResponse.StockReqResponseFailResponse(encryptedResponse);
                break;
            case "StockReport":
                delegateStockReportResponse.StockReportFailureResponse(encryptedResponse);
                break;
            case "ViewUser":
                delegateBalTransferResponse.BalanceTransferFailureResponse(encryptedResponse);
                break;
            case "GetNotification":
                delegateMainResponse.selectNotificationFailureResponse(encryptedResponse);
                break;
            case "SearchNumber":
                delegateSearchNumber.decryptSearchNumberReportListFailureResponse(encryptedResponse);
                break;
            case "Electricity":
                delegateElectricity.decryptViewBillFailureResponse(encryptedResponse);
                break;
            case "ElectricityBillPay":
                delegateElectricity.decryptPayBillFailureResponse(encryptedResponse);
                break;
            case "CommmissionStructure":
                delegateCommissionStructure.decryptCommissionStructureFailureResponse(encryptedResponse);
                break;
            case "AddComplaint":
                delegateAddComplaint.decryptAddComplaintFailureResponse(encryptedResponse);
                break;
            case "ViewComplaint":
                delegateComplaintReportResponse.decreptedComplaintReportFailureResponse(encryptedResponse);
                break;
            case "ChangeMobileNo":
                delegateChangeMobileNoResponse.decryptedChangeMobileNumberFailureResponse(encryptedResponse);
                break;
            case "ForgotPassword":
                inject.ForgetFailuerResponse(encryptedResponse);
                break;


            //Money Transfer
            case "MoneyTransferReport":
                delegateMoneyTransferReport.decryptMoneyTransferReportListFailureResponse(encryptedResponse);
                break;
            case "SenderRegistration":
                delegateSenderRegistration.SenderRegistrationFailureResponse(encryptedResponse);
                break;
            case "BeneficiaryList":
                delegateBeneficiaryList.decryptBeneficiaryListFailureResponse(encryptedResponse);
                break;
            case "OTP":
                delegateAddBeneficiary.decryptOTPFailureResponse(encryptedResponse);
                break;
            case "OTPADDB":
                delegateAddBeneficiary.decryptOTPFailureResponse(encryptedResponse);
                break;
            case "AddBeneficiary":
                delegateAddBeneficiary.decryptAddBeneficiaryFailureResponse(encryptedResponse);
                break;
            case "GetBank":
                delegateAddBeneficiary.decryptGetBankNameFailureResponse(encryptedResponse);
                break;

            case "SenderDetails":
                delegateBeneficiaryList.SenderDetailsFailureResponse(encryptedResponse);
                break;
            case "Verification":
                delegateAddBeneficiary.VerificationFailureResponseResponse(encryptedResponse);
                break;
        }
    }


    private void dismissDialog(String Tag) {
        switch (Tag) {
            case "Login":
                inject.dismissDialog();
                break;
            case "LoginWithOTP":
                inject.dismissDialog();
                break;
            case "ForgotPassword":
                inject.dismissDialog();
                break;
            case "Operator":
                delegateRechargeResponse.dismissDialog();
                break;
            case "Recharge":
                delegateRechargeResponse.dismissDialog();
                break;
            case "FreeUserRegistration":
                delegateFreeUserResponse.dismissPDialog();
                break;
            case "UserActivation":
                delegateActivationResponse.dismissPDialog();
                break;
            case "LastTransaction":
                delegateLastTransactionResponse.dismissDialog();
                break;
            case "DayReport":
                delegateDayResponse.dismissDialog();
                break;
            case "PurchaseReport":
                delegatePurchaseReport.dismissDialog();
                break;
            case "PurchaseReportCredit":
                delegatePurchaseReport.dismissDialog();
                break;
            case "ChangePassword":
                delegateChangePassResponse.dismissPDialog();
                break;
            case "ChangePin":
                delegateChangePassResponse.dismissPDialog();
                break;
            case "BalanceTransfer":
                delegateBalTransferResponse.dismissPDialog();
                break;
            case "BalanceReverse":
                delegateBalTransferResponse.dismissPDialog();
                break;
            case "DownLineForDropDown":
                delegateBalTransferResponse.dismissPDialog();
                break;
            case "CheckAvailableBalance":
                delegateAvailableBalanceResponse.dismissPDialog();
                break;
            case "DownLineList":
                delegatDownlineListResponse.dismissPDialog();
                break;
            case "BalanceTransReport":
                delegateBalanceReportResponse.dismissPDialog();
                break;
            case "BalanceReceiveReport":
                delegateBalanceReportResponse.dismissPDialog();
                break;
            case "WalletTranReport":
                delegateBalanceReportResponse.dismissPDialog();
                break;
            case "StockRequest":
                delegateStockRequestResponse.dismissPDialog();
                break;
            case "StockReport":
                delegateStockReportResponse.dismissPDialog();
                break;
            case "ViewUser":
                delegateBalTransferResponse.dismissPDialog();
                break;
            case "SearchNumber":
                delegateSearchNumber.dismissPDialog();
                break;
            case "Electricity":
                delegateElectricity.dismissPDialog();
                break;
            case "ElectricityBillPay":
                delegateElectricity.dismissPDialog();
                break;
            case "CommmissionStructure":
                delegateCommissionStructure.dismissPDialog();
                break;
            case "AddComplaint":
                delegateAddComplaint.dismissPDialog();
                break;
            case "ViewComplaint":
                delegateComplaintReportResponse.dismissDialog();
                break;
            case "ChangeMobileNo":
                delegateChangeMobileNoResponse.dismissPDialog();
                break;


            //Money Transfer
            case "MoneyTransferReport":
                delegateMoneyTransferReport.dismissPDialog();
                break;
            case "SenderRegistration":
                delegateSenderRegistration.dismissPDialog();
                break;
            case "BeneficiaryList":
                delegateBeneficiaryList.dismissPDialog();
                break;
            case "OTP":
                delegateAddBeneficiary.dismissPDialog();
                break;
            case "OTPAddB":
                delegateAddBeneficiary.dismissPDialog();
                break;
            case "AddBeneficiary":
                delegateAddBeneficiary.dismissPDialog();
                break;
            case "GetBank":
                delegateAddBeneficiary.dismissPDialog();
                break;

        }
    }


    private void somethingWrongMsg(String Tag) {
        switch (Tag) {

            case "Login":
                inject.errorAlert(AllMessages.somethingWrong);
                break;
            case "LoginWithOTP":
                inject.errorAlert(AllMessages.somethingWrong);
                break;
            case "ForgotPassword":
                inject.errorAlert(AllMessages.internetError);
                break;
            case "Operator":
                delegateRechargeResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "Recharge":
                delegateRechargeResponse.errorAlert(AllMessages.internetError);
                break;
            case "FreeUserRegistration":
                delegateFreeUserResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "UserActivation":
                delegateActivationResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "LastTransaction":
                delegateLastTransactionResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "DayReport":
                delegateDayResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "ChangePassword":
                delegateChangePassResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "ChangePin":
                delegateChangePassResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "PurchaseReport":
                delegatePurchaseReport.errorAlert(AllMessages.somethingWrong);
                break;
            case "PurchaseReportCredit":
                delegatePurchaseReport.errorAlert(AllMessages.somethingWrong);
                break;
            case "BalanceTransfer":
                delegateBalTransferResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "BalanceReverse":
                delegateBalTransferResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "DownLineForDropDown":
                delegateBalTransferResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "CheckAvailableBalance":
                delegateAvailableBalanceResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "DownLineList":
                delegatDownlineListResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "BalanceTransReport":
                delegateBalanceReportResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "BalanceReceiveReport":
                delegateBalanceReportResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "WalletTranReport":
                delegateBalanceReportResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "StockRequest":
                delegateStockRequestResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "StockReport":
                delegateStockReportResponse.errorAlert(AllMessages.internetError);
                break;
            case "ViewUser":
                delegateBalTransferResponse.errorAlert(AllMessages.somethingWrong);
                break;
            case "SearchNumber":
                delegateSearchNumber.errorAlert(AllMessages.somethingWrong);
                break;
            case "Electricity":
                delegateElectricity.errorAlert(AllMessages.somethingWrong);
                break;
            case "ElectricityBillPay":
                delegateElectricity.errorAlert(AllMessages.somethingWrong);
                break;
            case "CommmissionStructure":
                delegateCommissionStructure.errorAlert(AllMessages.somethingWrong);
                break;
            case "AddComplaint":
                delegateAddComplaint.errorAlert(AllMessages.somethingWrong);
                break;
            case "ViewComplaint":
                delegateComplaintReportResponse.errorAlert(AllMessages.internetError);
                break;
            case "ChangeMobileNo":
                delegateChangeMobileNoResponse.errorAlert(AllMessages.internetError);
                break;
        }

    }

    private void internetErrorMsg(String Tag) {
        switch (Tag) {

            case "Login":
                inject.errorAlert(AllMessages.internetError);
                break;
            case "LoginWithOTP":
                inject.errorAlert(AllMessages.internetError);
                break;
            case "Operator":
                delegateRechargeResponse.errorAlert(AllMessages.internetError);
                break;
            case "Recharge":
                delegateRechargeResponse.errorAlert(AllMessages.internetError);
                break;

            case "FreeUserRegistration":
                delegateFreeUserResponse.errorAlert(AllMessages.internetError);
                break;
            case "UserActivation":
                delegateActivationResponse.errorAlert(AllMessages.internetError);
                break;
            case "LastTransaction":
                delegateLastTransactionResponse.errorAlert(AllMessages.internetError);
                break;
            case "DayReport":
                delegateDayResponse.errorAlert(AllMessages.internetError);
                break;
            case "ChangePassword":
                delegateChangePassResponse.errorAlert(AllMessages.internetError);
                break;

            case "ChangeMobileNo":
                delegateChangeMobileNoResponse.errorAlert(AllMessages.internetError);
                break;
            case "BalanceTransfer":
                delegateBalTransferResponse.errorAlert(AllMessages.internetError);
                break;
            case "BalanceReverse":
                delegateBalTransferResponse.errorAlert(AllMessages.internetError);
                break;
            case "DownLineForDropDown":
                delegateBalTransferResponse.errorAlert(AllMessages.internetError);
                break;


            case "CheckAvailableBalance":
                delegateAvailableBalanceResponse.errorAlert(AllMessages.internetError);
                break;
            case "DownLineList":
                delegatDownlineListResponse.errorAlert(AllMessages.internetError);
                break;
            case "BalanceTransReport":
                delegateBalanceReportResponse.errorAlert(AllMessages.internetError);
                break;
            case "BalanceReceiveReport":
                delegateBalanceReportResponse.errorAlert(AllMessages.internetError);
                break;
            case "WalletTranReport":
                delegateBalanceReportResponse.errorAlert(AllMessages.internetError);
                break;
            case "StockRequest":
                delegateStockRequestResponse.errorAlert(AllMessages.internetError);
                break;
            case "StockReport":
                delegateStockReportResponse.errorAlert(AllMessages.internetError);
                break;
            case "ViewUser":
                delegateBalTransferResponse.errorAlert(AllMessages.internetError);
                break;
            case "Electricity":
                delegateElectricity.errorAlert(AllMessages.internetError);
                break;

            case "ElectricityBillPay":
                delegateElectricity.errorAlert(AllMessages.internetError);
                break;

            case "SenderRegistration":
                delegateSenderRegistration.errorAlert(AllMessages.internetError);
                break;
            case "OTP":
                delegateAddBeneficiary.errorAlert(AllMessages.internetError);
                break;
            case "BeneficiaryList":
                delegateBeneficiaryList.errorAlert(AllMessages.internetError);
                break;
            case "AddBeneficiary":
                delegateAddBeneficiary.errorAlert(AllMessages.internetError);
                break;
            case "GetBank":
                delegateAddBeneficiary.errorAlert(AllMessages.internetError);
                break;
            case "OTPAddB":
                delegateAddBeneficiary.errorAlert(AllMessages.internetError);
                break;
            case "AddComplaint":
                delegateAddComplaint.errorAlert(AllMessages.internetError);
                break;

            case "SearchNumber":
                delegateSearchNumber.errorAlert(AllMessages.internetError);
                break;
            case "CommmissionStructure":
                delegateCommissionStructure.errorAlert(AllMessages.internetError);
                break;

            case "ForgotPassword":
                inject.errorAlert(AllMessages.internetError);
                break;

            case "ViewComplaint":
                delegateComplaintReportResponse.errorAlert(AllMessages.internetError);
                break;
      /*      case "SearchSenderTransactions":
                delegateSearchSenderNumber.errorAlert(AllMessages.internetError);
                break;*/
            case "SenderDetails":
                delegateBeneficiaryList.errorAlert(AllMessages.internetError);
                break;
            case "Verification":
                delegateAddBeneficiary.errorAlert(AllMessages.internetError);
                break;
            case "MoneyTransferReport":
                delegateMoneyTransferReport.errorAlert(AllMessages.internetError);
                break;
        }
    }

    private void errorMsg(String Tag, String errorMsg) {
        switch (Tag) {

            case "Login":
                inject.errorAlert(errorMsg);
                break;
            case "LoginWithOTP":
                inject.errorAlert(errorMsg);
                break;
            case "Operator":
                delegateRechargeResponse.errorAlert(errorMsg);
                break;
            case "Recharge":
                delegateRechargeResponse.errorAlert(errorMsg);
                break;

            case "FreeUserRegistration":
                delegateFreeUserResponse.errorAlert(errorMsg);
                break;
            case "UserActivation":
                delegateActivationResponse.errorAlert(errorMsg);
                break;
            case "LastTransaction":
                delegateLastTransactionResponse.errorAlert(errorMsg);
                break;
            case "DayReport":
                delegateDayResponse.errorAlert(errorMsg);
                break;
            case "ChangePassword":
                delegateChangePassResponse.errorAlert(errorMsg);
                break;

            case "ChangeMobileNo":
                delegateChangeMobileNoResponse.errorAlert(errorMsg);
                break;
            case "BalanceTransfer":
                delegateBalTransferResponse.errorAlert(errorMsg);
                break;
            case "BalanceReverse":
                delegateBalTransferResponse.errorAlert(errorMsg);
                break;
            case "DownLineForDropDown":
                delegateBalTransferResponse.errorAlert(errorMsg);
                break;

            case "CheckAvailableBalance":
                delegateAvailableBalanceResponse.errorAlert(errorMsg);
                break;
            case "DownLineList":
                delegatDownlineListResponse.errorAlert(errorMsg);
                break;

            case "BalanceTransReport":
                delegateBalanceReportResponse.errorAlert(errorMsg);
                break;
            case "BalanceReceiveReport":
                delegateBalanceReportResponse.errorAlert(errorMsg);
                break;
            case "WalletTranReport":
                delegateBalanceReportResponse.errorAlert(errorMsg);
                break;
            case "StockRequest":
                delegateStockRequestResponse.errorAlert(errorMsg);
                break;
            case "StockReport":
                delegateStockReportResponse.errorAlert(errorMsg);
                break;
            case "ViewUser":
                delegateBalTransferResponse.errorAlert(errorMsg);
                break;
            case "Electricity":
                delegateElectricity.errorAlert(errorMsg);
                break;
            case "ElectricityBillPay":
                delegateElectricity.errorAlert(errorMsg);
                break;

            case "SenderRegistration":
                delegateSenderRegistration.errorAlert(errorMsg);
                break;
            case "BeneficiaryList":
                delegateBeneficiaryList.errorAlert(errorMsg);
                break;
            case "OTP":
                delegateAddBeneficiary.errorAlert(errorMsg);
                break;
            case "AddBeneficiary":
                delegateAddBeneficiary.errorAlert(errorMsg);
                break;
            case "GetBank":
                delegateAddBeneficiary.errorAlert(errorMsg);
                break;
            case "OTPAddB":
                delegateAddBeneficiary.errorAlert(errorMsg);
                break;
            case "AddComplaint":
                delegateAddComplaint.errorAlert(errorMsg);
                break;

            case "SearchNumber":
                delegateSearchNumber.errorAlert(errorMsg);
                break;
            case "CommmissionStructure":
                delegateCommissionStructure.errorAlert(errorMsg);
                break;

            case "ForgotPassword":
                inject.errorAlert(errorMsg);
                break;
            case "ViewComplaint":
                delegateComplaintReportResponse.errorAlert(errorMsg);
                break;
         /*   case "SearchSenderTransactions":
                delegateSearchSenderNumber.errorAlert(errorMsg);
                break;*/
            case "SenderDetails":
                delegateBeneficiaryList.errorAlert(errorMsg);
                break;
            case "Verification":
                delegateAddBeneficiary.errorAlert(errorMsg);
                break;
            case "MoneyTransferReport":
                delegateMoneyTransferReport.errorAlert(errorMsg);
                break;
        }
    }
}
