package in.omkarpayment.app.model;

/**
 * Created by consisty on 13/12/17.
 */

public interface IResendOTP {

    void getResendOtp();

    void getSuccessOtpMsg(String body);

    void getErrorResendOtp(String body);
}
