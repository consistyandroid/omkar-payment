package in.omkarpayment.app.model;

/**
 * Created by consisty on 10/3/18.
 */

public interface IMoneyTransferStatusResponse {
    void getStattusErrorBody(String body);

    void getMoneyTrStatusResponseSuccessBody(String body);
}
