package in.omkarpayment.app.model;

/**
 * Created by ${user} on 25/1/18.
 */

 public interface IGetOffer {
    void errorMsg(String status);

    void getResponse(String response);

    void getFailResponse(String response);

    void getViewPlans(String response);

    void getCustomerInfo(String response);
}
