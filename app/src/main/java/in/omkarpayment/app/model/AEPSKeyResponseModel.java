package in.omkarpayment.app.model;

import com.recharge.AEPS.IAEPSKeyResponcePresenter;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AEPSKeyResponseModel {
    public IAEPSKeyResponcePresenter delegate = null;
    LogWriter log = new LogWriter();

    public void aepsKeyResponse(String encrypted, String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.AepsKeyResponseWebservice(encrypted, keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            //  log.i("onResponseBody", response.body().getBody());
                            delegate.AEPSKeyResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.AEPSKeyResponceFailResponse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }
}
