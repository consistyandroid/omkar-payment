package in.omkarpayment.app.model;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.recharge.IRechargePresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Krish on 05-Aug-17.
 */

public class WebModelForTesting {

    public IRechargePresenter delegate = null;
    IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
    Call<WebserviceResponsePojo> call;
    LogWriter log = new LogWriter();

    public void webserviceMethod(final String encryptedString, final String data, final String Tag) {
        call = webObject.GetOperatorWebservice(encryptedString, data);
        call.enqueue(new Callback<WebserviceResponsePojo>() {
            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                delegate.dismissDialog();
                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("onResponseBody123", response.body().getBody());
                            delegate.decryptRechargeResponse(response.body().getBody());
                            delegate.decryptOperatorResponse(response.body().getBody());
                        } else {

                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                delegate.dismissDialog();
                log.i("onFailure", t.toString());

            }
        });
    }
}
