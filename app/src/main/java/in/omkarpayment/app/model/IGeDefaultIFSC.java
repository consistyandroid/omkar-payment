package in.omkarpayment.app.model;

/**
 * Created by ${user} on 12/3/18.
 */

public interface IGeDefaultIFSC {
    void ifscResponse(String response);

    void ifscFailResponse(String response);
}
