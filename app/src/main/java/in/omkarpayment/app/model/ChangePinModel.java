package in.omkarpayment.app.model;

import in.omkarpayment.app.changePin.IChangePinPresenter;
import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 10/4/2018.
 */

public class ChangePinModel {
    public IChangePinPresenter delegate = null;
    LogWriter log = new LogWriter();

    public void changePin(String encrypted, String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.ChangePinWebservice(encrypted, keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            //  log.i("onResponseBody", response.body().getBody());
                            delegate.ChangePinResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.ChangePinFailureResponse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }
}
