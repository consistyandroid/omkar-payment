package in.omkarpayment.app.model;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Somnath-Laptop on 14-Nov-2017.
 */

public class LoginWebService {
    public ILoginService delegate = null;
    LogWriter log = new LogWriter();
    Call<WebserviceResponsePojo> call;

    public void LoginDetails_api(String request, String body) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
        call = webObject.LoginWebservice(request, body);
        call.enqueue(new Callback<WebserviceResponsePojo>() {
            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equalsIgnoreCase("")) {
                            log.i("onResponseBody", response.body().getBody());
                            delegate.getLoginResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.FailResponse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getBody());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                delegate.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void ForgotPassword(String request) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
        call = webObject.ForgotWebservice(request, "");
        call.enqueue(new Callback<WebserviceResponsePojo>() {
            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equalsIgnoreCase("")) {
                            log.i("onResponseBody", response.body().getBody());
                            delegate.forgotPasswordResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.FailResponse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getBody());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                delegate.errorAlert(AllMessages.internetError);
            }
        });
    }
}
