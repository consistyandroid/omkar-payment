package in.omkarpayment.app.model;


import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.report.MoneyTxCommissionStructure.IMoneyTxCommissionPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lenovo on 8/17/2018.
 */

public class GetMoneyTxCommissionStrModel {
    public IMoneyTxCommissionPresenter delegate = null;
    LogWriter log = new LogWriter();

    public void getMoneyTxCommission(String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.GetMoneyTxCommissionWebservice(keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            //  log.i("onResponseBody", response.body().getBody());
                            delegate.decryptMoneyTxCommissionStructureResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.decryptMoneyTxCommissionStructureFailureResponse(response.body().getBody());
                        } else {
                            delegate.errorAlert(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }
}
