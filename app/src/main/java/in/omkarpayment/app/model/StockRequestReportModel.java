package in.omkarpayment.app.model;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.report.StockReport.StockReportPresenter;
import in.omkarpayment.app.stockRequest.StockRequestPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockRequestReportModel {
    public StockReportPresenter delegateBalanceRequestReport = null;
    public StockRequestPresenter delegateBalanceRequest = null;



//    public void BalanceRequestWebService(String encryptString, String keyData) {
//        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
//
//        Call<WebserviceResponsePojo> call = webObject.BalanceRequest(encryptString, keyData);
//        call.enqueue(new Callback<WebserviceResponsePojo>() {
//
//            @Override
//            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
//
//                try {
//                    LogWriter log = new LogWriter();
//                    if (response.code() != 200) {
//                        delegateBalanceRequest.errorAlert(AllMessages.wrongResponce);
//                        return;
//                    }
//                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
//                        log.i("onResponseBody", response.body().getBody());
//                        delegateBalanceRequest.BalanceRequestResponse(response.body().getBody());
//
//                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
//                        log.i("onResponseBodyF", response.body().getBody());
//                        delegateBalanceRequest.BalanceRequestFailResponse(response.body().getBody());
//
//                    } else {
//                        log.i("onResponseBodyElse", response.body().getBody());
//                        delegateBalanceRequest.errorAlert(response.body().getStatus());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            @Override
//            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
//                LogWriter log = new LogWriter();
//                log.i("onFailure", t.getMessage());
//                delegateBalanceRequest.errorAlert(AllMessages.internetError);
//            }
//        });
//    }

    public void rejectBalanceRequestWebService(String encryptString, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.RejectBalanceRequestWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {
                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        delegateBalanceRequestReport.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        delegateBalanceRequestReport.acceptRejectBalanceRequestResponse(response.body().getBody());

                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        delegateBalanceRequestReport.acceptRejectBalanceRequestFailResponse(response.body().getBody());

                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        delegateBalanceRequestReport.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                delegateBalanceRequestReport.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void acceptBalanceRequestWebService(String encryptString, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.AcceptBalanceRequestWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {

                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        delegateBalanceRequestReport.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        delegateBalanceRequestReport.acceptRejectBalanceRequestResponse(response.body().getBody());

                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        delegateBalanceRequestReport.acceptRejectBalanceRequestFailResponse(response.body().getBody());

                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        delegateBalanceRequestReport.errorAlert(response.body().getStatus());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                delegateBalanceRequestReport.errorAlert(AllMessages.internetError);
            }
        });
    }
}
