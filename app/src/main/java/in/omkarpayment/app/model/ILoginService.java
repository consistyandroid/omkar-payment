package in.omkarpayment.app.model;

/**
 * Created by ${user} on 5/12/17.
 */

public interface ILoginService {
    void getLoginResponse(String response);

    void FailResponse(String response);

    void errorAlert(String msg);

    void forgotPasswordResponse(String response);
}
