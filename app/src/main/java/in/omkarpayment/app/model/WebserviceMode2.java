package in.omkarpayment.app.model;

import in.omkarpayment.app.json.ElectricityBillViewPojo;
import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.recharge.electricity.IElectricityBillPresenter;
import in.omkarpayment.app.userContent.AllMessages;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Krish on 11-Aug-17.
 */

public class WebserviceMode2 {
    public IElectricityBillPresenter delegate = null;

    public IGeDefaultIFSC iGeDefaultIFSC = null;
    LogWriter log = new LogWriter();
    Call<WebserviceResponsePojo> call;

    public void viewBillApi(String ConsumerNumber) {
        IWebServicesModel webObject = IWebServicesModel.retrofit2.create(IWebServicesModel.class);

        Call<ElectricityBillViewPojo> call =
                webObject.ElectricityBillView(ConsumerNumber);

        call.enqueue(new Callback<ElectricityBillViewPojo>() {

            @Override
            public void onResponse(Call<ElectricityBillViewPojo> call, Response<ElectricityBillViewPojo> response) {
                delegate.dismissPDialog();
                log.i("ResponseCode", response.code() + "");
                if (response.code() == 200) {
                    delegate.viewBillResponse(response.body().getRecharge());

                } else {
                    delegate.errorAlert(AllMessages.somethingWrong);
                }
            }

            @Override
            public void onFailure(Call<ElectricityBillViewPojo> call, Throwable t) {
                delegate.dismissPDialog();
                log.i("err", t.getMessage());
                delegate.errorAlert(AllMessages.internetError);
            }
        });
    }



    public void complaintReportWebservice(String body) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
        // Call<WebserviceResponsePojo> call = webObject.ComplaitReportWebservice("", body);
        call.enqueue(new Callback<WebserviceResponsePojo>() {
            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equalsIgnoreCase("")) {
                            log.i("onResponseBody", response.body().getBody());
                            //     iComplainReport.getCompaintResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {

                        } else {
                            //  iComplainReport.errorAlert(response.body().getBody());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                //   iComplainReport.errorAlert(AllMessages.internetError);
            }
        });
    }



    public void checkDefaultIFSC(String encryptString, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);
        Call<WebserviceRespoPojo2> call;
        call = webObject.checkForDefaultIFSC(encryptString, keyData);
        call.enqueue(new Callback<WebserviceRespoPojo2>() {

            @Override
            public void onResponse(Call<WebserviceRespoPojo2> call, Response<WebserviceRespoPojo2> response) {
                //   iAddSenderBenifEPay. dismissDialog(Tag);
                log.i("re", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("onResponseBody", response.body().getBody());
                            iGeDefaultIFSC.ifscResponse(response.body().getBody());
                        } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                            iGeDefaultIFSC.ifscFailResponse(response.body().getBody());
                        } else {
                            // errorMsg(Tag, response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceRespoPojo2> call, Throwable t) {
                // iAddSenderBenifEPay. dismissDialog(Tag);
                //internetErrorMsg(Tag);
            }
        });
    }

}
