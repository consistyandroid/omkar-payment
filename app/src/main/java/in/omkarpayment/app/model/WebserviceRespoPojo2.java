package in.omkarpayment.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ${user} on 12/3/18.
 */

public  class WebserviceRespoPojo2 {
    @SerializedName("Body")
    @Expose
    private String body;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
