package in.omkarpayment.app.model;

import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import in.omkarpayment.app.MoneyTransferpkg.MoneyTransfer.IMoneyTransfer;
import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.ApplicationURL;
import in.omkarpayment.app.userContent.UserDetails;


/**
 * Created by consisty on 13/10/17.
 */

public class MoneyTransferAsync extends AsyncTask<String, Void, String> {
    public IMoneyTransfer delegate = null;
    HttpURLConnection urlConnection;

    String encryptString;

    @Override
    protected String doInBackground(String... params) {
        String keyData = new KeyDataReader().get();
        StringBuilder senderRegistrationResult = new StringBuilder();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject senderRegistrationRequest = new JSONObject();
        int index = 0;
        try {

            senderRegistrationRequest.put("BenficiaryID", params[index++]);
            senderRegistrationRequest.put("Type", params[index++]);
            senderRegistrationRequest.put("Amount", params[index++]);
            senderRegistrationRequest.put("SenderMobileNumber", params[index++]);
            senderRegistrationRequest.put("Pin", params[index++]);
            senderRegistrationRequest.put("IPAddress", UserDetails.IPCOnst);

            String jsonrequest = senderRegistrationRequest.toString();
            LogWriter.i("Sender Registration Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            LogWriter.i("encrypted Registration Request", encryptString);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL(ApplicationURL.base_URL + "DMT/MoneyTransferWithPin.asmx/Transfer");//DMT/MoneyTransfer.asmx/Transfer
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("Request", encryptString)
                    .appendQueryParameter("Keydata", keyData);

            String query = builder.build().getEncodedQuery();

            urlConnection = (HttpURLConnection) url.openConnection();
            LogWriter.e("urll", url.toString());

            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestMethod("POST");

            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());

            wr.writeBytes(query);
            wr.flush();
            wr.close();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                senderRegistrationResult.append(line);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        LogWriter.i("moneytransferResult", senderRegistrationResult.toString());

        return senderRegistrationResult.toString();
    }

    @Override
    protected void onPostExecute(String senderRegistrationResult) {
        LogWriter.i("Transfer Details", senderRegistrationResult);
        String body = "";

        Cryptography_Android data = new Cryptography_Android();

        Gson gson = new Gson();
        LoginBodyPojo response = gson.fromJson(senderRegistrationResult, LoginBodyPojo.class);
        try {
            try {
                body = data.Decrypt(response.getBody());
                LogWriter.i("Decrypted body 1 ", body);
                if (response.getStatus().equalsIgnoreCase("FAILURE") ||
                        response.getStatus() == null ||
                        response.getStatus().equalsIgnoreCase("PROCESS")) {

                    delegate.getErrorMessage(response.getBody());
                } else if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                    delegate.getSuccessMessageTransfer(response.getBody());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}