package in.omkarpayment.app.model;


import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${user} on 25/1/18.
 */

public class GetOfferModel {

    public IGetOffer delegate = null;
    LogWriter log = new LogWriter();

    public void getROffers(String encrypted, String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.RofferWebservice(encrypted, keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            //  log.i("onResponseBody", response.body().getBody());
                            delegate.getResponse(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.getFailResponse(response.body().getBody());
                        } else {
                            delegate.errorMsg(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                //    delegate.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void getBrowsplans(String encrypted, String keydata) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.BrowesPlansWebservice(encrypted, keydata);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                       log.i("onResponseBody", response.body().getBody());
                            delegate.getViewPlans(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.getFailResponse(response.body().getBody());
                        } else {
                            delegate.errorMsg(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                delegate.errorMsg(AllMessages.somethingWrong);
            }
        });
    }

    public void getDTHPlans(String encryptString, String keyData) {

        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.DTHPlansWebservice(encryptString, keyData);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("onResponseBody", response.body().getBody());
                            delegate.getViewPlans(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.getFailResponse(response.body().getBody());
                        } else {
                            delegate.errorMsg(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                delegate.errorMsg(AllMessages.somethingWrong);
            }
        });
    }
    public void getCustomerInfo(String encryptString, String keyData) {

        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call =
                webObject.CustomerInfoWebservice(encryptString, keyData);

        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                log.i("onResponseCode", response.code() + "");
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equals("SUCCESS") && !response.body().getBody().equals("")) {
                            log.i("onResponseBody", response.body().getBody());
                            delegate.getCustomerInfo(response.body().getBody());
                        } else if (response.body().getStatus().equals("FAILURE") && !response.body().getBody().equals("")) {
                            delegate.getFailResponse(response.body().getBody());
                        } else {
                            delegate.errorMsg(response.body().getStatus());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {

                log.i("err", t.getMessage());
                delegate.errorMsg(AllMessages.somethingWrong);
            }
        });
    }
}
