package in.omkarpayment.app.model;

import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.ApplicationURL;
import in.omkarpayment.app.userContent.UserDetails;


/**
 * Created by consisty on 22/12/17.
 */

public class MoneyTransferComplaintAsync
        extends AsyncTask<String, Void, String>

{
  //  public IComplaintView delegate = null;
    HttpURLConnection urlConnection;
    LogWriter log = new LogWriter();
    String encryptString;

    @Override
    protected String doInBackground(String... params) {
        String keyData = new KeyDataReader().get();
        StringBuilder senderRegistrationResult = new StringBuilder();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject senderRegistrationRequest = new JSONObject();
        int index = 0;
        try {

            senderRegistrationRequest.put("UserID", UserDetails.UserId);
            senderRegistrationRequest.put("OperatorID", params[index++]);
            senderRegistrationRequest.put("Amount", params[index++]);
            senderRegistrationRequest.put("RechargeID", params[index++]);
            senderRegistrationRequest.put("Description", params[index++]);
            senderRegistrationRequest.put("Subject", params[index++]);
            senderRegistrationRequest.put("CustomerNumber", params[index++]);

            String jsonrequest = senderRegistrationRequest.toString();
            log.i("Sender Registration Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted Registration Request", encryptString);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            URL url = new URL(ApplicationURL.base_URL + "Complaint.asmx/ComplaintInsert");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("Request", encryptString)
                    .appendQueryParameter("Keydata", keyData);

            String query = builder.build().getEncodedQuery();

            urlConnection = (HttpURLConnection) url.openConnection();
            log.e("urll", url.toString());

            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestMethod("POST");

            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());

            wr.writeBytes(query);
            wr.flush();
            wr.close();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                senderRegistrationResult.append(line);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        log.i("complaint", senderRegistrationResult.toString());

        return senderRegistrationResult.toString();
    }

    @Override
    protected void onPostExecute(String beneficiaryResult) {
        log.i("Transfer Details", beneficiaryResult);
        String body = "";

        Cryptography_Android data = new Cryptography_Android();

        Gson gson = new Gson();
        LoginBodyPojo response = gson.fromJson(beneficiaryResult, LoginBodyPojo.class);
        try {
            try {
                body = data.Decrypt(response.getBody());
                if (response.getStatus().equalsIgnoreCase("FAILURE") || response.getStatus() == null || response.getStatus().equalsIgnoreCase("PROCESS")) {
                    log.i("Decrypted body", body);
                  //  delegate.getMoneyTrErrorMessage(body);
                } else if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                 //   delegate.getSuccessMessageComplaint(body);
                }

            } catch (Exception e) {

            }
        } catch (Exception e) {

        }
    }


}


