package in.omkarpayment.app.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.DateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import in.omkarpayment.app.R;
import in.omkarpayment.app.dataBaseProcessing.DataBaseHelper;
import in.omkarpayment.app.userContent.UserDetails;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        if (remoteMessage.getNotification() != null) {
            Log.e("Notification", "Body: " + remoteMessage.getNotification());
            String message = remoteMessage.getNotification().getTitle();

            Log.e("message", "Body: " + message);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "M_CH_ID")
                    .setSmallIcon(R.drawable.downline_background)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setVibrate(new long[]{500, 100, 500, 100})
                    .setLights(Color.WHITE, 500, 500)
                    .setAutoCancel(true)
                    .setColor(Color.parseColor("#FF2C17A6"))
                    .setPriority(NotificationCompat.PRIORITY_HIGH);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String channelId = "M_CH_ID";

                NotificationChannel channel = new NotificationChannel(channelId,
                        getResources().getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_HIGH);

                channel.setDescription(message);
                channel.enableLights(true);
                channel.setLightColor(Color.WHITE);
                channel.setVibrationPattern(new long[]{500, 100, 500, 100});
                channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannel(channel);
                mNotificationManager.notify(0, mBuilder.build());
            } else {
                mBuilder.setContentTitle(getResources().getString(R.string.app_name));
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                notificationManager.notify(0, mBuilder.build());
            }

            String today = DateFormat.getDateTimeInstance().format(new Date());
            dbHelper.insertNotification(today, message);
            return;
        }

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "getData Body: " + remoteMessage.getData().get("balance"));

            String scrollNotification = remoteMessage.getData().get("ScrollNotification");
            String balance = remoteMessage.getData().get("balance");
            String dmrBalance = remoteMessage.getData().get("dmrBalance");
            //String outstandingAmount = remoteMessage.getData().get("outstandingAmount");
            //sendBalance(balance, dmrBalance, outstandingAmount);
            sendBalance(balance, dmrBalance, scrollNotification);
        }
    }

    private void sendBalance(String balance, String dmrBalance, String ScrollNotification) {

        if (ScrollNotification != null && !ScrollNotification.isEmpty()) {
            Intent intent = new Intent("ScrollNotification");
            intent.putExtra("ScrollNotification", ScrollNotification);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
        if (balance == null) {
            balance = UserDetails.UserBalance.toString();
        }
        if (balance.isEmpty()) {
            return;
        }
        if (dmrBalance == null) {
            return;
        }
        if (dmrBalance.isEmpty()) {
            return;
        }
        /*if (outstandingAmount == null) {
            return;
        }
        if (outstandingAmount.isEmpty()) {
            return;
        }*/

        try {
            Float decimalbal = Float.parseFloat(balance);
            Float decimalDmrBal = Float.parseFloat(dmrBalance);
            //Float decimalOutAmt = Float.parseFloat(outstandingAmount);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Intent intent = new Intent("BalanceReceive");
        intent.putExtra("balance", balance);
        intent.putExtra("dmrBalance", dmrBalance);
        // intent.putExtra("outstandingAmount", outstandingAmount);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        // sendBroadcast(i);
    }

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        storeRegIdInPref(token); // send to Shared preference

        sendRegistrationToServer(token);  // send to our server

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        UserDetails.AndroidToken = token;
        Log.i("FCMToken", token);
        Log.i("savedFCMToken", UserDetails.AndroidToken);
        Log.i(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        try {
            UserDetails.AndroidToken = token;
            Log.i("FCMToken", token);
            SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("FCMToken", token);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
