package in.omkarpayment.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by ${user} on 13/4/17.
 */

public class NetworkUtility {


    public boolean checkInternet(Context context) {

        if (context != null) {
            ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            //mobile
            NetworkInfo.State mobile = conMan.getNetworkInfo(0).getState();
            //wifi
            NetworkInfo.State wifi = conMan.getNetworkInfo(1).getState();

            if (mobile == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTED) {

                return true;

            } else {

                return false;
            }

        }
        return false;

    }
}
