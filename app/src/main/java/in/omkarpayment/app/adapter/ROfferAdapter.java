package in.omkarpayment.app.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.RofferPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.recharge.recharge.RofferActivity;


/**
 * Created by ${user} on 25/1/18.
 */

public class ROfferAdapter extends RecyclerView.Adapter<ROfferAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    Dialog dialog;
    AppCompatActivity activity;
    Context context;
    LogWriter log = new LogWriter();
    private ArrayList<RofferPojo> list;

    public ROfferAdapter(Context context, ArrayList<RofferPojo> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ROfferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_roffer, parent, false);

        return new ROfferAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ROfferAdapter.MyViewHolder holder, int position) {
        final RofferPojo pojo = list.get(position);
        //  holder.txtdate.setText(pojo.getDOC());

        holder.txtAmount.setText(pojo.getRs());
        holder.txtDesc.setText(pojo.getDesc());
        holder.bgShape.setColor(Color.parseColor("#FF4DB776"));
        holder.txtsetOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((RofferActivity) context).setResult(2, intent);
                intent.putExtra("amount", pojo.getRs());
                ((RofferActivity) context).finish();
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDesc, txtAmount, txtsetOperator;
        GradientDrawable bgShape;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtAmount = (TextView) rowView.findViewById(R.id.txtAmount);
            txtDesc = (TextView) rowView.findViewById(R.id.txtDesc);
            txtsetOperator = (TextView) rowView.findViewById(R.id.txtsetOperator);
            bgShape = (GradientDrawable) txtsetOperator.getBackground();

        }
    }
}
