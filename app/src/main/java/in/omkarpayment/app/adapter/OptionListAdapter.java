package in.omkarpayment.app.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import in.omkarpayment.app.HelpActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.balanceTransfer.BalanceReverseActivity;
import in.omkarpayment.app.balanceTransfer.BalanceTransferActivity;
import in.omkarpayment.app.changePassword.ChangePasswordActivity;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.downlinelist.DownlineListActivity;
import in.omkarpayment.app.json.optionData;
import in.omkarpayment.app.login.LoginActivity;
import in.omkarpayment.app.report.balanceReport.BalanceReportActivity;
import in.omkarpayment.app.report.dayreport.DayReportActivity;
import in.omkarpayment.app.report.lasttransaction.LastTransactionActivity;
import in.omkarpayment.app.report.searchMobile.SearchNumberActivity;
import in.omkarpayment.app.userActivation.UserActivationActivity;

import java.util.ArrayList;
import java.util.List;

public class OptionListAdapter extends RecyclerView.Adapter<OptionListAdapter.MyViewHolder> {

    Context context;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    private List<optionData> list;

    public OptionListAdapter(Context context, ArrayList<optionData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public OptionListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_option_list, parent, false);

        return new OptionListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OptionListAdapter.MyViewHolder holder, int position) {
        final optionData obj = list.get(position);
        holder.txtListItem.setText(obj.getItemName());


        holder.txtListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (obj.getItemId().equals("1")) {
                    Intent intent = new Intent(context, LastTransactionActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("2")) {
                    Intent intent = new Intent(context, DayReportActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("0")) {
                    iAvailableBalancePresenter = new AvailableBalancePresenter((IAvailableBalanceView) context);
                    iAvailableBalancePresenter.getAvailableBalance();
                } else if (obj.getItemId().equals("3")) {
                    Intent intent = new Intent(context, SearchNumberActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("4")) {
                    Intent intent = new Intent(context, BalanceTransferActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("5")) {
                    Intent intent = new Intent(context, BalanceReportActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("6")) {
                    Intent intent = new Intent(context, UserActivationActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("7")) {
                    Intent intent = new Intent(context, DownlineListActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("9")) {
                    Intent intent = new Intent(context, BalanceReverseActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("10")) {
                    Intent intent = new Intent(context, ChangePasswordActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("11")) {
                    Intent intent = new Intent(context, HelpActivity.class);
                    context.startActivity(intent);

                } else if (obj.getItemId().equals("8")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.warning_dialog);

                    TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                    text.setText("Are you sure to logout?");

                    Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                    Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent loginIntent = new Intent(context, LoginActivity.class);
                            context.startActivity(loginIntent);
                            dialog.dismiss();
                        }
                    });
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtListItem;
        public MyViewHolder(View rowView) {
            super(rowView);
            txtListItem = (TextView) rowView.findViewById(R.id.txtListItem);
        }
    }
}
