package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.BalanceTransferReportPojo;

/**
 * Created by Krish on 17-Aug-17.
 */

public class BalanceReportAdapter extends RecyclerView.Adapter<BalanceReportAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    LinearLayout LayDispute, Layout;
    Context context ;
    private List<BalanceTransferReportPojo> list;

    public BalanceReportAdapter(Context context,List<BalanceTransferReportPojo> list) {
        this.context=context;
        this.list = list;
    }

    @Override
    public BalanceReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpater_balance_report, parent, false);
        return new BalanceReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BalanceReportAdapter.MyViewHolder holder, int position) {
       final BalanceTransferReportPojo pojo = list.get(position);
        holder.txtdate.setText(pojo.getDOC());
        holder.txtSenderName.setText("(" + pojo.getSenderID() + ")" + pojo.getSender());
        holder.txtReceiverName.setText("(" + pojo.getReceiverID() + ")" + pojo.getReceiver());
        holder.txtAmount.setText("₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getAmount()))));

        holder.imgShareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = ">Date : "+pojo.getDOC()+
                        "\n>Sender name : "+"(" + pojo.getSenderID() + ")" + pojo.getSender()+
                        "\n>Reciever name : "+ String.valueOf(pojo.getReceiverName())+
                        "\n>Tx. Amount : "+"₹ " + String.valueOf(pojo.getAmount());

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Balance Transfer Status");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.share_using)));
            }
        });

        if (pojo.getBalanceType().equalsIgnoreCase("DMT")){
            holder.txtBalanceType.setText(pojo.getBalanceType());
            holder.txtOpenbal.setText("DMT Op Bal.\n"+"₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getOpeningBalance()))));
            holder.txtclosebal.setText("DMT Cl Bal.\n"+"₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getClosingBalance()))));
        }else {
            holder.txtBalanceType.setText(pojo.getBalanceType());
            holder.txtOpenbal.setText("Op Bal.\n"+"₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getOpeningBalance()))));
            holder.txtclosebal.setText("Cl Bal.\n"+"₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getClosingBalance()))));

        }
        try {
            //float bal = Float.parseFloat(String.valueOf(pojo.getAmount()));
            //  holder.txtCommi.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getAmount()))));
            /*holder.txtclosebal.setText("₹ " + String.format("%.00f", (String.valueOf(pojo.getClosingBalance()))));
            holder.txtOpenbal.setText("₹ " + String.format("%.00f", (String.valueOf(pojo.getOpeningBalance()))));*/
            // holder.txtAmount.setText("₹ " + String.format("%.00f", (String.valueOf(pojo.getAmount()))));

        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtOpenbal, txtdate, txtSenderId, txtAmount, txtbalance, txtclosebal,txtOpenbalance,txtClosebalance,txtBalanceType,
                txtReceiverId, txtReceiverName, txtSenderName;
        ImageView imgoperator,imgShareImage;

        /*{"BalanceTransferID":5,"SenderID":2,"ReceiverID":3,"Amount":5,"OpeningBalanceSender":5.94000,
        "CloseBalanceSender":0.94000,"DOC":"13 Sep 2017 13:25:43:280","Receiver":"sai","RUserTypeID":3,"Sender":"Priya","ParentID":61,"DUserTypeID":2}*/
        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = (TextView) rowView.findViewById(R.id.txtdate);
            // txtSenderId = (TextView) rowView.findViewById(R.id.txtSenderId);
            //txtReceiverId = (TextView) rowView.findViewById(R.id.txtReceiverId);
            txtReceiverName = (TextView) rowView.findViewById(R.id.txtReceiverName);
            txtSenderName = (TextView) rowView.findViewById(R.id.txtSenderName);
            txtAmount = (TextView) rowView.findViewById(R.id.txtAmount);
            txtOpenbal = (TextView) rowView.findViewById(R.id.txtOpenbalance);
            txtclosebal = (TextView) rowView.findViewById(R.id.txtClosebalance);
            imgShareImage = (ImageView) rowView.findViewById(R.id.imgShareImage);
            txtBalanceType = (TextView)rowView.findViewById(R.id.txtBalanceType);


        }
    }
}
