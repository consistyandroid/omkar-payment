package in.omkarpayment.app.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.recharge.aepsCon.aepsReport.AepsReportActivity;
import com.recharge.aepsCon.json.AepsReportPojo;

import java.util.List;

import in.omkarpayment.app.R;


public class AepsReportAdapter extends RecyclerView.Adapter<AepsReportAdapter.MyViewHolder> {
    public static int i;
    Context context;
    private List<AepsReportPojo> list;
    private AepsReportActivity dayReportActivity;

    public AepsReportAdapter(Context context, AepsReportActivity dayReportActivity, List<AepsReportPojo> list) {
        this.context = context;
        this.list = list;
        this.dayReportActivity = dayReportActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_aepsreport, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final AepsReportPojo pojo = list.get(position);

        holder.txtdate.setText(pojo.getDOC());
        holder.txtUserName.setText(pojo.getUserName());
        holder.txtamount.setText(String.valueOf(pojo.getAmount()));
        holder.txtStatus.setText(pojo.getSTATUS());
//        holder.imgShareImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(context, RechargeReceiptActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("ConsumerNo", pojo.getConsumerNumber());
//                bundle.putString("Operator", pojo.getOperatorName());
//                bundle.putString("Amount", String.valueOf(pojo.getAmount()));
//                bundle.putString("Date", String.valueOf(pojo.getDOC()));
//                bundle.putString("TxID", String.valueOf(pojo.getTransactionID()));
//                bundle.putString("RechargeStatus", pojo.getStatus());
//                bundle.putString("OperatorID", String.valueOf(pojo.getOperatorID()));
//                intent.putExtras(bundle);
//                context.startActivity(intent);
//            }
//        });

        try {
            holder.txtClBal.setText(" ₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getClosingBalance()))));
            holder.txtOpBal.setText(" ₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getOpeningBalance()))));
            holder.txtRetailerComm.setText(" ₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getRetailerCommission()))));
            holder.txtNetAmount.setText(" ₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getNetAmount()))));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (pojo.getSTATUS().equalsIgnoreCase("SUCCESS")) {

                holder.txtamount.setTextColor(context.getResources().getColor(R.color.green));
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.green));
               // holder.bgShape.setColor(context.getResources().getColor(R.color.newgreen));

            } else if (pojo.getSTATUS().equalsIgnoreCase("FAIL") || pojo.getSTATUS().equalsIgnoreCase("WRONG NUMBER") ||
                    pojo.getSTATUS().equalsIgnoreCase("REFUND") || pojo.getSTATUS().equalsIgnoreCase("FAILURE")) {
                holder.bgShape.setColor(context.getResources().getColor(R.color.red));
                holder.txtamount.setTextColor(context.getResources().getColor(R.color.red));


            } else if (pojo.getSTATUS().equalsIgnoreCase("PROCESS") || pojo.getSTATUS().equalsIgnoreCase("WAITING")) {
                holder.bgShape.setColor(context.getResources().getColor(R.color.orange));
                holder.txtamount.setTextColor(context.getResources().getColor(R.color.orange));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtStatus, txtdate, txtUserName, txtRetailerComm, txtamount, txtNetAmount, txtClBal, txtOpBal;
        ImageView imgoperator, imgShareImage;
        GradientDrawable bgShape;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = rowView.findViewById(R.id.txtDate);
            txtUserName = rowView.findViewById(R.id.txtUserName);
            txtRetailerComm = rowView.findViewById(R.id.txtRetailerComm);
            txtamount = rowView.findViewById(R.id.txtAmount);
            txtNetAmount = rowView.findViewById(R.id.txtNetAmount);
            txtClBal = rowView.findViewById(R.id.txtClosebalance);
            txtOpBal = rowView.findViewById(R.id.txtOpenbalance);
            txtStatus = rowView.findViewById(R.id.txtStatus);
            bgShape = (GradientDrawable) txtStatus.getBackground();
        }
    }
}
