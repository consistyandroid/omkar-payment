package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.ComplaintReportPojo;

/**
 * Created by Somnath-Laptop on 01-Feb-2018.
 */

public class ComplaintReportAdapter extends RecyclerView.Adapter<ComplaintReportAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    LinearLayout LayDispute, Layout;
    private List<ComplaintReportPojo> list;

    public ComplaintReportAdapter(Context context, List<ComplaintReportPojo> list) {
        this.list = list;
    }

    @Override
    public ComplaintReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_complaint_report, parent, false);
        return new ComplaintReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ComplaintReportAdapter.MyViewHolder holder, int position) {
        ComplaintReportPojo pojo = list.get(position);
        holder.txtdate.setText(pojo.getDOC());
        holder.txtmobileno.setText(pojo.getConsumerNumber());
        holder.txtrechargeid.setText(Integer.toString(pojo.getRechargeID()));
        holder.txtstatus.setText(pojo.getStatus());
        holder.txtamount.setText("₹ " + String.valueOf(pojo.getAmount()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtdate, txtmobileno, txtrechargeid, txtstatus, txtamount;


        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = (TextView) rowView.findViewById(R.id.txtdate);
            txtmobileno = (TextView) rowView.findViewById(R.id.txtMobileNo);
            txtrechargeid = (TextView) rowView.findViewById(R.id.txtRechargeID);
            txtstatus = (TextView) rowView.findViewById(R.id.txtStatus);
            txtamount = (TextView) rowView.findViewById(R.id.txtamount);

        }
    }
}
