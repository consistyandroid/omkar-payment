package in.omkarpayment.app.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.SenderMoneyTransferPojoModel;

public class SenderMoneyTransferAdapter extends RecyclerView.Adapter<SenderMoneyTransferAdapter.MyViewHolder> {
    public static int i;
    private List<SenderMoneyTransferPojoModel> list;

    public SenderMoneyTransferAdapter(Context context, List<SenderMoneyTransferPojoModel> list) {
        this.list = list;
    }

    @Override
    public SenderMoneyTransferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_sender_transaction_details, parent, false);
        return new SenderMoneyTransferAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SenderMoneyTransferAdapter.MyViewHolder holder, int position) {
        SenderMoneyTransferPojoModel pojo = list.get(position);

        holder.txtBeneficiaryName.setText(pojo.getBeneficiaryName());
        holder.txtBeneficiaryName.setSelected(true);
        holder.txtBeneficiaryName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtBeneficiaryName.setSingleLine(true);
        holder.txtDOC.setText(String.format(pojo.getDOC()));
        holder.txtAmount.setText("₹ " + String.format(String.valueOf(pojo.getAmount())));
        holder.txtCharges.setText("₹ " + String.format(String.valueOf(pojo.getCharges())));
        holder.txtTransactionNo.setText(String.format(pojo.getTransactionID()));
        holder.txtTransactionNo.setSelected(true);
        holder.txtTransactionNo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtTransactionNo.setSingleLine(true);
        holder.txtBankName.setText(String.format(pojo.getBank()));
        holder.txtBankName.setSelected(true);
        holder.txtBankName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtBankName.setSingleLine(true);
        holder.txtAccountNo.setText(String.format(pojo.getAccountNo()));
        holder.txtAccountNo.setSelected(true);
        holder.txtAccountNo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtAccountNo.setSingleLine(true);
        holder.txtStatus.setText(String.format(pojo.getStatus()));


        if (pojo.getStatus().equalsIgnoreCase("SUCCESS")) {
            holder.txtStatus.setTextColor(Color.GREEN);
        } else if (pojo.getStatus().equalsIgnoreCase("FAILURE")) {
            holder.txtStatus.setTextColor(Color.RED);
        }else if (pojo.getStatus().equalsIgnoreCase("VERIFIED")) {
            holder.txtStatus.setTextColor(Color.YELLOW);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtBeneficiaryName, txtDOC, txtAmount, txtCharges, txtTransactionNo, txtBankName, txtAccountNo, txtStatus;


        public MyViewHolder(View rowView) {
            super(rowView);
            txtBeneficiaryName = (TextView) rowView.findViewById(R.id.txtBeneficiaryName);
            txtDOC = (TextView) rowView.findViewById(R.id.txtDOC);
            txtAmount = (TextView) rowView.findViewById(R.id.txtAmount);
            txtCharges = (TextView) rowView.findViewById(R.id.txtCharges);
            txtTransactionNo = (TextView) rowView.findViewById(R.id.txtTransactionNo);
            txtBankName = (TextView) rowView.findViewById(R.id.txtBankName);
            txtStatus = (TextView) rowView.findViewById(R.id.txtStatus);
            txtAccountNo = (TextView) rowView.findViewById(R.id.txtAccountNo);
        }

    }
}
