package in.omkarpayment.app.adapter;

import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.DownlineListPojo;

public class DownLineSearchMemAdapter extends RecyclerView.Adapter<DownLineSearchMemAdapter.MyViewHolder> {
    public static int i;
    GradientDrawable bgShape;
    private List<DownlineListPojo> list;

    public DownLineSearchMemAdapter(List<DownlineListPojo> list) {
        this.list = list;
    }

    @Override
    public DownLineSearchMemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpater_downline_list, parent, false);

        return new DownLineSearchMemAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DownLineSearchMemAdapter.MyViewHolder holder, int position) {
        DownlineListPojo pojo = list.get(position);
        holder.txtName.setText(pojo.getUserName() + "(" + pojo.getUserID() + ")" + "-" + pojo.getMobileNumber());
        try {
            float bal = Float.parseFloat(String.valueOf(pojo.getCurrentBalance()));
            holder.txtBalance.setText("Bal.:" + String.format("%.02f", bal) + " ₹");
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<DownlineListPojo> temp) {

        list = temp;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName, txtMobile, txtId, txtBalance;
        public LinearLayout backDownline;

        public MyViewHolder(View rowView) {
            super(rowView);
            //        backDownline = (LinearLayout) rowView.findViewById(R.id.backDownline);
//            bgShape = (GradientDrawable) backDownline.getBackground();
            txtName = (TextView) rowView.findViewById(R.id.txtName);
            // txtMobile = (TextView) rowView.findViewById(R.id.txtMobile);
            //txtId = (TextView) rowView.findViewById(R.id.txtId);
            txtBalance = (TextView) rowView.findViewById(R.id.txtBalance);

        }
    }
}

