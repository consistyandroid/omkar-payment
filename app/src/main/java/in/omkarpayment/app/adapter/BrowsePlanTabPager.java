package in.omkarpayment.app.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import in.omkarpayment.app.browsPlan.BrowsePlanListFragment;


/**
 * Created by Admin on 16 Dec 2016.
 */

public class BrowsePlanTabPager extends FragmentPagerAdapter {
    public BrowsePlanTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;//super.getItemPosition(object)
    }

    @Override
    public Fragment getItem(int index) {
        BrowsePlanListFragment fragmet = new BrowsePlanListFragment();
        Bundle args = new Bundle();
        switch (index) {
            case 0:
                args.putString("plans", "full");
                fragmet.setArguments(args);
                return fragmet;
            case 1:
                args.putString("plans", "top");
                fragmet.setArguments(args);
                return fragmet;
            case 2:
                args.putString("plans", "2g");
                fragmet.setArguments(args);
                return fragmet;
            case 3:
                args.putString("plans", "3g");
                fragmet.setArguments(args);
                return fragmet;
            case 4:
                args.putString("plans", "RATECUTTER");
                fragmet.setArguments(args);
                return fragmet;
            case 5:
                args.putString("plans", "SMS");
                fragmet.setArguments(args);
                return fragmet;
            case 6:
                args.putString("plans", "Romaing");
                fragmet.setArguments(args);
                return fragmet;
            case 7:
                args.putString("plans", "COMBO");
                fragmet.setArguments(args);
                return fragmet;
            case 8:
                args.putString("plans", "FRC");
                fragmet.setArguments(args);
                return fragmet;
        }

        return fragmet;
    }

    @Override
    public int getCount() {
        return 9;
    }

}
