package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.LastFiveRechargePojo;

public class NewLastRechargeReportAdapter extends RecyclerView.Adapter<NewLastRechargeReportAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    Context context;
    LinearLayout Layscrollline, Layout;
    private List<LastFiveRechargePojo> list;

    public NewLastRechargeReportAdapter(Context context, List<LastFiveRechargePojo> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public NewLastRechargeReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_lasttransaction, parent, false);

        return new NewLastRechargeReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewLastRechargeReportAdapter.MyViewHolder holder, int position) {
        final LastFiveRechargePojo pojo = list.get(position);
        holder.txtdate.setText(pojo.getDOC());
        holder.txtStatus.setText(pojo.getSTATUS());
        holder.txtmobno.setText(pojo.getConsumerNumber());
        holder.txtamount.setText("₹ " + String.valueOf(pojo.getAmount()));
        holder.txttransid.setText(pojo.getTransactionID());
        holder.txtOperatorName.setText(pojo.getOperatorName());
        holder.imgShareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = "> Mobile Number : " + pojo.getConsumerNumber() +
                        "\n> Operator name : " + pojo.getOperatorName() +
                        "\n> Amount : " + "₹ " + String.valueOf(pojo.getAmount()) +
                        "\n> Date : " + String.valueOf(pojo.getDOC()) +
                        "\n> Tx.Id : " + String.valueOf(pojo.getTransactionID()) +
                        "\n> Recharge Status : " + pojo.getSTATUS();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Abhi9 Recharge Status");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, "Share Using"));
            }
        });


        try {
            float clbal = Float.parseFloat(String.valueOf(pojo.getClosingBalance()));
            float opbal = Float.parseFloat(String.valueOf(pojo.getOpeningBalance()));


            holder.txtCommi.setText("₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getDiscountAmount()))));
            holder.txtopbal.setText("₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getOpeningBalance()))));

            holder.txtbalance.setText("₹ " + String.format("%.02f", clbal));

        } catch (Exception e) {

        }
        try {
            if (pojo.getSTATUS().equalsIgnoreCase("SUCCESS")) {

                holder.bgShape.setColor(Color.parseColor("#FF4DB776"));
                Layscrollline.setBackgroundColor(Color.parseColor("#FF4DB776"));
            } else if (pojo.getSTATUS().equalsIgnoreCase("FAIL") || pojo.getSTATUS().equalsIgnoreCase("WRONG NUMBER") || pojo.getSTATUS().equalsIgnoreCase("REFUND") || pojo.getSTATUS().equalsIgnoreCase("FAILURE")) {

                holder.bgShape.setColor(Color.RED);
                Layscrollline.setBackgroundColor(Color.RED);

            } else if (pojo.getSTATUS().equalsIgnoreCase("PROCESS") || pojo.getSTATUS().equalsIgnoreCase("WAITING")) {
                holder.bgShape.setColor(Color.parseColor("#FF8C00"));
                Layscrollline.setBackgroundColor(Color.parseColor("#FF8C00"));
            }
        } catch (Exception e) {
        }

        if (pojo.getOperatorName().toLowerCase().contains("AIRCEL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.aircel);
        } else if (pojo.getOperatorName().toLowerCase().contains("AIRTELDTH".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.airtel);
        } else if (pojo.getOperatorName().toLowerCase().contains("AIRTEL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.airtel);
        } else if (pojo.getOperatorName().toLowerCase().contains("BSNL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.bsnl);
        } else if (pojo.getOperatorName().toLowerCase().contains("DOCOMO".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.docomo);
        } else if (pojo.getOperatorName().toLowerCase().contains("RELIANCE".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.reliance);
        } else if (pojo.getOperatorName().toLowerCase().contains("UNINOR".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.uninor);
        } else if (pojo.getOperatorName().toLowerCase().contains("JIO".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.jio);
        } else if (pojo.getOperatorName().toLowerCase().contains("IDEA".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.idea);
        } else if (pojo.getOperatorName().toLowerCase().contains("LOOP".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.loop);
        } else if (pojo.getOperatorName().toLowerCase().contains("MTS".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mts);
        } else if (pojo.getOperatorName().toLowerCase().contains("VODAFONE".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.vodafone);
        } else if (pojo.getOperatorName().toLowerCase().contains("BIG".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.big);
        } else if (pojo.getOperatorName().toLowerCase().contains("DISH".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.dishtv);
        } else if (pojo.getOperatorName().toLowerCase().contains("SUNDIRECT".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.sundirect);
        } else if (pojo.getOperatorName().toLowerCase().contains("TATASKY".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.tatasky);
        } else if (pojo.getOperatorName().toLowerCase().contains("TELENOR".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.uninor);
        } else if (pojo.getOperatorName().toLowerCase().contains("Tata Indicom".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.indicom);
        } else if (pojo.getOperatorName().toLowerCase().contains("MTNL Mumbai".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.mtnl);
        } else if (pojo.getOperatorName().toLowerCase().contains("MTNL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.mtnl);
        }
        //Elctricity
        else if (pojo.getOperatorName().toLowerCase().contains("MSEB".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.msedcl);
        } else if (pojo.getOperatorName().toLowerCase().contains("TOURENT POWER".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.torrent);
        } else if (pojo.getOperatorName().toLowerCase().contains("BSES Rajdhani".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.bses_rajdhani);
        } else if (pojo.getOperatorName().toLowerCase().contains("BSES Yamuna".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.bses_yamuna);
        } else if (pojo.getOperatorName().toLowerCase().contains("Brihan Mumbai Electric Supply".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.brihan_mumbai_electric_supply);
        } else if (pojo.getOperatorName().toLowerCase().contains("North Delhi".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.ndpl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Jaipur".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.rvvnl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Bangalore".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.banglor);
        } else if (pojo.getOperatorName().toLowerCase().contains("Madhya Pradesh".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mpmkvvlbl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Noida".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.npcl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Paschim".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mppkvvi);
        } else if (pojo.getOperatorName().toLowerCase().contains("Calcutta".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.calcuttaelectricity);
        } else if (pojo.getOperatorName().toLowerCase().contains("Chhattisgarh".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.cseb);
        } else if (pojo.getOperatorName().toLowerCase().contains("Corporation".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.ipcl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Jamshedpur".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.juscl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Tripura".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.t);
        } //Landline
        else if (pojo.getOperatorName().toLowerCase().contains("AIRTEL LANDLINE".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.airtel);
        } else if (pojo.getOperatorName().toLowerCase().contains("BSNL LANDLINE BILL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.bsnl);
        } else if (pojo.getOperatorName().toLowerCase().contains("DOCOMO LANDLINE".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.docomo);
        } else if (pojo.getOperatorName().toLowerCase().contains("MTNL DELHI".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mtnl);
        } else if (pojo.getOperatorName().toLowerCase().contains("TIKONA".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.tikona);
        }//Gas
        else if (pojo.getOperatorName().toLowerCase().contains("Mahanagar".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mgl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Indraprast".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.igl);
        } else if (pojo.getOperatorName().toLowerCase().contains("Gujarat".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.ggcl);
        } else if (pojo.getOperatorName().toLowerCase().contains("ADANI".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.adanigas);
        } else {
            holder.imgoperator.setImageResource(R.mipmap.operater);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtStatus, txtdate, txtoperator, txtamount, txttransid, txtbalance, txtmobno, txtPreBalance, txtCommi,
                txtPPrize, txtOperatorName, txtopbal;
        ImageView imgoperator, imgShareImage;
        GradientDrawable bgShape;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = (TextView) rowView.findViewById(R.id.txtdate);
            txtStatus = (TextView) rowView.findViewById(R.id.txtStatus);
            txtoperator = (TextView) rowView.findViewById(R.id.txtoperator);
            txtamount = (TextView) rowView.findViewById(R.id.txtAmount);
            txttransid = (TextView) rowView.findViewById(R.id.txtTxId);
            txtbalance = (TextView) rowView.findViewById(R.id.txtClosingBalance);
            txtmobno = (TextView) rowView.findViewById(R.id.txtMobileNo);
            imgoperator = (ImageView) rowView.findViewById(R.id.imageView);
            imgShareImage = (ImageView) rowView.findViewById(R.id.imgShareImage);
            txtCommi = (TextView) rowView.findViewById(R.id.txtCommi);
            bgShape = (GradientDrawable) txtStatus.getBackground();
            txtOperatorName = (TextView) rowView.findViewById(R.id.txtOperatorName);
            txtopbal = (TextView) rowView.findViewById(R.id.txtOpeningBalance);
            Layscrollline = (LinearLayout) rowView.findViewById(R.id.Layscrollline);
        }
    }
}
