package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.PurchaseReportPojo;

import java.util.List;


/**
 * Created by consisty on 12/4/17.
 */

public class PurchaseAdapterlist extends RecyclerView.Adapter<PurchaseAdapterlist.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    LinearLayout LayDispute, Layout;
    private List<PurchaseReportPojo> list;

    public PurchaseAdapterlist(List<PurchaseReportPojo> list) {
        this.list = list;
    }

    @Override
    public PurchaseAdapterlist.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchase_list_report, parent, false);
        return new PurchaseAdapterlist.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(PurchaseAdapterlist.MyViewHolder holder, int position) {
        PurchaseReportPojo pojo = list.get(position);
        holder.txtdate.setText(pojo.getDOC());
        holder.txtSenderName.setText(pojo.getSenderName());
        holder.txtReceiverName.setText(pojo.getReceiverName());
        holder.txtReceiverId.setText(String.valueOf(pojo.getReceiverID()));
        holder.txtSenderId.setText(String.valueOf(pojo.getSenderID()));
        holder.txtAmount.setText(String.valueOf(pojo.getAmount()));
        holder.txtTransactioID.setText(String.valueOf(pojo.getBalanceTransferID()));
        holder.txtOpenbal.setText(String.valueOf(pojo.getOpeningBalance()));
        holder.txtclosebal.setText(String.valueOf(pojo.getClosingBalance()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtOpenbal, txtdate, txtSenderId, txtAmount, txtbalance, txtclosebal,
                txtReceiverId,txtReceiverName,txtSenderName,txtTransactioID;
        ImageView imgoperator;

        /*{"BalanceTransferID":5,"SenderID":2,"ReceiverID":3,"Amount":5,"OpeningBalanceSender":5.94000,
        "CloseBalanceSender":0.94000,"DOC":"13 Sep 2017 13:25:43:280","Receiver":"sai","RUserTypeID":3,"Sender":"Priya","ParentID":61,"DUserTypeID":2}*/
        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = (TextView) rowView.findViewById(R.id.txtdate);
            txtReceiverName = (TextView) rowView.findViewById(R.id.txtReceiverName);
            txtReceiverId = (TextView) rowView.findViewById(R.id.txtReceiverID);
            txtSenderName = (TextView) rowView.findViewById(R.id.txtsenderName);
            txtSenderId = (TextView) rowView.findViewById(R.id.txtSenderId);
            txtAmount = (TextView) rowView.findViewById(R.id.txtAmount);
            txtOpenbal = (TextView) rowView.findViewById(R.id.txtOpeningBalance);
            txtclosebal = (TextView) rowView.findViewById(R.id.txtClosingBalance);
            txtTransactioID = (TextView)rowView.findViewById(R.id.txtTransactionId);

        }
    }
}
