package in.omkarpayment.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.browsPlan.BrowsePlansActivity;
import in.omkarpayment.app.json.ViewPlansDecPojo;


public class BrowsePlansAdapterList extends ArrayAdapter<ViewPlansDecPojo> {
    private final Context context;
    List<ViewPlansDecPojo> baltranlist;
    LinearLayout LayCard;


    public BrowsePlansAdapterList(Context context, List<ViewPlansDecPojo> baltranlist) {
        super(context, R.layout.adapter_brows_plan, baltranlist);
        //super(context, R.layout.activity_balance_transfer_reportadapter, list);
        this.context = context;
        this.baltranlist = baltranlist;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.adapter_brows_plan, parent, false);

        TextView txtValidity = (TextView) rowView.findViewById(R.id.txtValidity);
        TextView txtDescription = (TextView) rowView.findViewById(R.id.txtDiscription);
        final TextView textAmount = (TextView) rowView.findViewById(R.id.textAmount);
        TextView txtset = (TextView) rowView.findViewById(R.id.txtset);
        LayCard = (LinearLayout) rowView.findViewById(R.id.LayCard);
        GradientDrawable bgShape = (GradientDrawable) txtset.getBackground();
        bgShape.setColor(Color.parseColor("#FF4DB776"));
        ViewPlansDecPojo last = new ViewPlansDecPojo();
        last = baltranlist.get(position);
        final ViewPlansDecPojo finalLast = last;
        txtset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((BrowsePlansActivity) context).setResult(2, intent);
                intent.putExtra("amount", finalLast.getRs());
                ((BrowsePlansActivity) context).finish();
            }
        });
        textAmount.setText(last.getRs());
        txtValidity.setText(last.getValidity());
        txtDescription.setText(last.getDesc());

        return rowView;
    }


}
