package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.MoneyTxCommissionPojo;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;


/**
 * Created by lenovo on 8/17/2018.
 */

public class MoneyTxCommissionStrAdapter extends RecyclerView.Adapter<MoneyTxCommissionStrAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    LinearLayout LayDispute, Layout;
    GradientDrawable bgShape;
    private List<MoneyTxCommissionPojo> list;

    public MoneyTxCommissionStrAdapter(List<MoneyTxCommissionPojo> list) {
        this.list = list;
    }

    @Override
    public MoneyTxCommissionStrAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_money_tx_comm_str, parent, false);

        return new MoneyTxCommissionStrAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MoneyTxCommissionStrAdapter.MyViewHolder holder, int position) {
        MoneyTxCommissionPojo pojo = list.get(position);
        try {
            holder.txtFromAmt.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getFromAmount()))));
            holder.txtToAmt.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getToAmount()))));


            if (UserDetails.UserType.equals(UserType.Retailer)) {
                holder.txtFixCommission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getRetailerFixRs()))));
                holder.txtFlexiCOmmission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getRetailerFlexi()))));

            } else if (UserDetails.UserType.equals(UserType.Distributor)) {
                holder.txtFixCommission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getDistributorFixRs()))));
                holder.txtFlexiCOmmission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getDistributorFlexi()))));
            } else if (UserDetails.UserType.equals(UserType.SuperDistributor)) {
                holder.txtFixCommission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getSuperDistributorFixRs()))));
                holder.txtFlexiCOmmission.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getSuperDistributorFlexi()))));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtFromAmt, txtToAmt, txtFixCommission, txtFlexiCOmmission;


        public MyViewHolder(View rowView) {
            super(rowView);
            txtFromAmt = (TextView) rowView.findViewById(R.id.txtFromAmt);
            txtToAmt = (TextView) rowView.findViewById(R.id.txtToAmt);
            txtFixCommission = (TextView) rowView.findViewById(R.id.txtFixCommission);
            txtFlexiCOmmission = (TextView) rowView.findViewById(R.id.txtFlexiCommission);

        }
    }
}
