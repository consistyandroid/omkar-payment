package in.omkarpayment.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.GetBankNamesPojo;

/**
 * Created by Consisty on 01-Sep-17.
 */

public class AutocompleteForBankNameAdapter extends ArrayAdapter<GetBankNamesPojo> implements Filterable {
    private final Context mContext;
    private final List<GetBankNamesPojo> listMemebers;
    private final List<GetBankNamesPojo> listMemebers_All;
    private final List<GetBankNamesPojo> listMemebers_Suggestion;
    private final int mLayoutResourceId;

    public AutocompleteForBankNameAdapter(Context context, int resource, List<GetBankNamesPojo> departments) {
        super(context, resource, departments);
        this.mContext = context;
        this.mLayoutResourceId = resource;
        this.listMemebers = new ArrayList<>(departments);
        this.listMemebers_All = new ArrayList<>(departments);
        this.listMemebers_Suggestion = new ArrayList<>();
    }


    public int getCount() {
        return listMemebers.size();
    }

    public GetBankNamesPojo getItem(int position) {
        return listMemebers.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(mLayoutResourceId, parent, false);
            }
            GetBankNamesPojo department = getItem(position);
            //  ImageView imageView=(ImageView)convertView.findViewById(R.id.img);
            //imageView.setImageResource(department.getOperator());
            TextView name = (TextView) convertView.findViewById(R.id.autoText);
            name.setText(department.getBank());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                return ((GetBankNamesPojo) resultValue).getBank();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    listMemebers_Suggestion.clear();
                    for (GetBankNamesPojo department : listMemebers_All) {
                        if (department.getBank().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            listMemebers_Suggestion.add(department);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = listMemebers_Suggestion;
                    filterResults.count = listMemebers_Suggestion.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listMemebers.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using listMemebers.addAll((ArrayList<Department>) results.values);
                    List<?> result = (List<?>) results.values;
                    for (Object object : result) {
                        if (object instanceof GetBankNamesPojo) {
                            listMemebers.add((GetBankNamesPojo) object);
                        }
                    }
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    listMemebers.addAll(listMemebers_All);
                }
                notifyDataSetChanged();
            }
        };
    }
}
