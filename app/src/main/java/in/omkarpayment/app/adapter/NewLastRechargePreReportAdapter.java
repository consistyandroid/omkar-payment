package in.omkarpayment.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import in.omkarpayment.app.R;
import in.omkarpayment.app.json.LastFiveRechargePojo;

public class NewLastRechargePreReportAdapter extends RecyclerView.Adapter<NewLastRechargePreReportAdapter.MyViewHolder> {
    public static int i;
    GradientDrawable bgShape, bgShape2;
    Context context;
    LinearLayout Layscrollline;
    private ArrayList<LastFiveRechargePojo> list;

    public NewLastRechargePreReportAdapter(Context context, ArrayList<LastFiveRechargePojo> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public NewLastRechargePreReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_lasttransaction, parent, false);

        return new NewLastRechargePreReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewLastRechargePreReportAdapter.MyViewHolder holder, int position) {
       final LastFiveRechargePojo pojo = list.get(position);


        holder.txtdate.setText(pojo.getDOC());
        holder.txtStatus.setText(pojo.getSTATUS());
        holder.txtmobno.setText(pojo.getConsumerNumber());
        holder.txtamount.setText(String.valueOf(pojo.getAmount()));
        holder.txttransid.setText(pojo.getTransactionID());
        holder.txtOperatorName.setText(pojo.getOperatorName());
        holder.imgShareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = ">Mobile Number : "+pojo.getConsumerNumber()+
                        "\nOperator name : "+pojo.getOperatorName()+
                        "\nAmount : "+"₹ " + String.valueOf(pojo.getAmount())+
                        "\nDate : "+ String.valueOf(pojo.getDOC())+
                        "\nTx.Id : "+ String.valueOf(pojo.getAmount())+
                        "\nRecharge Status : "+pojo.getSTATUS();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Recharge Status");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.share_using)));
            }
        });
        try {
            holder.txtClBal.setText("₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getClosingBalance()))));
            holder.txtOpBal.setText("₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getOpeningBalance()))));
        } catch (Exception e) {

        }
        try {
            if (pojo.getSTATUS().equalsIgnoreCase("SUCCESS")) {
                bgShape.setColor(Color.parseColor("#FF4DB776"));
                Layscrollline.setBackgroundColor(Color.parseColor("#FF4DB776"));
            } else if (pojo.getSTATUS().equalsIgnoreCase("FAIL") || pojo.getSTATUS().equalsIgnoreCase("WRONG NUMBER") || pojo.getSTATUS().equalsIgnoreCase("REFUND") || pojo.getSTATUS().equalsIgnoreCase("FAILURE")) {
                bgShape.setColor(Color.RED);
                Layscrollline.setBackgroundColor(Color.RED);
            } else if (pojo.getSTATUS().equalsIgnoreCase("PROCESS") || pojo.getSTATUS().equalsIgnoreCase("WAITING")) {
                bgShape.setColor(Color.parseColor("#FF8C00"));
                Layscrollline.setBackgroundColor(Color.parseColor("#FF8C00"));
            }
        } catch (Exception e) {
        }

        if (pojo.getOperatorName().toLowerCase().contains("AIRCEL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.aircel);
        } else if (pojo.getOperatorName().toLowerCase().contains("AIRTELDTH".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.airtel);
        } else if (pojo.getOperatorName().toLowerCase().contains("AIRTEL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.airtel);
        } else if (pojo.getOperatorName().toLowerCase().contains("BSNL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.bsnl);
        } else if (pojo.getOperatorName().toLowerCase().contains("DOCOMO".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.docomo);
        } else if (pojo.getOperatorName().toLowerCase().contains("RELIANCE".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.reliance);
        } else if (pojo.getOperatorName().toLowerCase().contains("UNINOR".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.uninor);
        } else if (pojo.getOperatorName().toLowerCase().contains("JIO".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.jio);
        } else if (pojo.getOperatorName().toLowerCase().contains("IDEA".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.idea);
        } else if (pojo.getOperatorName().toLowerCase().contains("MTS".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mts);
        } else if (pojo.getOperatorName().toLowerCase().contains("VODAFONE".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.vodafone);
        } else if (pojo.getOperatorName().toLowerCase().contains("BIG".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.big);
        } else if (pojo.getOperatorName().toLowerCase().contains("LOOP".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.loop);
        } else if (pojo.getOperatorName().toLowerCase().contains("DISH".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.dishtv);
        } else if (pojo.getOperatorName().toLowerCase().contains("Sun Direct".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.sundirect);
        } else if (pojo.getOperatorName().toLowerCase().contains("Videocon D2H".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.videocondth);
        } else if (pojo.getOperatorName().toLowerCase().contains("TATASKY".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.tatasky);
        } else if (pojo.getOperatorName().toLowerCase().contains("TELENOR".toLowerCase())) {
            holder.imgoperator.setImageResource(R.drawable.uninor);
        } else if (pojo.getOperatorName().toLowerCase().contains("MSEB".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mseb);
        } else if (pojo.getOperatorName().toLowerCase().contains("Tata Indicom".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.indicom);
        } else if (pojo.getOperatorName().toLowerCase().contains("BEST".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.best);
        } else if (pojo.getOperatorName().toLowerCase().contains("MTNL-Mumbai".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mtnl);
        }else if (pojo.getOperatorName().toLowerCase().contains("MTNL".toLowerCase())) {
            holder.imgoperator.setImageResource(R.mipmap.mtnl);
        }else {
            holder.imgoperator.setImageResource(R.mipmap.operater);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtStatus, txtdate, txtoperator, txtamount, txttransid, txtbalance, txtmobno, txtOpBal, txtClBal,
                txtgrad, txtOperatorName, txtCommi;
        ImageView imgoperator,imgShareImage;


        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = (TextView) rowView.findViewById(R.id.txtdate);
            txtStatus = (TextView) rowView.findViewById(R.id.txtStatus);
            txtoperator = (TextView) rowView.findViewById(R.id.txtoperator);
            txtamount = (TextView) rowView.findViewById(R.id.txtAmount);
            txttransid = (TextView) rowView.findViewById(R.id.txtTxId);
            txtbalance = (TextView) rowView.findViewById(R.id.txtClosingBalance);
            txtmobno = (TextView) rowView.findViewById(R.id.txtMobileNo);
            imgoperator = (ImageView) rowView.findViewById(R.id.imageView);
            imgShareImage = (ImageView) rowView.findViewById(R.id.imgShareImage);
            txtCommi = (TextView) rowView.findViewById(R.id.txtCommi);
            bgShape = (GradientDrawable) txtStatus.getBackground();
            txtOperatorName = (TextView) rowView.findViewById(R.id.txtOperatorName);
            Layscrollline = (LinearLayout) rowView.findViewById(R.id.Layscrollline);
            txtClBal = (TextView) rowView.findViewById(R.id.txtClosingBalance);
            txtOpBal = (TextView) rowView.findViewById(R.id.txtOpeningBalance);
        }
    }
}
