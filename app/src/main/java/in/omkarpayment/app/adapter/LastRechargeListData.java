package in.omkarpayment.app.adapter;

/**
 * Created by Consisty on 26-May-17.
 */

public class LastRechargeListData {

    String mobilenovcno;
    Double amount;
    String status;
    String txid;
    String doc;
    Integer rechargeid;
    String operatorname;
    Double ClosingBal;

    public LastRechargeListData(String mobilenovcno, Double amount, String status, String txid,
                                String doc, Integer rechargeid, String operatorname, Double ClosingBal) {
        this.mobilenovcno = mobilenovcno;
        this.amount = amount;
        this.status = status;
        this.txid = txid;
        this.doc = doc;
        this.rechargeid = rechargeid;
        this.operatorname = operatorname;
        this.ClosingBal = ClosingBal;

    }


    public String getTransactionID() {
        return txid;
    }

    public void setTransactionID(String transactionID) {
        this.txid = txid;
    }

    public Integer getRechargeID() {
        return rechargeid;
    }

    public void setRechargeID(Integer rechargeID) {
        this.rechargeid = rechargeid;
    }



    public String getConsumerNumber() {
        return mobilenovcno;
    }

    public void setConsumerNumber(String consumerNumber) {
        this.mobilenovcno = mobilenovcno;
    }

    public Double getClosingBalance() {
        return ClosingBal;
    }

    public void setClosingBalance(Double closingBalance) {
        this.ClosingBal = ClosingBal;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDOC() {
        return doc;
    }

    public void setDOC(String dOC) {
        this.doc = doc;
    }




    public String getOperatorName() {
        return operatorname;
    }

    public void setOperatorName(String operatorName) {
        this.operatorname = operatorname;
    }





    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




}
