package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import in.omkarpayment.app.R;
import in.omkarpayment.app.json.StockReportPojo;
import in.omkarpayment.app.report.StockReport.StockReportActivity;
import in.omkarpayment.app.userContent.UserDetails;

/**
 * Created by Somnath-Laptop on 29-Jan-2018.
 */

public class StockReportAdapter extends RecyclerView.Adapter<StockReportAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    LinearLayout LayDispute, Layout;
    StockReportActivity stockReportActivity;
    Context context;
    private List<StockReportPojo> list;

    public StockReportAdapter(StockReportActivity stockReportActivity, Context context, List<StockReportPojo> list) {
        this.stockReportActivity = stockReportActivity;
        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_stock_report, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final StockReportPojo pojo = list.get(position);
        holder.txtdate.setText(pojo.getDOC());
        holder.txtReqFrom.setText(pojo.getRequestFrom());
        holder.txtReqTo.setText(pojo.getRequestTo());
        holder.txtStatus.setText(pojo.getStatus());
        holder.txtBillno.setText(pojo.getBillNumber());
        holder.txtType.setText(pojo.getPayMode());
        holder.txtBalanceType.setText(pojo.getBalanceType());

        try {
            float bal = Float.parseFloat(String.valueOf(pojo.getAmount()));
            holder.txtAmount.setText("₹ " + String.format("%.00f", bal));
        } catch (Exception e) {

        }

        holder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stockReportActivity.rejectBalanceRequest(pojo.getRequestID());
            }
        });

        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stockReportActivity.acceptBalanceRequest(pojo);
            }
        });
        String userID = String.valueOf(UserDetails.UserId);

        if (!userID.toLowerCase().equalsIgnoreCase(pojo.getRequestTo().toLowerCase())) {
            holder.btnAccept.setVisibility(View.INVISIBLE);
            holder.btnReject.setVisibility(View.INVISIBLE);
            return;
        }

        if (pojo.getStatus().equalsIgnoreCase("Pending")) {
            holder.btnAccept.setVisibility(View.VISIBLE);
            holder.btnReject.setVisibility(View.VISIBLE);
        }else {
            holder.btnAccept.setVisibility(View.INVISIBLE);
            holder.btnReject.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtReqFrom, txtdate, txtReqTo, txtAmount, txtbalance, txtBillno, txtType, txtBalanceType,
                txtStatus;
        ImageView imgoperator;
        Button btnReject,btnAccept;

        /*[{"RequestID":1,"UserID":3,"Amount":3.00000,"PayMode":"Cash","BillNumber":"CashPayment","Status":"Pending","IsDeleted":false,"RequestFrom":"sai","RUserTypeID":3,
                "DUserTypeID":2,"RequestTo":"2","ParentName":"Priya","DOC":"19 Jan 2018 12:03:43:210"}]*/
        public MyViewHolder(View rowView) {
            super(rowView);
            txtdate = (TextView) rowView.findViewById(R.id.txtdate);
            txtReqFrom = (TextView) rowView.findViewById(R.id.txtReqFrom);
            txtReqTo = (TextView) rowView.findViewById(R.id.txtRequestTo);
            txtBillno = (TextView) rowView.findViewById(R.id.txtBillNo);
            txtStatus = (TextView) rowView.findViewById(R.id.txtStatus);
            txtAmount = (TextView) rowView.findViewById(R.id.txtAmount);
            txtType = (TextView) rowView.findViewById(R.id.txtType);
            btnReject = (Button) rowView.findViewById(R.id.btnReject);
            btnAccept = (Button) rowView.findViewById(R.id.btnAccept);
            txtBalanceType = (TextView) rowView.findViewById(R.id.txtBalanceType);


        }
    }
}
