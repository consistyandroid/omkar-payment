package in.omkarpayment.app.adapter;

import android.app.ProgressDialog;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.report.commissionStructure.CommissionStructureJSON;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

public class CommissionStructureReportAdapter extends RecyclerView.Adapter<CommissionStructureReportAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    LinearLayout LayDispute, Layout;
    GradientDrawable bgShape;
    private List<CommissionStructureJSON> list;

    public CommissionStructureReportAdapter(List<CommissionStructureJSON> list) {
        this.list = list;
    }

    @Override
    public CommissionStructureReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.commission_structure_adapter, parent, false);

        return new CommissionStructureReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommissionStructureReportAdapter.MyViewHolder holder, int position) {
        CommissionStructureJSON pojo = list.get(position);

        holder.txtService.setText(pojo.getServiceName());
        holder.txtOprator.setText(pojo.getOperatorName());
        holder.txtRetailer.setText(pojo.getRetailerCommission());
        if (UserDetails.UserType.equals(UserType.Retailer)) {
            holder.txtRetailer.setText(pojo.getRetailerCommission());
        } else if (UserDetails.UserType.equals(UserType.Distributor))
        {
            holder.txtRetailer.setText(pojo.getDistCommission());
        }

          /*  try {
                float bal = Float.parseFloat(String.valueOf(pojo.getClosingBalance()));
                //holder.txtCommi.setText(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getDiscountAmount()))));
                holder.txtbalance.setText(String.format("%.02f", bal));
            } catch (Exception e) {

            }*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtService, txtOprator, txtRetailer;


        public MyViewHolder(View rowView) {
            super(rowView);
            txtService = (TextView) rowView.findViewById(R.id.txtService);
            txtOprator = (TextView) rowView.findViewById(R.id.txtOprator);
            txtRetailer = (TextView) rowView.findViewById(R.id.txtRetailer);

        }
    }
}
