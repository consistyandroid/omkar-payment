package in.omkarpayment.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.browsPlan.BrowsePlansActivity;
import in.omkarpayment.app.json.DTHPlansAddOnPojo;


/**
 * Created by ${user} on 5/2/18.
 */

public class DTHAddOnAdapter extends ArrayAdapter<DTHPlansAddOnPojo> {
    private final Context context;
    List<DTHPlansAddOnPojo> baltranlist;
    LinearLayout LayCard;


    public DTHAddOnAdapter(Context context, List<DTHPlansAddOnPojo> baltranlist) {
        super(context, R.layout.adapter_addon, baltranlist);
        //super(context, R.layout.activity_balance_transfer_reportadapter, list);
        this.context = context;
        this.baltranlist = baltranlist;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.adapter_addon, parent, false);

        TextView txtValidity = (TextView) rowView.findViewById(R.id.txtValidity);
        TextView txtDescription = (TextView) rowView.findViewById(R.id.txtDiscription);
        final TextView textAmount = (TextView) rowView.findViewById(R.id.textAmount);
        TextView txtset = (TextView) rowView.findViewById(R.id.txtset);
        TextView txtPlanName = (TextView) rowView.findViewById(R.id.txtPlanName);
        LayCard = (LinearLayout) rowView.findViewById(R.id.LayCard);
        GradientDrawable bgShape = (GradientDrawable) txtset.getBackground();
        bgShape.setColor(Color.parseColor("#FF4DB776"));
        DTHPlansAddOnPojo last = new DTHPlansAddOnPojo();
        last = baltranlist.get(position);
        final DTHPlansAddOnPojo finalLast = last;
        txtset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((BrowsePlansActivity) context).setResult(3, intent);
                intent.putExtra("amount", finalLast.getRs().get1MONTHS());
                ((BrowsePlansActivity) context).finish();
            }
        });
        textAmount.setText(last.getRs().get1MONTHS());
        //   txtValidity.setText(last.getValidity());
        txtDescription.setText(last.getDesc());
        txtPlanName.setText(last.getPlanName());

        return rowView;
    }

}
