package in.omkarpayment.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.browsPlan.BrowsePlansActivity;
import in.omkarpayment.app.json.DTHPlansPojo;


/**
 * Created by ${user} on 5/2/18.
 */

public class DTHPlanAdapter extends ArrayAdapter<DTHPlansPojo> {
    private final Context context;
    List<DTHPlansPojo> baltranlist;
    LinearLayout Lay1M, Lay3M, Lay6M, Lay1Y;


    public DTHPlanAdapter(Context context, List<DTHPlansPojo> baltranlist) {
        super(context, R.layout.adapter_dth_plans, baltranlist);
        //super(context, R.layout.activity_balance_transfer_reportadapter, list);
        this.context = context;
        this.baltranlist = baltranlist;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.adapter_dth_plans, parent, false);

        TextView txtPlanName = (TextView) rowView.findViewById(R.id.txtPlanName);
        TextView txtDescription = (TextView) rowView.findViewById(R.id.txtDiscription);
        final TextView text1MAmount = (TextView) rowView.findViewById(R.id.text1MAmount);
        final TextView text3MAmount = (TextView) rowView.findViewById(R.id.text3MAmount);
        final TextView text6MAmount = (TextView) rowView.findViewById(R.id.text6MAmount);
        final TextView text1YAmount = (TextView) rowView.findViewById(R.id.text1YAmount);
        TextView txtset = (TextView) rowView.findViewById(R.id.txtset);
        Lay1M = (LinearLayout) rowView.findViewById(R.id.Lay1M);
        Lay3M = (LinearLayout) rowView.findViewById(R.id.Lay3M);
        Lay6M = (LinearLayout) rowView.findViewById(R.id.Lay6M);
        Lay1Y = (LinearLayout) rowView.findViewById(R.id.Lay1Y);

        GradientDrawable bgShape1 = (GradientDrawable) text1MAmount.getBackground();
        GradientDrawable bgShape2 = (GradientDrawable) text3MAmount.getBackground();
        GradientDrawable bgShape3 = (GradientDrawable) text6MAmount.getBackground();
        GradientDrawable bgShape4 = (GradientDrawable) text1YAmount.getBackground();
        bgShape1.setColor(Color.parseColor("#FF4DB776"));
        bgShape2.setColor(Color.parseColor("#FF4DB776"));
        bgShape3.setColor(Color.parseColor("#FF4DB776"));
        bgShape4.setColor(Color.parseColor("#FF4DB776"));
        DTHPlansPojo last = new DTHPlansPojo();
        last = baltranlist.get(position);
        final DTHPlansPojo finalLast = last;

        text1MAmount.setText(last.get1Month());
        text3MAmount.setText(last.get3Month());
        text6MAmount.setText(last.get6Month());
        text1YAmount.setText(last.get1Year());

        Lay1M.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((BrowsePlansActivity) context).setResult(3, intent);
                intent.putExtra("amount", finalLast.get1Month());
                ((BrowsePlansActivity) context).finish();
            }
        });
        Lay3M.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((BrowsePlansActivity) context).setResult(3, intent);
                intent.putExtra("amount", finalLast.get3Month());
                ((BrowsePlansActivity) context).finish();
            }
        });
        Lay6M.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((BrowsePlansActivity) context).setResult(3, intent);
                intent.putExtra("amount", finalLast.get6Month());
                ((BrowsePlansActivity) context).finish();
            }
        });
        Lay1Y.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ((BrowsePlansActivity) context).setResult(3, intent);
                intent.putExtra("amount", finalLast.get1Year());
                ((BrowsePlansActivity) context).finish();
            }
        });
        txtPlanName.setText(last.getPlanName());
        txtDescription.setText(last.getDesc());

        return rowView;
    }
}
