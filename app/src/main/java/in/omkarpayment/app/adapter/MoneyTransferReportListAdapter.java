package in.omkarpayment.app.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.omkarpayment.app.MoneyTransferpkg.MoneyTransfer.IMoneyTransfer;
import in.omkarpayment.app.R;
import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.model.IMoneyTransferStatusResponse;
import in.omkarpayment.app.model.MoneyTransferAsync;
import in.omkarpayment.app.model.MoneyTransferReportStatusAsync;
import in.omkarpayment.app.report.MoneyTransferReport.MoneyTransferReportPojo;
import in.omkarpayment.app.report.MoneyTransferReport.ReportReceiptActivity;

/**
 * Created by ${user} on 12/9/17.
 */

public class MoneyTransferReportListAdapter extends RecyclerView.Adapter<MoneyTransferReportListAdapter.MyViewHolder> implements IMoneyTransfer, IMoneyTransferStatusResponse {
    public static int i;
    public ProgressDialog pDialog;
    View view;
    Bitmap bitmap;
    Canvas canvas;
    Context context;
    Cryptography_Android data = new Cryptography_Android();
    Dialog dialog;
    String Type;
    EditText edtAmount;
    TextView spSub, txtSenderNo, txtBankNameComplaint, txtAmount, txtTransNo, txtDesc;
    private ArrayList<MoneyTransferReportPojo> list;

    public MoneyTransferReportListAdapter(Context context, ArrayList<MoneyTransferReportPojo> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MoneyTransferReportListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.moneytransfer_report, parent, false);
        return new MoneyTransferReportListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MoneyTransferReportListAdapter.MyViewHolder holder, final int position) {


        final MoneyTransferReportPojo pojo = list.get(position);

        holder.txtSender.setText(String.valueOf(pojo.getName()));
        holder.txtBeneficiaryName.setText(pojo.getBeneficiaryName());
        holder.txtBankName.setText(pojo.getBank());
        holder.txtAccountNumber.setText(pojo.getAccountNo());
        double Amount = pojo.getAmount();
        String amount = String.valueOf(Amount);
        holder.txtAmount.setText("₹ " + amount);
        double Charges = pojo.getCharges();
        String charge = String.valueOf(Charges);
        holder.txtCharges.setText("₹ " + charge);
        double CloseBal = pojo.getClosingBal();
        String CloseBalance = String.valueOf(CloseBal);
        holder.txtClosingBal.setText("₹ " + CloseBalance);
        holder.txtComplaintView.setVisibility(View.GONE);
        holder.txtTxnNo.setText(pojo.getTransactionID());
        holder.txtSenderMobileNumber.setText(pojo.getSenderNumber());
        holder.txtRemark.setText(pojo.getResponse());
        holder.txtMode.setText(pojo.getMode());
        holder.txtIFSC.setText(String.valueOf(pojo.getIFSCCode()));
        holder.txtDate.setText(pojo.getDOC()) ;
        holder.txtComplaintView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  complaintRequest(pojo.getDOC(), pojo.getSenderNumber(), pojo.getAmount(), pojo.getBank(), pojo.getTransactionID(), pojo.getStatus());
                //Log.i("SenderNumber", pojo.getSenderNumber());
                complaintRequest(pojo.getInstapayID());
            }
        });


        try {
            int bal = Integer.parseInt(String.valueOf(pojo.getClosingBal()));
            holder.txtClosingBal.setText(String.format("%.02f", bal));
        } catch (Exception e) {

        }
       /* if (!holder.txtStatus.getText().equals("Success")) {
            holder.btnReceipt.setVisibility(View.GONE);
        }*/
        if (pojo.getStatus().equalsIgnoreCase("SUCCESS")) {
            holder.txtStatus.setText("Success");
            holder.txtReceipt.setVisibility(View.VISIBLE);
            holder.txtComplaintView.setVisibility(View.GONE);
            holder.statusBackgrounds.setColor(Color.parseColor("#FF4DB776"));
        } else if (pojo.getStatus().equalsIgnoreCase("FAIL") || pojo.getStatus().equalsIgnoreCase("WRONG NUMBER") || pojo.getStatus().equalsIgnoreCase("REFUND") || pojo.getStatus().equalsIgnoreCase("FAILURE")) {
            holder.statusBackgrounds.setColor(Color.RED);
            holder.txtStatus.setText("Fail");
            holder.txtReceipt.setVisibility(View.GONE);
            holder.txtComplaintView.setVisibility(View.GONE);
        } else if (pojo.getStatus().equalsIgnoreCase("PROCESS") || pojo.getStatus().equalsIgnoreCase("WAITING")) {
            holder.statusBackgrounds.setColor(Color.parseColor("#FF8C00"));
            holder.txtStatus.setText("Awaiting From Bank");
            holder.txtReceipt.setVisibility(View.GONE);
            holder.txtComplaintView.setVisibility(View.VISIBLE);
        } else if (pojo.getStatus().equalsIgnoreCase("VERIFIED")) {
            holder.statusBackgrounds.setColor(Color.parseColor("#FF8C00"));
            holder.txtStatus.setText("VERIFIED");
        }
        holder.txtReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReportReceiptActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("TransType", "IMPS");
                bundle.putString("TxId", pojo.getTransactionID());
                bundle.putString("AccountNo", pojo.getAccountNo());
                bundle.putString("IFSCCode", pojo.getIFSCCode());
                bundle.putString("Amount", String.valueOf(pojo.getAmount()));
                bundle.putString("BankName", pojo.getBank());
                bundle.putString("BenifName", pojo.getBeneficiaryName());
                bundle.putString("PayDate", pojo.getDOC());
                bundle.putString("SenderName", pojo.getSenderNumber());
                intent.putExtras(bundle);
                context.startActivity(intent);
               // transferMoney(pojo.getSenderNumber(), pojo.getBeneficiaryName(), pojo.getBank(), pojo.getAccountNo(), pojo.getAmount(), pojo.getTransactionID(), pojo.getDOC(), pojo.getName(), pojo.getCharges(), pojo.getMode(), pojo.getIFSCCode());
            }


        });
    }

    private void complaintRequest(String instapayID) {
        MoneyTransferReportStatusAsync status = new MoneyTransferReportStatusAsync();
        status.delegate = this;
        status.execute(instapayID);
    }

   /* private void complaintRequest(String doc, String Snumber, Double amount, final String bank, String transactionID, String status) {

        dialog = new Dialog(context, R.style.Theme_AppCompat_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.dialog_moneytransfer_complaint);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Submit Complaint");
        spSub = (TextView) dialog.findViewById(R.id.spSub);

        txtBankNameComplaint = (TextView) dialog.findViewById(R.id.txtBankNameComplaint);
        txtAmount = (TextView) dialog.findViewById(R.id.txtAmount);
        txtTransNo = (TextView) dialog.findViewById(R.id.txtTransNo);
        txtDesc = (EditText) dialog.findViewById(R.id.txtDesc);
        txtSenderNo = (TextView) dialog.findViewById(R.id.txtSendNo);


        Button btnSubmmit = (Button) dialog.findViewById(R.id.btnSubmmit);
        txtSenderNo.setText(String.valueOf(Snumber));
        txtBankNameComplaint.setText(bank);
        txtBankNameComplaint.setFocusable(false);
        txtAmount.setText(String.valueOf(amount));
        txtTransNo.setText(transactionID);
        txtDesc.setText(status);
        spSub.setText("Money Transfer");
        dialog.show();

        btnSubmmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtSenderNo.getText().toString().isEmpty()) {
                    txtSenderNo.setError("Please enter Sender number");
                } else if (txtBankNameComplaint.getText().toString().isEmpty()) {
                    txtBankNameComplaint.setError("Please enter Bank");
                } else if (txtAmount.getText().toString().isEmpty()) {
                    txtAmount.setError("Please enter amount");
                } else if (txtTransNo.getText().toString().isEmpty()) {
                    txtTransNo.setError("Please enter transaction no.");
                } else if (txtDesc.getText().toString().isEmpty()) {
                    txtDesc.setError("Please enter desc.");
                } else {

                    *//*  senderRegistrationRequest.put("UserID", UserDetails.UserId);
            senderRegistrationRequest.put("OperatorID", params[index++]);
            senderRegistrationRequest.put("Amount", params[index++]);
            senderRegistrationRequest.put("RechargeID", params[index++]);
            senderRegistrationRequest.put("Description", params[index++]);
            senderRegistrationRequest.put("Subject", params[index++]);
            senderRegistrationRequest.put("CustomerNumber", params[index++]);*//*
                    String amount;
                    amount = txtAmount.getText().toString();
                    String[] request = {"0", amount, "0", txtDesc.getText().toString(), bank + " " + spSub.getText().toString(), txtSenderNo.getText().toString()};
                    runComplaint(request);
                }
            }

        });
    }*/

   /* @Override
    public void getSuccessMessageComplaint(String body) {
        dialog.dismiss();
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(body)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void getMoneyTrErrorMessage(String body) {
        dialog.dismiss();
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(body)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();

    }*/


 /*   private void runComplaint(String[] request) {
        MoneyTransferComplaintAsync complaint = new MoneyTransferComplaintAsync();
        complaint.delegate = this;
        complaint.execute(request);
    }*/

    @SuppressLint("NewApi")
    private void transferMoney(final String SenderNumber, final String BeneficiaryName, final String BankName, final String AccountNo, final Double Amount, final String TxnNo, final String Date, final String SenderName, final Double Charges, final String mode, final String IFSC) {

        dialog = new Dialog(context, R.style.Theme_AppCompat_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(R.layout.moneytransfer_receipt_report);
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
        dialog.setTitle("Money Transfer");
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please Wait....");
        pDialog.setIndeterminate(true);
        pDialog.setIndeterminateDrawable(context.getDrawable(R.mipmap.ic_launcher));
        pDialog.setCancelable(false);
        pDialog.dismiss();
        dialog.show();
        edtAmount = (EditText) dialog.findViewById(R.id.edtAmount);
        TextView txtTransferDate = (TextView) dialog.findViewById(R.id.txtTransferDate);
        TextView txtSenderNumber = (TextView) dialog.findViewById(R.id.txtSenderNumber);
        TextView txtBeneficiaryName = (TextView) dialog.findViewById(R.id.txtBeneficiaryName);
        TextView txtAmount = (TextView) dialog.findViewById(R.id.txtAmount);
        TextView txtCharges = (TextView) dialog.findViewById(R.id.txtCharges);
        TextView txtAccountNo = (TextView) dialog.findViewById(R.id.txtAccountNo);
        TextView txtBankName = (TextView) dialog.findViewById(R.id.txtBankName);
        TextView txtTransactionID = (TextView) dialog.findViewById(R.id.txtTransactionID);
        TextView txtBankIFSC = (TextView) dialog.findViewById(R.id.txtBankIFSC);
        TextView txtMode = (TextView) dialog.findViewById(R.id.txtMode);
        /*txtTransferDate,txtSenderNumber,txtBeneficiaryName,txtAmount,txtAccountNo,txtTransactionID*/
        TextView txtSenderName = (TextView) dialog.findViewById(R.id.txtSenderReName);
        txtSenderNumber.setText(SenderNumber);
        txtBeneficiaryName.setText(BeneficiaryName);
        double amt = Amount;
        String amount = String.valueOf(amt);
        txtAmount.setText(amount);
        txtTransactionID.setText(TxnNo);
        txtAccountNo.setText(AccountNo);
        txtBankName.setText(BankName);
        txtTransferDate.setText(Date);
        txtSenderName.setText(SenderName);
        txtBankIFSC.setText(IFSC);
        double charges = Charges;
        String Charge = String.valueOf(charges);
        txtCharges.setText(Charge);
        txtMode.setText(mode);
    }

    private void runMoneyTransferAsync(String[] request) {
        MoneyTransferAsync moneyTransfer = new MoneyTransferAsync();
        moneyTransfer.delegate = this;
        moneyTransfer.execute(request);
    }

    public String getType() {
        return Type;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void getSuccessMessageTransfer(String body) {
        String Response = "";
        try {
            Response = data.Decrypt(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        proressdialogDissmiss();
        DialogBoxDissmiss();
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(Response)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    private void progressBarShow() {
        if (pDialog == null) {
            pDialog.show();
        }
    }

    private void DialogBoxDissmiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void proressdialogDissmiss() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    @Override
    public void getErrorMessage(String body) {
        String Response = "";

        try {
            Response = data.Decrypt(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        proressdialogDissmiss();
        DialogBoxDissmiss();
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(Response)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void getOtpForDeletBen(String body) {

    }

    @Override
    public void getRequestForOtp(String body) {

    }

    @Override
    public void getStattusErrorBody(String body) {
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Message")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(body)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void getMoneyTrStatusResponseSuccessBody(String body) {


        String Status = "";
        Status = String.valueOf(Html.fromHtml(Html.fromHtml(body).toString()));
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Message")
                .setCancelText("Cancel")
                .setCustomImage(R.mipmap.ic_launcher)
                .setContentText(Status)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtSenderNumber, txtBeneficiaryName, txtBankName, txtAccountNumber, txtAmount, txtTxnNo, txtClosingBal, txtSender,
                txtStatus, txtDate, txtReceipt, txtOpeningBalance, txtCharges, txtComplaintView, txtRemark, txtSenderMobileNumber, txtIFSC, txtMode;
        public Button btnRefund;
        GradientDrawable statusBackgrounds;
/*[{"SenderNumber":"9595215859","BeneficiaryName":"Naveed","BankName":"HDFC","AccountNo":"13","Amount":"10","TxnNo":"123456","ClosingBal":"1000","Date":"10/10/2017","Status":"Success"}]*/

        public MyViewHolder(View rowView) {
            super(rowView);
            txtDate = (TextView) rowView.findViewById(R.id.txtDate);
            txtSenderNumber = (TextView) rowView.findViewById(R.id.txtSenderNumber);
            txtBeneficiaryName = (TextView) rowView.findViewById(R.id.txtBeneficiaryName);
            txtBankName = (TextView) rowView.findViewById(R.id.txtBankName);
            txtAccountNumber = (TextView) rowView.findViewById(R.id.txtAccountNumber);
            txtAmount = (TextView) rowView.findViewById(R.id.txtAmount);
            txtCharges = (TextView) rowView.findViewById(R.id.txtCharges);
            txtRemark = (TextView) rowView.findViewById(R.id.txtRemark);
            txtSenderMobileNumber = (TextView) rowView.findViewById(R.id.txtSenderMobileNumber);
            txtTxnNo = (TextView) rowView.findViewById(R.id.txtTxnNo);
            txtClosingBal = (TextView) rowView.findViewById(R.id.txtClosingBal);
            txtStatus = (TextView) rowView.findViewById(R.id.txtStatus);
            // btnRefund = (Button) rowView.findViewById(R.id.btnRefund);
            statusBackgrounds = (GradientDrawable) txtStatus.getBackground();
            txtReceipt = (TextView) rowView.findViewById(R.id.txtReceipt);
            txtComplaintView = (TextView) rowView.findViewById(R.id.txtComplaintView);
            txtOpeningBalance = (TextView) rowView.findViewById(R.id.txtOpeningBalance);
            txtSender = (TextView) rowView.findViewById(R.id.txtSender);
            txtMode = (TextView) rowView.findViewById(R.id.txtMode);
            txtIFSC = (TextView) rowView.findViewById(R.id.txtIFSC);


        }
    }
}