package in.omkarpayment.app.cashCollection.cashCollection;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import es.dmoral.toasty.Toasty;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.AutocompleteTextAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

import java.util.ArrayList;



import in.omkarpayment.app.databinding.ActivityCashCollectionBinding;

public class CashCollectionActivity extends AppCompatActivity implements ICashCollectionView {

    ActivityCashCollectionBinding cashCollectionBinding;
    CustomProgressDialog progressDialog;
    AlertImpl alert;
    Context context = this;
    String receiverID = "", parentID, receiverName = "";
    ICashCollectionPresenter iCashCollectionPresenter;
    TextView txtToolbarBalance;
    NetworkState ns = new NetworkState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cashCollectionBinding = DataBindingUtil.setContentView(this, R.layout.activity_cash_collection);
        progressDialog = new CustomProgressDialog(this);
        alert = new AlertImpl(this);
        iCashCollectionPresenter = new CashCollectionPresenter(this);

        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
        } else {
            iCashCollectionPresenter.getDownLineList();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Cash Collection";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        //TextView txtHeader = findViewById(R.id.txtHeader);
      //  txtHeader.setText(title);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        cashCollectionBinding.editMemberId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                BalTransDownlineListPojo op = (BalTransDownlineListPojo) parent.getItemAtPosition(pos);
                receiverName = String.valueOf(op.getUserName());
                receiverID = String.valueOf(op.getUserID().toString());
                parentID = String.valueOf(op.getParentID());
            }
        });

        cashCollectionBinding.btnCollectCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toasty.error(context, AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                iCashCollectionPresenter.validateCashCollection();
            }
        });

        /*txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));*/
    }

    @Override
    public void getCashCollectionDownLine(ArrayList<BalTransDownlineListPojo> downlineList) {
        AutocompleteTextAdapter adapter;
        adapter = new AutocompleteTextAdapter(context, R.layout.adapter_autocomplete_text, downlineList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cashCollectionBinding.editMemberId.setAdapter(adapter);
        cashCollectionBinding.editMemberId.setThreshold(1);
    }

    @Override
    public void errorToast(String message) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public String getDownLineMember() {
        return receiverName;
    }

    @Override
    public void editMemberIdError() {
        alert.errorAlert(String.format("Please select member ID"));
        cashCollectionBinding.editMemberId.requestFocus();
    }

    @Override
    public String getReceiverID() {
        return receiverID;
    }

    @Override
    public void editAmountError() {
        cashCollectionBinding.editAmonut.setError("Please enter amount");
        cashCollectionBinding.editAmonut.requestFocus();
    }

    @Override
    public String getRemark() {
        return cashCollectionBinding.editRemark.getText().toString().trim();
    }

    @Override
    public void editRemarkError() {
        cashCollectionBinding.editRemark.setError("Please enter remark");
        cashCollectionBinding.editRemark.requestFocus();
    }

    @Override
    public void confirmClick() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Name : %s\nMember ID: %s\nAmount: %s", getDownLineMember(), getReceiverID(), getAmount()));
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        Button btnNo = dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toasty.error(context, AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                iCashCollectionPresenter.cashCollection();
                progressDialog.showPDialog();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public String getAmount() {
        return cashCollectionBinding.editAmonut.getText().toString().trim();
    }

    @Override
    public void errorAlert(String errorMsg) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        alert.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        clear();
        alert.successAlert(msg);
    }

    private void clear() {
        receiverID = "";
        parentID = "";
        receiverName = "";
        cashCollectionBinding.editMemberId.setText("");
        cashCollectionBinding.editAmonut.setText("");
        cashCollectionBinding.editRemark.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
             finish(); // close this activity and return to preview activity (if there is any)
            //overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

}
