package in.omkarpayment.app.cashCollection.cashCollection;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import in.omkarpayment.app.cashCollection.cashCollectionModel.CashCollectionModel;
import in.omkarpayment.app.cashCollection.cashCollectionModel.ICashCollectionModel;
import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CashCollectionPresenter implements ICashCollectionPresenter, ICashCollectionModel {

    ICashCollectionView iCashCollectionView;

    public CashCollectionPresenter(ICashCollectionView iCashCollectionView) {
        this.iCashCollectionView = iCashCollectionView;
    }


    @Override
    public void errorAlert(String message) {
        iCashCollectionView.errorAlert(message);
    }

    @Override
    public void getDownLineList() {
        String keyData = new KeyDataReader().get();
        CashCollectionModel model = new CashCollectionModel();
        model.delegateCashCollectionModel = this;
        model.DownLineListWebService("", keyData);
    }

    @Override
    public void downLineListResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            String body = data.Decrypt(response);
            Log.i("Downline :", body);
            ArrayList<BalTransDownlineListPojo> downlineList = gson.fromJson(body, new TypeToken<ArrayList<BalTransDownlineListPojo>>() {
            }.getType());

            if (downlineList != null) {
                iCashCollectionView.getCashCollectionDownLine(downlineList);
                return;
            }
            iCashCollectionView.errorToast(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downLineListFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {

            String body = data.Decrypt(response);
            iCashCollectionView.errorToast(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void validateCashCollection() {

        Validation validation = new Validation();
        if (!validation.isNullOrEmpty(iCashCollectionView.getDownLineMember())) {
            iCashCollectionView.editMemberIdError();
            return;
        }

        if (iCashCollectionView.getDownLineMember().toLowerCase().equals("Select User".toLowerCase())) {
            iCashCollectionView.editMemberIdError();
            return;
        }

        if (!validation.isNullOrEmpty(iCashCollectionView.getReceiverID())) {
            iCashCollectionView.editMemberIdError();
            return;
        }

        if (!validation.isNullOrEmpty(iCashCollectionView.getAmount())) {
            iCashCollectionView.editAmountError();
            return;
        }

        if (!validation.isNullOrEmpty(iCashCollectionView.getRemark())) {
            iCashCollectionView.editRemarkError();
            return;
        }

        iCashCollectionView.confirmClick();
    }

    @Override
    public void cashCollection() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject cashCollectionRequest = new JSONObject();
        try {
            cashCollectionRequest.put("SenderID", UserDetails.UserId);
            cashCollectionRequest.put("ReceiverID", iCashCollectionView.getReceiverID());
            cashCollectionRequest.put("Amount", iCashCollectionView.getAmount());
            cashCollectionRequest.put("Remark", iCashCollectionView.getRemark());

            String jsonRequest = cashCollectionRequest.toString();
            Log.i("cash collection req", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCashCollectionView.errorToast("Unable to fetch/receive data");
                return;
            }

            CashCollectionModel model = new CashCollectionModel();
            model.delegateCashCollection = this;
            model.CashCollectionWebService(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void CashCollectionResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();

        try {
            Log.i("cash collection respo", response);
            String body = data.Decrypt(response);
            Log.i("cash collection respo", body);

            if (!body.toLowerCase().contains("{\"Message\":\"".toLowerCase())) {
                iCashCollectionView.successAlert(body);
                return;
            }
            Gson gson = new Gson();
            CashCollectionPojo collectionPojo = gson.fromJson(body, CashCollectionPojo.class);
            if (collectionPojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                iCashCollectionView.successAlert(collectionPojo.getMessage());
                return;
            }

            iCashCollectionView.successAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void CashCollectionFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();

        try {
            Log.i("cash collection respo", response);
            String body = data.Decrypt(response);
            Log.i("cash collection respo", body);
            iCashCollectionView.errorAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
