package in.omkarpayment.app.cashCollection.cashCollectionModel;

public interface ICashCollectionModel {

    void errorAlert(String response);

    void downLineListResponse(String response);

    void downLineListFailResponse(String response);

}
