package in.omkarpayment.app.cashCollection.cashCollection;

public interface ICashCollectionPresenter {

    void errorAlert(String message);

    void getDownLineList();

    void validateCashCollection();

    void cashCollection();

    void CashCollectionResponse(String response);

    void CashCollectionFailResponse(String response);

}
