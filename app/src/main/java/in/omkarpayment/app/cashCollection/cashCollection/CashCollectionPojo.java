package in.omkarpayment.app.cashCollection.cashCollection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashCollectionPojo {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Balance")
    @Expose
    private String balance;
    @SerializedName("DMRBalance")
    @Expose
    private String dMRBalance;
    @SerializedName("BalanceType")
    @Expose
    private String balanceType;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDMRBalance() {
        return dMRBalance;
    }

    public void setDMRBalance(String dMRBalance) {
        this.dMRBalance = dMRBalance;
    }

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
