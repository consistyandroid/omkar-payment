package in.omkarpayment.app.cashCollection.cashCollectionReport;

import android.app.DatePickerDialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.adapter.AutocompleteTextAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.omkarpayment.app.R;
import in.omkarpayment.app.databinding.ActivityCashCollectionReportBinding;

public class CashCollectionReportActivity extends AppCompatActivity implements ICashCollectionReportView {

    ActivityCashCollectionReportBinding cashCollectionReportBinding;
    String strFromDate = "", strToDate = "";
    CashCollectionReportAdapter adapter;
    AlertImpl alert;
    TextView txtToolbarBalance;
    String receiverID = "", parentID, receiverName = "";
    CustomProgressDialog progressDialog;
    ICashCollectionReportPresenter iCashCollectionReportPresenter;
    Context context = this;
    NetworkState ns = new NetworkState();
    private int mYear, mMonth, mDay, fDate, tDate, fMonth, tMonth, fYear, tYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cashCollectionReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_cash_collection_report);
        progressDialog = new CustomProgressDialog(this);
        iCashCollectionReportPresenter = new CashCollectionReportPresenter(this);
        //txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
       // txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Cash Collection Report";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
       /* TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);*/
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        cashCollectionReportBinding.listView.setLayoutManager(mLayoutManager);
        cashCollectionReportBinding.listView.setItemAnimator(new DefaultItemAnimator());
        alert = new AlertImpl(this);

        setDateDefaultValue();

        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
        } else {
            iCashCollectionReportPresenter.getDownLineList();
        }

        if (UserDetails.UserType.equals(UserType.Retailer) ||
                UserDetails.UserType.equals(UserType.APIUSER)) {
            receiverName = UserDetails.Username;
            receiverID = String.valueOf(UserDetails.UserId);
            parentID = String.valueOf(UserDetails.ParentId);
            cashCollectionReportBinding.layoutMemberID.setVisibility(View.GONE);
        }

        // new custom Date Picker dialog for API level lower than API 23
        cashCollectionReportBinding.txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(CashCollectionReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        tDate = selectedday;
                        tMonth = selectedmonth + 1;
                        tYear = selectedyear;
                        String dateSelected = padLeftZero(selectedday + "") + "/" + padLeftZero((selectedmonth + 1) + "") + "/" + selectedyear;
                        cashCollectionReportBinding.txtToDate.setText(dateSelected);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.show();
            }
        });

        cashCollectionReportBinding.txtFromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(CashCollectionReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        fDate = selectedday;
                        fMonth = selectedmonth + 1;
                        fYear = selectedyear;
                        String dateSelected = padLeftZero(selectedday + "") + "/" + padLeftZero((selectedmonth + 1) + "") + "/" + selectedyear;
                        cashCollectionReportBinding.txtFromDate.setText(dateSelected);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.show();
            }

        });

        cashCollectionReportBinding.editMemberId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                BalTransDownlineListPojo op = (BalTransDownlineListPojo) parent.getItemAtPosition(pos);
                receiverName = String.valueOf(op.getUserName());
                receiverID = String.valueOf(op.getUserID().toString());
                parentID = String.valueOf(op.getParentID());
            }
        });

        cashCollectionReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (fYear > tYear) {
                    alert.errorAlert("Please select valid from date");
                    return;
                } else {
                    if (fMonth > tMonth) {
                        alert.errorAlert("Please select valid from date");
                        return;
                    } else {
                        if (fMonth == tMonth) {
                            if (fDate > tDate) {
                                alert.errorAlert("Please select valid from date");
                                return;
                            }
                        }
                    }
                }

                iCashCollectionReportPresenter.validateDate();
                cashCollectionReportBinding.listView.setAdapter(null);
            }
        });

    }

    public String padLeftZero(String input) {
        String outPut = String.format("%2s", input).replace(' ', '0');
        return outPut;
    }

    @Override
    public void getCashCollectionDownLine(ArrayList<BalTransDownlineListPojo> downlineList) {
        AutocompleteTextAdapter adapter;
        downlineList.get(0).setUserName("-- All --");
        adapter = new AutocompleteTextAdapter(context, R.layout.adapter_autocomplete_text, downlineList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cashCollectionReportBinding.editMemberId.setAdapter(adapter);
        cashCollectionReportBinding.editMemberId.setThreshold(1);
    }

    @Override
    public void errorToast(String message) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public String getDownLineMember() {
        return receiverName;
    }

    @Override
    public void editMemberIdError() {
        alert.errorAlert(String.format("Please select member from downline"));
        cashCollectionReportBinding.editMemberId.requestFocus();
    }

    @Override
    public String getReceiverID() {
        return receiverID;
    }

    @Override
    public String strFromDate() {
        return cashCollectionReportBinding.txtFromDate.getText().toString();
    }

    @Override
    public String strToDate() {
        return cashCollectionReportBinding.txtToDate.getText().toString();
    }

    @Override
    public void editFromDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "From Date"));
        cashCollectionReportBinding.txtFromDate.requestFocus();
    }

    @Override
    public void editToDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "To Date"));
        cashCollectionReportBinding.txtToDate.requestFocus();
    }

    @Override
    public void getCashCollectionReport() {
        progressDialog.showPDialog();
        iCashCollectionReportPresenter.getCashCollectionReport();
    }

    @Override
    public void errorAlert(String msg) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }

        if (msg.toLowerCase().contains("Data not avai".toLowerCase())) {
            cashCollectionReportBinding.listView.setAdapter(null);
            cashCollectionReportBinding.listView.setVisibility(View.GONE);
            cashCollectionReportBinding.imgDataNotFound.setVisibility(View.VISIBLE);
            return;
        }
        alert.errorAlert(msg);
    }

   /* @Override
    public void successAlert(String sttus) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        alert.successAlert(sttus);
    }*/


    @Override
    public void getUserWiseCashColletionReport(ArrayList<CashCollectionReportPojo> downlineList) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        cashCollectionReportBinding.listView.setAdapter(null);
        cashCollectionReportBinding.listView.setVisibility(View.VISIBLE);
        cashCollectionReportBinding.imgDataNotFound.setVisibility(View.GONE);
        adapter = new CashCollectionReportAdapter(CashCollectionReportActivity.this, context, downlineList);
        cashCollectionReportBinding.listView.setAdapter(adapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
             finish(); // close this activity and return to preview activity (if there is any)
            //overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;

        String[] splitDate = date.split("/");
        fDate = Integer.parseInt(splitDate[0]);
        tDate = Integer.parseInt(splitDate[0]);
        fMonth = Integer.parseInt(splitDate[1]);
        tMonth = Integer.parseInt(splitDate[1]);
        fYear = Integer.parseInt(splitDate[2]);
        tYear = Integer.parseInt(splitDate[2]);

        cashCollectionReportBinding.txtToDate.setText(date);
        cashCollectionReportBinding.txtToDate.setError(null);

        cashCollectionReportBinding.txtFromDate.setText(date);
        cashCollectionReportBinding.txtFromDate.setError(null);

    }
}
