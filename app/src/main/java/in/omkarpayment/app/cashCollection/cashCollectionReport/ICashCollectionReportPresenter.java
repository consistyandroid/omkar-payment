package in.omkarpayment.app.cashCollection.cashCollectionReport;

public interface ICashCollectionReportPresenter {

    void getDownLineList();

    void validateDate();

    void getCashCollectionReport();

    void cashCollectionReportResponse(String response);

    void cashCollectionReportFailResponse(String response);

}
