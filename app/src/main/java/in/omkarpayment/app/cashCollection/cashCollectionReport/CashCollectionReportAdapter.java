package in.omkarpayment.app.cashCollection.cashCollectionReport;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.omkarpayment.app.R;


public class CashCollectionReportAdapter extends RecyclerView.Adapter<CashCollectionReportAdapter.MyViewHolder> {
    public static int i;
    //Button btnDispute;
    ProgressDialog pDialog;
    Context context;
    private List<CashCollectionReportPojo> list;
    CashCollectionReportActivity cashCollectionReportActivity;

    public CashCollectionReportAdapter(CashCollectionReportActivity cashCollectionReportActivity, Context context, List<CashCollectionReportPojo> list) {
        this.cashCollectionReportActivity = cashCollectionReportActivity;
        this.context = context;
        this.list = list;
    }

    @Override
    public CashCollectionReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_cash_collection_report, parent, false);

        return new CashCollectionReportAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CashCollectionReportAdapter.MyViewHolder holder, int position) {
        final CashCollectionReportPojo pojo = list.get(position);

        holder.txtDate.setText(pojo.getDOC());
        holder.txtCollectedFrom.setText(pojo.getUserName());
        holder.txtAmount.setText(String.valueOf(pojo.getAmount()));
        holder.txtRemark.setText(pojo.getDescription());
        holder.txtStatus.setText(pojo.getCreditDebit());

        try {
            holder.txtClosingBalance.setText(" ₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getClosingBalance()))));
            holder.txtOpeningBalance.setText(" ₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getOpeningBalance()))));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (pojo.getCreditDebit().equalsIgnoreCase("CREDIT")) {

                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
                holder.txtAmount.setTextColor(context.getResources().getColor(R.color.red));
                holder.txtRupeeSymbol.setTextColor(context.getResources().getColor(R.color.red));
                return;
            }

            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.green));
            holder.txtAmount.setTextColor(context.getResources().getColor(R.color.green));
            holder.txtRupeeSymbol.setTextColor(context.getResources().getColor(R.color.green));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtStatus, txtDate, txtCollectedFrom, txtAmount, txtOpeningBalance,
                txtClosingBalance, txtRemark, txtRupeeSymbol;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtDate = rowView.findViewById(R.id.txtDate);
            txtStatus = rowView.findViewById(R.id.txtStatus);
            txtCollectedFrom = rowView.findViewById(R.id.txtCollectedFrom);
            txtAmount = rowView.findViewById(R.id.txtAmount);
            txtRupeeSymbol = rowView.findViewById(R.id.txtRupeeSymbol);
            txtRemark = rowView.findViewById(R.id.txtRemark);
            txtClosingBalance = rowView.findViewById(R.id.txtClosingBalance);
            txtOpeningBalance = rowView.findViewById(R.id.txtOpeningBalance);
        }
    }
}
