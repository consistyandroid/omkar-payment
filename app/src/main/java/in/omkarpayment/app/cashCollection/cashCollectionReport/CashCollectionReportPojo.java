package in.omkarpayment.app.cashCollection.cashCollectionReport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashCollectionReportPojo {

    @SerializedName("ReceiverID")
    @Expose
    private Integer receiverID;
    @SerializedName("SenderID")
    @Expose
    private Integer senderID;
    @SerializedName("UserType")
    @Expose
    private String userType;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("ShopeName")
    @Expose
    private String shopeName;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("ToDate")
    @Expose
    private String toDate;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("OpeningBalance")
    @Expose
    private Double openingBalance;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("ClosingBalance")
    @Expose
    private Double closingBalance;
    @SerializedName("CreditDebit")
    @Expose
    private String creditDebit;
    @SerializedName("Description")
    @Expose
    private String description;

    public Integer getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(Integer receiverID) {
        this.receiverID = receiverID;
    }

    public Integer getSenderID() {
        return senderID;
    }

    public void setSenderID(Integer senderID) {
        this.senderID = senderID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getShopeName() {
        return shopeName;
    }

    public void setShopeName(String shopeName) {
        this.shopeName = shopeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public Double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(Double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getCreditDebit() {
        return creditDebit;
    }

    public void setCreditDebit(String creditDebit) {
        this.creditDebit = creditDebit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
