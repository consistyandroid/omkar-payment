package in.omkarpayment.app.cashCollection.cashCollectionModel;

import in.omkarpayment.app.cashCollection.cashCollection.CashCollectionPresenter;
import in.omkarpayment.app.cashCollection.cashCollectionReport.CashCollectionReportPresenter;
import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.IWebServicesModel;
import in.omkarpayment.app.userContent.AllMessages;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CashCollectionModel {

    public ICashCollectionModel delegateCashCollectionModel = null;
    public CashCollectionPresenter delegateCashCollection = null;
    public CashCollectionReportPresenter delegateCashCollectionReport = null;

    public void DownLineListWebService(String encryptString, String keyData) {

        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.DownLineForDropWebservice(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        delegateCashCollectionModel.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        delegateCashCollectionModel.downLineListResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        delegateCashCollectionModel.downLineListFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        delegateCashCollectionModel.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                delegateCashCollectionModel.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void CashCollectionWebService(String encryptString, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.CashCollectionWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        delegateCashCollection.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        delegateCashCollection.CashCollectionResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        delegateCashCollection.CashCollectionFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        delegateCashCollection.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                delegateCashCollection.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void CashCollectionReportWebService(String encryptString, String keyData) {
        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.CashCollectionReportWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        delegateCashCollectionReport.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        delegateCashCollectionReport.cashCollectionReportResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        delegateCashCollectionReport.cashCollectionReportFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        delegateCashCollectionReport.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                delegateCashCollectionReport.errorAlert(AllMessages.internetError);
            }
        });
    }

}
