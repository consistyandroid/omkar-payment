package in.omkarpayment.app.cashCollection.cashCollectionReport;



import java.util.ArrayList;

import in.omkarpayment.app.json.BalTransDownlineListPojo;

public interface ICashCollectionReportView {

    void errorAlert(String message);

    void getCashCollectionDownLine(ArrayList<BalTransDownlineListPojo> downlineList);

    void errorToast(String body);

    String getDownLineMember();

    void editMemberIdError();

    String strFromDate();

    void editFromDateError();

    String strToDate();

    void editToDateError();

    void getCashCollectionReport();

    String getReceiverID();

    void getUserWiseCashColletionReport(ArrayList<CashCollectionReportPojo> dayreport);

}
