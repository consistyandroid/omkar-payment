package in.omkarpayment.app.cashCollection.cashCollection;



import java.util.ArrayList;

import in.omkarpayment.app.json.BalTransDownlineListPojo;

public interface ICashCollectionView {

    void errorAlert(String message);

    void getCashCollectionDownLine(ArrayList<BalTransDownlineListPojo> downLineList);

    void errorToast(String message);

    String getDownLineMember();

    void editMemberIdError();

    String getReceiverID();

    String getAmount();

    void editAmountError();

    String getRemark();

    void editRemarkError();

    void confirmClick();

    void successAlert(String message);

}
