package in.omkarpayment.app.cashCollection.cashCollectionReport;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import in.omkarpayment.app.cashCollection.cashCollectionModel.CashCollectionModel;
import in.omkarpayment.app.cashCollection.cashCollectionModel.ICashCollectionModel;
import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.userContent.Validation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CashCollectionReportPresenter implements ICashCollectionReportPresenter, ICashCollectionModel {

    ICashCollectionReportView iCashCollectionReportView;

    public CashCollectionReportPresenter(ICashCollectionReportView iCashCollectionReportView) {
        this.iCashCollectionReportView = iCashCollectionReportView;
    }

    @Override
    public void errorAlert(String message) {
        iCashCollectionReportView.errorAlert(message);
    }

    @Override
    public void getDownLineList() {
        String keyData = new KeyDataReader().get();
        CashCollectionModel model = new CashCollectionModel();
        model.delegateCashCollectionModel = this;
        model.DownLineListWebService("", keyData);
    }

    @Override
    public void validateDate() {

        Validation validation = new Validation();
        if (!validation.isNullOrEmpty(iCashCollectionReportView.getDownLineMember())) {
            iCashCollectionReportView.editMemberIdError();
            return;
        }

        if (!validation.isNullOrEmpty(iCashCollectionReportView.strFromDate())) {
            iCashCollectionReportView.editFromDateError();
            return;
        }

        if (!validation.isNullOrEmpty(iCashCollectionReportView.strToDate())) {
            iCashCollectionReportView.editToDateError();
            return;
        }

        iCashCollectionReportView.getCashCollectionReport();
    }

    @Override
    public void getCashCollectionReport() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject cashCollectionRequest = new JSONObject();
        try {
            cashCollectionRequest.put("FromDate", iCashCollectionReportView.strFromDate().trim());
            cashCollectionRequest.put("ToDate", iCashCollectionReportView.strToDate().trim());
            cashCollectionRequest.put("ReceiverID", iCashCollectionReportView.getReceiverID());

            String jsonRequest = cashCollectionRequest.toString();
            Log.i("cash collection req", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCashCollectionReportView.errorToast("Unable to fetch/receive data");
                return;
            }

            CashCollectionModel model = new CashCollectionModel();
            model.delegateCashCollectionReport = this;
            model.CashCollectionReportWebService(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cashCollectionReportResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            String body = data.Decrypt(response);
            Log.i("cash coll. report", body);

            ArrayList<CashCollectionReportPojo> cashReport = gson.fromJson(body, new TypeToken<ArrayList<CashCollectionReportPojo>>() {
            }.getType());

            if (cashReport != null) {
                iCashCollectionReportView.getUserWiseCashColletionReport(cashReport);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cashCollectionReportFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            Log.i("Decrept response", body);
            iCashCollectionReportView.errorAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downLineListResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            String body = data.Decrypt(response);
            Log.i("Downline :", body);
            ArrayList<BalTransDownlineListPojo> downlineList = gson.fromJson(body, new TypeToken<ArrayList<BalTransDownlineListPojo>>() {
            }.getType());

            if (downlineList != null) {
                iCashCollectionReportView.getCashCollectionDownLine(downlineList);
                return;
            }
            iCashCollectionReportView.errorToast(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downLineListFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {

            String body = data.Decrypt(response);
            iCashCollectionReportView.errorToast(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
