package in.omkarpayment.app.stockRequest;

import android.app.Dialog;
import android.content.Context;

import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityStockRequestBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.SpinnerData;
import in.omkarpayment.app.userContent.UserDetails;

public class StockRequestActivity extends AppCompatActivity implements IStockRequestView {
    ActivityStockRequestBinding stockRequestBinding;
    ArrayAdapter<String> spAdapter;
    ArrayAdapter<String> spAdapterBank;
    ArrayAdapter<String> spRequestBalanceTypeAdapter;
    String payMode, parentId, parentName, BankName, BalanceRequestType;
    AlertImpl alert;
    Context context= this;
    CustomProgressDialog pDialog;
    IStockRequestPresenter iStockRequestPresenter;
    LinearLayout layoutBankDetails;
    TextView txtBankDetails;
    NetworkState ns = new NetworkState();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stockRequestBinding = DataBindingUtil.setContentView(StockRequestActivity.this, R.layout.activity_stock_request);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Stock Request";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        layoutBankDetails = (LinearLayout) findViewById(R.id.layoutBankDetails);
        layoutBankDetails.setVisibility(View.GONE);

        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);

        parentId = String.valueOf(UserDetails.ParentId);
        parentName = UserDetails.ParentName;
        stockRequestBinding.txtRequestTo.setText("(" + parentId + ")" + " -" + parentName);

        iStockRequestPresenter = new StockRequestPresenter(this);

        spAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SpinnerData.PayMode);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stockRequestBinding.spPayMode.setAdapter(spAdapter);

        spRequestBalanceTypeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.RequestBalanceType));
        spRequestBalanceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stockRequestBinding.spRequestBalanceType.setAdapter(spRequestBalanceTypeAdapter);
        stockRequestBinding.spRequestBalanceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                BalanceRequestType = getResources().getStringArray(R.array.RequestBalanceType)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
        spAdapterBank = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.Bank));
        spAdapterBank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stockRequestBinding.editBankName.setAdapter(spAdapterBank);
        stockRequestBinding.editBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                BankName = getResources().getStringArray(R.array.Bank)[position];
                if (position == 0) {
                    stockRequestBinding.txtBankDetails.setText("");
                    stockRequestBinding.txtBankDetails.setText(getResources().getText(R.string.PNB));
                } else if (position == 1) {
                    stockRequestBinding.txtBankDetails.setText("");
                    stockRequestBinding.txtBankDetails.setText(getResources().getText(R.string.SBI));
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
        stockRequestBinding.editAmonut.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        stockRequestBinding.spPayMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                payMode = SpinnerData.PayMode[position];

                if (stockRequestBinding.spPayMode.getSelectedItem().equals("Cash")) {
                    stockRequestBinding.editRemark.setVisibility(View.VISIBLE);
                    stockRequestBinding.editUTRNumber.setVisibility(View.GONE);
                    stockRequestBinding.editBankName.setVisibility(View.GONE);
                    layoutBankDetails.setVisibility(View.GONE);
                } else if (stockRequestBinding.spPayMode.getSelectedItem().equals("Bank Transfer")) {
                    stockRequestBinding.editRemark.setVisibility(View.VISIBLE);
                    stockRequestBinding.editUTRNumber.setVisibility(View.VISIBLE);
                    stockRequestBinding.editBankName.setVisibility(View.VISIBLE);
                    layoutBankDetails.setVisibility(View.VISIBLE);
                } else if (stockRequestBinding.spPayMode.getSelectedItem().equals("Cash Deposit in Bank")) {
                    stockRequestBinding.editRemark.setVisibility(View.VISIBLE);
                    stockRequestBinding.editUTRNumber.setVisibility(View.GONE);
                    stockRequestBinding.editBankName.setVisibility(View.VISIBLE);
                    layoutBankDetails.setVisibility(View.VISIBLE);
                } /*else if (stockRequestBinding.spPayMode.getSelectedItem().equals("Credit")) {
                    stockRequestBinding.editRemark.setVisibility(View.GONE);
                    stockRequestBinding.editUTRNumber.setVisibility(View.GONE);
                    stockRequestBinding.editBankName.setVisibility(View.GONE);
                    layoutBankDetails.setVisibility(View.GONE);
                }*/
            }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }


        });

        stockRequestBinding.btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(context)) {
                    iStockRequestPresenter.validateRequest();
                } else {
                    Toast.makeText(StockRequestActivity.this, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
             finish(); // close this activity and return to preview activity (if there is any)
           /* Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            this.overridePendingTransition(R.anim.animation_leave,
                    R.anim.animation_enter);*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
        clear();
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
        clear();
    }

    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public String Amount() {
        return stockRequestBinding.editAmonut.getText().toString();
    }

    @Override
    public String UTRNumber() {
        return stockRequestBinding.editUTRNumber.getText().toString();
    }

    @Override
    public String Remark() {
        return stockRequestBinding.editRemark.getText().toString();
    }

    @Override
    public String BankName() {
        return BankName;
    }

    @Override
    public String RequestBalanceType() {
        return BalanceRequestType;
    }

    @Override
    public void editAmountError() {
        stockRequestBinding.editAmonut.setError(String.format(AllMessages.pleaseEnter, "Amount"));
        stockRequestBinding.editAmonut.requestFocus();
    }

    @Override
    public void editRemarkError() {
        stockRequestBinding.editRemark.setError(String.format(AllMessages.pleaseEnter, "Remark"));
        stockRequestBinding.editRemark.requestFocus();
    }

    @Override
    public void editBankNameError() {
        /*stockRequestBinding.editBankName.setError(String.format(AllMessages.pleaseEnter, "Bank Name"));
        stockRequestBinding.editBankName.requestFocus();*/
    }

    @Override
    public void editUTRError() {
        stockRequestBinding.editUTRNumber.setError(String.format(AllMessages.pleaseEnter, "UTR Number"));
        stockRequestBinding.editUTRNumber.requestFocus();
    }

    @Override
    public String ParentId() {
        return parentId;
    }

    @Override
    public String spPayMode() {
        return payMode;
    }

    @Override
    public void clear() {
        stockRequestBinding.editUTRNumber.setText("");
        stockRequestBinding.editRemark.setText("");
        stockRequestBinding.editAmonut.setText("");

    }

    @Override
    public void confirmClick() {

        final Dialog dialog = new Dialog(StockRequestActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Number: %s\nAmount: %s", parentId, Amount()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iStockRequestPresenter.sendRequest();
                dialog.dismiss();
                showPDialog();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
