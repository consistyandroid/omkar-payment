package in.omkarpayment.app.stockRequest;

/**
 * Created by Consisty on 16-Aug-17.
 */

public interface IStockRequestPresenter {

    void errorAlert(String msg);

    void successAlert(String msg);

    void showPDialog();

    void dismissPDialog();

    void validateRequest();

    void sendRequest();

    boolean validation();

    void StockReqResponseResponse(String encryptedResponse);

    void StockReqResponseFailResponse(String encryptedResponse);
}
