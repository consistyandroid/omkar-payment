package in.omkarpayment.app.stockRequest;

import org.json.JSONException;
import org.json.JSONObject;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by Consisty on 16-Aug-17.
 */

public class StockRequestPresenter implements IStockRequestPresenter {
    IStockRequestView iStockRequestView;
    LogWriter log = new LogWriter();

    public StockRequestPresenter(IStockRequestView iStockRequestView) {
        this.iStockRequestView = iStockRequestView;

    }

    @Override
    public void errorAlert(String msg) {
        iStockRequestView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iStockRequestView.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        iStockRequestView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iStockRequestView.dismissPDialog();
    }

    @Override
    public void validateRequest() {
        if (validation()) {
            iStockRequestView.confirmClick();
        }
    }

    @Override
    public void sendRequest() {
        String encryptString = "";
        String keyData = new KeyDataReader().get();

        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        int index = 0;
        try {

            /*Amount  -//Transfer amount
PayMode  -// Mode of payment like bank tansfer or cash or as suggested by Anymulti (vinod kumar)
 BankName -//Name of the bank if mode is bank transfer in case of cash pass it blank
ParentID      -//To whomw sending the request in short parent id of request sender
Remark       -// Any remark while sending the request , present in case of cash deposite other wise pass blank
UTRNo       -// incase of bank transfer pass it otherwise blank

Type -// T pass "T" ;

*/
            loginrequest.put("Amount", iStockRequestView.Amount());
            loginrequest.put("PayMode", iStockRequestView.spPayMode());
            loginrequest.put("BankName", iStockRequestView.BankName());
            loginrequest.put("ParentID", iStockRequestView.ParentId());
            loginrequest.put("Remark", iStockRequestView.Remark());
            loginrequest.put("UTRNo", iStockRequestView.UTRNumber());
            loginrequest.put("BalanceType", iStockRequestView.RequestBalanceType());
            loginrequest.put("Type", "1");
            String jsonrequest = loginrequest.toString();
            log.i("Balance Request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateStockRequestResponse = this;
                model.webserviceMethod(encryptString, keyData, "StockRequest");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @Override
    public boolean validation() {

        if (iStockRequestView.Amount().isEmpty()) {
            iStockRequestView.editAmountError();
            return false;
        }
        if (iStockRequestView.spPayMode().equals("Cash")) {
            if (iStockRequestView.Remark().isEmpty()) {
                iStockRequestView.editRemarkError();
                return false;
            }
        } else if (iStockRequestView.spPayMode().equals("Bank Transfer")) {
            if (iStockRequestView.BankName().isEmpty()) {
                iStockRequestView.editBankNameError();
                return false;
            }
        } else if (iStockRequestView.spPayMode().equals("Cash Deposit in Bank")) {
            if (iStockRequestView.BankName().isEmpty()) {
                iStockRequestView.editBankNameError();
                return false;
            }
            if (iStockRequestView.Remark().isEmpty()) {
                iStockRequestView.editRemarkError();
                return false;
            }

        }
        return true;
    }
    @Override
    public void StockReqResponseResponse(String encryptedResponse) {
        log.i("encryptedResponse", encryptedResponse);
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            log.i("Decrypted body", body);
            successAlert(body);
        } catch (Exception e) {
        }
    }

    @Override
    public void StockReqResponseFailResponse(String encryptedResponse) {
        log.i("Decrypted Fbody", encryptedResponse);
        //String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(encryptedResponse);
            log.i("DecryptedFF1 body", body);
            String body2 = data.Decrypt(encryptedResponse);
            log.i("DecryptedFF body", body2);
            errorAlert(body2);
        } catch (Exception e) {

        }
    }
}
