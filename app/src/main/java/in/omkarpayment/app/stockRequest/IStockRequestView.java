package in.omkarpayment.app.stockRequest;

/**
 * Created by Consisty on 16-Aug-17.
 */

public interface IStockRequestView {

    void errorAlert(String msg);

    void successAlert(String msg);

    void showPDialog();

    void dismissPDialog();

    String Amount();

    String UTRNumber();

    String Remark();

    String BankName();

    String RequestBalanceType();

    void editAmountError();

    void editRemarkError();

    void editBankNameError();

    void editUTRError();

    String ParentId();

    String spPayMode();

    void clear();

    void confirmClick();


}
