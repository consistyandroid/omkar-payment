package in.omkarpayment.app.userActivation;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.WebServiceModel;

/**
 * Created by Krish on 12-Jul-17.
 */

public class ActivationPresenter implements IActivationPresenter {
    IActivationView iActivationView;
    LogWriter log = new LogWriter();

    public ActivationPresenter(IActivationView iActivationView) {
        this.iActivationView = iActivationView;
    }

    @Override
    public void validateActivation() {
        if (doValidation()) {

            iActivationView.confirmClick();

        }
    }

    @Override
    public void errorAlert(String msg) {
        iActivationView.errorAlert(msg);
    }

    @Override
    public void ActivateUser(String Name, String MobileNo, String Address, String Shopname,
                             String ParentID, String Services, String UserName) {

        // String UserTypeID =  iActivationView.UserType();
        String keyData = new KeyDataReader().get();
        Log.i("keyData", keyData);
        String encryptString = "";
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("Name", Name);
            loginrequest.put("ShopeName", Shopname);
            loginrequest.put("MobileNo", MobileNo);
            loginrequest.put("Address", Address);
            loginrequest.put("EmailId", "");
            loginrequest.put("PanNo", "");
            loginrequest.put("Adhar", "");
            loginrequest.put("Gst", "");
            loginrequest.put("ParentID", ParentID);
            loginrequest.put("Services", "[1,2,4]");
            loginrequest.put("UserName", UserName);
            loginrequest.put("DateOfBirth", "");
            loginrequest.put("RechargePin", "1234");
            loginrequest.put("UserTypeID", iActivationView.UserType());
            String jsonrequest = loginrequest.toString();
            log.i("request", jsonrequest);
            encryptString = data.Encrypt(jsonrequest);
            log.i("encryptString", encryptString);
            if (!encryptString.equals("")) {
                WebServiceModel model = new WebServiceModel();
                model.delegateActivationResponse = this;
                model.webserviceMethod(encryptString, keyData, "UserActivation");
                showPDialog();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void successAlert(String msg) {
        iActivationView.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        iActivationView.showPDialog();
    }

    @Override
    public boolean doValidation() {

        if (iActivationView.Name().isEmpty()) {
            iActivationView.editNameError();
            return false;
        }
        if (iActivationView.shopName().isEmpty()) {
            iActivationView.editShopNameError();
            return false;
        }

        if (iActivationView.Address().isEmpty()) {
            iActivationView.editAddressError();
            return false;
        }
        if (iActivationView.MobileNo().isEmpty()) {
            iActivationView.editMobileNoError();
            return false;
        }
        if (iActivationView.MobileNo().length() != 10) {
            iActivationView.editMobileNoError();
            return false;
        }
      /*  if (iActivationView.MArsID().isEmpty()) {
            iActivationView.edtMarsIDError();
            return false;
        }*/
        return true;
    }

    boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    boolean isValidPan(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
        Matcher matcher = pattern.matcher(target);

        if (matcher.matches()) {
            return true;
        } else {
            iActivationView.editPanNumberError();
            return false;

        }
    }

    @Override
    public void dismissPDialog() {
        iActivationView.dismissPDialog();
    }

    @Override
    public void decryptActivationResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        log.i("bodyText : ", encryptedResponse);
        try {
            body = data.Decrypt(encryptedResponse);
            log.i("Decrypted body", body);
            iActivationView.successAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
