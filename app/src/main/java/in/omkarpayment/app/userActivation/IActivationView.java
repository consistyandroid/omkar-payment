package in.omkarpayment.app.userActivation;

/**
 * Created by Krish on 12-Jul-17.
 */

public interface IActivationView {

    void AdharNo();

    void editNameError();

    void editAddressError();

    void editMobileNoError();

    void editPinError();

    void checkBoxError();

    void editProprietorNameError();

    void editEmailIdError();

    void editDoBError();

    void editPanNumberError();

    void editShopNameError();

    String ProprietorName();

    void emailId();

    void strDoB();

    String UserType();

    void PanNumber();

    String shopName();

    String Name();

    String Address();

    String MobileNo();

    String Pin();

    String MArsID();

    void confirmClick();

    void editAdharNoError();

    void errorAlert(String msg);

    void successAlert(String msg);

    void showPDialog();

    void dismissPDialog();

    boolean panValidate();

    void edtMarsIDError();
}
