package in.omkarpayment.app.userActivation;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityActivationBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

public class UserActivationActivity extends Fragment implements IActivationView {
    ActivityActivationBinding activationBinding;
    CustomProgressDialog progressDialog;
    IActivationPresenter activationPresenter;
    AlertImpl alert;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    String strDate = "", UserTypeId, str_Pan_Number, editTxtName, editTxtShopName;
    boolean panValidate = false;
    List<String> serviceList;
    NetworkState ns = new NetworkState();
    View v;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        activationBinding = DataBindingUtil.inflate(inflater, R.layout.activity_activation, container, false);
        v = activationBinding.getRoot();
        progressDialog = new CustomProgressDialog(getActivity());
        activationPresenter = new ActivationPresenter(this);
        alert = new AlertImpl(getActivity());


        if (UserDetails.UserType.equals(UserType.Distributor)) {
            UserTypeId = "3";
        } else if (UserDetails.UserType.equals(UserType.SuperDistributor)) {
            UserTypeId = "2";
        } else if (UserDetails.UserType.equals(UserType.Admin)) {
            UserTypeId = "2";
        } else {
            UserTypeId = "3";
        }

        activationBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ns.isInternetAvailable(getActivity())) {
                    activationPresenter.validateActivation();
                } else {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }

            }
        });
        activationBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        /*date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                if (isValidAge(year, monthOfYear, dayOfMonth)) {
                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    activationBinding.txtDoB.setText(sdf.format(userAge.getTime()));
                    // log.i("Age in ", sdf.format(userAge.getTime()));
                    strDate = sdf.format(userAge.getTime());
                    activationBinding.txtDoB.setError(null);
                } else {
                    Toast.makeText(getActivity(), "Your age should be more than 18+ year(s)", Toast.LENGTH_SHORT).show();
                    activationBinding.txtDoB.setText("");

                }
            }
        };*/

      /*  activationBinding.txtDoB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });*/


        serviceList = new ArrayList<String>();
        activationBinding.chkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String type = "";
                if (isChecked) {
                    type = "0";
                    serviceList.add(type);
                } else {
                    serviceList.remove(type);
                }
            }
        });


        activationBinding.checkMobileNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "15";
                serviceList.add(type);

            }
        });
        activationBinding.chkMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "1";
                serviceList.add(type);

            }
        });
        activationBinding.chkDth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "2";
                serviceList.add(type);

            }
        });
        activationBinding.chkDatacard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "3";
                serviceList.add(type);

            }
        });
        activationBinding.chkPostpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "4";
                serviceList.add(type);

            }
        });
        activationBinding.checkInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "16";
                serviceList.add(type);

            }
        });
        activationBinding.chkElectricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "14";
                serviceList.add(type);

            }
        });

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
        );
        return v;
    }

    public void clear() {
        activationBinding.editName.requestFocus();
        activationBinding.editName.setText("");
        activationBinding.editName.setError(null);

        activationBinding.editAddress.setText("");
        activationBinding.editAddress.setError(null);

        activationBinding.editMobileNo.setText("");
        activationBinding.editMobileNo.setError(null);


        activationBinding.editShopName.setText("");
        activationBinding.editShopName.setError(null);
    }

    @Override
    public void AdharNo() {
    }

    @Override
    public void editNameError() {
        activationBinding.editName.setError(String.format(AllMessages.pleaseEnter, "name"));
        activationBinding.editName.requestFocus();
    }

    @Override
    public void editAddressError() {
        activationBinding.editAddress.setError(String.format(AllMessages.pleaseEnter, "address"));
        activationBinding.editAddress.requestFocus();
    }

    @Override
    public void editMobileNoError() {
        activationBinding.editMobileNo.setError(String.format(AllMessages.pleaseEnter, "mobile no"));
        activationBinding.editMobileNo.requestFocus();
    }

    @Override
    public void editPinError() {
      /*  activationBinding.editPin.setError(String.format(AllMessages.pleaseEnter, "pin"));
        activationBinding.editPin.requestFocus();*/
    }

    @Override
    public void checkBoxError() {
        Toast.makeText(getActivity(), String.format(AllMessages.pleaseSelect, "service"), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void editProprietorNameError() {
        activationBinding.editName.setError(String.format(AllMessages.pleaseEnter, "name"));
        activationBinding.editName.requestFocus();
     /*   activationBinding.editProprietorName.setError(String.format(AllMessages.pleaseEnter, "Proprietor Name"));
        activationBinding.editProprietorName.requestFocus();*/
    }

    @Override
    public void editEmailIdError() {

    }

    @Override
    public void editDoBError() {

    }

    @Override
    public void editPanNumberError() {

    }

    @Override
    public void editShopNameError() {
        activationBinding.editShopName.setError(String.format(AllMessages.pleaseEnter, "name"));
        activationBinding.editShopName.requestFocus();
       /* activationBinding.editFirmName.setError(String.format(AllMessages.pleaseEnter, "Shop Name"));
        activationBinding.editFirmName.requestFocus();*/
    }

    @Override
    public String ProprietorName() {
        return activationBinding.editName.getText().toString();
    }

    @Override
    public void emailId() {

    }

    @Override
    public void strDoB() {

    }

    @Override
    public String UserType() {
        return UserTypeId;
    }

    @Override
    public void PanNumber() {

    }

    @Override
    public String shopName() {
        return activationBinding.editShopName.getText().toString();
    }

    @Override
    public String Name() {
        return activationBinding.editName.getText().toString().trim();
    }

    @Override
    public String Address() {
        return activationBinding.editAddress.getText().toString().trim();
    }

    @Override
    public String MobileNo() {
        return activationBinding.editMobileNo.getText().toString().trim();
    }

    @Override
    public String Pin() {
        return "";
    }

    @Override
    public String MArsID() {
        return activationBinding.edtMarsID.getText().toString();
    }

    @Override
    public void confirmClick() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Name: %s\nShopname: %s\nAddress: %s\nMobile No: %s", Name(), shopName(), Address(), MobileNo()));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activationPresenter.ActivateUser(ProprietorName(),
                        MobileNo(),
                        Address(),
                        shopName(),
                        String.valueOf(UserDetails.UserId), "0", Name()

                );
                clear();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void editAdharNoError() {

    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public boolean panValidate() {
        return panValidate;
    }

    @Override
    public void edtMarsIDError() {
        alert.errorAlert("Please Enter Marse ID");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private boolean isValidAge(int year, int monthOfYear, int dayOfMonth) {
        Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        //log.i("Age", userAge.toString());
        if (minAdultAge.before(userAge)) {
            return false;
        } else {
            return true;
        }
    }

    private Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private int getDiffYears(Date first) {
        Calendar userDate = toCalendar(first);

        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, 0);

        Calendar currentDate = minAdultAge;
        int diff = currentDate.get(Calendar.YEAR) - userDate.get(Calendar.YEAR);
        if (userDate.get(Calendar.MONTH) > currentDate.get(Calendar.MONTH) ||
                (userDate.get(Calendar.MONTH) == currentDate.get(Calendar.MONTH) && userDate.get(Calendar.DATE) > currentDate.get(Calendar.DATE))) {
            diff--;
        }
        // log.i("Diff", diff + "");
        return diff;
    }

    private boolean isValidAge(String dob) {
        try {
            //log.i("Age Typed ", dob);
            DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sourceFormat.parse(dob);
            int diff = getDiffYears(date);
            //  log.i("Diff", diff + "");
            if (diff < 18) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

    }

}
