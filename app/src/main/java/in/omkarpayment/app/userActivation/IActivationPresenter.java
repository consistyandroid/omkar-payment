package in.omkarpayment.app.userActivation;

/**
 * Created by Krish on 12-Jul-17.
 */

public interface IActivationPresenter {

    void validateActivation();


    void errorAlert(String msg);

    void ActivateUser( String Name, String MobileNo, String Address,String ShopName,
                       String ParentID, String Services, String UserName);

    void successAlert(String msg);

    void showPDialog();

    boolean doValidation();


    void dismissPDialog();

    void decryptActivationResponse(String encryptedResponse);

}
