package in.omkarpayment.app.downlinelist;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.DownlineListPojo;
import in.omkarpayment.app.model.WebServiceModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by Krish on 14-Aug-17.
 */

public class DownlineListPresenter implements IDownlineListPresenter {
    IDownlineListView iDownlineListView;

    public DownlineListPresenter(IDownlineListView iDownlineListView) {
        this.iDownlineListView = iDownlineListView;
    }


    @Override
    public void getDownLineList() {
        String keyData = new KeyDataReader().get();
        WebServiceModel model = new WebServiceModel();
        model.delegatDownlineListResponse = this;
        model.webserviceMethod("", keyData, "DownLineList");
        showPDialog();
    }

    @Override
    public void showPDialog() {
        iDownlineListView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iDownlineListView.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        iDownlineListView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iDownlineListView.successAlert(msg);
    }

    @Override
    public void DownlineListResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            ArrayList<DownlineListPojo> downlineList = gson.fromJson(body, new TypeToken<ArrayList<DownlineListPojo>>() {
            }.getType());
            if (downlineList != null) {
                iDownlineListView.downlineListResponse(downlineList);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void DownlineListFailResponse(String encryptedResponse) {
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        try {
            body = data.Decrypt(encryptedResponse);
            errorAlert(body);

        } catch (Exception e) {

        }
    }
}
