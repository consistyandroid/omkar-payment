package in.omkarpayment.app.downlinelist;

import in.omkarpayment.app.json.DownlineListPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krish on 14-Aug-17.
 */

public interface IDownlineListView {


    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void downlineListResponse(List<DownlineListPojo> downlineList);
}
