package in.omkarpayment.app.downlinelist;

/**
 * Created by Krish on 14-Aug-17.
 */

public interface IDownlineListPresenter {


    void getDownLineList();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void DownlineListResponse(String encryptedResponse);

    void DownlineListFailResponse(String encryptedResponse);
}
