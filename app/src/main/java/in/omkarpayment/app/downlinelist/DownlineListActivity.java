package in.omkarpayment.app.downlinelist;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.DownLineSearchMemAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.json.DownlineListPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

public class DownlineListActivity extends AppCompatActivity implements IDownlineListView {

    CustomProgressDialog pDialog;
    AlertImpl alert;
    IDownlineListPresenter downlineListPresenter;
    RecyclerView recyclerView;
    Context context = this;
    EditText editSearch;
    int userCount = 0;
    double userTotalBalance, userBalance = 0.0;
    TextView txtTotalMembers, txtTotalBalance;
    DownLineSearchMemAdapter adapter;
    List<DownlineListPojo> Downlinelist_search = new ArrayList<DownlineListPojo>();
    CollapsingToolbarLayout collapsingToolbarLayout;
    NetworkState ns = new NetworkState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downline_list);

        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        downlineListPresenter = new DownlineListPresenter(this);
        recyclerView = (RecyclerView) findViewById(R.id.listView);
        editSearch = (EditText) findViewById(R.id.editSearch);
        editSearch.requestFocus();
        txtTotalMembers = (TextView) findViewById(R.id.txtTotalMembers);
        txtTotalBalance = (TextView) findViewById(R.id.txtTotalBalance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String title = "DownLine List";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        if (ns.isInternetAvailable(context)) {
            downlineListPresenter.getDownLineList();
        } else {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
        }

        recyclerView.setAdapter(null);

        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtTotalMembers.setText(null);
                txtTotalBalance.setText(null);
                userCount = 0;
                userBalance = 0.0;
                userTotalBalance = 0.0;
                filter(s.toString());
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                txtTotalMembers.setText(null);
                txtTotalBalance.setText(null);
                userCount = 0;
                userBalance = 0.0;
                userTotalBalance = 0.0;
                filter(s.toString());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void downlineListResponse(List<DownlineListPojo> downlineList) {
        Downlinelist_search = downlineList;
        Set<String> itemIds = new HashSet<String>();
        for (DownlineListPojo pojo : downlineList) {
            String user = pojo.getUserName();
            itemIds.add(user);
            userCount = itemIds.size();
            userBalance = pojo.getCurrentBalance();
            userTotalBalance += userBalance;
            Log.i("Downline:", pojo.getMobileNumber().toString() + pojo.getUserID().toString() + pojo.getUserName().toString());
        }
        txtTotalMembers.setText("Members : " + String.valueOf(userCount));
        txtTotalBalance.setText("Balance : " + String.valueOf(userTotalBalance + " ₹"));

        adapter = new DownLineSearchMemAdapter(downlineList);
        recyclerView.setAdapter(adapter);

    }

    void filter(String text) {
        Set<String> itemIds = new HashSet<String>();
        List<DownlineListPojo> temp = new ArrayList<DownlineListPojo>();
        for (DownlineListPojo d : Downlinelist_search) {
            if (d.getMobileNumber().contains(text)
                    || d.getUserName().toLowerCase().contains(text.toLowerCase())
                    || d.getUserID().equals(text)) {
                String user = d.getUserName();
                itemIds.add(user);
                userCount = itemIds.size();
                userBalance = d.getCurrentBalance();
                userTotalBalance += userBalance;
                temp.add(d);
            }
        }
        txtTotalMembers.setText("Members : " + String.valueOf(userCount));
        txtTotalBalance.setText("Balance : " + String.valueOf(userTotalBalance + " ₹"));
        adapter.updateList(temp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }
    @Override
    protected void onPause() {
        super.onPause();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

    }
}
