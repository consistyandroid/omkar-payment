package in.omkarpayment.app.notificationHistory;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.dataBaseProcessing.DataBaseHelper;
import in.omkarpayment.app.databinding.ActivityNotificationHistoryBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;

public class NotificationHistory extends AppCompatActivity {

    DataBaseHelper dbHelper;
    AlertImpl alert;
    Context context = this;
    ActivityNotificationHistoryBinding notificationHistoryBinding;
    CustomProgressDialog progressDialog;
    private Toolbar toolbar;

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationHistoryBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification_history);
        alert = new AlertImpl(this);
        dbHelper = new DataBaseHelper(this);
        progressDialog = new CustomProgressDialog(context);

//        TextView txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
//        txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Notification Inbox";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setTitle(title);
//        TextView txtHeader = findViewById(R.id.txtHeader);
//        txtHeader.setText(title);

        notificationHistoryBinding.btnClearHistory.setVisibility(View.GONE);

        notificationHistoryBinding.btnClearHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.showPDialog();
                int i = dbHelper.DeleteUser();
                if (i > 0) {
                    if (progressDialog != null) {
                        progressDialog.dismissPDialog();
                    }
                    notificationHistoryBinding.listView.setAdapter(null);
                    notificationHistoryBinding.btnClearHistory.setVisibility(View.GONE);
                    alert.errorAlert("Notification history deleted successfully.");
                    return;
                }
                alert.errorAlert("Error while deleting data.");
                selectData();
            }
        });

        selectData();
    }

    private void selectData() {
        progressDialog.showPDialog();
        notificationHistoryBinding.listView.setAdapter(null);
        ArrayList<HashMap> userList = dbHelper.selectData();

        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }

        int listSize = userList.size();
        if (listSize == 0) {
            notificationHistoryBinding.btnClearHistory.setVisibility(View.GONE);
            alert.errorAlert("No data found");
            return;
        }

        notificationHistoryBinding.btnClearHistory.setVisibility(View.VISIBLE);
        NotificationHistoryAdapter testAdapter = new NotificationHistoryAdapter(context, userList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        notificationHistoryBinding.listView.setLayoutManager(mLayoutManager);
        notificationHistoryBinding.listView.setItemAnimator(new DefaultItemAnimator());
        notificationHistoryBinding.listView.setAdapter(testAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

}
