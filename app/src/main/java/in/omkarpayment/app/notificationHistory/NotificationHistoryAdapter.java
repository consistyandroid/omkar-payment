package in.omkarpayment.app.notificationHistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.recyclerview.widget.RecyclerView;
import in.omkarpayment.app.R;


public class NotificationHistoryAdapter extends RecyclerView.Adapter<NotificationHistoryAdapter.MyViewHolder> {

    Context context;
    private ArrayList<HashMap> notificationList;

    public NotificationHistoryAdapter(Context context, ArrayList<HashMap> aryList) {
        this.context = context;
        this.notificationList = aryList;
    }

    @Override
    public NotificationHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_notification_history, parent, false);
        return new NotificationHistoryAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(NotificationHistoryAdapter.MyViewHolder holder, int position) {

        holder.txtDate.setText(notificationList.get(position).get("DOC").toString());
        holder.txtNotification.setText(notificationList.get(position).get("NotificationMessage").toString());
    }


    @Override
    public int getItemCount() {
        return notificationList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtDate, txtNotification;

        public MyViewHolder(View rowView) {
            super(rowView);
            txtDate = rowView.findViewById(R.id.txtDate);
            txtNotification = rowView.findViewById(R.id.txtNotification);
        }
    }


}
