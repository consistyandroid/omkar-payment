package in.omkarpayment.app.changePin;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityChangePinBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;

public class ChangePinActivity extends AppCompatActivity implements IChangePinView {
    ActivityChangePinBinding changePinBinding;
    AlertImpl alert;
    CustomProgressDialog pDialog;
    IChangePinPresenter iChangePinPresenter;
    Dialog dialog;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changePinBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_pin);

        pDialog = new CustomProgressDialog(this);
        alert = new AlertImpl(this);
        iChangePinPresenter = new ChangePinPresenterImpl(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Change Pin";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        changePinBinding.btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iChangePinPresenter.changePin();

            }
        });
        changePinBinding.txtForgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(context, R.style.ThemeWithCorners);
                dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
                dialog.setContentView(R.layout.dialog_forgot_pin);
                dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);
                dialog.setTitle("Reset Pin");
                Button btn_Reset = (Button) dialog.findViewById(R.id.btn_forget);


                final EditText editMobileNo = (EditText) dialog.findViewById(R.id.editMobileNo);

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

                dialog.show();
                btn_Reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (editMobileNo.getText().toString().equals("")) {
                            editMobileNo.setError("Please Enter Register No");
                            editMobileNo.requestFocus();
                        } else {
                            iChangePinPresenter.resetPin(editMobileNo.getText().toString().trim());
                        }

                    }
                });


            }
        });
    }

    @Override
    public void editOldPinError() {
        changePinBinding.editOldPin.setError(String.format(AllMessages.pleaseEnter, "new pin"));
        changePinBinding.editOldPin.requestFocus();
    }

    @Override
    public String getOldPin() {
        return changePinBinding.editOldPin.getText().toString();
    }

    @Override
    public String getNewPin() {
        return changePinBinding.editNewPin.getText().toString();
    }

    @Override
    public String getConfirmPin() {
        return changePinBinding.editConfirmPin.getText().toString();
    }


    @Override
    public void showPDialog() {
        pDialog.showPDialog();
    }

    @Override
    public void validate() {

    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        alert.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
        changePinBinding.editOldPin.setText("");
        changePinBinding.editNewPin.setText("");
        changePinBinding.editConfirmPin.setText("");
    }

    @Override
    public void moveToLogin() {

    }

    @Override
    public void editNewPinError() {
        changePinBinding.editNewPin.setError(String.format(AllMessages.pleaseEnter, "new pin"));
        changePinBinding.editNewPin.requestFocus();
    }

    @Override
    public void editConfirmPinError() {
        changePinBinding.editConfirmPin.setError(String.format(AllMessages.pleaseEnter, "new pin"));
        changePinBinding.editConfirmPin.requestFocus();
    }

    @Override
    public void changePassword() {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
