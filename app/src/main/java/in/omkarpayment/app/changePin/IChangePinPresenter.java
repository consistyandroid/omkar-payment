package in.omkarpayment.app.changePin;

/**
 * Created by lenovo on 10/4/2018.
 */

public interface IChangePinPresenter {
    boolean validateField();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String errorMsg);

    void successAlert(String msg);

    void ChangePinResponse(String encryptedResponse);

    void ChangePinFailureResponse(String encryptedResponse);

    void changePin();

    void  resetPin(String Mobileno);
}
