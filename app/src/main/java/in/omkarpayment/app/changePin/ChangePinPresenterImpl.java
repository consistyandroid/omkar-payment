package in.omkarpayment.app.changePin;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.ChangePinModel;

/**
 * Created by lenovo on 10/4/2018.
 */

public class ChangePinPresenterImpl implements IChangePinPresenter {
    IChangePinView iChangePinView;
    LogWriter log = new LogWriter();

    public ChangePinPresenterImpl(IChangePinView iChangePinView) {
        this.iChangePinView = iChangePinView;
    }

    @Override
    public boolean validateField() {
        if (iChangePinView.getOldPin().isEmpty()) {
            iChangePinView.editOldPinError();

        } else if (iChangePinView.getNewPin().isEmpty()) {
            iChangePinView.editNewPinError();
            return false;
        } else if (iChangePinView.getConfirmPin().isEmpty()) {
            iChangePinView.editConfirmPinError();
            return false;
        }

        return true;
    }

    @Override
    public void showPDialog() {
        iChangePinView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iChangePinView.dismissPDialog();
    }

    @Override
    public void errorAlert(String errorMsg) {
        iChangePinView.errorAlert(errorMsg);
    }

    @Override
    public void successAlert(String msg) {
        iChangePinView.successAlert(msg);
    }

    @Override
    public void ChangePinResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("body", body);
            iChangePinView.successAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ChangePinFailureResponse(String encryptedResponse) {
        dismissPDialog();
        String body = "";
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            body = data.Decrypt(encryptedResponse);
            Log.i("body", body);
            iChangePinView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changePin() {
        if (iChangePinView.getOldPin().isEmpty()) {
            iChangePinView.editOldPinError();

        } else if (iChangePinView.getNewPin().isEmpty()) {
            iChangePinView.editNewPinError();

        } else if (iChangePinView.getConfirmPin().isEmpty()) {
            iChangePinView.editConfirmPinError();

        } else {
            String encryptString = "";
            String keyData = new KeyDataReader().get();

            Cryptography_Android data = new Cryptography_Android();
            JSONObject loginrequest = new JSONObject();
            try {
                loginrequest.put("OldPin", iChangePinView.getOldPin());
                loginrequest.put("RechargePin", iChangePinView.getNewPin());
                String jsonrequest = loginrequest.toString();
                encryptString = data.Encrypt(jsonrequest);
                log.i("encrypted String", encryptString);
                if (!encryptString.equals("")) {
                    ChangePinModel model = new ChangePinModel();
                    model.delegate = this;
                    model.changePin(encryptString, keyData);
                    showPDialog();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }



    @Override
    public void resetPin(String Mobileno) {

    }


}
