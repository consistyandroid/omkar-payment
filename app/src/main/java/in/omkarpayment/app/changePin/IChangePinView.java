package in.omkarpayment.app.changePin;

/**
 * Created by lenovo on 10/4/2018.
 */

public interface IChangePinView {
    void editOldPinError();

    String getOldPin();

    String getNewPin();

    String getConfirmPin();

    void showPDialog();

    void validate();

    void dismissPDialog();

    void errorAlert(String errorMsg);

    void successAlert(String msg);

    void moveToLogin();

    void editNewPinError();

    void editConfirmPinError();

    void changePassword();
}
