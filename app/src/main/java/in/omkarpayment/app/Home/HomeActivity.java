package in.omkarpayment.app.Home;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import in.omkarpayment.app.NetworkUtility;
import in.omkarpayment.app.R;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.fragment.BalanceFragment;
import in.omkarpayment.app.fragment.DistributorFragment;
import in.omkarpayment.app.fragment.GetMoneyTransferUI.IMoneyTransferUIPresenter;
import in.omkarpayment.app.fragment.GetMoneyTransferUI.IMoneyTransferUIView;
import in.omkarpayment.app.fragment.GetMoneyTransferUI.MoneyTransferUIPresenterImpl;
import in.omkarpayment.app.fragment.HomeFragment;
import in.omkarpayment.app.fragment.RechargeFragment;
import in.omkarpayment.app.fragment.RetailerFragment;
import in.omkarpayment.app.fragment.UtilityFragment;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.Notification;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.main.IMainPresenter;
import in.omkarpayment.app.main.IMainView;
import in.omkarpayment.app.main.MainPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;

public class HomeActivity extends AppCompatActivity implements IAvailableBalanceView, IMainView, IMoneyTransferUIView {
    TextView txtUserName, txtMobileNo, txtUserType, txtAccountBalance, txtnotification, txtSaleComm;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    Typeface tf;
    Double CurrentBalance, DMRBalance;
    Context context = this;
    Animation animation;
    ImageView btnRefresh;
    IMainPresenter iMainPresenter;
    NetworkUtility ns = new NetworkUtility();
    IMoneyTransferUIPresenter iMoneyTransferUIPresenter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onResume() {
        super.onResume();
        keypad();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        keypad();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iMainPresenter = new MainPresenter(this);
        iMainPresenter.getNotification();
        iAvailableBalancePresenter.getAvailableBalance();
        iAvailableBalancePresenter.getSaleCommission();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        String fontPath = "font/Muli.ttf";
        tf = Typeface.createFromAsset(getAssets(), fontPath);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtMobileNo = (TextView) findViewById(R.id.txtMobileNo);
        txtAccountBalance = (TextView) findViewById(R.id.txtAccountBalance);
        txtSaleComm = (TextView) findViewById(R.id.txtSaleComm);
        txtUserType = (TextView) findViewById(R.id.txtUserType);
        btnRefresh = (ImageView) findViewById(R.id.btnRefresh);
        txtUserName.setText(UserDetails.Username + "(" + UserDetails.UserType + ")");
        txtUserName.setSelected(true);
        txtUserName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txtUserName.setSingleLine(true);

        txtnotification = (TextView) findViewById(R.id.txtNotification);
        txtnotification.setSelected(true);
        txtnotification.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        txtnotification.setSingleLine(true);

        iMoneyTransferUIPresenter = new MoneyTransferUIPresenterImpl(this);
        iMoneyTransferUIPresenter.getMoneyTransferUI();
//        Log.i("UI Resp.",UserDetails.MoneyTxUI);

        txtMobileNo.setText(UserDetails.MobileNumber);
        txtUserType.setText("(" + UserDetails.UserType + ")");
        txtUserName.setTypeface(tf);
        txtUserType.setTypeface(tf);
        txtMobileNo.setTypeface(tf);


        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAccountBalance.setText("");
                txtSaleComm.setText("");
                animation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.rotate);
                btnRefresh.startAnimation(animation);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iAvailableBalancePresenter.getAvailableBalance();
                        iAvailableBalancePresenter.getSaleCommission();
                    }
                }, 2000);
            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }


    public void keypad() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    @Override
    public void onBackPressed() {
        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(String.format("Do you want to logout ?"));

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        try {


            if (UserDetails.UserType.equalsIgnoreCase(UserType.Retailer)) {

                adapter.addFrag(new RechargeFragment(), "Recharge");
                adapter.addFrag(new RetailerFragment(), "More");
                adapter.addFrag(new HomeFragment(), "Home");
                adapter.addFrag(new UtilityFragment(), "Utility");//Utility fragment
                //retailer fragment
            } else if (UserDetails.UserType.equalsIgnoreCase(UserType.Admin) ||
                    (UserDetails.UserType.equalsIgnoreCase(UserType.Distributor))) {
                adapter.addFrag(new HomeFragment(), "Home");
                adapter.addFrag(new BalanceFragment(), "Balance Tx.");//Balancefragmnet
                adapter.addFrag(new DistributorFragment(), "More");//distributor fragment
            } else if (UserDetails.UserType.equalsIgnoreCase(UserType.SuperDistributor)) {
                adapter.addFrag(new BalanceFragment(), "Balance Tx.");
                adapter.addFrag(new DistributorFragment(), "More");//distributor fragment
            }
            viewPager.setSaveFromParentEnabled(false);
            viewPager.setAdapter(adapter);
        } catch (Exception r) {
            r.printStackTrace();
        }
    }

    private void setupTabIcons() {
        try {


            if (UserDetails.UserType.equalsIgnoreCase(UserType.Retailer)) {
                TextView tabRecharge = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabRecharge.setText("Recharge");
                tabRecharge.setTypeface(tf);
                tabRecharge.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_mobile, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabRecharge);

                TextView tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabHome.setText("Home");
                tabHome.setTypeface(tf);
                tabHome.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_home, 0, 0);
                tabLayout.getTabAt(2).setCustomView(tabHome);

                TextView tabUtility = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabUtility.setText("Utility");
                tabUtility.setTypeface(tf);
                tabUtility.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_electricity, 0, 0);
                tabLayout.getTabAt(3).setCustomView(tabUtility);

                TextView tabMore = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabMore.setText("More");
                tabMore.setTypeface(tf);
                tabMore.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_more, 0, 0);
                tabLayout.getTabAt(1).setCustomView(tabMore);

            } else if (UserDetails.UserType.equalsIgnoreCase(UserType.Admin) ||
                    UserDetails.UserType.equalsIgnoreCase(UserType.Distributor)) {
                TextView tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabHome.setText("Home");
                tabHome.setTypeface(tf);

                tabHome.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_home, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabHome);

                TextView tabBalanceTransfer = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabBalanceTransfer.setText("Balance Tx");
                tabBalanceTransfer.setTypeface(tf);
                tabBalanceTransfer.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_baltx, 0, 0);
                tabLayout.getTabAt(1).setCustomView(tabBalanceTransfer);

                TextView tabMore = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabMore.setText("More");
                tabMore.setTypeface(tf);
                tabMore.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_more, 0, 0);
                tabLayout.getTabAt(2).setCustomView(tabMore);
            } else if (UserDetails.UserType.equalsIgnoreCase(UserType.SuperDistributor)) {
                TextView tabBalanceTransfer = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabBalanceTransfer.setText("Balance Tx.");
                tabBalanceTransfer.setTypeface(tf);
                tabBalanceTransfer.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_baltx, 0, 0);
                tabLayout.getTabAt(0).setCustomView(tabBalanceTransfer);

                TextView tabMore = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tabMore.setText("More");
                tabMore.setTypeface(tf);
                tabMore.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_more, 0, 0);
                tabLayout.getTabAt(1).setCustomView(tabMore);
            }
        } catch (Exception r) {
            r.printStackTrace();
        }
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                CurrentBalance = obj.getCurrentBalance();
                DMRBalance = obj.getDMRBalance();
                UserDetails.UserDMRBalance = obj.getDMRBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                //txtAccountBalance.setText("BAL:"+"₹ " + String.valueOf(obj.getCurrentBalance()));
                txtAccountBalance.setText("Bal:"+"₹ " + String.format("%.02f", Float.parseFloat(String.valueOf(obj.getCurrentBalance()))));
                // txtDMRBalance.setText("DMR ₹" + String.valueOf(obj.getDMRBalance()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {
        for (SaleCommissionPojo obj : lastrasaction) {
            try {

                UserDetails.UserSaleCommission = obj.getDiscountAmount();
                Log.i("Sale comm", String.valueOf(UserDetails.UserSaleCommission));
                txtSaleComm.setText("Com:"+"₹" + String.valueOf(obj.getDiscountAmount()) + "(" + (obj.getDiscountRechargeCount()) + ")");
                // txtDMRBalance.setText("DMR ₹" + String.valueOf(obj.getDMRBalance()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showPDialog() {

    }

    @Override
    public void Statusresponse(ArrayList<Notification> notification) {
        String ntification = "";
        for (Notification obj : notification) {
            try {
                ntification = String.valueOf(obj.getNotificationText());
                txtnotification.setText(ntification);
            } catch (Exception e) {

            }

        }
    }

    public void updateBal() {
        txtAccountBalance.setText("");
        txtSaleComm.setText("");
        animation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.rotate);
        btnRefresh.startAnimation(animation);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ns.checkInternet(context)) {
                    iAvailableBalancePresenter.getAvailableBalance();
                    iAvailableBalancePresenter.getSaleCommission();
                } else {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                }
            }
        }, 2000);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        updateBal();
    }

    @Override
    public void showDialog() {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void successAlert(String msg) {

    }

    @Override
    public void errorAlert(String msg) {

    }

    @Override
    public void getSuccessResponse(String msg) {

    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
