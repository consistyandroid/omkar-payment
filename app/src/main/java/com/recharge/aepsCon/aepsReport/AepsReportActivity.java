package com.recharge.aepsCon.aepsReport;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.recharge.aepsCon.json.AepsReportPojo;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.AepsReportAdapter;
import in.omkarpayment.app.adapter.AutocompleteTextAdapter;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.cashCollection.cashCollectionReport.CashCollectionReportPojo;
import in.omkarpayment.app.cashCollection.cashCollectionReport.CashCollectionReportPresenter;
import in.omkarpayment.app.cashCollection.cashCollectionReport.ICashCollectionReportView;
import in.omkarpayment.app.databinding.ActivityAepsReportBinding;
import in.omkarpayment.app.json.BalTransDownlineListPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.UserType;


public class AepsReportActivity extends AppCompatActivity implements IAepsReportView, ICashCollectionReportView {
    ActivityAepsReportBinding activityAepsReportBinding;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener Frmdate, todate;
    String strFromDate = "", strToDate = "";
    AepsReportAdapter adapter;
    AlertImpl alert;
    TextView txtToolbarBalance;
    CustomProgressDialog progressDialog;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AepsReportPresenterImpl aepsReportPresenter;
    Context context = this;
    CashCollectionReportPresenter cashCollectionReportPresenter;
    String UserID = "";
    NetworkState ns = new NetworkState();
    private int mYear, mMonth, mDay, fDate, tDate, fMonth, tMonth, fYear, tYear;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            activityAepsReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_aeps_report);
            progressDialog = new CustomProgressDialog(this);
            aepsReportPresenter = new AepsReportPresenterImpl(this);



            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            String title = "AEPS Report";
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(title);
            SpannableString set = new SpannableString(title);
            set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            getSupportActionBar().setTitle(set);
            final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            activityAepsReportBinding.listView.setLayoutManager(mLayoutManager);
            activityAepsReportBinding.listView.setItemAnimator(new DefaultItemAnimator());
            alert = new AlertImpl(this);

            setDateDefaultValue();
            activityAepsReportBinding.editMemberId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                        long id) {
                    BalTransDownlineListPojo op = (BalTransDownlineListPojo) parent.getItemAtPosition(pos);
                    UserID = String.valueOf(op.getUserID());
                }
            });
            if (!ns.isInternetAvailable(context)) {
                Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
            } else {
                if (!UserDetails.UserType.equals(UserType.Retailer) &&
                        !UserDetails.UserType.equals(UserType.APIUSER)) {
                    cashCollectionReportPresenter.getDownLineList();
                }
            }
            if (UserDetails.UserType.equals(UserType.Retailer) ||
                    UserDetails.UserType.equals(UserType.APIUSER)) {
                UserID = String.valueOf(UserDetails.UserId);
                activityAepsReportBinding.layoutMemberID.setVisibility(View.GONE);
            }

            // new custom Date Picker dialog for API level lower than API 23
            activityAepsReportBinding.txtToDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar mcurrentDate = Calendar.getInstance();
                    mYear = mcurrentDate.get(Calendar.YEAR);
                    mMonth = mcurrentDate.get(Calendar.MONTH);
                    mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog mDatePicker = new DatePickerDialog(AepsReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                            tDate = selectedday;
                            tMonth = selectedmonth + 1;
                            tYear = selectedyear;
                            String dateSelected = padLeftZero(selectedday + "") + "/" + padLeftZero((selectedmonth + 1) + "") + "/" + selectedyear;
                            activityAepsReportBinding.txtToDate.setText(dateSelected);
                        }
                    }, mYear, mMonth, mDay);
                    mDatePicker.show();
                }
            });

            activityAepsReportBinding.txtFromDate.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    final Calendar mcurrentDate = Calendar.getInstance();
                    mYear = mcurrentDate.get(Calendar.YEAR);
                    mMonth = mcurrentDate.get(Calendar.MONTH);
                    mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog mDatePicker = new DatePickerDialog(AepsReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                            fDate = selectedday;
                            fMonth = selectedmonth + 1;
                            fYear = selectedyear;
                            String dateSelected = padLeftZero(selectedday + "") + "/" + padLeftZero((selectedmonth + 1) + "") + "/" + selectedyear;
                            activityAepsReportBinding.txtFromDate.setText(dateSelected);
                        }
                    }, mYear, mMonth, mDay);
                    mDatePicker.show();
                }

            });


            activityAepsReportBinding.btnShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    aepsReportPresenter.validateDate();
                    activityAepsReportBinding.listView.setAdapter(null);
                }
            });

        }

    public String padLeftZero(String input) {
        String outPut = String.format("%2s", input).replace(' ', '0');
        return outPut;
    }

    @Override
    public String strFromDate() {
        return activityAepsReportBinding.txtFromDate.getText().toString();
    }

    @Override
    public String strToDate() {
        return activityAepsReportBinding.txtToDate.getText().toString();
    }

    @Override
    public String SelectedUserID() {
        return String.valueOf(UserDetails.UserId);
    }

    @Override
    public void editFromDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "From Date"));
        activityAepsReportBinding.txtFromDate.requestFocus();
    }

    @Override
    public void editToDateError() {
        alert.errorAlert(String.format(AllMessages.pleaseEnter, "To Date"));
        activityAepsReportBinding.txtToDate.requestFocus();
    }

    @Override
    public void getCashCollectionReport() {

    }

    @Override
    public String getReceiverID() {
        return UserID;
    }

    @Override
    public void getUserWiseCashColletionReport(ArrayList<CashCollectionReportPojo> dayreport) {

    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void errorAlert(String msg) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        if (msg.toLowerCase().contains("Data not avai".toLowerCase())) {
            activityAepsReportBinding.listView.setAdapter(null);
            activityAepsReportBinding.listView.setVisibility(View.GONE);
            activityAepsReportBinding.imgDataNotFound.setVisibility(View.VISIBLE);
            return;
        }
        alert.errorAlert(msg);
    }

    @Override
    public void getCashCollectionDownLine(ArrayList<BalTransDownlineListPojo> downlineList) {
        AutocompleteTextAdapter adapter;
        BalTransDownlineListPojo pojo = new BalTransDownlineListPojo();
        pojo.setUserID(UserDetails.UserId);
        pojo.setUserName("-- Own --");
       // pojo.setName("-- Own --");
        pojo.setMobileNumber(" ");
        downlineList.add(0, pojo);

        adapter = new AutocompleteTextAdapter(context, R.layout.adapter_autocomplete_text, downlineList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityAepsReportBinding.editMemberId.setAdapter(adapter);
        activityAepsReportBinding.editMemberId.setThreshold(1);
    }

    @Override
    public void errorToast(String body) {
        if (progressDialog != null) {
            progressDialog.dismissPDialog();
        }
        Toast.makeText(context, body, Toast.LENGTH_LONG).show();
    }

    @Override
    public String getDownLineMember() {
        return null;
    }

    @Override
    public void editMemberIdError() {
        alert.errorAlert(String.format("Please select member from downline"));
        activityAepsReportBinding.editMemberId.requestFocus();
    }

    @Override
    public void successAlert(String sttus) {
        alert.successAlert(sttus);
    }

    @Override
    public void getAepsReport(ArrayList<AepsReportPojo> dayReport) {
        activityAepsReportBinding.listView.setAdapter(null);
        activityAepsReportBinding.listView.setVisibility(View.VISIBLE);
        activityAepsReportBinding.imgDataNotFound.setVisibility(View.GONE);
        adapter = new AepsReportAdapter(context, AepsReportActivity.this, dayReport);
        activityAepsReportBinding.listView.setAdapter(adapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setDateDefaultValue() {

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        strToDate = date;
        strFromDate = date;

        String[] splitDate = date.split("/");
        fDate = Integer.parseInt(splitDate[0]);
        tDate = Integer.parseInt(splitDate[0]);
        fMonth = Integer.parseInt(splitDate[1]);
        tMonth = Integer.parseInt(splitDate[1]);
        fYear = Integer.parseInt(splitDate[2]);
        tYear = Integer.parseInt(splitDate[2]);

        activityAepsReportBinding.txtToDate.setText(date);
        activityAepsReportBinding.txtToDate.setError(null);

        activityAepsReportBinding.txtFromDate.setText(date);
        activityAepsReportBinding.txtFromDate.setError(null);

    }


}
