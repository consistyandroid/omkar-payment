package com.recharge.aepsCon.aepsReport;

import com.recharge.aepsCon.json.AepsReportPojo;

import java.util.ArrayList;

public interface IAepsReportPresenter {
    void validateDate();

    void showDialog();

    void dismissDialog();

    void successAlert(String msg);

    void errorAlert(String msg);

    void getAepsReport(ArrayList<AepsReportPojo> dayReport);

    void AEPSReportResponse(String encryptedResponse);

    void AEPSReportFailResponse(String encryptedResponse);

}
