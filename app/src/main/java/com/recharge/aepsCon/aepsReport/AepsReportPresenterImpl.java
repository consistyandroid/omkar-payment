package com.recharge.aepsCon.aepsReport;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.aepsCon.json.AepsReportPojo;
import com.recharge.aepsCon.model.AEPSReportModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;

public class AepsReportPresenterImpl implements IAepsReportPresenter  {
    IAepsReportView iAepsReportView;
    LogWriter log = new LogWriter();

    public AepsReportPresenterImpl(IAepsReportView iAepsReportView) {
        this.iAepsReportView = iAepsReportView;
    }

    @Override
    public void validateDate() {
        if (iAepsReportView.strFromDate().equals("") || iAepsReportView.strFromDate().isEmpty()) {
            iAepsReportView.editFromDateError();
            return;
        }
        if (iAepsReportView.strToDate().equals("") || iAepsReportView.strToDate().isEmpty()) {
            iAepsReportView.editToDateError();
            return;
        }
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject dayreportrequest = new JSONObject();
        try {

            dayreportrequest.put("SelectedUserID", iAepsReportView.getReceiverID());
            dayreportrequest.put("Date", iAepsReportView.strFromDate());
            dayreportrequest.put("ToDate", iAepsReportView.strToDate());

            String jsonrequest = dayreportrequest.toString();
            log.i("Report Request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);

            if (!encryptString.equals("")) {
                AEPSReportModel model = new AEPSReportModel();
                model.iaepsModel = this;
                model.getAEPSTransactionReport(encryptString, keyData);
                showDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showDialog() {
iAepsReportView.showPDialog();
    }

    @Override
    public void dismissDialog() {
iAepsReportView.dismissPDialog();
    }

    @Override
    public void successAlert(String msg) {
        iAepsReportView.successAlert(msg);
    }

    @Override
    public void errorAlert(String msg) {
        iAepsReportView.errorAlert(msg);
    }

    @Override
    public void getAepsReport(ArrayList<AepsReportPojo> dayReport) {

    }

    @Override
    public void AEPSReportResponse(String encryptedResponse) {
        iAepsReportView.dismissPDialog();
        Cryptography_Android data = new Cryptography_Android();
        Gson gson = new Gson();
        try {
            String body = data.Decrypt(encryptedResponse);
            Log.i("AEPS report", body);

            ArrayList<AepsReportPojo> aepsReport = gson.fromJson(body, new TypeToken<ArrayList<AepsReportPojo>>() {
            }.getType());

            if (aepsReport != null) {
                iAepsReportView.getAepsReport(aepsReport);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void AEPSReportFailResponse(String encryptedResponse) {
        iAepsReportView.dismissPDialog();
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(encryptedResponse);
            Log.i("Decrept response", body);
            iAepsReportView.errorAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
