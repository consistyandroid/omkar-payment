package com.recharge.aepsCon.aepsReport;

import com.recharge.aepsCon.json.AepsReportPojo;

import java.util.ArrayList;

public interface IAepsReportView {
    String strFromDate();

    String strToDate();

    String getReceiverID();

    String SelectedUserID();

    void editFromDateError();

    void editToDateError();

    void showPDialog();

    void dismissPDialog();

    void errorAlert(String msg);

    void successAlert(String msg);

    void getAepsReport(ArrayList<AepsReportPojo> dayReport);
}
