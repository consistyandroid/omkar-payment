package com.recharge.aepsCon;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.minkspay.minkspayaepslibrary.MinkspayRegisterActivity;

import com.recharge.aepsCon.json.AEPSTransactionInitFailPojo;


import java.util.ArrayList;
import java.util.Locale;


import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.currentLocationService.GetCurrentLocationService;
import in.omkarpayment.app.currentLocationService.IGetCurrentLocationService;
import in.omkarpayment.app.currentLocationService.LocationPojo;
import in.omkarpayment.app.databinding.FragmentAepsBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;

import static android.app.Activity.RESULT_OK;

public class AEPSFragment extends Fragment implements IAEPSView, IAvailableBalanceView, IGetCurrentLocationService {

    FragmentAepsBinding fragmentAepsBinding;
    View rowView;
    AlertImpl alert;
    CustomProgressDialog pDialog;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    NetworkState ns = new NetworkState();
    int AEPSRegistrationRequestCode = 4;
    IAEPSPresenter iaepsPresenter;
    AEPSInformation aepsInformation;
    Context context;
    GetCurrentLocationService getCurrentLocationService;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentAepsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_aeps, container, false);
        rowView = fragmentAepsBinding.getRoot();

        context = this.getContext();

        alert = new AlertImpl(getContext());
        pDialog = new CustomProgressDialog(getContext());
        iaepsPresenter = new AEPSPresenter(this);
        aepsInformation = new AEPSInformation();
        getCurrentLocationService = new GetCurrentLocationService(context, this);
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);

        iAvailableBalancePresenter.getAvailableBalance();
//
//        fragmentAepsBinding.btnMakeAEPSTxn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!ns.isInternetAvailable(getContext())) {
//                    Toast.makeText(getContext(), AllMessages.internetError, Toast.LENGTH_LONG).show();
//                    return;
//                }
//                getCurrentLocationService.getLocation();
//            }
//        });

        return rowView;
    }


    @Override
    public void errorAlert(String message) {
        dismissPDialog();
        alert.errorAlert(message);
    }

    private void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
    }

    @Override
    public void initAEPSTransaction() {
        dismissPDialog();
        if (!ns.isInternetAvailable(getContext())) {
            Snackbar.make(getActivity().getWindow().getDecorView(), AllMessages.internetError, Snackbar.LENGTH_SHORT).show();
            return;
        }
        SendToAEPSSDK();
    }

    @Override
    public void getAEPSTransactionId() {

    }

    @Override
    public String getAmount() {
        return null;
    }

    @Override
    public Integer BalnceType() {
        return null;
    }

    @Override
    public void edtAmountError() {

    }

    @Override
    public void showActivateAepsAlert(final AEPSTransactionInitFailPojo aepsTransactionInitFailPojo) {
        dismissPDialog();

        String message = "Dear " + UserDetails.Username + ", your AEPS service is disabled. To enable your AEPS service you need to pay service charge of ₹ " +
                String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(aepsTransactionInitFailPojo.getCharge()))) +
                "\n Do you want to enable your AEPS service?";

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        Button btnNo = dialog.findViewById(R.id.btnNo);

        ImageView btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnNo.setText("Cancel");
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogButton.setText("Enable");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (aepsTransactionInitFailPojo.getCharge() > UserDetails.UserBalance) {
                    alert.errorAlert("Your current balance is low to proceed. Please update your balance and try again.");
                    return;
                }
                pDialog.showPDialog();
                iaepsPresenter.enableAepsService();
            }
        });
        dialog.show();
    }

    @Override
    public void enableAepsServiceSuccess(String message) {
        dismissPDialog();

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!ns.isInternetAvailable(getContext())) {
                    Toast.makeText(getContext(), AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                pDialog.showPDialog();
                getCurrentLocationService.getLocation();
            }
        });
        dialog.show();
    }

    private void SendToAEPSSDK() {

        Log.i("SendToAEPSSDK Lat===", String.valueOf(UserDetails.currentLocationPojo.getLatitude()));
        Log.i("SendToAEPSSDK Long===", String.valueOf(UserDetails.currentLocationPojo.getLongitude()));


        Intent intent = new Intent(getContext(), MinkspayRegisterActivity.class);
        intent.putExtra("developer_key", "cWYxM3NoS1l2M0NFQnBEckFwd2l3b09IZmU0WmxVREFWaTFqVnFORU9URHZmYlJmckxwSm1nL2Y5Y1pzVnN3SXQvOHZhdFZ4Wm9ZVGw2cmZ4ZmNVT1E9PQ==");
        intent.putExtra("developer_pass", "$2y$10$LP7XvYzUk9Cfm9NFcum2BOTvYjGXEOmR/4/DVnkmvhRLVtqbYKZ");
        intent.putExtra("retailer_mobile_number", UserDetails.MobileNumber); //"7588215033");
        intent.putExtra("partner_agent_id", aepsInformation.getAepsTransactionIdDetailsPojo().getUniqueRefNo()); //"53544444445");
        intent.putExtra("txn_req_id", aepsInformation.getAepsTransactionIdDetailsPojo().getRequestId());
        intent.putExtra("actionbar_title", getResources().getString(R.string.app_name));
        intent.putExtra("lattitude", UserDetails.currentLocationPojo.getLatitude());
        intent.putExtra("longitude", UserDetails.currentLocationPojo.getLongitude());
        intent.putExtra("imei", UserDetails.IMEI);
        startActivityForResult(intent, AEPSRegistrationRequestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AEPSRegistrationRequestCode && resultCode == RESULT_OK) {
            String message = data.getStringExtra("message");
            boolean status = data.getBooleanExtra("status", false);
            String transType = data.getStringExtra("transType");
            String bankRrn = data.getStringExtra("bankRrn");
            Double balAmount = data.getDoubleExtra("balAmount", 0.0);
            Double transAmount = data.getDoubleExtra("transAmount", 0.0);
            showTransactionResponse(message, status, transType, bankRrn, balAmount, transAmount);
        }
    }

    private void showTransactionResponse(String message, boolean status, String transType, String bankRrn, Double balAmount, Double transAmount) {
        Validation validation = new Validation();
        String displayMessage = "", txnStatus = "";

        if (!validation.isNullOrEmpty(message)) {
            message = "";
        }
        if (!validation.isNullOrEmpty(transType)) {
            transType = "";
        }
        if (!validation.isNullOrEmpty(bankRrn)) {
            bankRrn = "";
        }

        String transactionAmount = String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(transAmount)));
        String balanceAmount = String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(balAmount)));

        if (status) {
            txnStatus = "SUCCESS";
            displayMessage = "Tx Status : " + txnStatus + "\nMessage : " + message + "\n\nTx Amount : " + transactionAmount +
                    "\nTxn Type : " + transType + "\nBal Amount : " + balanceAmount + "\nBank Ref No : " + bankRrn;
        } else {
            txnStatus = "FAIL";
            displayMessage = "Tx Status : " + txnStatus + "\nMessage : " + message;
        }
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custome_pops);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(displayMessage);
        final ImageView btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        iAvailableBalancePresenter.getAvailableBalance();
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {

        for (AvailableBalancePojo pojo : lastrasaction) {
            try {
                UserDetails.UserBalance = Double.parseDouble(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getCurrentBalance()))));
              //  UserDetails.DMRBalance = Double.parseDouble(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getDMRBalance()))));
                ((MainActivity) getActivity()).updateBal();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }

    @Override
    public void GetDeviceCurrentLocation(LocationPojo currentLocationPojo) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        Log.i("location ===", currentLocationPojo.getAddress());
        Log.i("locationLat ===", String.valueOf(currentLocationPojo.getLatitude()));
        Log.i("locationLong ===", String.valueOf(currentLocationPojo.getLongitude()));

        UserDetails.currentLocationPojo = currentLocationPojo;
        if (!ns.isInternetAvailable(getContext())) {
            Toast.makeText(getContext(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        pDialog.showPDialog();
        iaepsPresenter.getAEPSTransactionId();
    }

    @Override
    public void showPDialog() {
        if (pDialog != null) {
            pDialog.showPDialog();
        }
    }
}