package com.recharge.aepsCon.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AepsReportPojo {
    @SerializedName("AEPSTransactionID")
    @Expose
    private Integer aEPSTransactionID;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("STATUS")
    @Expose
    private String sTATUS;
    @SerializedName("OpeningBalance")
    @Expose
    private Double openingBalance;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("NetAmount")
    @Expose
    private Double netAmount;
    @SerializedName("ClosingBalance")
    @Expose
    private Double closingBalance;
    @SerializedName("RetailerCommission")
    @Expose
    private Double retailerCommission;
    @SerializedName("DistributerCommission")
    @Expose
    private Double distributerCommission;
    @SerializedName("SuperDistribiterCommission")
    @Expose
    private Double superDistribiterCommission;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("UpdatedDOC")
    @Expose
    private String updatedDOC;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("BankRRN")
    @Expose
    private String bankRRN;
    @SerializedName("IpAddress")
    @Expose
    private String ipAddress;

    public Integer getAEPSTransactionID() {
        return aEPSTransactionID;
    }

    public void setAEPSTransactionID(Integer aEPSTransactionID) {
        this.aEPSTransactionID = aEPSTransactionID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public Double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(Double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Double getRetailerCommission() {
        return retailerCommission;
    }

    public void setRetailerCommission(Double retailerCommission) {
        this.retailerCommission = retailerCommission;
    }

    public Double getDistributerCommission() {
        return distributerCommission;
    }

    public void setDistributerCommission(Double distributerCommission) {
        this.distributerCommission = distributerCommission;
    }

    public Double getSuperDistribiterCommission() {
        return superDistribiterCommission;
    }

    public void setSuperDistribiterCommission(Double superDistribiterCommission) {
        this.superDistribiterCommission = superDistribiterCommission;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getUpdatedDOC() {
        return updatedDOC;
    }

    public void setUpdatedDOC(String updatedDOC) {
        this.updatedDOC = updatedDOC;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBankRRN() {
        return bankRRN;
    }

    public void setBankRRN(String bankRRN) {
        this.bankRRN = bankRRN;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
