package com.recharge.aepsCon.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AepsTransactionPojo {

    @SerializedName("transactionAmount")
    @Expose
    private String transactionAmount;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("transactionNO")
    @Expose
    private String transactionNO;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("uidNo")
    @Expose
    private String uidNo;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("stanNo")
    @Expose
    private String stanNo;
    @SerializedName("bankrefrenceNo")
    @Expose
    private String bankrefrenceNo;

    public String getTransactionAmount() {
        return transactionAmount == null ? "" : transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount == null ? "" : transactionAmount;
    }

    public String getTransactionId() {
        return transactionId == null ? "" : transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId == null ? "" : transactionId;
    }

    public String getTransactionNO() {
        return transactionNO == null ? "" : transactionNO;
    }

    public void setTransactionNO(String transactionNO) {
        this.transactionNO = transactionNO == null ? "" : transactionNO;
    }

    public String getBankName() {
        return bankName == null ? "" : bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? "" : bankName;
    }

    public String getUidNo() {
        return uidNo == null ? "" : uidNo;
    }

    public void setUidNo(String uidNo) {
        this.uidNo = uidNo == null ? "" : uidNo;
    }

    public String getStatus() {
        return status == null ? "" : status;
    }

    public void setStatus(String status) {
        this.status = status == null ? "" : status;
    }

    public String getService() {
        return service == null ? "" : service;
    }

    public void setService(String service) {
        this.service = service == null ? "" : service;
    }

    public String getStanNo() {
        return stanNo == null ? "" : stanNo;
    }

    public void setStanNo(String stanNo) {
        this.stanNo = stanNo == null ? "" : stanNo;
    }

    public String getBankrefrenceNo() {
        return bankrefrenceNo == null ? "" : bankrefrenceNo;
    }

    public void setBankrefrenceNo(String bankrefrenceNo) {
        this.bankrefrenceNo = bankrefrenceNo == null ? "" : bankrefrenceNo;
    }

}