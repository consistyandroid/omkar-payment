package com.recharge.aepsCon.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AEPSTransactionIdDetailsPojo {

    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("RequestId")
    @Expose
    private String requestId;
    @SerializedName("UniqueRefNo")
    @Expose
    private String uniqueRefNo;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUniqueRefNo() {
        return uniqueRefNo;
    }

    public void setUniqueRefNo(String uniqueRefNo) {
        this.uniqueRefNo = uniqueRefNo;
    }

}