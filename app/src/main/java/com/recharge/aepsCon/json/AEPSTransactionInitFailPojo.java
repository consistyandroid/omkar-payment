package com.recharge.aepsCon.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AEPSTransactionInitFailPojo {

    @SerializedName("IsServiceActive")
    @Expose
    private Boolean isServiceActive;
    @SerializedName("Charge")
    @Expose
    private Double charge;
    @SerializedName("Message")
    @Expose
    private String message;

    public Boolean getIsServiceActive() {
        return isServiceActive == null ? false : isServiceActive;
    }

    public void setIsServiceActive(Boolean isServiceActive) {
        this.isServiceActive = isServiceActive == null ? false : isServiceActive;
    }

    public Double getCharge() {
        return charge == null ? 0.00 : charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge == null ? 0.00 : charge;
    }

    public String getMessage() {
        return message == null ? "" : message;
    }

    public void setMessage(String message) {
        this.message = message == null ? "" : message;
    }

}