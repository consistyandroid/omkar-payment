package com.recharge.aepsCon;

import com.recharge.aepsCon.json.AEPSTransactionIdDetailsPojo;

public class AEPSInformation {

    private static AEPSTransactionIdDetailsPojo aepsTransactionIdDetailsPojo = new AEPSTransactionIdDetailsPojo();

    public AEPSTransactionIdDetailsPojo getAepsTransactionIdDetailsPojo() {
        return aepsTransactionIdDetailsPojo;
    }

    public void setAepsTransactionIdDetailsPojo(AEPSTransactionIdDetailsPojo aepsTransactionIdDetails) {
        aepsTransactionIdDetailsPojo = aepsTransactionIdDetails;
    }


}