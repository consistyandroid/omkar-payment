package com.recharge.aepsCon.model;


import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.IWebServicesModel;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AEPSModel {

    private LogWriter log = new LogWriter();
    public IAEPSModel iaepsModel = null;

    public void getAEPSTransactionId(String encryptString, String keyData) {

        IWebServicesModel webObject = IWebServicesModel.aepsRetrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.getAEPSTransactionId(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {

                    if (response.code() != 200) {
                        iaepsModel.errorAlert(AllMessages.dataNot);
                        return;
                    }
                    if (response.body().getBody().equals("")) {
                        log.i("onResponseBodyEmpty", "Empty Response Body");
                        iaepsModel.errorAlert(AllMessages.unableToFetchData);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        log.i("onResponseBody", response.body().getBody());
                        iaepsModel.getAEPSTransactionIdResponse(response.body().getBody());
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("FAILURE")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        iaepsModel.getAEPSTransactionIdFailResponse(response.body().getBody());
                        return;
                    }
                    log.i("onResponseBodyElse", response.body().getBody());
                    iaepsModel.errorAlert(AllMessages.unableToFetchData);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.i("onResponseException", "onResponseException\nStatus : " + response.body().getStatus() +
                            "\nBody : " + response.body().getBody());
                    iaepsModel.errorAlert(AllMessages.unableToFetchData);
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                log.i("onFailure", t.getMessage());
                iaepsModel.errorAlert(AllMessages.unableToFetchData);
            }
        });
    }

    public void AEPSTransactionReport(String encryptString, String keyData) {

        IWebServicesModel webObject = IWebServicesModel.retrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.AEPSTransactionReport(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {

                    if (response.code() != 200) {
                        iaepsModel.errorAlert(AllMessages.dataNot);
                        return;
                    }
                    if (response.body().getBody().equals("")) {
                        log.i("onResponseBodyEmpty", "Empty Response Body");
                        iaepsModel.errorAlert(AllMessages.unableToFetchData);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        log.i("onResponseBody", response.body().getBody());
                        iaepsModel.AEPSTransactionReportResponse(response.body().getBody());
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("FAILURE")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        iaepsModel.AEPSTransactionReportFailResponse(response.body().getBody());
                        return;
                    }
                    log.i("onResponseBodyElse", response.body().getBody());
                    iaepsModel.errorAlert(AllMessages.unableToFetchData);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.i("onResponseException", "onResponseException\nStatus : " + response.body().getStatus() +
                            "\nBody : " + response.body().getBody());
                    iaepsModel.errorAlert(AllMessages.unableToFetchData);
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                log.i("onFailure", t.getMessage());
                iaepsModel.errorAlert(AllMessages.unableToFetchData);
            }
        });
    }

    public void enableUserService(String encryptString, String keyData) {

        IWebServicesModel webObject = IWebServicesModel.aepsRetrofit.create(IWebServicesModel.class);

        Call<WebserviceResponsePojo> call = webObject.enableUserService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {

                    if (response.code() != 200) {
                        iaepsModel.errorAlert(AllMessages.dataNot);
                        return;
                    }
                    if (response.body().getBody().equals("")) {
                        log.i("onResponseBodyEmpty", "Empty Response Body");
                        iaepsModel.errorAlert(AllMessages.unableToFetchData);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        log.i("onResponseBody", response.body().getBody());
                        iaepsModel.enableUserServiceResponse(response.body().getBody());
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("FAILURE")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        iaepsModel.enableUserServiceFailResponse(response.body().getBody());
                        return;
                    }
                    log.i("onResponseBodyElse", response.body().getBody());
                    iaepsModel.errorAlert(AllMessages.unableToFetchData);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.i("onResponseException", "onResponseException\nStatus : " + response.body().getStatus() +
                            "\nBody : " + response.body().getBody());
                    iaepsModel.errorAlert(AllMessages.unableToFetchData);
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                log.i("onFailure", t.getMessage());
                iaepsModel.errorAlert(AllMessages.unableToFetchData);
            }
        });
    }


}