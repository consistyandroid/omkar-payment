package com.recharge.aepsCon.model;

public interface IAEPSModel {

    void errorAlert(String response);

    void getAEPSTransactionIdResponse(String response);

    void getAEPSTransactionIdFailResponse(String response);

    void AEPSTransactionReportResponse(String response);

    void AEPSTransactionReportFailResponse(String response);

    void enableUserServiceResponse(String body);

    void enableUserServiceFailResponse(String body);

}