package com.recharge.aepsCon;

public interface IAEPSPresenter {
    void validateAEPSTransactionId();

    void getAEPSTransactionId();
    void validateAEPSCashEnquiry();
    void enableAepsService();

}