package com.recharge.aepsCon;

import com.recharge.aepsCon.json.AEPSTransactionInitFailPojo;

public interface IAEPSView {

    void errorAlert(String message);

    void initAEPSTransaction();

    void getAEPSTransactionId();

    String getAmount();

    Integer BalnceType();

    void edtAmountError();

    void showActivateAepsAlert(AEPSTransactionInitFailPojo aepsTransactionInitFailPojo);

    void enableAepsServiceSuccess(String message);


}