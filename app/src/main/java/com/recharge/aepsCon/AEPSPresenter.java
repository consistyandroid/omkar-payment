package com.recharge.aepsCon;

import com.google.gson.Gson;
import com.recharge.aepsCon.json.AEPSTransactionIdDetailsPojo;
import com.recharge.aepsCon.json.AEPSTransactionInitFailPojo;
import com.recharge.aepsCon.model.AEPSModel;
import com.recharge.aepsCon.model.IAEPSModel;


import org.json.JSONObject;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;

public class AEPSPresenter implements IAEPSPresenter, IAEPSModel {

    private LogWriter log = new LogWriter();
    private Validation validation = new Validation();
    private IAEPSView iAepsView;

    public AEPSPresenter(IAEPSView iAepsView) {
        this.iAepsView = iAepsView;
    }

    @Override
    public void AEPSTransactionReportResponse(String response) {

    }

    @Override
    public void AEPSTransactionReportFailResponse(String response) {

    }

    @Override
    public void errorAlert(String response) {
        iAepsView.errorAlert(response);
    }

    @Override
    public void validateAEPSTransactionId() {
        if (!validation.isNullOrEmpty(iAepsView.getAmount())) {
            iAepsView.edtAmountError();
            return;
        }
        double amt = Double.parseDouble(iAepsView.getAmount());
        if (amt < 10 && amt != 0) {
            iAepsView.edtAmountError();
            return;
        }
        iAepsView.getAEPSTransactionId();
    }

    @Override
    public void getAEPSTransactionId() {
        try {
            JSONObject getAEPSTransactionIdRequest = new JSONObject();
            getAEPSTransactionIdRequest.put("Amount", iAepsView.getAmount());

            String jsonRequest = getAEPSTransactionIdRequest.toString();
            log.i("get AEPS TransactionId Request", jsonRequest);
            String encryptedString = new Cryptography_Android().Encrypt(jsonRequest);
            log.i("get AEPS TransactionId Request", encryptedString);

            if (!validation.isNullOrEmpty(encryptedString)) {
                errorAlert(AllMessages.unableToFetchData);
                return;
            }

            AEPSModel model = new AEPSModel();
            model.iaepsModel = this;
            model.getAEPSTransactionId(encryptedString, new KeyDataReader().get().trim());
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert("Unable to fetch/receive data from server");
        }
    }

    @Override
    public void validateAEPSCashEnquiry() {
        iAepsView.getAEPSTransactionId();
    }

    @Override
    public void getAEPSTransactionIdResponse(String response) {
        try {
            log.i("AEPSTransactionId Success Response", response);
            String body = new Cryptography_Android().Decrypt(response);
            log.i("AEPSTransactionId Success Response", body);

            if (!validation.isNullOrEmpty(body)) {
                errorAlert("Unable to fetch/receive data from server");
                return;
            }

            AEPSTransactionIdDetailsPojo detailsPojo = new Gson().fromJson(body, AEPSTransactionIdDetailsPojo.class);
            AEPSInformation aepsInformation = new AEPSInformation();
            aepsInformation.setAepsTransactionIdDetailsPojo(detailsPojo);

            AEPSTransactionIdDetailsPojo transacationIdDetailsPojo = aepsInformation.getAepsTransactionIdDetailsPojo();
            if (transacationIdDetailsPojo == null) {
                errorAlert("Unable to init AEPS transaction");
                return;
            }
            if (transacationIdDetailsPojo.getUserId() <= 0) {
                errorAlert("Unable to init AEPS transaction");
                return;
            }
            if (!validation.isNullOrEmpty(transacationIdDetailsPojo.getRequestId())) {
                errorAlert("Unable to init AEPS transaction");
                return;
            }
            if (!validation.isNullOrEmpty(transacationIdDetailsPojo.getUniqueRefNo())) {
                errorAlert("Unable to init AEPS transaction");
                return;
            }
            iAepsView.initAEPSTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert("Unable to fetch/receive data from server");
        }
    }

    @Override
    public void getAEPSTransactionIdFailResponse(String response) {
        try {
            log.i("AEPSTransactionId Fail Response", response);
            String body = new Cryptography_Android().Decrypt(response);
            log.i("AEPSTransactionId Fail Response", body);

            if (!validation.isNullOrEmpty(body)) {
                errorAlert(AllMessages.unableToFetchData);
                return;
            }

            AEPSTransactionInitFailPojo aepsTransactionInitFailPojo = new Gson().fromJson(body, AEPSTransactionInitFailPojo.class);
            if (body == null) {
                errorAlert(AllMessages.unableToFetchData);
                return;
            }

            if (aepsTransactionInitFailPojo.getIsServiceActive()) {
                if (validation.isNullOrEmpty(aepsTransactionInitFailPojo.getMessage())) {
                    errorAlert(aepsTransactionInitFailPojo.getMessage());
                    return;
                }
                errorAlert(AllMessages.unableToFetchData);
                return;
            }
            iAepsView.showActivateAepsAlert(aepsTransactionInitFailPojo);
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert("Unable to fetch/receive data from server");
        }
    }

    @Override
    public void enableAepsService() {
        try {
            JSONObject enableAepsServiceRequest = new JSONObject();
            enableAepsServiceRequest.put("IsActive", true);
            enableAepsServiceRequest.put("ServiceID", 22);
            enableAepsServiceRequest.put("UserID", UserDetails.UserId);

            String jsonRequest = enableAepsServiceRequest.toString();
            log.i("enable aeps service req", jsonRequest);
            String encryptedString = new Cryptography_Android().Encrypt(jsonRequest);
            log.i("enable aeps service req", encryptedString);

            if (!validation.isNullOrEmpty(encryptedString)) {
                errorAlert(AllMessages.unableToFetchData);
                return;
            }

            AEPSModel model = new AEPSModel();
            model.iaepsModel = this;
            model.enableUserService(encryptedString, new KeyDataReader().get().trim());
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert(AllMessages.unableToFetchData);
        }
    }

    @Override
    public void enableUserServiceResponse(String response) {
        try {
            log.i("enableUserService success", response);
            String body = new Cryptography_Android().Decrypt(response);
            log.i("enableUserService success", body);

            if (!validation.isNullOrEmpty(body)) {
                errorAlert(AllMessages.unableToFetchData);
                return;
            }

            iAepsView.enableAepsServiceSuccess(body);
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert(AllMessages.unableToFetchData);
        }
    }

    @Override
    public void enableUserServiceFailResponse(String response) {
        try {
            log.i("enableUserService success", response);
            String body = new Cryptography_Android().Decrypt(response);
            log.i("enableUserService success", body);

            if (!validation.isNullOrEmpty(body)) {
                errorAlert(AllMessages.unableToFetchData);
                return;
            }

            errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert(AllMessages.unableToFetchData);
        }
    }

}