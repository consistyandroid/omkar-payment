package com.recharge.aepsCon;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.snackbar.Snackbar;
import com.minkspay.minkspayaepslibrary.MinkspayRegisterActivity;

import com.recharge.aepsCon.json.AEPSTransactionInitFailPojo;


import java.util.ArrayList;
import java.util.Locale;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.currentLocationService.GetCurrentLocationService;
import in.omkarpayment.app.currentLocationService.IGetCurrentLocationService;
import in.omkarpayment.app.currentLocationService.LocationPojo;
import in.omkarpayment.app.databinding.FragmentAepsBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;


public class AEPSActivity extends AppCompatActivity implements IAEPSView, IAvailableBalanceView, IGetCurrentLocationService {

    FragmentAepsBinding fragmentAepsBinding;
    AlertImpl alert;
    Context context = this;
    CustomProgressDialog pDialog;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    NetworkState ns = new NetworkState();
    int AEPSRegistrationRequestCode = 6;
    TextView txtToolbarBalance;
    IAEPSPresenter iaepsPresenter;
    AEPSInformation aepsInformation = new AEPSInformation();
    GetCurrentLocationService getCurrentLocationService;
    Integer BalanceType;
    String Amount = "0";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentAepsBinding = DataBindingUtil.setContentView(this, R.layout.fragment_aeps);

        alert = new AlertImpl(context);
        pDialog = new CustomProgressDialog(context);
        iaepsPresenter = new AEPSPresenter(this);
        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        getCurrentLocationService = new GetCurrentLocationService(context, this);

//        txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
//        txtToolbarBalance.setText("₹ " + String.format("%.2f", UserDetails.UserBalance) + " :  RCH \n"
//                + "₹ " + String.format("%.2f", UserDetails.DMRBalance) + " : DMT");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "AEPS Transaction";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
//        TextView txtHeader = findViewById(R.id.txtHeader);
//        txtHeader.setText(title);

        iAvailableBalancePresenter.getAvailableBalance();

        fragmentAepsBinding.btnBalanceEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Amount="0";
                fragmentAepsBinding.txtNote.setVisibility(View.GONE);
                fragmentAepsBinding.editAmount.setVisibility(View.GONE);
                BalanceType = 4;
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                iaepsPresenter.validateAEPSCashEnquiry();
            }
        });

        fragmentAepsBinding.btnCashWithDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Amount = fragmentAepsBinding.editAmount.getText().toString().trim();
                BalanceType = 2;
                fragmentAepsBinding.editAmount.setVisibility(View.VISIBLE);
                fragmentAepsBinding.txtNote.setVisibility(View.VISIBLE);
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                iaepsPresenter.validateAEPSTransactionId();
            }
        });

    }

    @Override
    public void errorAlert(String message) {
        dismissPDialog();
        alert.errorAlert(message);
    }

    private void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
    }

    @Override
    public void initAEPSTransaction() {
        dismissPDialog();
        if (!ns.isInternetAvailable(context)) {
            Snackbar.make(getWindow().getDecorView(), AllMessages.internetError, Snackbar.LENGTH_SHORT).show();
            return;
        }
        SendToAEPSSDK();
    }


    @Override
    public void getAEPSTransactionId() {
        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        getCurrentLocationService.getLocation();
    }

    @Override
    public String getAmount() {
        return Amount;
    }

    public boolean amountValidation() {
        Integer amount = Integer.valueOf(getAmount());
        if (amount < 10) {
            fragmentAepsBinding.editAmount.setError("Enter amount greater than 10");
            fragmentAepsBinding.editAmount.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public Integer BalnceType() {
        return BalanceType;
    }

    @Override
    public void edtAmountError() {
        fragmentAepsBinding.editAmount.setError("Please enter valid amount");
        fragmentAepsBinding.editAmount.requestFocus();
    }

    @Override
    public void showActivateAepsAlert(final AEPSTransactionInitFailPojo aepsTransactionInitFailPojo) {
        dismissPDialog();

        String message = "Dear " + UserDetails.Username + ", your AEPS service is disabled. To enable your AEPS service you need to pay service charge of ₹ " +
                String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(aepsTransactionInitFailPojo.getCharge()))) +
                "\n Do you want to enable your AEPS service?";

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.warning_dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        Button btnNo = dialog.findViewById(R.id.btnNo);

//        ImageView btnClose = dialog.findViewById(R.id.btnClose);
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });

        btnNo.setText("Cancel");
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogButton.setText("Enable");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (aepsTransactionInitFailPojo.getCharge() > UserDetails.UserBalance) {
                    alert.errorAlert("Your current balance is low to proceed. Please update your balance and try again.");
                    return;
                }
                pDialog.showPDialog();
                iaepsPresenter.enableAepsService();
            }
        });
        dialog.show();
    }

    @Override
    public void enableAepsServiceSuccess(String message) {
        dismissPDialog();

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                pDialog.showPDialog();
                getCurrentLocationService.getLocation();
            }
        });
        dialog.show();
    }

    private void SendToAEPSSDK() {

        Log.i("SendToAEPSSDK Lat===", String.valueOf(UserDetails.currentLocationPojo.getLatitude()));
        Log.i("SendToAEPSSDK Long===", String.valueOf(UserDetails.currentLocationPojo.getLongitude()));

        Intent intent = new Intent(context, MinkspayRegisterActivity.class);
        intent.putExtra("developer_key", "cWYxM3NoS1l2M0NFQnBEckFwd2l3b09IZmU0WmxVREFWaTFqVnFORU9URHZmYlJmckxwSm1nL2Y5Y1pzVnN3SXQvOHZhdFZ4Wm9ZVGw2cmZ4ZmNVT1E9PQ==");
        intent.putExtra("developer_pass", "$2y$10$LP7XvYzUk9Cfm9NFcum2BOTvYjGXEOmR/4/DVnkmvhRLVtqbYKZ..");
        intent.putExtra("retailer_mobile_number", UserDetails.MobileNumber);
        intent.putExtra("partner_agent_id", aepsInformation.getAepsTransactionIdDetailsPojo().getUniqueRefNo());
        intent.putExtra("txn_req_id", aepsInformation.getAepsTransactionIdDetailsPojo().getRequestId());
        intent.putExtra("actionbar_title", getResources().getString(R.string.app_name));
        intent.putExtra("lattitude", UserDetails.currentLocationPojo.getLatitude());
        intent.putExtra("longitude", UserDetails.currentLocationPojo.getLongitude());
        intent.putExtra("imei", UserDetails.IMEI);
        intent.putExtra("udd", "90#" + aepsInformation.getAepsTransactionIdDetailsPojo().getUniqueRefNo() + "#" + UserDetails.MobileNumber);
        intent.putExtra("amount", getAmount());
        Log.i("SendToAEPSSDK amount===", getAmount());
        intent.putExtra("amount_editable", false);
        intent.putExtra("type", BalnceType());
        Log.i("Balance Type", String.valueOf(BalanceType));
        Log.i("udd", "90#" + aepsInformation.getAepsTransactionIdDetailsPojo().getUniqueRefNo() + "#" + UserDetails.MobileNumber);

        try {
            startActivityForResult(intent, AEPSRegistrationRequestCode);
        } catch (Exception e) {
            e.printStackTrace();
            errorAlert("Unable to init APES Data");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AEPSRegistrationRequestCode && resultCode == RESULT_OK) {
            String message = data.getStringExtra("message");
            boolean status = data.getBooleanExtra("status", false);
            String transType = data.getStringExtra("transType");
            String bankRrn = data.getStringExtra("bankRrn");
            Double balAmount = data.getDoubleExtra("balAmount", 0.0);
            Double transAmount = data.getDoubleExtra("transAmount", 0.0);
            showTransactionResponse(message, status, transType, bankRrn, balAmount, transAmount);
        }
    }

    private void showTransactionResponse(String message, boolean status, String transType, String bankRrn, Double balAmount, Double transAmount) {
        Validation validation = new Validation();
        String displayMessage = "", txnStatus = "";

        if (!validation.isNullOrEmpty(message)) {
            message = "";
        }
        if (!validation.isNullOrEmpty(transType)) {
            transType = "";
        }
        if (!validation.isNullOrEmpty(bankRrn)) {
            bankRrn = "";
        }

        String transactionAmount = String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(transAmount)));
        String balanceAmount = String.format(Locale.getDefault(), "%.02f", Float.parseFloat(String.valueOf(balAmount)));

        if (status) {
            txnStatus = "SUCCESS";
            displayMessage = "Tx Status : " + txnStatus + "\nMessage : " + message + "\n\nTx Amount : " + transactionAmount +
                    "\nTxn Type : " + transType + "\nBal Amount : " + balanceAmount + "\nBank Ref No : " + bankRrn;
        } else {
            txnStatus = "FAIL";
            displayMessage = "Tx Status : " + txnStatus + "\nMessage : " + message;
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custome_pops);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(displayMessage);
        final ImageView btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        iAvailableBalancePresenter.getAvailableBalance();
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {

        for (AvailableBalancePojo pojo : lastrasaction) {
            try {
                UserDetails.UserBalance = Double.parseDouble(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getCurrentBalance()))));
                // UserDetails.DMRBalance = Double.parseDouble(String.format("%.02f", Float.parseFloat(String.valueOf(pojo.getDMRBalance()))));
                //((MainActivity)context).updateBal();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }

    @Override
    public void GetDeviceCurrentLocation(LocationPojo currentLocationPojo) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        Log.i("location ===", currentLocationPojo.getAddress());
        Log.i("locationLat ===", String.valueOf(currentLocationPojo.getLatitude()));
        Log.i("locationLong ===", String.valueOf(currentLocationPojo.getLongitude()));

        UserDetails.currentLocationPojo = currentLocationPojo;
        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        pDialog.showPDialog();
        iaepsPresenter.getAEPSTransactionId();
    }


    @Override
    public void showPDialog() {
        if (pDialog != null) {
            pDialog.showPDialog();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }
}