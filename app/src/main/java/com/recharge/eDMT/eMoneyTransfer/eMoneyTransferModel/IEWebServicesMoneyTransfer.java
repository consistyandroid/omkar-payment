package com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel;

import com.recharge.eDMT.eMoneyTransfer.JSON.MoneyTransferResponsePojo;


import java.util.concurrent.TimeUnit;

import in.omkarpayment.app.userContent.ApplicationURL;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IEWebServicesMoneyTransfer {

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(80, TimeUnit.SECONDS)
            .writeTimeout(80, TimeUnit.SECONDS)
            .readTimeout(80, TimeUnit.SECONDS)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ApplicationURL.AMoneyTransfer_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();


    @GET("AMoneyTransfer.asmx/GetSenderDetails")
    Call<MoneyTransferResponsePojo> GetSenderDetailsWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("AMoneyTransfer.asmx/SendertBeneficiaryRegistration")
    Call<MoneyTransferResponsePojo> addSenderBeneficiaryWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/BeneficiaryRegistration")
    Call<MoneyTransferResponsePojo> addBeneficiaryWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/ReSendOTP")
    Call<MoneyTransferResponsePojo> resendOTPOTPWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/VerifyRegistrationRequest")
    Call<MoneyTransferResponsePojo> verifyOTPforAddBeneficiary(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/DeleteBeneficiary")
    Call<MoneyTransferResponsePojo> deleteBeneficiaryWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/GetBanks")
    Call<MoneyTransferResponsePojo> getBanksWebservice(
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/VerifyRegistrationRequest")
    Call<MoneyTransferResponsePojo> BeneficiaryDeleteOTPVerificationWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/MoneyTransferCall")
    Call<MoneyTransferResponsePojo> TransferWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AMoneyTransfer.asmx/VerifyAccount")
    Call<MoneyTransferResponsePojo> VerifyBeneficiaryAccountWebservice(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );
}
