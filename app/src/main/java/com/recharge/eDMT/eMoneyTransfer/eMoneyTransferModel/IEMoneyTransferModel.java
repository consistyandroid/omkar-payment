package com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel;

public interface IEMoneyTransferModel {

    void getTransferResponseFail(String response);

    void getTransferResponse(String response);

    void alert(String status);
}
