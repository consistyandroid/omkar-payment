package com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel;

import android.util.Log;

import com.recharge.eDMT.eMoneyTransfer.JSON.MoneyTransferResponsePojo;


import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EMoneyTransferModel {

    public IEMoneyTransferModel ieMoneyTransferModel = null;


    public void transferWebservice(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.TransferWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    if (response.code() != 200) {
                        ieMoneyTransferModel.alert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        Log.i("onResponseBody", response.body().getBody());
                        ieMoneyTransferModel.getTransferResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") || !response.body().getBody().equals("")) {
                        Log.i("onResponseBody", response.body().getBody());
                        ieMoneyTransferModel.getTransferResponseFail(response.body().getBody());
                    } else {
                        ieMoneyTransferModel.alert(response.body().getBody());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                Log.i("onFailure", t.getMessage());
                ieMoneyTransferModel.alert(AllMessages.internetError);
            }
        });
    }
}
