package com.recharge.eDMT.eMoneyTransfer;

import com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel.EMoneyTransferModel;
import com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel.IEMoneyTransferModel;


import org.json.JSONException;
import org.json.JSONObject;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

public class eMoneyTransferPresenter implements IeMoneyTransferPresenter, IEMoneyTransferModel {

    private IeMoneyTransferView iEMoneyTransferView;

    public eMoneyTransferPresenter(IeMoneyTransferView iEMoneyTransferView) {
        this.iEMoneyTransferView = iEMoneyTransferView;
    }

    @Override
    public void makeTransfer() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject validateTransfer = new JSONObject();
        try {
            validateTransfer.put("BeneficiaryName", iEMoneyTransferView.BeneficiaryName());
            validateTransfer.put("BeneficiaryAccountNo", iEMoneyTransferView.AccountNumber());
            validateTransfer.put("IFSCCode", iEMoneyTransferView.IFSCCode());
            validateTransfer.put("BankName", iEMoneyTransferView.BankName());
            validateTransfer.put("SenderMobileNumber", iEMoneyTransferView.SenderMobileNumber());
            validateTransfer.put("BeneficiaryID", iEMoneyTransferView.BeneficiaryID());
            validateTransfer.put("SenderID", iEMoneyTransferView.SenderId());
            validateTransfer.put("SenderName", iEMoneyTransferView.SenderName());
            validateTransfer.put("BeneficiaryCode", iEMoneyTransferView.BeneficiaryCode());
            validateTransfer.put("Amount", iEMoneyTransferView.Amount());
            validateTransfer.put("TransferType", iEMoneyTransferView.TransferType());
            validateTransfer.put("Type", "IMPS");
            validateTransfer.put("AccountType", "SAVINGS");
            validateTransfer.put("IPAddress", UserDetails.IMEI);

            LogWriter log = new LogWriter();
            String jsonrequest = validateTransfer.toString();
            log.i("request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);


            if (encryptString.equals("")) {
                iEMoneyTransferView.dismissPDialog();
                return;
            }
            EMoneyTransferModel moneyTransferModel = new EMoneyTransferModel();
            moneyTransferModel.ieMoneyTransferModel = this;
            moneyTransferModel.transferWebservice(encryptString, keyData);


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void validateTransfer() {
        if (iEMoneyTransferView.Amount().isEmpty() || iEMoneyTransferView.Amount().equals("")) {
            iEMoneyTransferView.editAmountError();
            return;
        }
        Double amount;
        amount = Double.parseDouble(iEMoneyTransferView.Amount());

        if (amount > 25000 || amount < 10) {
            iEMoneyTransferView.errorAlert("Please enter amount between ₹ 10 and ₹ 25000");
            return;
        }
        if (amount > UserDetails.UserBalance) {
            iEMoneyTransferView.errorAlert(AllMessages.lowBalanceMessage);
            return;
        }
        if (iEMoneyTransferView.Amount().isEmpty() || iEMoneyTransferView.Amount().equals("")) {
            iEMoneyTransferView.editAmountError();
            return;
        }
        iEMoneyTransferView.confirmClick();
    }

    @Override
    public void getTransferResponseFail(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("getTransferresponseFail", body);
            iEMoneyTransferView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getTransferResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("getTransferresponse", body);
            iEMoneyTransferView.transferSuccess(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {
        iEMoneyTransferView.alert(status);
    }
}