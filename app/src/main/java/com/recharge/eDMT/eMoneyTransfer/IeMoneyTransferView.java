package com.recharge.eDMT.eMoneyTransfer;

public interface IeMoneyTransferView {

    String TransferType();

    String Amount();

    String AccountNumber();

    String SenderMobileNumber();

    String BeneficiaryID();

    String SenderId();

    void confirmClick();

    String BeneficiaryName();

    String SenderName();

    String BeneficiaryCode();

    String BankName();

    String IFSCCode();

    void editAmountError();

    void alert(String body);

    void errorAlert(String body);

    void transferSuccess(String response);

    void dismissPDialog();


}
