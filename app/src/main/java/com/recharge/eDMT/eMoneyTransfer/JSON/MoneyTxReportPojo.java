package com.recharge.eDMT.eMoneyTransfer.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoneyTxReportPojo {

    @SerializedName("SenderNumber")
    @Expose
    private String senderNumber;
    @SerializedName("BeneficiaryAccountNo")
    @Expose
    private String beneficiaryAccountNo;
    @SerializedName("BeneficiaryIFSCCode")
    @Expose
    private String beneficiaryIFSCCode;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("TransferType")
    @Expose
    private String transferType;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName(value = "Status", alternate = {"status", "STATUS"})
    @Expose
    private String status;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("BeneficiaryID")
    @Expose
    private Integer beneficiaryID;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("BranchName")
    @Expose
    private String branchName;
    @SerializedName("MoneyTransferID")
    @Expose
    private Integer moneyTransferID;
    @SerializedName("DeductAmount")
    @Expose
    private Double deductAmount;
    @SerializedName("ClosingBal")
    @Expose
    private Double closingBal;
    @SerializedName("BeneficiaryCode")
    @Expose
    private Integer beneficiaryCode;

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }

    public String getBeneficiaryAccountNo() {
        return beneficiaryAccountNo;
    }

    public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
        this.beneficiaryAccountNo = beneficiaryAccountNo;
    }

    public String getBeneficiaryIFSCCode() {
        return beneficiaryIFSCCode;
    }

    public void setBeneficiaryIFSCCode(String beneficiaryIFSCCode) {
        this.beneficiaryIFSCCode = beneficiaryIFSCCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public Integer getBeneficiaryID() {
        return beneficiaryID;
    }

    public void setBeneficiaryID(Integer beneficiaryID) {
        this.beneficiaryID = beneficiaryID;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Integer getMoneyTransferID() {
        return moneyTransferID;
    }

    public void setMoneyTransferID(Integer moneyTransferID) {
        this.moneyTransferID = moneyTransferID;
    }

    public Double getDeductAmount() {
        return deductAmount;
    }

    public void setDeductAmount(Double deductAmount) {
        this.deductAmount = deductAmount;
    }

    public Double getClosingBal() {
        return closingBal;
    }

    public void setClosingBal(Double closingBal) {
        this.closingBal = closingBal;
    }

    public Integer getBeneficiaryCode() {
        return beneficiaryCode;
    }

    public void setBeneficiaryCode(Integer beneficiaryCode) {
        this.beneficiaryCode = beneficiaryCode;
    }

}