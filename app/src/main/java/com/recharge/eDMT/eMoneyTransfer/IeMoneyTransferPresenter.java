package com.recharge.eDMT.eMoneyTransfer;

public interface IeMoneyTransferPresenter {
    
    void makeTransfer();

    void validateTransfer();
}
