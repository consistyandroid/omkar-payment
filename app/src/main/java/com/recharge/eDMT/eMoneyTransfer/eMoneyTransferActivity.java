package com.recharge.eDMT.eMoneyTransfer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import es.dmoral.toasty.Toasty;
import in.omkarpayment.app.NetworkUtility;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityTransferAdmrBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;


public class eMoneyTransferActivity extends AppCompatActivity implements IeMoneyTransferView {
    public static String[] MoneyTransType = {"IMPS", "NEFT"};
    String TransferType, strSenderName, strAccNumber, IFSC, BenifId, SenderNumber, SenderId, BenifName, BankName, BenifCode, SenderName, transferAmount;
    ActivityTransferAdmrBinding activityTransferEdmrBinding;
    ArrayAdapter<String> spAdapter;

    Context context = this;
    Double amount = 0.00;
    AlertImpl alert;
    boolean isDefaultIFSC;
    CustomProgressDialog pDialog;
    IeMoneyTransferPresenter iEMoneyTransferPresenter;
    NetworkUtility ns = new NetworkUtility();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityTransferEdmrBinding = DataBindingUtil.setContentView(this, R.layout.activity_transfer_admr);
        iEMoneyTransferPresenter = new eMoneyTransferPresenter(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "DMT 1";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);

        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        /*TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);

        TextView txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        txtToolbarBalance.setText("RCH : ₹ " + UserDetails.UserBalance + "\nDMR : ₹ " + UserDetails.DMRBalance);*/

        Bundle extras = getIntent().getExtras();
        strSenderName = extras.getString("SenderName");
        strAccNumber = extras.getString("AccountNo");
        IFSC = extras.getString("IFSCCode");
        BenifCode = extras.getString("BeneficiaryCode");
        BenifId = extras.getString("BeneficiaryID");
        BankName = extras.getString("BankName");
        SenderId = extras.getString("SenderID");
        SenderNumber = extras.getString("Mobile");
        SenderName = extras.getString("SenderName");
        BenifName = extras.getString("BeneficiaryName");

        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);

        activityTransferEdmrBinding.txtSenderName.setText(strSenderName);
        activityTransferEdmrBinding.txtAccNumber.setText(strAccNumber);
        activityTransferEdmrBinding.txtBankName.setText(BankName);
        activityTransferEdmrBinding.txtIFSC.setText(IFSC);


        spAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, MoneyTransType);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityTransferEdmrBinding.spAccountType.setAdapter(spAdapter);

        activityTransferEdmrBinding.edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    int num = Integer.parseInt(activityTransferEdmrBinding.edtAmount.getText().toString());
                    if (num != 0) {
                        return;
                    } else {
                        Toast.makeText(context, "Please Enter Valid Amount", Toast.LENGTH_SHORT).show();
                        activityTransferEdmrBinding.edtAmount.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
        });

        activityTransferEdmrBinding.btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ns.checkInternet(context)) {
                    Toasty.error(context, AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                iEMoneyTransferPresenter.validateTransfer();
            }
        });


        activityTransferEdmrBinding.spAccountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TransferType = MoneyTransType[position];
                if (position == 1) {
                    activityTransferEdmrBinding.txtIFSC.setEnabled(true);
                    activityTransferEdmrBinding.txtIFSC.setText("");
                } else {
                    activityTransferEdmrBinding.txtIFSC.setEnabled(false);
                    activityTransferEdmrBinding.ifscLay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }

    @Override
    public String TransferType() {
        return TransferType;
    }

    @Override
    public String Amount() {
        transferAmount = activityTransferEdmrBinding.edtAmount.getText().toString().trim();
        return transferAmount;
    }

    @Override
    public String AccountNumber() {
        return strAccNumber;
    }

    @Override
    public String SenderMobileNumber() {
        return SenderNumber;
    }

    @Override
    public String BeneficiaryID() {
        return BenifId;
    }

    @Override
    public String SenderId() {
        return SenderId;
    }

    @Override
    public String SenderName() {
        return SenderName;
    }

    @Override
    public String BeneficiaryCode() {
        return BenifCode;
    }

    @Override
    public void confirmClick() {
        amount = Double.parseDouble(Amount());
        if (amount > UserDetails.UserBalance) {
            alert.errorAlert(AllMessages.lowBalanceMessage);
        } else {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_confirmtrans);
            dialog.setCancelable(false);
            TextView a = dialog.findViewById(R.id.a);
            a.setText("Confirm Transfer");
            TextView txtName = dialog.findViewById(R.id.txtName);
            TextView txtAmount = dialog.findViewById(R.id.txtAmount);
            TextView txtAccNo = dialog.findViewById(R.id.txtAccNo);
            final TextView txtType = dialog.findViewById(R.id.txtType);
            final TextView txtBank = dialog.findViewById(R.id.txtBank);

            final Button btnYes = dialog.findViewById(R.id.btnYes);
            final Button btnNo = dialog.findViewById(R.id.btnNo);
            final ImageView btnClose = dialog.findViewById(R.id.btnClose);
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            txtName.setText(BenifName);

            txtAmount.setText(Amount());
            txtAccNo.setText(AccountNumber());
            txtType.setText(TransferType());
            txtBank.setText(BankName);
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ns.checkInternet(context)) {
                        internetError();
                        return;
                    }
                    iEMoneyTransferPresenter.makeTransfer();
                    pDialog.showPDialog();

                    dialog.dismiss();
                }
            });
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activityTransferEdmrBinding.btnTransfer.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    public String BeneficiaryName() {
        return BenifName;
    }

    public void internetError() {
        activityTransferEdmrBinding.btnTransfer.setVisibility(View.VISIBLE);
        Toasty.error(context, AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
    }


    @Override
    public String BankName() {
        return BankName;
    }

    @Override
    public String IFSCCode() {
        return IFSC;
    }

    @Override
    public void editAmountError() {
        activityTransferEdmrBinding.edtAmount.setError(String.format(AllMessages.pleaseEnter, "amount"));
        activityTransferEdmrBinding.edtAmount.requestFocus();
        activityTransferEdmrBinding.btnTransfer.setVisibility(View.VISIBLE);
    }

    @Override
    public void alert(String body) {
        pDialog.dismissPDialog();
        callIntent(body);
    }

    @Override
    public void errorAlert(String body) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        ImageView btnClose = dialog.findViewById(R.id.btnClose);
       // btnClose.setVisibility(View.VISIBLE);
        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(body);
       /* btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void transferSuccess(String response) {
        pDialog.dismissPDialog();
        callIntent(response);
    }

    @Override
    public void dismissPDialog() {
        pDialog.dismissPDialog();
    }


    private void callIntent(String body) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(body);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.putExtra("SenderMobileNumber", SenderNumber);
                setResult(3, intent);
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyPad();
        Intent intent = getIntent();
        intent.putExtra("SenderMobileNumber", SenderNumber);
        setResult(3, intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            Intent intent = getIntent();
            intent.putExtra("SenderMobileNumber", SenderNumber);
            setResult(3, intent);
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

    private void hideKeyPad() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

