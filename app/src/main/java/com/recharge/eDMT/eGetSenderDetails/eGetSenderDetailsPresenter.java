package com.recharge.eDMT.eGetSenderDetails;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddBeneficiaryOTPVerificationPojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddSenderBeneficiaryResponsePojo;
import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel.IeOTP;
import com.recharge.eDMT.eGetSenderDetails.JSON.Body;
import com.recharge.eDMT.eGetSenderDetails.JSON.EResendOTPResponsePojo;
import com.recharge.eDMT.eGetSenderDetails.JSON.EVerifyBeneficiaryAccountPojo;
import com.recharge.eDMT.eGetSenderDetails.JSON.GetSenderDetailsPojo;
import com.recharge.eDMT.eGetSenderDetails.JSON.SenderNotFoundPojo;
import com.recharge.eDMT.eGetSenderDetails.eGetSenderDetailsModel.eGetSenderDetailsModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.UserDetails;

public class eGetSenderDetailsPresenter implements IeGetSenderDetailsPresenter, IeOTP {

    private IeGetSenderDetailsView ieGetSenderDetailsView;

    public eGetSenderDetailsPresenter(IeGetSenderDetailsView iCGetSenderDetailsView) {
        this.ieGetSenderDetailsView = iCGetSenderDetailsView;
    }

    @Override
    public void validate() {
        if (ieGetSenderDetailsView.mobileNumber().isEmpty() ||
                ieGetSenderDetailsView.mobileNumber().length() < 10) {
            ieGetSenderDetailsView.editMobileNumberError();
            return;
        }
        ieGetSenderDetailsView.SearchNumber();
    }

    @Override
    public void getSenderDetails() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject loginrequest = new JSONObject();
        try {
            loginrequest.put("MobileNo", ieGetSenderDetailsView.mobileNumber());
            String jsonrequest = loginrequest.toString();
            LogWriter log = new LogWriter();
            log.i("request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }
            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IeGetSenderDetailsPresenter = this;
            model.GetSenderDetailsWebservice(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiary(Integer beneficiaryID) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject deleteBeneficiary = new JSONObject();

        try {
            deleteBeneficiary.put("SenderID", AMoneyTransferUserDetails.ASenderID);
            deleteBeneficiary.put("BeneficiaryID", beneficiaryID);
            deleteBeneficiary.put("Mobile", AMoneyTransferUserDetails.ASenderMobileNumber);
            deleteBeneficiary.put("Otp", "1234");
            deleteBeneficiary.put("Actiontype", AMoneyTransferUserDetails.AActionTypeDeleteBeneficiary);

            LogWriter log = new LogWriter();
            String jsonrequest = deleteBeneficiary.toString();
            log.i("bene delete request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }
            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IeGetSenderDetailsPresenter = this;
            model.DeleteBeneficiaryWebService(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyOTPForDeleteBeneficiary(String otp, String BeneficiaryIDForDelete) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyDeleteBeneficiaryOTP = new JSONObject();
        try {
            verifyDeleteBeneficiaryOTP.put("SenderID", AMoneyTransferUserDetails.ASenderID);
            verifyDeleteBeneficiaryOTP.put("MobileNo", AMoneyTransferUserDetails.ASenderMobileNumber);
            verifyDeleteBeneficiaryOTP.put("BeneficiaryID", BeneficiaryIDForDelete);
            verifyDeleteBeneficiaryOTP.put("OTP", otp);
            verifyDeleteBeneficiaryOTP.put("Actiontype", AMoneyTransferUserDetails.AActionTypeDeleteBeneficiary);

            LogWriter log = new LogWriter();
            String jsonrequest = verifyDeleteBeneficiaryOTP.toString();
            log.i("verifyDeleteBeneficiaryOTP request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }
            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IeGetSenderDetailsPresenter = this;
            model.verifyOTPForDeleteBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resendOTP(String BeneficiaryID) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject resendOTP = new JSONObject();
        try {
            resendOTP.put("MobileNo", AMoneyTransferUserDetails.ASenderMobileNumber);
            resendOTP.put("BeneficiaryID", BeneficiaryID);

            LogWriter log = new LogWriter();
            String jsonrequest = resendOTP.toString();
            log.i("resend OTP request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IAOTP = this;
            model.resendOTPForAddBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resendOTPForDeleteBeneficiary(String BeneficiaryID) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject resendOTP = new JSONObject();
        try {
            resendOTP.put("MobileNo", AMoneyTransferUserDetails.ASenderMobileNumber);
            resendOTP.put("BeneficiaryID", BeneficiaryID);

            LogWriter log = new LogWriter();
            String jsonrequest = resendOTP.toString();
            log.i("resend OTP request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IAOTP = this;
            model.resendOTPForDeleteBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyOTPForResendOTP(String otp, String senderID, String beneIDForResend) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addBenif = new JSONObject();
        try {
            addBenif.put("OTP", otp);
            addBenif.put("SenderID", senderID);
            addBenif.put("BeneID", beneIDForResend);

            LogWriter log = new LogWriter();
            String jsonrequest = addBenif.toString();
            log.i("resendOTP Verification request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }
            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IAOTP = this;
            model.verifyOTPForDeleteBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiaryOTPVerificationResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();

        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("deleteBeneOTPVerifyResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());

            if (!messageResponsePojo.getBody().getMessage().equalsIgnoreCase("Deleted successfully")) {
                alert("Not Verified");
                return;
            }
            ieGetSenderDetailsView.deleteBeneficiaryOTPVerificationSuccessResponse(messageResponsePojo.getBody().getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiaryOTPVerificationFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("senderDetailsFailResponse", body);

            ieGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiaryResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("DeleteBeneResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AAddSenderBeneficiaryResponsePojo deleteBeneficiaryResponsePojo = gson.fromJson(body, new TypeToken<AAddSenderBeneficiaryResponsePojo>() {
            }.getType());

            if (!deleteBeneficiaryResponsePojo.getMessage().equals("SUCCESS")) {
                ieGetSenderDetailsView.alert(deleteBeneficiaryResponsePojo.getMessage());
                return;
            }
            ieGetSenderDetailsView.deleteBeneficiarySuccessResponse(deleteBeneficiaryResponsePojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiaryFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("senderDetailsFailResponse", body);

            ieGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void senderDetailsResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("senderDetailsResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.unableToFetchError();
                return;
            }

            Gson gson = new Gson();
            SenderNotFoundPojo senderNotFoundPojo = gson.fromJson(body, SenderNotFoundPojo.class);

            if (senderNotFoundPojo.getMessage().equalsIgnoreCase("Sender Not Found")) {
                ieGetSenderDetailsView.dismissPDialog();
                ieGetSenderDetailsView.noSenderFound();
                return;
            }
            if (senderNotFoundPojo.getMessage().equalsIgnoreCase("Beneficiary Not Found")) {
                ieGetSenderDetailsView.dismissPDialog();
                ieGetSenderDetailsView.noBeneficiaryFound();
                return;
            }

            Gson gson1 = new Gson();
            GetSenderDetailsPojo getSenderDetailsPojo = gson1.fromJson(body, new TypeToken<GetSenderDetailsPojo>() {
            }.getType());

            if (getSenderDetailsPojo.getStatus().equalsIgnoreCase("SUCCESS") || getSenderDetailsPojo.getMessage().equalsIgnoreCase("SUCCESS")) {

                List<Body> benelist = getSenderDetailsPojo.getBody();
                if (!benelist.get(0).getSenderID().toString().equalsIgnoreCase("0") && !benelist.get(0).getBeneficiaryID().toString().equalsIgnoreCase("0")) {
                    ieGetSenderDetailsView.dismissPDialog();
                    ieGetSenderDetailsView.getSenderDetailsResponse(benelist);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void senderDetailsFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("senderDetailsFailResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            try {
                body = data.Decrypt(response);
                log.i("senderDetailsFailResponse", body);

                ieGetSenderDetailsView.alert(body);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorAlert(String status) {
        LogWriter log = new LogWriter();
        log.i("errorAlert", status);
        ieGetSenderDetailsView.alert(status);
    }

    @Override
    public void getAddBeneficiaryVerifyOTPResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("getVerifyOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            /*CAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body, new TypeToken<CAddBeneficiaryOTPVerificationPojo>() {
            }.getType());
            if (!messageResponsePojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                alert(messageResponsePojo.getMessage());
                return;
            }
            iCGetSenderDetailsView.resendOTPVerificationSucessAlert(messageResponsePojo.getMessage());*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("getVerifyOTPFailResponse", body);


            /*if (body == null || body.isEmpty() || body.equals("[]")) {
                iCGetSenderDetailsView.dismissPDialog();
                return;
            }

            if (body.contains("Please enter correct OTP")) {
                iCGetSenderDetailsView.resendOTPVerificationSucessAlert(body);
            } else {
                Gson gson = new Gson();
                CAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body, new TypeToken<CAddBeneficiaryOTPVerificationPojo>() {
                }.getType());
                if (!messageResponsePojo.getStatus().equalsIgnoreCase("FAIL")
                        || messageResponsePojo.getStatus().equalsIgnoreCase("FAILURE")) {
                    iCGetSenderDetailsView.dismissPDialog();
                    return;
                }
                iCGetSenderDetailsView.resendOTPVerificationSucessAlert(messageResponsePojo.getMessage());
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {
        ieGetSenderDetailsView.alert(status);
    }

    @Override
    public void getResendOTPResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo eResendOTPResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());
            if (eResendOTPResponsePojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                ieGetSenderDetailsView.resendOTPSuccessAlert(eResendOTPResponsePojo.getMessage());
                return;
            }
            ieGetSenderDetailsView.resendOTPSuccessAlert(eResendOTPResponsePojo.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getResendOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            EResendOTPResponsePojo eResendOTPResponsePojo = gson.fromJson(body, new TypeToken<EResendOTPResponsePojo>() {
            }.getType());
            log.i("resendOTPResponseFailStatus", eResendOTPResponsePojo.getStatus());
            ieGetSenderDetailsView.resendOTPSuccessAlert(eResendOTPResponsePojo.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("verrifyAccountResponse", body);
            Gson gson = new Gson();
            EVerifyBeneficiaryAccountPojo verifyBeneAccountPojo = gson.fromJson(body, new TypeToken<EVerifyBeneficiaryAccountPojo>() {
            }.getType());
            if (verifyBeneAccountPojo.getStatus() == null) {
                ieGetSenderDetailsView.alert(verifyBeneAccountPojo.getMessage());
                return;
            }
            if (!verifyBeneAccountPojo.getStatus().equals("SUCCESS")) {
                ieGetSenderDetailsView.alert(verifyBeneAccountPojo.getMessage());
                return;
            }
            ieGetSenderDetailsView.verifyBeneficiaryAccountSuccessAlert(verifyBeneAccountPojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("verifyBeneAccountFailResponse", body);
            ieGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccount(Integer beneficiaryID, Integer senderID, String beneficiaryName, String mobile, String accountNo,
                                         String bankName, String ifsc) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyBeneAccount = new JSONObject();
        try {
            verifyBeneAccount.put("BeneficiaryID", beneficiaryID);
            verifyBeneAccount.put("SenderID", senderID);
            verifyBeneAccount.put("BeneficiaryName", beneficiaryName);
            verifyBeneAccount.put("SenderMobileNumber", mobile);
            verifyBeneAccount.put("BeneficiaryAccountNo", accountNo);
            verifyBeneAccount.put("BankName", bankName);
            verifyBeneAccount.put("Amount", 1);
            verifyBeneAccount.put("IFSCCode", ifsc);
            verifyBeneAccount.put("Type", "IMPS");
            verifyBeneAccount.put("AccountType", "SAVING");
            verifyBeneAccount.put("IPAddress", UserDetails.IMEI);
            verifyBeneAccount.put("Through", "Android");
            verifyBeneAccount.put("UserID", UserDetails.UserId);

            LogWriter log = new LogWriter();
            String jsonrequest = verifyBeneAccount.toString();
            log.i("verify initially bene Account request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                ieGetSenderDetailsView.dismissPDialog();
                return;
            }

            eGetSenderDetailsModel model = new eGetSenderDetailsModel();
            model.IeGetSenderDetailsPresenter = this;
            model.verifyBeneficiaryAccount(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
