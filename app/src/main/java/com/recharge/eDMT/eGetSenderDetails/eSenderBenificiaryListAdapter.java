package com.recharge.eDMT.eGetSenderDetails;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.recharge.eDMT.eGetSenderDetails.JSON.Body;


import java.util.List;

import es.dmoral.toasty.Toasty;

import in.omkarpayment.app.NetworkUtility;
import in.omkarpayment.app.R;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;

public class eSenderBenificiaryListAdapter extends RecyclerView.Adapter<eSenderBenificiaryListAdapter.MyViewHolder> {
    public static int i;
    public static ProgressDialog AdapterDialog;

    String SenderNumber;
    LogWriter log = new LogWriter();
    Context activity;
    eGetSenderDetailsFragment eGetSenderDetailsFragment;
    NetworkUtility ns = new NetworkUtility();
    private List<Body> list;


    public eSenderBenificiaryListAdapter(Context activity, List<Body> list, String SenderNumber, eGetSenderDetailsFragment eGetSenderDetailsFragment) {
        this.list = list;
        this.activity = activity;
        this.SenderNumber = SenderNumber;
        this.eGetSenderDetailsFragment = eGetSenderDetailsFragment;
    }

    @Override
    public eSenderBenificiaryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_benif_list_admr, parent, false);
        AdapterDialog = new ProgressDialog(activity);
        AdapterDialog.setMessage("Please wait..");
        AdapterDialog.setIndeterminate(true);
        AdapterDialog.setCancelable(false);

        return new eSenderBenificiaryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final eSenderBenificiaryListAdapter.MyViewHolder holder, final int position) {
        final Body pojo = list.get(position);

        holder.txtBeneficiaryName.setText(pojo.getBeneficiaryName());
        holder.txtAccountNumber.setText(pojo.getAccountNo());
        holder.txtIFSC.setText(pojo.getIFSCCode());

        if (pojo.getIsVerified().equals(true)) {
            holder.imageVerify.setVisibility(View.GONE);
            holder.imageVerified.setVisibility(View.VISIBLE);
        } else {
            holder.imageVerify.setVisibility(View.VISIBLE);
            holder.imageVerified.setVisibility(View.GONE);
        }

        holder.imageVerify.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.warning_dialog);
               // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                TextView text = dialog.findViewById(R.id.text_dialog);
                text.setText("Are you sure to verify this account?");

                Button dialogButton = dialog.findViewById(R.id.btn_dialog);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eGetSenderDetailsFragment.verifyBeneficiaryAccount(pojo.getBeneficiaryID(), pojo.getSenderID(),
                                pojo.getBeneficiaryName(), pojo.getMobile(), pojo.getAccountNo(), pojo.getBankName(), pojo.getIFSCCode());

                        dialog.dismiss();
                    }
                });

                Button btnNo = dialog.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                ImageView btnClose = dialog.findViewById(R.id.btnClose);
                /*btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/

                dialog.show();
            }
        });

        holder.imageVerified.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setTitle("Verify Account");
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog);

                TextView text = dialog.findViewById(R.id.text_dialog);
                text.setText("Account is verified");

                Button dialogButton = dialog.findViewById(R.id.btn_dialog);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        holder.btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.checkInternet(activity)) {
                    Toasty.error(activity, AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                eGetSenderDetailsFragment.transferIntent(pojo.getBeneficiaryName(), pojo.getAccountNo(), pojo.getIFSCCode(), pojo.getBankName(),
                        pojo.getMobile(), pojo.getBeneficiaryID(), pojo.getSenderID(), pojo.getSenderName(), pojo.getBeneficiaryCode());
            }
        });


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.checkInternet(activity)) {
                    Toasty.error(activity, AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
               // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setTitle("Verify Account");
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.warning_dialog);

                TextView text = dialog.findViewById(R.id.text_dialog);
                text.setText("Are you sure to delete this beneficiary?");

                Button dialogButton = dialog.findViewById(R.id.btn_dialog);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        eGetSenderDetailsFragment.deleteBeneficiary(pojo.getBeneficiaryID());
                        dialog.dismiss();
                    }
                });

                Button btnNo = dialog.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

               /* ImageView btnClose = dialog.findViewById(R.id.btnClose);
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });*/

                dialog.show();
            }
        });

    }

    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtBeneficiaryName, txtAccountNumber, txtIFSC, txtBankName;
        Button imageVerify, imageVerified;
        Button btnDelete, btnTransfer;


        public MyViewHolder(View rowView) {
            super(rowView);
            btnTransfer = rowView.findViewById(R.id.btnTransfer);
            btnDelete = rowView.findViewById(R.id.btnDelete);

            txtBeneficiaryName = rowView.findViewById(R.id.txtBeneficiaryName);
            txtAccountNumber = rowView.findViewById(R.id.txtAccountNumber);
            txtIFSC = rowView.findViewById(R.id.txtIFSC);
            txtBankName = rowView.findViewById(R.id.txtBankName);

            imageVerify = rowView.findViewById(R.id.imageVerify);
            imageVerified = rowView.findViewById(R.id.imageVerified);
        }

    }
}
