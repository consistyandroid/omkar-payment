package com.recharge.eDMT.eGetSenderDetails;

public interface IeGetSenderDetailsPresenter {

    void validate();

    void getSenderDetails();

    void deleteBeneficiary(Integer beneficiaryID);

    void verifyOTPForDeleteBeneficiary(String otp, String BeneficiaryIDForDelete);

    void resendOTP(String BeneficiaryID);

    void resendOTPForDeleteBeneficiary(String BeneficiaryID);

    void verifyOTPForResendOTP(String otp, String senderID, String beneIDForResend);

    void verifyBeneficiaryAccount(Integer beneficiaryID, Integer senderID, String beneficiaryName, String mobile, String accountNo,
                                  String bankName, String ifsc);

    void verifyBeneficiaryAccountResponse(String response);

    void verifyBeneficiaryAccountFailResponse(String response);

    void senderDetailsResponse(String response);

    void senderDetailsFailResponse(String response);

    void errorAlert(String status);

    void deleteBeneficiaryResponse(String response);

    void deleteBeneficiaryFailResponse(String response);

    void deleteBeneficiaryOTPVerificationResponse(String response);

    void deleteBeneficiaryOTPVerificationFailResponse(String response);
}
