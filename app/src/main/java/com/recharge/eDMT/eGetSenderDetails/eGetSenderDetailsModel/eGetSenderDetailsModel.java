package com.recharge.eDMT.eGetSenderDetails.eGetSenderDetailsModel;

import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel.IeOTP;
import com.recharge.eDMT.eGetSenderDetails.IeGetSenderDetailsPresenter;
import com.recharge.eDMT.eMoneyTransfer.JSON.MoneyTransferResponsePojo;
import com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel.IEWebServicesMoneyTransfer;


import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class eGetSenderDetailsModel {

    public IeGetSenderDetailsPresenter IeGetSenderDetailsPresenter = null;
    public IeOTP IAOTP = null;


    public void GetSenderDetailsWebservice(String encryptString, String keyData) {

        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.GetSenderDetailsWebService(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {
                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        IeGetSenderDetailsPresenter.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        IeGetSenderDetailsPresenter.senderDetailsResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IeGetSenderDetailsPresenter.senderDetailsFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        IeGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IeGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }

        });
    }


    public void DeleteBeneficiaryWebService(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.deleteBeneficiaryWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        IeGetSenderDetailsPresenter.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        IeGetSenderDetailsPresenter.deleteBeneficiaryResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IeGetSenderDetailsPresenter.deleteBeneficiaryFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        IeGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IeGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }
        });
    }


    public void verifyOTPForDeleteBeneficiary(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.BeneficiaryDeleteOTPVerificationWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        IeGetSenderDetailsPresenter.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        IeGetSenderDetailsPresenter.deleteBeneficiaryOTPVerificationResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IeGetSenderDetailsPresenter.deleteBeneficiaryOTPVerificationFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        IeGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IeGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }

        });
    }


    public void verifyBeneficiaryAccount(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.VerifyBeneficiaryAccountWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        IeGetSenderDetailsPresenter.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        IeGetSenderDetailsPresenter.verifyBeneficiaryAccountResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IeGetSenderDetailsPresenter.verifyBeneficiaryAccountFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        IeGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IeGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }

        });
    }

    public void resendOTPForAddBeneficiary(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.resendOTPOTPWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        IeGetSenderDetailsPresenter.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        IeGetSenderDetailsPresenter.deleteBeneficiaryResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IeGetSenderDetailsPresenter.deleteBeneficiaryFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        IeGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IeGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }

        });
    }

    public void resendOTPForDeleteBeneficiary(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.resendOTPOTPWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        IeGetSenderDetailsPresenter.errorAlert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        IAOTP.getResendOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IAOTP.getResendOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        IeGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IeGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }

        });
    }

}