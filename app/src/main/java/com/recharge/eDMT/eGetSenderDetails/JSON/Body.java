package com.recharge.eDMT.eGetSenderDetails.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Body {
    @SerializedName("Message")
    @Expose
    String message;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("IsVerified")
    @Expose
    private Boolean isVerified;
    @SerializedName("AccountNo")
    @Expose
    private String accountNo;
    @SerializedName("IFSCCode")
    @Expose
    private String iFSCCode;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("BeneficiaryID")
    @Expose
    private Integer beneficiaryID;
    @SerializedName("SenderID")
    @Expose
    private Integer senderID;
    @SerializedName("SenderName")
    @Expose
    private String senderName;
    @SerializedName("SenderLimit")
    @Expose
    private String senderLimit;
    @SerializedName("SenderSale")
    @Expose
    private String senderSale;
    @SerializedName("SenderCode")
    @Expose
    private String senderCode;
    @SerializedName("BeneficiaryCode")
    @Expose
    private String beneficiaryCode;

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getBeneficiaryID() {
        return beneficiaryID;
    }

    public void setBeneficiaryID(Integer beneficiaryID) {
        this.beneficiaryID = beneficiaryID;
    }

    public Integer getSenderID() {
        return senderID;
    }

    public void setSenderID(Integer senderID) {
        this.senderID = senderID;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderLimit() {
        return senderLimit;
    }

    public void setSenderLimit(String senderLimit) {
        this.senderLimit = senderLimit;
    }

    public String getSenderSale() {
        return senderSale;
    }

    public void setSenderSale(String senderSale) {
        this.senderSale = senderSale;
    }

    public String getSenderCode() {
        return senderCode;
    }

    public void setSenderCode(String senderCode) {
        this.senderCode = senderCode;
    }

    public String getBeneficiaryCode() {
        return beneficiaryCode;
    }

    public void setBeneficiaryCode(String beneficiaryCode) {
        this.beneficiaryCode = beneficiaryCode;
    }
}
