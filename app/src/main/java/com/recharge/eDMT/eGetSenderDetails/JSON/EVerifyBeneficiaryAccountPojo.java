package com.recharge.eDMT.eGetSenderDetails.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EVerifyBeneficiaryAccountPojo {
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("BeneficiaryID")
    @Expose
    private Integer beneficiaryID;
    @SerializedName("Message")
    @Expose
    private String message;

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getBeneficiaryID() {
        return beneficiaryID;
    }

    public void setBeneficiaryID(Integer beneficiaryID) {
        this.beneficiaryID = beneficiaryID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
