package com.recharge.eDMT.eGetSenderDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryActivity;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddSenderBeneficiaryResponsePojo;
import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel.IeOTP;
import com.recharge.eDMT.eGetSenderDetails.JSON.Body;
import com.recharge.eDMT.eGetSenderDetails.JSON.EVerifyBeneficiaryAccountPojo;
import com.recharge.eDMT.eMoneyTransfer.eMoneyTransferActivity;


import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import in.omkarpayment.app.NetworkUtility;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.ActivitySearchNumberAdmrBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;


import static android.app.Activity.RESULT_OK;
import static android.widget.Toast.LENGTH_LONG;

public class eGetSenderDetailsFragment extends Fragment implements IeGetSenderDetailsView, IeOTP, IAvailableBalanceView {

    View rovView;
    ActivitySearchNumberAdmrBinding activitySearchNumberAdmrBinding;
    //ActivitySearchNumber2Binding searchNumber2Binding;
    IeGetSenderDetailsPresenter IeGetSenderDetailsPresenter;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    CustomProgressDialog pDialog;
    AlertImpl alert;
    int flag = 0;
    String SenderID, BeneficiaryIDForDelete;
    int onActivityResultCount = 0, onPauseCount, onStopCount, onStartCount;
    NetworkUtility ns = new NetworkUtility();
    TextView txtToolbarBalance;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        activitySearchNumberAdmrBinding = DataBindingUtil.inflate(inflater, R.layout.activity_search_number_admr, container, false);
        rovView = activitySearchNumberAdmrBinding.getRoot();

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySearchNumberAdmrBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_number_edmr);
        txtToolbarBalance = (TextView) findViewById(R.id.txtToolbarBalance);
        txtToolbarBalance.setText("RCH : ₹ " + UserDetails.UserBalance + "\nDMR : ₹ " + UserDetails.DMRBalance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "Money Transfer";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText(title);*/

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        activitySearchNumberAdmrBinding.listView.setLayoutManager(mLayoutManager);
        activitySearchNumberAdmrBinding.listView.setItemAnimator(new DefaultItemAnimator());
        alert = new AlertImpl(getActivity());
        pDialog = new CustomProgressDialog(getActivity());

        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();

        IeGetSenderDetailsPresenter = new eGetSenderDetailsPresenter(this);
        activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
        activitySearchNumberAdmrBinding.editSearchNumber.requestFocus();
        activitySearchNumberAdmrBinding.editSearchNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (activitySearchNumberAdmrBinding.editSearchNumber.getText().toString().length() != 10) {
                    activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
                    activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
                    activitySearchNumberAdmrBinding.listView.setAdapter(null);
                    return;
                }

                if (onPauseCount == 1 && onStartCount != 1) {
                    activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
                    activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
                    activitySearchNumberAdmrBinding.listView.setAdapter(null);
                    onPauseCount = 0;
                    return;
                }

                if (!ns.checkInternet(getActivity())) {
                    Toasty.error(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }

                resetVariables();   // reset all AMoneyTransferUserDetails

                activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
                activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
                activitySearchNumberAdmrBinding.listView.setAdapter(null);
                AMoneyTransferUserDetails.ASenderMobileNumber = activitySearchNumberAdmrBinding.editSearchNumber.getText().toString().trim();
                IeGetSenderDetailsPresenter.validate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        activitySearchNumberAdmrBinding.txtAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.checkInternet(getActivity())) {
                    Toasty.error(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT, true).show();
                    return;
                }
                Intent intent = new Intent(getActivity(), eAddSenderBeneficiaryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("SenderMobileNumber", mobileNumber());
                bundle.putString("Type", "AddBeneficiary");
                bundle.putString("Title", "Add Beneficiary");
                intent.putExtras(bundle);
                startActivityForResult(intent, 2);
            }
        });

        return rovView;
    }

    @Override
    public String mobileNumber() {
        return AMoneyTransferUserDetails.ASenderMobileNumber;
    }

    @Override
    public void editMobileNumberError() {
        activitySearchNumberAdmrBinding.editSearchNumber.setError("Please enter sender number");
        activitySearchNumberAdmrBinding.editSearchNumber.requestFocus();
    }

    @Override
    public void SearchNumber() {
        if (!ns.checkInternet(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        IeGetSenderDetailsPresenter.getSenderDetails();
        pDialog.showPDialog();
    }

    @Override
    public void getSenderDetailsResponse(List<Body> getSenderDetailsPojos) {
        pDialog.dismissPDialog();
        hideKeyPad();
        AMoneyTransferUserDetails.ASenderMobileNumber = getSenderDetailsPojos.get(0).getMobile();
        AMoneyTransferUserDetails.ASenderID = getSenderDetailsPojos.get(0).getSenderCode();
        AMoneyTransferUserDetails.ASenderName = getSenderDetailsPojos.get(0).getSenderName();
        activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.VISIBLE);
        activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.VISIBLE);
        eSenderBenificiaryListAdapter adapter = new eSenderBenificiaryListAdapter(getActivity(),
                getSenderDetailsPojos, mobileNumber(), eGetSenderDetailsFragment.this);
        activitySearchNumberAdmrBinding.txtSenderName.setText(getSenderDetailsPojos.get(0).getSenderName());
        float amount = Float.parseFloat(String.valueOf(getSenderDetailsPojos.get(0).getSenderLimit()));
        activitySearchNumberAdmrBinding.txtRemainingLimit.setText(String.format("%.2f", amount));
        activitySearchNumberAdmrBinding.listView.setAdapter(adapter);
    }

    @Override
    public void alert(String status) {
        pDialog.dismissPDialog();
        alert.errorAlert(status);
    }

    @Override
    public void getResendOTPResponse(String response) {
        pDialog.dismissPDialog();
        Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
        openOTPForResendOTP();
    }

    @Override
    public void getResendOTPFailResponse(String response) {
        pDialog.dismissPDialog();
        Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
    }


    @Override
    public void noSenderFound() {
        pDialog.dismissPDialog();
        activitySearchNumberAdmrBinding.listView.setAdapter(null);
        Intent intent = new Intent(getActivity(), eAddSenderBeneficiaryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("SenderMobileNumber", mobileNumber());
        bundle.putString("Type", "AddSender");
        bundle.putString("Title", "Add Sender");
        intent.putExtras(bundle);
        startActivityForResult(intent, 2);
    }

    @Override
    public void noBeneficiaryFound() {
        pDialog.dismissPDialog();
        activitySearchNumberAdmrBinding.listView.setAdapter(null);
        Intent intent = new Intent(getActivity(), eAddSenderBeneficiaryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("SenderMobileNumber", mobileNumber());
        bundle.putString("Type", "AddBeneficiary");
        bundle.putString("Title", "Add Beneficiary");
        intent.putExtras(bundle);
        startActivityForResult(intent, 2);
    }

    @Override
    public void unableToFetchError() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive data");
    }

    @Override
    public void deleteBeneficiarySuccessResponse(AAddSenderBeneficiaryResponsePojo deleteBeneResponsePojo) {
        pDialog.dismissPDialog();

        SenderID = deleteBeneResponsePojo.getSenderID().toString();
        BeneficiaryIDForDelete = deleteBeneResponsePojo.getBeneficiaryID().toString();
        final Dialog OTPDialog = new Dialog(getActivity(), R.style.ThemeWithCorners);
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        final Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ns.checkInternet(getActivity())) {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }

                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                    return;
                }

                IeGetSenderDetailsPresenter.verifyOTPForDeleteBeneficiary(editOTP.getText().toString(), BeneficiaryIDForDelete);
                OTPDialog.dismiss();
                pDialog.showPDialog();

            }
        });

        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });

        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);

        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AMoneyTransferUserDetails.ASenderMobileNumber.equals("")) {
                    if (!ns.checkInternet(getActivity())) {
                        Toast.makeText(getActivity(), AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    IeGetSenderDetailsPresenter.resendOTPForDeleteBeneficiary(BeneficiaryIDForDelete);

                    btnResend.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    pDialog.showPDialog();

                } else {
                    alert.errorAlert("Something Wrong");
                }
            }
        });
        try {
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    btnResend.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.GONE);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    @Override
    public void deleteBeneficiaryOTPVerificationSuccessResponse(String message) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Delete OTP Verification");
        text.setText("Beneficiary deleted successfully");
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySearchNumberAdmrBinding.listView.setAdapter(null);
                if (!ns.checkInternet(getActivity())) {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
                activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
                IeGetSenderDetailsPresenter.getSenderDetails();
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            onActivityResultCount = 1;
            onStartCount = 1;
            if (resultCode == RESULT_OK || requestCode == 3) {
                String SenderNumber = data.getStringExtra("SenderMobileNumber");
                if (SenderNumber != null) {
                    AMoneyTransferUserDetails.ASenderMobileNumber = SenderNumber;
                    activitySearchNumberAdmrBinding.editSearchNumber.setText(SenderNumber);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void transferIntent(String beneficiaryName, String accountNo, String ifscCode, String bankName,
                               String mobile, Integer beneficiaryID, Integer senderID, String senderName, String beneficiaryCode) {

        Intent in = new Intent(getActivity(), eMoneyTransferActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("BeneficiaryName", beneficiaryName);
        bundle.putString("AccountNo", accountNo);
        bundle.putString("IFSCCode", ifscCode);
        bundle.putString("BankName", bankName);
        bundle.putString("Mobile", mobile);
        bundle.putString("BeneficiaryID", String.valueOf(beneficiaryID));
        bundle.putString("SenderID", String.valueOf(senderID));
        bundle.putString("SenderName", senderName);
        bundle.putString("BeneficiaryCode", beneficiaryCode);

        in.putExtras(bundle);
        startActivityForResult(in, 3);
    }

    private void hideKeyPad() {
        try {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPResponse(String response) {

    }

    @Override
    public void getAddBeneficiaryVerifyOTPFailResponse(String response) {

    }

    @Override
    public void onStop() {
        super.onStop();
        onStopCount = 1;
        onStartCount = 0;
    }

    @Override
    public void onStart() {
        super.onStart();
        onStartCount = 1;
    }

    @Override
    public void onPause() {
        super.onPause();
        onPauseCount = 1;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onActivityResultCount == 1) {
            onActivityResultCount = 0;
            return;
        }
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        activitySearchNumberAdmrBinding.listView.setAdapter(null);
        activitySearchNumberAdmrBinding.editSearchNumber.setText("");
        activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
        activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
    }

    public void deleteBeneficiary(Integer BeneficiaryID) {

        if (!ns.checkInternet(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        IeGetSenderDetailsPresenter.deleteBeneficiary(BeneficiaryID);
        pDialog.showPDialog();
    }

    @Override
    public void resendOTPVerificationSuccessAlert(String message) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary OTP Verification");
        text.setText(message);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySearchNumberAdmrBinding.editSearchNumber.setText(AMoneyTransferUserDetails.ASenderMobileNumber);
                activitySearchNumberAdmrBinding.listView.setAdapter(null);
                activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
                activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
                SearchNumber();
            }
        });
        dialog.show();
    }

    @Override
    public void verifyBeneficiaryAccountSuccessAlert(EVerifyBeneficiaryAccountPojo verifyBeneficiaryAccountPojo) {
        pDialog.dismissPDialog();
        iAvailableBalancePresenter.getAvailableBalance();

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Verification Status - " + verifyBeneficiaryAccountPojo.getStatus());
        if (verifyBeneficiaryAccountPojo.getStatus().equals("SUCCESS")) {
            dialogTitle.setTextColor(Color.GREEN);
        }
        text.setText("Beneficiary Name :- " + verifyBeneficiaryAccountPojo.getBeneficiaryName() +
                "\nBank Name :- " + AMoneyTransferUserDetails.ABeneficiaryBankNameForVerify +
                "\nAccount Number :- " + AMoneyTransferUserDetails.ABeneficiaryAccountNumberForVerify);
        text.setGravity(0);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activitySearchNumberAdmrBinding.layoutSenderDetails.setVisibility(View.GONE);
                activitySearchNumberAdmrBinding.txtAddBeneficiary.setVisibility(View.GONE);
                activitySearchNumberAdmrBinding.listView.setAdapter(null);
                SearchNumber();
            }
        });
        dialog.show();
    }

    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
    }

    @Override
    public void resendOTPSuccessAlert(String message) {
        pDialog.dismissPDialog();
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        openOTPForResendOTP();
    }

    private void openOTPForResendOTP() {
        final Dialog OTPDialog = new Dialog(getActivity(), R.style.ThemeWithCorners);
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);

        Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                } else {
                    if (!ns.checkInternet(getActivity())) {
                        Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
                        return;
                    }
                    IeGetSenderDetailsPresenter.verifyOTPForResendOTP(editOTP.getText().toString(),
                            AMoneyTransferUserDetails.ASenderID, AMoneyTransferUserDetails.ABeneficiaryIDForResend);
                    OTPDialog.dismiss();
                    pDialog.showPDialog();
                }
            }
        });

        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }


    public void verifyBeneficiaryAccount(Integer beneficiaryID, Integer senderID, String beneficiaryName, String mobile, String accountNo,
                                         String bankName, String ifsc) {

        AMoneyTransferUserDetails.ABeneficiaryBankNameForVerify = bankName;
        AMoneyTransferUserDetails.ABeneficiaryAccountNumberForVerify = accountNo;

        if (!ns.checkInternet(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        IeGetSenderDetailsPresenter.verifyBeneficiaryAccount(beneficiaryID, senderID, beneficiaryName, mobile,
                accountNo, bankName, ifsc);
        pDialog.showPDialog();
    }

    private void resetVariables() {
        AMoneyTransferUserDetails.ASenderName = "";
        AMoneyTransferUserDetails.ASenderMobileNumber = "";
        AMoneyTransferUserDetails.ASenderID = "";
        AMoneyTransferUserDetails.ASenderPinEode = "";

        AMoneyTransferUserDetails.ABeneficiaryName = "";
        AMoneyTransferUserDetails.ABeneficiaryBankName = "";
        AMoneyTransferUserDetails.ABeneficiaryIFSC = "";

        AMoneyTransferUserDetails.ABeneficiaryIDForDelete = "";
        AMoneyTransferUserDetails.ABeneficiaryIDForResend = "";
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                double CurrentBalance = obj.getCurrentBalance();
                double currentDMRBalance = obj.getDMRBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
               // UserDetails.DMRBalance = obj.getDMRBalance();
                //((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyPad();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }*/
}
