package com.recharge.eDMT.eGetSenderDetails;

import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddSenderBeneficiaryResponsePojo;
import com.recharge.eDMT.eGetSenderDetails.JSON.Body;
import com.recharge.eDMT.eGetSenderDetails.JSON.EVerifyBeneficiaryAccountPojo;

import java.util.List;

public interface IeGetSenderDetailsView {

    String mobileNumber();

    void editMobileNumberError();

    void SearchNumber();

    void getSenderDetailsResponse(List<Body> getSenderDetailsPojos);

    void alert(String status);

    void noSenderFound();

    void noBeneficiaryFound();

    void unableToFetchError();

    void deleteBeneficiarySuccessResponse(AAddSenderBeneficiaryResponsePojo messageResponsePojo);

    void deleteBeneficiaryOTPVerificationSuccessResponse(String message);

    void resendOTPSuccessAlert(String message);

    void resendOTPVerificationSuccessAlert(String message);

    void verifyBeneficiaryAccountSuccessAlert(EVerifyBeneficiaryAccountPojo verifyBeneficiaryAccountPojo);

    void dismissPDialog();

}
