package com.recharge.eDMT.eGetSenderDetails.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SenderNotFoundPojo {

    @SerializedName("Status")
    @Expose
    private String status;

    @SerializedName("Body")
    @Expose
    private List<Object> body = null;

    @SerializedName("Message")
    @Expose
    private String message;

    @SerializedName("UDF")
    @Expose
    private Object uDF;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Object> getBody() {
        return body;
    }

    public void setBody(List<Object> body) {
        this.body = body;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getUDF() {
        return uDF;
    }

    public void setUDF(Object uDF) {
        this.uDF = uDF;
    }


}
