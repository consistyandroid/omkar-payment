package com.recharge.eDMT.eAddSenderBeneficiary.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AAddSenderBeneficiaryResponsePojo {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("SenderID")
    @Expose
    private Integer senderID;
    @SerializedName("BeneficiaryID")
    @Expose
    private Integer beneficiaryID;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSenderID() {
        return senderID;
    }

    public void setSenderID(Integer senderID) {
        this.senderID = senderID;
    }

    public Integer getBeneficiaryID() {
        return beneficiaryID;
    }

    public void setBeneficiaryID(Integer beneficiaryID) {
        this.beneficiaryID = beneficiaryID;
    }

}

