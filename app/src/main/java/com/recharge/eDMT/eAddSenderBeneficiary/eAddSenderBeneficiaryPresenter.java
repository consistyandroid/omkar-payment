package com.recharge.eDMT.eAddSenderBeneficiary;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddBeneficiaryOTPVerificationPojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddSenderBeneficiaryResponsePojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AVerifyBeneficiaryAccountPojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.GetBankNamesPojo;
import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel.eAddSenderBeneficiaryModel;
import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel.IeAddSenderBeneficiaryModel;
import com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel.IeOTP;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;

public class eAddSenderBeneficiaryPresenter implements IeAddSenderBeneficiaryPresenter, IeAddSenderBeneficiaryModel, IeOTP {

    public eAddSenderBeneficiaryView iAddSenderBeneficiaryView;

    public eAddSenderBeneficiaryPresenter(eAddSenderBeneficiaryView iAddSenderBeneficiaryView) {
        this.iAddSenderBeneficiaryView = iAddSenderBeneficiaryView;
    }

    @Override
    public void validateBeneficiary() {
        if (validation()) {
            iAddSenderBeneficiaryView.confirmDialog();
        }
    }

    @Override
    public void validateSenderBeneficiary() {
        if (senderNameValidate()) {
            if (validation()) {
                iAddSenderBeneficiaryView.confirmDialog();
            }
        }
    }

    private boolean senderNameValidate() {
        if (iAddSenderBeneficiaryView.senderName().length() <= 0) {
            iAddSenderBeneficiaryView.editSenderNameError();
            return false;
        }
       /* if (iAddSenderBeneficiaryView.senderPinCode().length() != 6) {
            iAddSenderBeneficiaryView.editSenderPinCodeError();
            return false;
        }*/
        return true;
    }

    @Override
    public boolean validation() {

        if (iAddSenderBeneficiaryView.beneficiaryName().isEmpty()) {
            iAddSenderBeneficiaryView.editBeneficiaryNameError();
            return false;
        }
        if (iAddSenderBeneficiaryView.bankName().isEmpty()) {
            iAddSenderBeneficiaryView.editBankNameError();
            return false;
        }
        if (iAddSenderBeneficiaryView.IFSCCode().isEmpty()) {
            iAddSenderBeneficiaryView.editIFSCError();
            return false;
        }
        if (iAddSenderBeneficiaryView.IFSCCode().length() < 8) {
            iAddSenderBeneficiaryView.editIFSCError();
            return false;
        }
        if (iAddSenderBeneficiaryView.accountNumber().isEmpty()) {
            iAddSenderBeneficiaryView.editAccNoError();
            return false;
        }
        return true;

    }

    @Override
    public void addBeneficiary() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addBenif = new JSONObject();
        try {
            addBenif.put("BeneficiaryName", iAddSenderBeneficiaryView.beneficiaryName());
            addBenif.put("MobileNo", iAddSenderBeneficiaryView.mobileNumber());
            addBenif.put("AccountNo", iAddSenderBeneficiaryView.accountNumber());
            addBenif.put("AccountType", "Savings");
            addBenif.put("IFSC", iAddSenderBeneficiaryView.IFSCCode());
            addBenif.put("BankName", iAddSenderBeneficiaryView.bankName());

            String jsonrequest = addBenif.toString();
            LogWriter log = new LogWriter();
            log.i("request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            eAddSenderBeneficiaryModel model = new eAddSenderBeneficiaryModel();
            model.iEAddSenderBeneficiaryModel = this;
            model.addBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addSenderBeneficiary() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addSenderBene = new JSONObject();
        try {
            addSenderBene.put("Name", iAddSenderBeneficiaryView.senderName());
            addSenderBene.put("BeneficiaryName", iAddSenderBeneficiaryView.beneficiaryName());
            addSenderBene.put("MobileNo", iAddSenderBeneficiaryView.mobileNumber());
            addSenderBene.put("AccountNo", iAddSenderBeneficiaryView.accountNumber());
            addSenderBene.put("AccountType", "Savings");
            addSenderBene.put("IFSC", iAddSenderBeneficiaryView.IFSCCode());
            addSenderBene.put("BankName", iAddSenderBeneficiaryView.bankName());

            String jsonrequest = addSenderBene.toString();
            LogWriter log = new LogWriter();
            log.i("request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("sender encrypted String", encryptString);

            if (encryptString.equals("")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            eAddSenderBeneficiaryModel model = new eAddSenderBeneficiaryModel();
            model.iEAddSenderBeneficiaryModel = this;
            model.addSenderBeneficiaryWebservice(encryptString, keyData);

        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    @Override
    public void getBankNames() {
        String keyData = new KeyDataReader().get();
        eAddSenderBeneficiaryModel model = new eAddSenderBeneficiaryModel();
        model.iEAddSenderBeneficiaryModel = this;
        model.getBankNames(keyData);
    }

    @Override
    public void resendOTP(String senderNumber, String BeneID) {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addBenif = new JSONObject();
        try {

            addBenif.put("MobileNo", senderNumber);
            addBenif.put("BeneficiaryID", BeneID);

            String jsonrequest = addBenif.toString();
            LogWriter log = new LogWriter();
            log.i("request", jsonrequest);
            String encryptString = data.Encrypt(jsonrequest);
            Log.i("encrypted String", encryptString);
            if (encryptString.equals("")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            eAddSenderBeneficiaryModel model = new eAddSenderBeneficiaryModel();
            model.IAOTP = this;
            model.resendOTP(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyOTPForAddBeneficiary(String OTP, String SenderID, String BeneficiaryIDForAddBene) {
        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyOTPForAddBeneficiary = new JSONObject();
        try {
            verifyOTPForAddBeneficiary.put("Otp", OTP);
            verifyOTPForAddBeneficiary.put("MobileNo", AMoneyTransferUserDetails.ASenderMobileNumber);
            verifyOTPForAddBeneficiary.put("SenderID", SenderID);
            verifyOTPForAddBeneficiary.put("BeneficiaryID", BeneficiaryIDForAddBene);
            verifyOTPForAddBeneficiary.put("ActionType", AMoneyTransferUserDetails.AActionTypeRegistration);

            String jsonRequest = verifyOTPForAddBeneficiary.toString();
            LogWriter log = new LogWriter();
            log.i("add beneficiary OTP Verification request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            eAddSenderBeneficiaryModel model = new eAddSenderBeneficiaryModel();
            model.IAOTP = this;
            model.verifyOTPforAddBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void validateVerifyAccount() {
        if (validation()) {
            iAddSenderBeneficiaryView.verifyBeneficiaryAccount();
        }
    }

    @Override
    public void addSenderBeneficiaryResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("addSenderBenifResponse", body);
            Gson gson = new Gson();
            AAddSenderBeneficiaryResponsePojo addBeneResponsePojo = gson.fromJson(body, new TypeToken<AAddSenderBeneficiaryResponsePojo>() {
            }.getType());

            if (!addBeneResponsePojo.getMessage().equals("SUCCESS")) {
                iAddSenderBeneficiaryView.alert("Unable to fetch/retrive data.");
                return;
            }
            iAddSenderBeneficiaryView.addBeneficiarySuccessResponse(addBeneResponsePojo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addSenderBeneficiaryFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("addSenderBeneficiaryFailResponse", body);
            iAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("getVerifyOTPResponse", body);


            Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());

            if (!messageResponsePojo.getStatus().equalsIgnoreCase("SUCCESS") &&
                    messageResponsePojo.getMessage() != null) {
                alert(messageResponsePojo.getMessage());
                return;
            }

            if (!messageResponsePojo.getBody().getMessage().equalsIgnoreCase("Verified")) {
                alert(messageResponsePojo.getBody().getMessage());
                return;
            }
            iAddSenderBeneficiaryView.registrationSuccess();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            LogWriter log = new LogWriter();
            String body = data.Decrypt(response);
            log.i("getVerifyOTPFailResponse", body);
            iAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {
        iAddSenderBeneficiaryView.alert(status);
    }

    @Override
    public void getResendOTPResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equalsIgnoreCase("[]")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo resendOTPResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());

            if (!resendOTPResponsePojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                iAddSenderBeneficiaryView.resendOTPSuccessAlert(resendOTPResponsePojo.getMessage());
                return;
            }
            iAddSenderBeneficiaryView.resendOTPSuccessAlert(resendOTPResponsePojo.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getResendOTPFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("getResendOTPResponse", body);

            if (body == null || body.equals("[]") || body.contains("")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo resendOTPResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());
            if (resendOTPResponsePojo.getStatus().equalsIgnoreCase("FAILURE")
                    || resendOTPResponsePojo.getStatus().equalsIgnoreCase("FAIL")) {
                iAddSenderBeneficiaryView.resendOTPSuccessAlert(resendOTPResponsePojo.getMessage());
                return;
            }

            iAddSenderBeneficiaryView.resendOTPSuccessAlert(resendOTPResponsePojo.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("verifyBeneAccountResponse", body);

            if (body == null || body.equals("[]")) {
                iAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AVerifyBeneficiaryAccountPojo CVerifyBeneficiaryAccountPojo = gson.fromJson(body, new TypeToken<AVerifyBeneficiaryAccountPojo>() {
            }.getType());

            if (CVerifyBeneficiaryAccountPojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                iAddSenderBeneficiaryView.verifyBeneficiaryAccountSuccessAlert(CVerifyBeneficiaryAccountPojo);
                return;
            }
            if (CVerifyBeneficiaryAccountPojo.getStatus().equalsIgnoreCase("FAIL") ||
                    CVerifyBeneficiaryAccountPojo.getStatus().equalsIgnoreCase("FAILURE")) {
                iAddSenderBeneficiaryView.alert(CVerifyBeneficiaryAccountPojo.getMessage());
                return;
            }
            iAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("verifyBeneAccount FailResponse", body);
            iAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getBanksNamesResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            LogWriter log = new LogWriter();
            log.i("getBanksNames", body);
            Gson gson = new Gson();
            ArrayList<GetBankNamesPojo> bankNames = gson.fromJson(body, new TypeToken<ArrayList<GetBankNamesPojo>>() {
            }.getType());
            iAddSenderBeneficiaryView.getBankNames(bankNames);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
