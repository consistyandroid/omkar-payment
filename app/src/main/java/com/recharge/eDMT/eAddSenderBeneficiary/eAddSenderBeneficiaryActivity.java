package com.recharge.eDMT.eAddSenderBeneficiary;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddSenderBeneficiaryResponsePojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AVerifyBeneficiaryAccountPojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.GetBankNamesPojo;


import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.adapter.AutocompleteForBankNameAdapterForADmr;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityAdmrAddBeneficiaryBinding;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;
import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class eAddSenderBeneficiaryActivity extends AppCompatActivity implements eAddSenderBeneficiaryView {

    public static int flag_type, verifyBeneficiaryAccountCountFlag = 0;
    ActivityAdmrAddBeneficiaryBinding activityAdmrAddBeneficiaryBinding;
    Context context = this;
    IeAddSenderBeneficiaryPresenter iEAddSenderBeneficiaryPresenter;
    String title, Type, SenderNumber;
    AlertImpl alert;
    CustomProgressDialog pDialog;
    NetworkState ns = new NetworkState();
    LogWriter log = new LogWriter();
    String SenderID, BeneficiaryIDForAddBene;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        activityAdmrAddBeneficiaryBinding = DataBindingUtil.setContentView(this, R.layout.activity_admr_add_beneficiary);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setBundle();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

       /* TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);

        TextView txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        txtToolbarBalance.setText("RCH : ₹ " + UserDetails.UserBalance + "\nDMR : ₹ " + UserDetails.DMRBalance);*/


        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        iEAddSenderBeneficiaryPresenter = new eAddSenderBeneficiaryPresenter(this);

        if (Type.equals("AddBeneficiary")) {
            flag_type = 2;
            activityAdmrAddBeneficiaryBinding.LaySender.setVisibility(View.GONE);
        } else {
            flag_type = 1;
            String number = AMoneyTransferUserDetails.ASenderMobileNumber;
            activityAdmrAddBeneficiaryBinding.editSenderMobileNumber.setText(number);
            activityAdmrAddBeneficiaryBinding.editSenderMobileNumber.setFocusable(false);
            /*activityAdmrAddBeneficiaryBinding.LayBeneficiary.setVisibility(View.GONE);*/
        }
        activityAdmrAddBeneficiaryBinding.txtBankName.setVisibility(View.GONE);
        activityAdmrAddBeneficiaryBinding.DetailLayout.setVisibility(View.GONE);
        iEAddSenderBeneficiaryPresenter.getBankNames();

        activityAdmrAddBeneficiaryBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        activityAdmrAddBeneficiaryBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                if (Type.equals("AddSender")) {
                    flag_type = 1;
                    iEAddSenderBeneficiaryPresenter.validateSenderBeneficiary();
                } else if (Type.equals("AddBeneficiary")) {
                    flag_type = 2;
                    AMoneyTransferUserDetails.ABeneficiaryBankName = activityAdmrAddBeneficiaryBinding.editBankName.getText().toString().trim();
                    iEAddSenderBeneficiaryPresenter.validateBeneficiary();
                }
                log.i("cc", "click");

            }
        });

        activityAdmrAddBeneficiaryBinding.editBankName.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                GetBankNamesPojo object = (GetBankNamesPojo) parent.getItemAtPosition(pos);
                AMoneyTransferUserDetails.ABeneficiaryBankName = object.getBankName();
                AMoneyTransferUserDetails.ABeneficiaryIFSC = object.getIFSCCode();
                if (AMoneyTransferUserDetails.ABeneficiaryIFSC != null) {
                    activityAdmrAddBeneficiaryBinding.editIFSC.setText(AMoneyTransferUserDetails.ABeneficiaryIFSC);
                }
            }


        });

        activityAdmrAddBeneficiaryBinding.editAccNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        activityAdmrAddBeneficiaryBinding.txtVerifyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyBeneficiaryAccountCountFlag = 1;
                activityAdmrAddBeneficiaryBinding.txtVerifyAccount.setVisibility(View.GONE);
                iEAddSenderBeneficiaryPresenter.validateVerifyAccount();
            }
        });
    }

    @Override
    public String beneficiaryName() {
        if (activityAdmrAddBeneficiaryBinding.editBenifName.getText().toString().length() > 1) {
           AMoneyTransferUserDetails.ABeneficiaryName = activityAdmrAddBeneficiaryBinding.editBenifName.getText().toString().trim();
        }
        return activityAdmrAddBeneficiaryBinding.editBenifName.getText().toString().trim();
    }

    @Override
    public String mobileNumber() {
        return AMoneyTransferUserDetails.ASenderMobileNumber;
    }

    @Override
    public String accountNumber() {
        return activityAdmrAddBeneficiaryBinding.editAccNo.getText().toString().trim();
    }

    @Override
    public String IFSCCode() {
        if (AMoneyTransferUserDetails.ABeneficiaryIFSC.isEmpty() ||
                AMoneyTransferUserDetails.ABeneficiaryIFSC.contains("") ||
                AMoneyTransferUserDetails.ABeneficiaryIFSC.length() < 4) {
            AMoneyTransferUserDetails.ABeneficiaryIFSC = activityAdmrAddBeneficiaryBinding.editIFSC.getText().toString().trim();
        }
        return AMoneyTransferUserDetails.ABeneficiaryIFSC;
    }

    @Override
    public String bankName() {
        return activityAdmrAddBeneficiaryBinding.editBankName.getText().toString();
    }

    @Override
    public String senderName() {
        return activityAdmrAddBeneficiaryBinding.editSenderName.getText().toString();
    }

    @Override
    public String senderPinCode() {
        return activityAdmrAddBeneficiaryBinding.editSenderPinCode.getText().toString();
    }

    @Override
    public void editBeneficiaryNameError() {
        activityAdmrAddBeneficiaryBinding.editBenifName.setError(String.format(AllMessages.pleaseEnter, "first name"));
        activityAdmrAddBeneficiaryBinding.editBenifName.requestFocus();
    }

    @Override
    public void editAccNoError() {
        activityAdmrAddBeneficiaryBinding.editAccNo.setError(String.format(AllMessages.pleaseEnter, "account number"));
        activityAdmrAddBeneficiaryBinding.editAccNo.requestFocus();
    }

    @Override
    public void confirmDialog() {
        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
            return;
        }

        if (Type.equals("AddSender")) {
            iEAddSenderBeneficiaryPresenter.addSenderBeneficiary();
        } else if (Type.equals("AddBeneficiary")) {
            iEAddSenderBeneficiaryPresenter.addBeneficiary();
        }
        pDialog.showPDialog();
    }

    @Override
    public void editIFSCError() {
        activityAdmrAddBeneficiaryBinding.editIFSC.setError(String.format(AllMessages.pleaseEnter, "IFSC"));
        activityAdmrAddBeneficiaryBinding.editIFSC.requestFocus();
    }

    @Override
    public void editValidIFSCError() {
        activityAdmrAddBeneficiaryBinding.editIFSC.setError(String.format(AllMessages.pleaseEnter, "valid IFSC"));
        activityAdmrAddBeneficiaryBinding.editIFSC.requestFocus();
    }

    @Override
    public void editSenderNameError() {
        activityAdmrAddBeneficiaryBinding.editSenderName.setError(String.format(AllMessages.pleaseEnter, "sender name"));
        activityAdmrAddBeneficiaryBinding.editSenderName.requestFocus();
    }

    @Override
    public void editSenderPinCodeError() {
        activityAdmrAddBeneficiaryBinding.editSenderPinCode.setError(String.format(AllMessages.pleaseEnter, "valid pincode"));
        activityAdmrAddBeneficiaryBinding.editSenderPinCode.requestFocus();
    }

    @Override
    public void alert(String msg) {
        pDialog.dismissPDialog();
        alert.successAlert(msg);
    }

    @Override
    public void editBankNameError() {
        activityAdmrAddBeneficiaryBinding.editBankName.setError(String.format(AllMessages.pleaseEnter, "bank name"));
        activityAdmrAddBeneficiaryBinding.editBankName.requestFocus();
    }

    @Override
    public void getBankNames(ArrayList<GetBankNamesPojo> bankNames) {
        AutocompleteForBankNameAdapterForADmr autocompleteTextAdapter = new AutocompleteForBankNameAdapterForADmr(context, R.layout.adapter_autocomplete_text, bankNames);
        activityAdmrAddBeneficiaryBinding.editBankName.setThreshold(1);
        activityAdmrAddBeneficiaryBinding.editBankName.setAdapter(autocompleteTextAdapter);
    }

    @Override
    public void addBeneficiarySuccessResponse(AAddSenderBeneficiaryResponsePojo addBeneResponsePojo) {
        pDialog.dismissPDialog();
        otpDialogAddSenderBeneficiary(addBeneResponsePojo);
    }

    @Override
    public void registrationSuccess() {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary OTP Verification");
        text.setText("Beneficiary register successfully");
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResultIntent();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void resendOTPSuccessAlert(String response) {
        pDialog.dismissPDialog();
        Toast.makeText(context, response, LENGTH_LONG).show();
    }

    @Override
    public void otpVerifyFailAlert(String msg) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary OTP Verification");
        text.setText(msg);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                iEAddSenderBeneficiaryPresenter.resendOTP(AMoneyTransferUserDetails.ASenderMobileNumber, AMoneyTransferUserDetails.ABeneficiaryIDForResend);
                dialog.dismiss();
                pDialog.showPDialog();
                reopenOTPDialog();
            }
        });
        dialog.show();
    }

    @Override
    public void verifyBeneficiaryAccount() {
        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
            return;
        }
        /*iEAddSenderBeneficiaryPresenter.verifyBeneficiaryAccount();*/
        pDialog.showPDialog();
    }

    @Override
    public void verifyBeneficiaryAccountSuccessAlert(AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Verification Status - " + aVerifyBeneficiaryAccountPojo.getVerificationStatus());
        if (aVerifyBeneficiaryAccountPojo.getVerificationStatus().equals("VERIFIED")) {
            dialogTitle.setTextColor(Color.GREEN);
        }
        text.setText("Beneficiary Name : " + aVerifyBeneficiaryAccountPojo.getBeneficiaryName() +
                "\nBank Name : " + bankName() + "\nAccount Number : " + accountNumber());
        text.setGravity(0);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activityAdmrAddBeneficiaryBinding.txtVerifiedAccount.setVisibility(View.VISIBLE);
            }
        });
        dialog.show();
    }


    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to send/receive data!");
    }

    private void clear() {
        activityAdmrAddBeneficiaryBinding.editSenderName.setText("");
        activityAdmrAddBeneficiaryBinding.editSenderLastName.setText("");
        activityAdmrAddBeneficiaryBinding.editSenderMobileNumber.setText("");
        activityAdmrAddBeneficiaryBinding.editSenderPinCode.setText("");

        activityAdmrAddBeneficiaryBinding.editBenifName.setText("");
        activityAdmrAddBeneficiaryBinding.editBenifLastName.setText("");
        activityAdmrAddBeneficiaryBinding.editBankName.setText("");
        activityAdmrAddBeneficiaryBinding.editAccNo.setText("");
        activityAdmrAddBeneficiaryBinding.editIFSC.setText("");
        if (Type.equals("AddBeneficiary"))
            activityAdmrAddBeneficiaryBinding.editBenifName.requestFocus();
        else
            activityAdmrAddBeneficiaryBinding.editSenderName.requestFocus();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = getIntent();
        intent.putExtra("SenderMobileNumber", SenderNumber);
        setResult(RESULT_OK, intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            Intent intent = getIntent();
            intent.putExtra("SenderMobileNumber", SenderNumber);
            setResult(RESULT_OK, intent);
            // finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onResultIntent() {
        Intent intent = getIntent();
        intent.putExtra("SenderMobileNumber", SenderNumber);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }


    private void otpDialogAddSenderBeneficiary(AAddSenderBeneficiaryResponsePojo addBenifResponsePojo) {

        SenderID = addBenifResponsePojo.getSenderID().toString();
        AMoneyTransferUserDetails.ASenderID = SenderID;
        BeneficiaryIDForAddBene = addBenifResponsePojo.getBeneficiaryID().toString();
        AMoneyTransferUserDetails.ABeneficiaryIDForAddBene = BeneficiaryIDForAddBene;


        final Dialog OTPDialog = new Dialog(context, R.style.ThemeWithCorners);
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        final Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                    return;
                }
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                iEAddSenderBeneficiaryPresenter.verifyOTPForAddBeneficiary(editOTP.getText().toString().trim(), SenderID, BeneficiaryIDForAddBene);
                OTPDialog.dismiss();
                pDialog.showPDialog();

            }
        });

        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });

        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AMoneyTransferUserDetails.ASenderMobileNumber.equals("")) {
                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    iEAddSenderBeneficiaryPresenter.resendOTP(SenderNumber, AMoneyTransferUserDetails.ABeneficiaryIDForAddBene);
                    btnResend.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    pDialog.showPDialog();
                } else {
                    alert.errorAlert("Something Wrong");
                }
            }
        });
        try {
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    btnResend.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.GONE);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    private void reopenOTPDialog() {

        pDialog.dismissPDialog();
        final Dialog OTPDialog = new Dialog(context, R.style.ThemeWithCorners);
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        final Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                } else {
                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    iEAddSenderBeneficiaryPresenter.verifyOTPForAddBeneficiary(editOTP.getText().toString().trim(), SenderID, BeneficiaryIDForAddBene);
                    OTPDialog.dismiss();
                    pDialog.showPDialog();
                }
            }
        });
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AMoneyTransferUserDetails.ASenderMobileNumber.equals("")) {
                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    iEAddSenderBeneficiaryPresenter.resendOTP(SenderNumber, AMoneyTransferUserDetails.ABeneficiaryIDForAddBene);
                    btnResend.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    pDialog.showPDialog();
                } else {
                    alert.errorAlert("Something Wrong");
                }
            }
        });
        try {
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    btnResend.setVisibility(View.VISIBLE);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    private void setBundle() {
        verifyBeneficiaryAccountCountFlag = 0;
        Bundle extras = getIntent().getExtras();
        AMoneyTransferUserDetails.ASenderMobileNumber = extras.getString("SenderMobileNumber");
        SenderNumber = extras.getString("SenderMobileNumber");
        Log.i("SenderMobileNumber", SenderNumber);
        Type = extras.getString("Type");
        try {
            title = extras.getString("Title");
        } catch (Exception ex) {
            title = "Add Beneficiary";
        }
    }
}
