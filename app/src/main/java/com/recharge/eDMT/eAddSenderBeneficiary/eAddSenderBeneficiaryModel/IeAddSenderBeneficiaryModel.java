package com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel;

public interface IeAddSenderBeneficiaryModel {

    void addSenderBeneficiaryResponse(String response);

    void addSenderBeneficiaryFailResponse(String response);

    void alert(String status);

    void verifyBeneficiaryAccountResponse(String response);

    void verifyBeneficiaryAccountFailResponse(String response);

    void getBanksNamesResponse(String body);
}