package com.recharge.eDMT.eAddSenderBeneficiary;

import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AAddSenderBeneficiaryResponsePojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.AVerifyBeneficiaryAccountPojo;
import com.recharge.eDMT.eAddSenderBeneficiary.JSON.GetBankNamesPojo;

import java.util.ArrayList;

public interface eAddSenderBeneficiaryView {
    String beneficiaryName();

    String mobileNumber();

    String accountNumber();

    String IFSCCode();

    String bankName();

    String senderName();

    String senderPinCode();

    void editBeneficiaryNameError();

    void editAccNoError();

    void confirmDialog();

    void editIFSCError();

    void editValidIFSCError();

    void editSenderNameError();

    void editSenderPinCodeError();

    void alert(String msg);

    void editBankNameError();

    void getBankNames(ArrayList<GetBankNamesPojo> bankNames);

    void addBeneficiarySuccessResponse(AAddSenderBeneficiaryResponsePojo addBenifResponsePojo);

    void registrationSuccess();

    void resendOTPSuccessAlert(String response);

    void otpVerifyFailAlert(String body);

    void verifyBeneficiaryAccount();

    void verifyBeneficiaryAccountSuccessAlert(AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo);

    void dismissPDialog();
}
