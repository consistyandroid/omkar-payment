package com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel;

import android.util.Log;

import com.recharge.eDMT.eMoneyTransfer.JSON.MoneyTransferResponsePojo;
import com.recharge.eDMT.eMoneyTransfer.eMoneyTransferModel.IEWebServicesMoneyTransfer;


import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class eAddSenderBeneficiaryModel {

    public IeAddSenderBeneficiaryModel iEAddSenderBeneficiaryModel = null;
    public IeOTP IAOTP = null;

    public void addSenderBeneficiaryWebservice(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.addSenderBeneficiaryWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                Log.i("Response code:", String.valueOf(response.code()));
                try {
                    if (response.code() != 200) {
                        iEAddSenderBeneficiaryModel.alert(AllMessages.somethingWrong);
                        return;
                    }
                    Log.i("Response code:", response.body().getBody());
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        Log.i("onResponseBody", response.body().getBody());
                        iEAddSenderBeneficiaryModel.addSenderBeneficiaryResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        iEAddSenderBeneficiaryModel.addSenderBeneficiaryFailResponse(response.body().getBody());
                    } else {
                        iEAddSenderBeneficiaryModel.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                Log.i("onFailure", t.getMessage());
                iEAddSenderBeneficiaryModel.alert(AllMessages.internetError);
            }
        });
    }


    public void addBeneficiary(String encryptString, String keyData) {

        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.addBeneficiaryWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    if (response.code() != 200) {
                        iEAddSenderBeneficiaryModel.alert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        Log.i("onResponseBody", response.body().getBody());
                        iEAddSenderBeneficiaryModel.addSenderBeneficiaryResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        iEAddSenderBeneficiaryModel.addSenderBeneficiaryFailResponse(response.body().getBody());
                    } else {
                        iEAddSenderBeneficiaryModel.alert(response.body().getStatus());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                Log.i("onFailure", t.getMessage());
                iEAddSenderBeneficiaryModel.alert(AllMessages.internetError);
            }
        });
    }


    public void resendOTP(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.resendOTPOTPWebservice(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        IAOTP.alert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        IAOTP.getResendOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IAOTP.getResendOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        IAOTP.alert(response.body().getStatus());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IAOTP.alert(AllMessages.internetError);
            }

        });
    }


    public void getBankNames(String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.getBanksWebservice(keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        iEAddSenderBeneficiaryModel.alert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        iEAddSenderBeneficiaryModel.getBanksNamesResponse(response.body().getBody());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                iEAddSenderBeneficiaryModel.alert(AllMessages.internetError);
            }
        });
    }


    public void verifyOTPforAddBeneficiary(String encryptString, String keyData) {
        IEWebServicesMoneyTransfer webObject = IEWebServicesMoneyTransfer.retrofit.create(IEWebServicesMoneyTransfer.class);

        Call<MoneyTransferResponsePojo> call = webObject.verifyOTPforAddBeneficiary(encryptString, keyData);
        call.enqueue(new Callback<MoneyTransferResponsePojo>() {

            @Override
            public void onResponse(Call<MoneyTransferResponsePojo> call, Response<MoneyTransferResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        iEAddSenderBeneficiaryModel.alert(AllMessages.somethingWrong);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        IAOTP.getAddBeneficiaryVerifyOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        IAOTP.getAddBeneficiaryVerifyOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        IAOTP.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                IAOTP.alert(AllMessages.internetError);
            }

        });
    }
}
