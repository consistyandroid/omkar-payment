package com.recharge.eDMT.eAddSenderBeneficiary.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AAddBeneficiaryOTPVerificationPojo {
    @SerializedName(value = "Status", alternate = {"status", "STATUS"})
    @Expose
    private String status;

    @SerializedName("Body")
    @Expose
    private Body body;

    @SerializedName("UDF")
    @Expose
    private String uDF;

    @SerializedName("Message")
    @Expose
    private String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }


    public String getUDF() {
        return uDF;
    }

    public void setUDF(String uDF) {
        this.uDF = uDF;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class Body {

        @SerializedName("Message")
        @Expose
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }
}
