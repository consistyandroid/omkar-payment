package com.recharge.eDMT.eAddSenderBeneficiary;

public interface IeAddSenderBeneficiaryPresenter {

    void validateBeneficiary();

    void validateSenderBeneficiary();

    boolean validation();

    void addBeneficiary();

    void addSenderBeneficiary();

    void getBankNames();

    void resendOTP(String senderNumber, String BeneID);

    void verifyOTPForAddBeneficiary(String OTP, String SenderID, String BeneficiaryIDForAddBene);

    void validateVerifyAccount();
}
