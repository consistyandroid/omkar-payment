package com.recharge.eDMT.eAddSenderBeneficiary.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AVerifyBeneficiaryAccountPojo {

    @SerializedName("SenderMobileNumber")
    @Expose
    private Object senderMobileNumber;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName(value = "Status", alternate = {"status", "STATUS"})
    @Expose
    private String status;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("AccountNo")
    @Expose
    private Object accountNo;
    @SerializedName("AccountType")
    @Expose
    private Object accountType;
    @SerializedName("IFSC")
    @Expose
    private Object iFSC;
    @SerializedName("Amount")
    @Expose
    private Object amount;
    @SerializedName("BankName")
    @Expose
    private Object bankName;
    @SerializedName("VerificationStatus")
    @Expose
    private String verificationStatus;
    @SerializedName("Balance")
    @Expose
    private Object balance;
    @SerializedName("IPayTransID")
    @Expose
    private Object iPayTransID;
    @SerializedName("Statuscode")
    @Expose
    private Object statuscode;
    @SerializedName("BankRefNo")
    @Expose
    private String bankRefNo;
    @SerializedName("OperatorTransID")
    @Expose
    private Object operatorTransID;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("PaymentRequest")
    @Expose
    private String paymentRequest;
    @SerializedName("PaymentResponse")
    @Expose
    private String paymentResponse;
    @SerializedName("SessionID")
    @Expose
    private String sessionID;
    @SerializedName("ValidationRequest")
    @Expose
    private String validationRequest;
    @SerializedName("ValidationResponse")
    @Expose
    private String validationResponse;

    public Object getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(Object senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public Object getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Object accountNo) {
        this.accountNo = accountNo;
    }

    public Object getAccountType() {
        return accountType;
    }

    public void setAccountType(Object accountType) {
        this.accountType = accountType;
    }

    public Object getIFSC() {
        return iFSC;
    }

    public void setIFSC(Object iFSC) {
        this.iFSC = iFSC;
    }

    public Object getAmount() {
        return amount;
    }

    public void setAmount(Object amount) {
        this.amount = amount;
    }

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public String getVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(String verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

    public Object getBalance() {
        return balance;
    }

    public void setBalance(Object balance) {
        this.balance = balance;
    }

    public Object getIPayTransID() {
        return iPayTransID;
    }

    public void setIPayTransID(Object iPayTransID) {
        this.iPayTransID = iPayTransID;
    }

    public Object getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(Object statuscode) {
        this.statuscode = statuscode;
    }

    public String getBankRefNo() {
        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {
        this.bankRefNo = bankRefNo;
    }

    public Object getOperatorTransID() {
        return operatorTransID;
    }

    public void setOperatorTransID(Object operatorTransID) {
        this.operatorTransID = operatorTransID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getValidationRequest() {
        return validationRequest;
    }

    public void setValidationRequest(String validationRequest) {
        this.validationRequest = validationRequest;
    }

    public String getValidationResponse() {
        return validationResponse;
    }

    public void setValidationResponse(String validationResponse) {
        this.validationResponse = validationResponse;
    }

}
