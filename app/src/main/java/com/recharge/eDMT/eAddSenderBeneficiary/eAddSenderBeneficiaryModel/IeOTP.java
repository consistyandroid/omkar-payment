package com.recharge.eDMT.eAddSenderBeneficiary.eAddSenderBeneficiaryModel;

public interface IeOTP {

    void getAddBeneficiaryVerifyOTPResponse(String response);

    void getAddBeneficiaryVerifyOTPFailResponse(String response);

    void alert(String status);

    void getResendOTPResponse(String response);

    void getResendOTPFailResponse(String response);

}
