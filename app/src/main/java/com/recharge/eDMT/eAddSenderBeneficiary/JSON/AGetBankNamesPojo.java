package com.recharge.eDMT.eAddSenderBeneficiary.JSON;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AGetBankNamesPojo {

    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("AccountNo")
    @Expose
    private Object accountNo;
    @SerializedName("IFSCCode")
    @Expose
    private String iFSCCode;
    @SerializedName("AccountName")
    @Expose
    private Object accountName;
    @SerializedName("BranchName")
    @Expose
    private Object branchName;
    @SerializedName("BankID")
    @Expose
    private Integer bankID;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Object getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Object accountNo) {
        this.accountNo = accountNo;
    }

    public String getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public Object getAccountName() {
        return accountName;
    }

    public void setAccountName(Object accountName) {
        this.accountName = accountName;
    }

    public Object getBranchName() {
        return branchName;
    }

    public void setBranchName(Object branchName) {
        this.branchName = branchName;
    }

    public Integer getBankID() {
        return bankID;
    }

    public void setBankID(Integer bankID) {
        this.bankID = bankID;
    }

}
