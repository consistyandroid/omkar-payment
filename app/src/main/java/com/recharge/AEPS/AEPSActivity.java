package com.recharge.AEPS;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import in.co.eko.ekopay.EkoPayActivity;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;

import in.omkarpayment.app.databinding.ActivityAepsBinding;
import in.omkarpayment.app.json.AEPSKeyResponsePojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AEPSKeyResponseDetails;

public class AEPSActivity extends AppCompatActivity implements IAEPSKeyResponceView {
    ActivityAepsBinding activityAepsBinding;
    Context contex = this;
    int AEPS_REQUEST_CODE;
    AlertImpl alert;
    CustomProgressDialog progressDialog;
    IAEPSKeyResponcePresenter iaepsKeyResponcePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        activityAepsBinding = DataBindingUtil.setContentView(this, R.layout.activity_aeps);
        alert = new AlertImpl(this);
        progressDialog = new CustomProgressDialog(this);
        iaepsKeyResponcePresenter = new IAEPSKeyResponcePresenterImpl(this);
        iaepsKeyResponcePresenter.getAepsKeyResponse();

        activityAepsBinding.btnCashWithDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AEPS_REQUEST_CODE = 10923;

                Intent intent = new Intent(contex, EkoPayActivity.class);
                Bundle bundle = new Bundle();

                String environment = "uat";
                String developer_key = AEPSKeyResponseDetails.developerkey;
                Log.i("Developer Key",developer_key);
                String initiator_id =AEPSKeyResponseDetails.initiatorid;
                String user_code = AEPSKeyResponseDetails.usercode;
                String initiator_logo_url = AEPSKeyResponseDetails.LogoURL;
                String partner_name = "PARTNER Name INC";
                String language = "en";
                String secret_key_timestamp = AEPSKeyResponseDetails.SecretKeyTimestamp;
                String secret_key = AEPSKeyResponseDetails.SecretKey;

                bundle.putString("environment", environment);
                bundle.putString("product", "aeps");
                bundle.putString("secret_key_timestamp", secret_key_timestamp);
                bundle.putString("secret_key", secret_key);
                bundle.putString("developer_key", developer_key);
                bundle.putString("initiator_id", initiator_id);
                bundle.putString("callback_url", "https://omkarpayment.in/callback.aspx");
                bundle.putString("user_code", user_code);
                bundle.putString("initiator_logo_url", initiator_logo_url);
                bundle.putString("partner_name", partner_name);
                bundle.putString("language", language);

                intent.putExtras(bundle);
                startActivityForResult(intent, AEPS_REQUEST_CODE);

            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AEPS_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) { //user taps CLOSE button after transaction -- case 1
                String response = data.getStringExtra("result");

            } else if (resultCode == Activity.RESULT_CANCELED) { // user presses back button
                if (data == null) {
                    // ------ If user pressed back without transaction -- case 2
                } else {
                    String response = data.getStringExtra("result");
                    Log.i("AEPS Resp.", response);
                    if (response != null && !response.equalsIgnoreCase("")) {
                        //------ If there is some error in partner parameters, response is that error
                        //------ when user performs the transaction, response is transaction data -- case 1
                    } else {

                    }
                }
            }
        }
    }

    @Override
    public void errorAlert(String msg) {
        alert.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        alert.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        progressDialog.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        progressDialog.dismissPDialog();
    }

    @Override
    public void AepsKeyResponse(AEPSKeyResponsePojo aepsKeyResponsePojo) {
        AEPSKeyResponseDetails.developerkey=aepsKeyResponsePojo.getDeveloperkey();
        AEPSKeyResponseDetails.initiatorid=aepsKeyResponsePojo.getInitiatorid();
        AEPSKeyResponseDetails.LogoURL=aepsKeyResponsePojo.getLogoURL();
        AEPSKeyResponseDetails.SecretKey=aepsKeyResponsePojo.getSecretKey();
        AEPSKeyResponseDetails.SecretKeyTimestamp=aepsKeyResponsePojo.getSecretKeyTimestamp();
        AEPSKeyResponseDetails.usercode=aepsKeyResponsePojo.getUsercode();
    }
}
