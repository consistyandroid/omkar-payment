package com.recharge.AEPS;

public interface IAEPSKeyResponcePresenter {

    void errorAlert(String msg);

    void successAlert(String msg);

    void showPDialog();

    void dismissPDialog();

    void getAepsKeyResponse();

    void AEPSKeyResponceFailResponse(String encryptedResponse);

    void AEPSKeyResponse(String encryptedResponse);

}
