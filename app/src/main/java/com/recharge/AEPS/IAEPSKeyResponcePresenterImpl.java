package com.recharge.AEPS;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.AEPSKeyResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.model.AEPSKeyResponseModel;

public class IAEPSKeyResponcePresenterImpl implements IAEPSKeyResponcePresenter {
    IAEPSKeyResponceView iaepsKeyResponceView;
    LogWriter log = new LogWriter();

    public IAEPSKeyResponcePresenterImpl(IAEPSKeyResponceView iaepsKeyResponceView) {
        this.iaepsKeyResponceView = iaepsKeyResponceView;
    }


    @Override
    public void errorAlert(String msg) {
        iaepsKeyResponceView.errorAlert(msg);
    }

    @Override
    public void successAlert(String msg) {
        iaepsKeyResponceView.successAlert(msg);
    }

    @Override
    public void showPDialog() {
        iaepsKeyResponceView.showPDialog();
    }

    @Override
    public void dismissPDialog() {
        iaepsKeyResponceView.dismissPDialog();
    }

    @Override
    public void getAepsKeyResponse() {
        String keyData = new KeyDataReader().get();
        AEPSKeyResponseModel model = new AEPSKeyResponseModel();
        model.delegate = this;
        model.aepsKeyResponse("", keyData);
    }

    @Override
    public void AEPSKeyResponceFailResponse(String encryptedResponse) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(encryptedResponse);
            log.i("Aeps Key Fail Resp.", body);
            iaepsKeyResponceView.errorAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void AEPSKeyResponse(String encryptedResponse) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(encryptedResponse);
            log.i("Aeps Key Resp.",body);
            Gson gson = new Gson();
            AEPSKeyResponsePojo aepsKeyResponsePojo = gson.fromJson(body, new TypeToken<AEPSKeyResponsePojo>() {
            }.getType());
            log.i("Aeps Key Resp.", aepsKeyResponsePojo.getSecretKeyTimestamp());
            iaepsKeyResponceView.AepsKeyResponse(aepsKeyResponsePojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
