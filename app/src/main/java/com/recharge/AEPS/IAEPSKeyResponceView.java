package com.recharge.AEPS;

import in.omkarpayment.app.json.AEPSKeyResponsePojo;

public interface IAEPSKeyResponceView {
    void errorAlert(String msg);

    void successAlert(String msg);

    void showPDialog();

    void dismissPDialog();
    void AepsKeyResponse(AEPSKeyResponsePojo aepsKeyResponsePojo);

}
