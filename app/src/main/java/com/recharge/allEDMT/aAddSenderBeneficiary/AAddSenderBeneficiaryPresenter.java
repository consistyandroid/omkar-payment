package com.recharge.allEDMT.aAddSenderBeneficiary;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AAddSenderBeneficiaryModel;
import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AIAddSenderBeneficiaryModel;
import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AIOTP;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AAddBeneficiaryOTPVerificationPojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AAddSenderBeneficiaryResponsePojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AGetBankNamesPojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AddBeneficiaryPojo;
import com.recharge.allEDMT.aGetSenderDetails.aGetSenderDetailsModel.AGetSenderDetailsModel;
import com.recharge.allEDMT.aGetSenderDetails.json.AResendOTPResponsePojo;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.json.GetBankNamesPojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;

public class AAddSenderBeneficiaryPresenter implements AIAddSenderBeneficiaryPresenter, AIAddSenderBeneficiaryModel, AIOTP {

    private AIAddSenderBeneficiaryView iCAddSenderBeneficiaryView;
    private LogWriter log = new LogWriter();

    public AAddSenderBeneficiaryPresenter(AIAddSenderBeneficiaryView iCAddSenderBeneficiaryView) {
        this.iCAddSenderBeneficiaryView = iCAddSenderBeneficiaryView;
    }

    @Override
    public void validateBeneficiary() {
        if (validation()) {
            iCAddSenderBeneficiaryView.confirmDialog();
        }
    }

    @Override
    public void validateSenderBeneficiary() {
        if (senderNameValidate()) {
            iCAddSenderBeneficiaryView.confirmDialog();
        }
    }

    @Override
    public void validateVerifyAccount() {
        if (!validation()) {
            iCAddSenderBeneficiaryView.resetValidationFlag();
            return;
        }
        iCAddSenderBeneficiaryView.verifyBeneficiaryAccount();
    }

    private boolean senderNameValidate() {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(iCAddSenderBeneficiaryView.senderName().trim())) {
            iCAddSenderBeneficiaryView.editSenderNameError();
            return false;
        }

        if (!validation.isValidSenderMobileNumber(iCAddSenderBeneficiaryView.senderMobileNumber().trim())) {
            iCAddSenderBeneficiaryView.editSenderMobileNumberError();
            return false;
        }

        return true;
    }

    @Override
    public boolean validation() {

        Validation validation = new Validation();

        if (!validation.isValidSenderMobileNumber(AMoneyTransferUserDetails.aSenderMobileNumber)) {
            iCAddSenderBeneficiaryView.unableToFetchError("Sender mobile number.");
            return false;
        }
        if (!validation.isNullOrEmpty(iCAddSenderBeneficiaryView.beneficiaryName().trim())) {
            iCAddSenderBeneficiaryView.editBeneficiaryNameError();
            return false;
        }
        if (!validation.isNullOrEmpty(iCAddSenderBeneficiaryView.bankName().trim())) {
            iCAddSenderBeneficiaryView.editBankNameError();
            return false;
        }
        if (!validation.isNullOrEmpty(iCAddSenderBeneficiaryView.accountNumber().trim())) {
            iCAddSenderBeneficiaryView.editAccNoError();
            return false;
        }
        if (!validation.isValidIFSCode(iCAddSenderBeneficiaryView.IFSCCode().trim())) {
            iCAddSenderBeneficiaryView.editIFSCError();
            return false;
        }

        return true;
    }

    @Override
    public void verifyBeneficiaryAccount() {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderName)) {
            iCAddSenderBeneficiaryView.unableToFetchError("Sender name.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyBeneficiaryAccount = new JSONObject();
        try {

            verifyBeneficiaryAccount.put("BeneficiaryName", iCAddSenderBeneficiaryView.beneficiaryName().trim());
            verifyBeneficiaryAccount.put("Bank", iCAddSenderBeneficiaryView.bankName());
            verifyBeneficiaryAccount.put("SenderName", AMoneyTransferUserDetails.aSenderName);
            verifyBeneficiaryAccount.put("AccountNumber", iCAddSenderBeneficiaryView.accountNumber().trim());
            verifyBeneficiaryAccount.put("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
            verifyBeneficiaryAccount.put("SenderId", AMoneyTransferUserDetails.aSenderID);
            verifyBeneficiaryAccount.put("IFSC", iCAddSenderBeneficiaryView.IFSCCode());
            verifyBeneficiaryAccount.put("IPAddress", UserDetails.IMEI);

            String jsonRequest = verifyBeneficiaryAccount.toString();
            log.i("verify initially bene Account request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
            model.aIAddSenderBeneficiaryModel = this;
            model.verifyBeneficiaryAccount(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyOTPForAddSender(String otp, String senderMobileNumber, String senderID) {
        Validation v = new Validation();

        if (!v.isNullOrEmpty(otp)) {
            iCAddSenderBeneficiaryView.unableToFetchError("OTP.");
            return;
        }

        if (!v.isValidSenderMobileNumber(senderMobileNumber)) {
            iCAddSenderBeneficiaryView.unableToFetchError("sender mobile number.");
            return;
        }

        if (!v.isNullOrEmpty(senderID)) {
            iCAddSenderBeneficiaryView.unableToFetchError("sender ID.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifySenderOTP = new JSONObject();
        try {
            verifySenderOTP.put("SenderID", senderID);
            verifySenderOTP.put("MobileNumber", senderMobileNumber);
            verifySenderOTP.put("OTP", otp);

            String jsonRequest = verifySenderOTP.toString();
            log.i("verify sender otp request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
            model.aIOTP = this;
            model.verifyOTPForAddSender(encryptString, keyData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBeneficiary() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addBeneficiary = new JSONObject();
        try {

            addBeneficiary.put("SenderId", AMoneyTransferUserDetails.aSenderID);
            addBeneficiary.put("Name", iCAddSenderBeneficiaryView.beneficiaryName().trim());
            addBeneficiary.put("LastName", iCAddSenderBeneficiaryView.beneficiaryName().trim());
            addBeneficiary.put("Bank", iCAddSenderBeneficiaryView.bankName().trim());
            addBeneficiary.put("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
            addBeneficiary.put("AccountNumber", iCAddSenderBeneficiaryView.accountNumber().trim());
            addBeneficiary.put("IFSC", iCAddSenderBeneficiaryView.IFSCCode());

            String jsonRequest = addBeneficiary.toString();
            log.i("add beneficiary request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
            model.aIAddSenderBeneficiaryModel = this;
            model.addBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addSenderBeneficiary() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject addSender = new JSONObject();
        try {

            addSender.put("FirstName", iCAddSenderBeneficiaryView.senderName());
            addSender.put("LastName", iCAddSenderBeneficiaryView.senderName());
            addSender.put("MobileNumber", iCAddSenderBeneficiaryView.senderMobileNumber());

            String jsonRequest = addSender.toString();
            log.i("add sender request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
            model.aIAddSenderBeneficiaryModel = this;
            model.addSenderWebservice(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getBankNames() {
        String keyData = new KeyDataReader().get();
        AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
        model.aIAddSenderBeneficiaryModel = this;
        model.getBanksNames(keyData);
    }

    @Override
    public void verifyOTPForAddBeneficiary(String otp) {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderID)) {
            iCAddSenderBeneficiaryView.unableToFetchError("Sender ID.");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryIDForAddVerification)) {
            iCAddSenderBeneficiaryView.unableToFetchError("Beneficiary ID.");
            return;
        }
        if (!validation.isNullOrEmpty(otp)) {
            iCAddSenderBeneficiaryView.unableToFetchError("OTP.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyOTPForAddBeneficiary = new JSONObject();
        try {
            verifyOTPForAddBeneficiary.put("OTP", otp);
            verifyOTPForAddBeneficiary.put("SenderID", AMoneyTransferUserDetails.aSenderID);
            verifyOTPForAddBeneficiary.put("BenficiaryID", AMoneyTransferUserDetails.aBeneficiaryIDForAddVerification);
            String jsonRequest = verifyOTPForAddBeneficiary.toString();
            log.i("add beneficiary OTP Verification request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
            model.aIOTP = this;
            model.verifyOTPForAddBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resendOTP() {

        Validation validation = new Validation();


        if (!validation.isValidSenderMobileNumber(AMoneyTransferUserDetails.aSenderMobileNumber)) {
            iCAddSenderBeneficiaryView.unableToFetchError("Sender Mobile Number.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject resendOTP = new JSONObject();
        try {
            resendOTP.put("MobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);

            String jsonRequest = resendOTP.toString();
            log.i("resend OTP request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }
            AGetSenderDetailsModel model = new AGetSenderDetailsModel();
            model.aIOTP = this;
            model.resendOTPForAddBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addSenderResponse(String response) {

        Validation v = new Validation();
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("addSenderResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            if (body.toLowerCase().contains("Sender registered successfully".toLowerCase())) {
                iCAddSenderBeneficiaryView.addSenderSuccessAlert(body);
                return;
            }

            Gson gson = new Gson();
            AAddSenderBeneficiaryResponsePojo addBeneficiaryResponsePojo = gson.fromJson(body, new TypeToken<AAddSenderBeneficiaryResponsePojo>() {
            }.getType());
            if (!addBeneficiaryResponsePojo.getStatus().equals("SUCCESS")) {
                iCAddSenderBeneficiaryView.alert(AllMessages.wrongResponce);
                return;
            }

            if (v.isNullOrEmpty(addBeneficiaryResponsePojo.getBenficiaryID())) {
                iCAddSenderBeneficiaryView.registrationSuccess("Beneficiary registered successfully");
                return;
            }
            iCAddSenderBeneficiaryView.addBeneficiarySuccessResponse(addBeneficiaryResponsePojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addSenderFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("addSenderFailResponse", body);

            if (body == null) {
                iCAddSenderBeneficiaryView.unableToFetchError("add sender/beneficiary data.");
                return;
            }

            iCAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verifyBeneficiaryAccountResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo = gson.fromJson(body, new TypeToken<AVerifyBeneficiaryAccountPojo>() {
            }.getType());

            if (aVerifyBeneficiaryAccountPojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                iCAddSenderBeneficiaryView.verifyBeneficiaryAccountSuccessAlert(aVerifyBeneficiaryAccountPojo);
                return;
            }
            iCAddSenderBeneficiaryView.alert(aVerifyBeneficiaryAccountPojo.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verify Beneficiary Account FailResponse", body);

            if (body == null) {
                iCAddSenderBeneficiaryView.unableToFetchError("verify Beneficiary Account data.");
                return;
            }
            iCAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddSenderVerifyOTPFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verify Add Sender Fail Response", body);

            if (body == null) {
                iCAddSenderBeneficiaryView.unableToFetchError("verify sender.");
                return;
            }
            iCAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddSenderVerifyOTPResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verify Add Sender Response", body);

            if (body == null) {
                iCAddSenderBeneficiaryView.unableToFetchError("verify sender.");
                return;
            }
            iCAddSenderBeneficiaryView.addSenderSuccessAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getVerifyOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            if (!body.toLowerCase().contains("Beneficiary Added Successfully".toLowerCase())) {
                alert(body);
                return;
            }
            iCAddSenderBeneficiaryView.registrationSuccess(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getVerifyOTPFailResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            if (body.toLowerCase().contains("OTP invalid".toLowerCase())) {
                iCAddSenderBeneficiaryView.otpVerifyFailAlert(body);
            } else {
                Gson gson = new Gson();
                AAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body,
                        new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
                        }.getType());
                if (messageResponsePojo.getStatus().equalsIgnoreCase("FAIL")
                        || messageResponsePojo.getStatus().equalsIgnoreCase("FAILURE")) {
                    iCAddSenderBeneficiaryView.registrationSuccess(messageResponsePojo.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {
        iCAddSenderBeneficiaryView.alert(status);
    }

    @Override
    public void getResendOTPResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            iCAddSenderBeneficiaryView.resendOTPSuccessAlert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getResendOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                iCAddSenderBeneficiaryView.dismissPDialog();
                return;
            }

            if (body.toLowerCase().contains("Beneficiary Already Verified Error".toLowerCase())) {
                body = body.replace("Error Message: ", "");
                body = body.replace(" Error code : 224 Result Code : 1", "");
                iCAddSenderBeneficiaryView.resendOTPAlreadyExistAlert(body);
                return;
            }

            Gson gson = new Gson();
            AResendOTPResponsePojo aResendOTPResponsePojo = gson.fromJson(body, new TypeToken<AResendOTPResponsePojo>() {
            }.getType());
            if (aResendOTPResponsePojo.getStatus().equalsIgnoreCase("FAILURE")
                    || aResendOTPResponsePojo.getStatus().equalsIgnoreCase("FAIL")) {
                iCAddSenderBeneficiaryView.resendOTPSuccessAlert(aResendOTPResponsePojo.getMessage());
                return;
            }
            iCAddSenderBeneficiaryView.resendOTPSuccessAlert(aResendOTPResponsePojo.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getBanksNames(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getBanksNames", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                return;
            }

            Gson gson = new Gson();
            List<AGetBankNamesPojo> bankNames = gson.fromJson(body, new TypeToken<List<AGetBankNamesPojo>>() {
            }.getType());

            iCAddSenderBeneficiaryView.getBankNames(bankNames);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBeneficiaryFailResponse(String response) {
        Cryptography_Android data = new Cryptography_Android();
        try {
            log.i("add Beneficiary Fail Response", response);
            String body = data.Decrypt(response);
            log.i("add Beneficiary Fail Response", body);

            iCAddSenderBeneficiaryView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBeneficiaryResponse(String response) {
        Validation v = new Validation();
        Cryptography_Android data = new Cryptography_Android();
        try {
            log.i("add Beneficiary Response", response);
            String body = data.Decrypt(response);
            log.i("add Beneficiary Response", body);

            if (!v.isNullOrEmpty(body)) {
                iCAddSenderBeneficiaryView.unableToFetchError("data.");
                return;
            }
            Gson gson = new Gson();
            AddBeneficiaryPojo beneficiaryPojo = gson.fromJson(body, AddBeneficiaryPojo.class);
            if (!beneficiaryPojo.getStatus().equalsIgnoreCase("SUCCESS") &&
                    !v.isNullOrEmpty(beneficiaryPojo.getBeneficiaryId())) {
                iCAddSenderBeneficiaryView.unableToFetchError("beneficiary.");
                return;
            }
            iCAddSenderBeneficiaryView.registrationSuccess(beneficiaryPojo.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
