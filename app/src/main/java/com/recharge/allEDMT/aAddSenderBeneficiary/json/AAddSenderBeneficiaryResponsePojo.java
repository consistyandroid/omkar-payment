package com.recharge.allEDMT.aAddSenderBeneficiary.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AAddSenderBeneficiaryResponsePojo {

    @SerializedName("BenficiaryID")
    @Expose
    private String benficiaryID;
    @SerializedName(value = "SenderId", alternate = {"SenderID", "senderID"})
    @Expose
    private String senderID;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("AccountNo")
    @Expose
    private String accountNo;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Bank")
    @Expose
    private String bank;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("IsOTPValidate")
    @Expose
    private String isOTPValidate;
    @SerializedName("SenderMobileNumber")
    @Expose
    private String senderMobileNumber;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getBenficiaryID() {
        return benficiaryID;
    }

    public void setBenficiaryID(String benficiaryID) {
        this.benficiaryID = benficiaryID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getIFSC() {
        return iFSC;
    }

    public void setIFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getIsOTPValidate() {
        return isOTPValidate;
    }

    public void setIsOTPValidate(String isOTPValidate) {
        this.isOTPValidate = isOTPValidate;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
