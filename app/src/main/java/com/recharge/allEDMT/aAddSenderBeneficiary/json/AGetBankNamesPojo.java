package com.recharge.allEDMT.aAddSenderBeneficiary.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AGetBankNamesPojo {

    @SerializedName("BankId")
    @Expose
    private Integer bankId;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("DefaultIFSC")
    @Expose
    private String defaultIFSC;
    @SerializedName("DOC")
    @Expose
    private String dOC;
    @SerializedName("IsIMPSEnabled")
    @Expose
    private Boolean isIMPSEnabled;
    @SerializedName("IsBankActive")
    @Expose
    private Boolean isBankActive;

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getDefaultIFSC() {
        return defaultIFSC;
    }

    public void setDefaultIFSC(String defaultIFSC) {
        this.defaultIFSC = defaultIFSC;
    }

    public String getDOC() {
        return dOC;
    }

    public void setDOC(String dOC) {
        this.dOC = dOC;
    }

    public Boolean getIsIMPSEnabled() {
        return isIMPSEnabled;
    }

    public void setIsIMPSEnabled(Boolean isIMPSEnabled) {
        this.isIMPSEnabled = isIMPSEnabled;
    }

    public Boolean getIsBankActive() {
        return isBankActive;
    }

    public void setIsBankActive(Boolean isBankActive) {
        this.isBankActive = isBankActive;
    }

}