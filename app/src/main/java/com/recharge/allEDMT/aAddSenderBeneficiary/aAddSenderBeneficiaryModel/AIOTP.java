package com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel;

/**
 * Created by ${user} on 7/2/18.
 */

public interface AIOTP {

    void getAddBeneficiaryVerifyOTPResponse(String response);

    void getAddBeneficiaryVerifyOTPFailResponse(String response);

    void alert(String status);

    void getResendOTPResponse(String response);

    void getResendOTPFailResponse(String response);

    void verifyBeneficiaryAccountResponse(String response);

    void verifyBeneficiaryAccountFailResponse(String body);

    void getAddSenderVerifyOTPFailResponse(String response);

    void getAddSenderVerifyOTPResponse(String response);

}
