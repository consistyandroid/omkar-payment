package com.recharge.allEDMT.aAddSenderBeneficiary.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AVerifyBeneficiaryAccountPojo {

    @SerializedName("Statuscode")
    @Expose
    private String statuscode;
    @SerializedName(value = "Status", alternate = {"STATUS", "status"})
    @Expose
    private String status;
    @SerializedName("ProviderDOCNo")
    @Expose
    private String providerDOCNo;
    @SerializedName("ProviderRefNo")
    @Expose
    private String providerRefNo;
    @SerializedName("ProviderOprID")
    @Expose
    private String providerOprID;
    @SerializedName("MoneyTransferProviderOpeningBal")
    @Expose
    private String moneyTransferProviderOpeningBal;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("OTP")
    @Expose
    private String oTP;
    @SerializedName("BeneficiaryId")
    @Expose
    private String beneficiaryId;
    @SerializedName("SenderId")
    @Expose
    private String senderId;
    @SerializedName("SenderMobileNumber")
    @Expose
    private String senderMobileNumber;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("SenderOpeningBalance")
    @Expose
    private String senderOpeningBalance;
    @SerializedName("NetAmount")
    @Expose
    private Double netAmount;
    @SerializedName("PaymentType")
    @Expose
    private String paymentType;
    @SerializedName("OperatorID")
    @Expose
    private String operatorID;
    @SerializedName("MoneyTransferProviderID")
    @Expose
    private String moneyTransferProviderID;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("BeneficiaryMobileNo")
    @Expose
    private String beneficiaryMobileNo;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("Charge")
    @Expose
    private Double charge;
    @SerializedName("MoneyTransferServiceID")
    @Expose
    private Integer moneyTransferServiceID;
    @SerializedName("Through")
    @Expose
    private String through;
    @SerializedName("IPAddress")
    @Expose
    private String iPAddress;
    @SerializedName("MoneyTransferProviderClosingBal")
    @Expose
    private String moneyTransferProviderClosingBal;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("ToDate")
    @Expose
    private String toDate;
    @SerializedName("Bank")
    @Expose
    private String bank;
    @SerializedName("UserClosingBalance")
    @Expose
    private Double userClosingBalance;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("ConsumerNo")
    @Expose
    private String consumerNo;
    @SerializedName("OperatorId")
    @Expose
    private String operatorId;
    @SerializedName("ServiceID")
    @Expose
    private String serviceID;
    @SerializedName("RetailerName")
    @Expose
    private String retailerName;
    @SerializedName("TransactionType")
    @Expose
    private String transactionType;
    @SerializedName("MoneyTransferID")
    @Expose
    private String moneyTransferID;
    @SerializedName("ProviderRequestID")
    @Expose
    private String providerRequestID;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;
    @SerializedName("DistributorCommisson")
    @Expose
    private String distributorCommisson;
    @SerializedName("DMRRequest")
    @Expose
    private String dMRRequest;
    @SerializedName("DMRResponse")
    @Expose
    private String dMRResponse;
    @SerializedName("UserOpeningBalance")
    @Expose
    private Double userOpeningBalance;
    @SerializedName("SenderName")
    @Expose
    private String senderName;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("ParentName")
    @Expose
    private String parentName;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("DUserTypeID")
    @Expose
    private Integer dUserTypeID;
    @SerializedName("RUserTypeID")
    @Expose
    private Integer rUserTypeID;
    @SerializedName("BillNumber")
    @Expose
    private String billNumber;
    @SerializedName("AmountList")
    @Expose
    private String amountList;
    @SerializedName("ShopeName")
    @Expose
    private String shopeName;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("InstapayRefNo")
    @Expose
    private String instapayRefNo;
    @SerializedName("ProviderOperatorTransactionID")
    @Expose
    private String providerOperatorTransactionID;
    @SerializedName("BankErrorCode")
    @Expose
    private String bankErrorCode;
    @SerializedName("ApiUserRequestId")
    @Expose
    private String apiUserRequestId;
    @SerializedName("ApiOprID")
    @Expose
    private String apiOprID;
    @SerializedName("ApiRefNo")
    @Expose
    private String apiRefNo;
    @SerializedName("InboxID")
    @Expose
    private Integer inboxID;
    @SerializedName("TransactionId")
    @Expose
    private String TransactionId;

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProviderDOCNo() {
        return providerDOCNo;
    }

    public void setProviderDOCNo(String providerDOCNo) {
        this.providerDOCNo = providerDOCNo;
    }

    public String getProviderRefNo() {
        return providerRefNo;
    }

    public void setProviderRefNo(String providerRefNo) {
        this.providerRefNo = providerRefNo;
    }

    public String getProviderOprID() {
        return providerOprID;
    }

    public void setProviderOprID(String providerOprID) {
        this.providerOprID = providerOprID;
    }

    public String getMoneyTransferProviderOpeningBal() {
        return moneyTransferProviderOpeningBal;
    }

    public void setMoneyTransferProviderOpeningBal(String moneyTransferProviderOpeningBal) {
        this.moneyTransferProviderOpeningBal = moneyTransferProviderOpeningBal;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOTP() {
        return oTP;
    }

    public void setOTP(String oTP) {
        this.oTP = oTP;
    }

    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSenderOpeningBalance() {
        return senderOpeningBalance;
    }

    public void setSenderOpeningBalance(String senderOpeningBalance) {
        this.senderOpeningBalance = senderOpeningBalance;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public String getMoneyTransferProviderID() {
        return moneyTransferProviderID;
    }

    public void setMoneyTransferProviderID(String moneyTransferProviderID) {
        this.moneyTransferProviderID = moneyTransferProviderID;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryMobileNo() {
        return beneficiaryMobileNo;
    }

    public void setBeneficiaryMobileNo(String beneficiaryMobileNo) {
        this.beneficiaryMobileNo = beneficiaryMobileNo;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Integer getMoneyTransferServiceID() {
        return moneyTransferServiceID;
    }

    public void setMoneyTransferServiceID(Integer moneyTransferServiceID) {
        this.moneyTransferServiceID = moneyTransferServiceID;
    }

    public String getThrough() {
        return through;
    }

    public void setThrough(String through) {
        this.through = through;
    }

    public String getIPAddress() {
        return iPAddress;
    }

    public void setIPAddress(String iPAddress) {
        this.iPAddress = iPAddress;
    }

    public String getMoneyTransferProviderClosingBal() {
        return moneyTransferProviderClosingBal;
    }

    public void setMoneyTransferProviderClosingBal(String moneyTransferProviderClosingBal) {
        this.moneyTransferProviderClosingBal = moneyTransferProviderClosingBal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Double getUserClosingBalance() {
        return userClosingBalance;
    }

    public void setUserClosingBalance(Double userClosingBalance) {
        this.userClosingBalance = userClosingBalance;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getMoneyTransferID() {
        return moneyTransferID;
    }

    public void setMoneyTransferID(String moneyTransferID) {
        this.moneyTransferID = moneyTransferID;
    }

    public String getProviderRequestID() {
        return providerRequestID;
    }

    public void setProviderRequestID(String providerRequestID) {
        this.providerRequestID = providerRequestID;
    }

    public String getIFSC() {
        return iFSC;
    }

    public void setIFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public String getDistributorCommisson() {
        return distributorCommisson;
    }

    public void setDistributorCommisson(String distributorCommisson) {
        this.distributorCommisson = distributorCommisson;
    }

    public String getDMRRequest() {
        return dMRRequest;
    }

    public void setDMRRequest(String dMRRequest) {
        this.dMRRequest = dMRRequest;
    }

    public String getDMRResponse() {
        return dMRResponse;
    }

    public void setDMRResponse(String dMRResponse) {
        this.dMRResponse = dMRResponse;
    }

    public Double getUserOpeningBalance() {
        return userOpeningBalance;
    }

    public void setUserOpeningBalance(Double userOpeningBalance) {
        this.userOpeningBalance = userOpeningBalance;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public Integer getDUserTypeID() {
        return dUserTypeID;
    }

    public void setDUserTypeID(Integer dUserTypeID) {
        this.dUserTypeID = dUserTypeID;
    }

    public Integer getRUserTypeID() {
        return rUserTypeID;
    }

    public void setRUserTypeID(Integer rUserTypeID) {
        this.rUserTypeID = rUserTypeID;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getAmountList() {
        return amountList;
    }

    public void setAmountList(String amountList) {
        this.amountList = amountList;
    }

    public String getShopeName() {
        return shopeName;
    }

    public void setShopeName(String shopeName) {
        this.shopeName = shopeName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getInstapayRefNo() {
        return instapayRefNo;
    }

    public void setInstapayRefNo(String instapayRefNo) {
        this.instapayRefNo = instapayRefNo;
    }

    public String getProviderOperatorTransactionID() {
        return providerOperatorTransactionID;
    }

    public void setProviderOperatorTransactionID(String providerOperatorTransactionID) {
        this.providerOperatorTransactionID = providerOperatorTransactionID;
    }

    public String getBankErrorCode() {
        return bankErrorCode;
    }

    public void setBankErrorCode(String bankErrorCode) {
        this.bankErrorCode = bankErrorCode;
    }

    public String getApiUserRequestId() {
        return apiUserRequestId;
    }

    public void setApiUserRequestId(String apiUserRequestId) {
        this.apiUserRequestId = apiUserRequestId;
    }

    public String getApiOprID() {
        return apiOprID;
    }

    public void setApiOprID(String apiOprID) {
        this.apiOprID = apiOprID;
    }

    public String getApiRefNo() {
        return apiRefNo;
    }

    public void setApiRefNo(String apiRefNo) {
        this.apiRefNo = apiRefNo;
    }

    public Integer getInboxID() {
        return inboxID;
    }

    public void setInboxID(Integer inboxID) {
        this.inboxID = inboxID;
    }

}