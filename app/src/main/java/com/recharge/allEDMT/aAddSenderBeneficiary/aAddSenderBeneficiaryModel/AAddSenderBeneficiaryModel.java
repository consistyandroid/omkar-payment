package com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel;

import com.recharge.allEDMT.aGetSenderDetails.aGetSenderDetailsModel.AIWebServicesMoneyTransfer;


import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AAddSenderBeneficiaryModel {

    public AIAddSenderBeneficiaryModel aIAddSenderBeneficiaryModel = null;
    public AIOTP aIOTP = null;

    public void getBanksNames(String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.GetBankDetailWebService(keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIAddSenderBeneficiaryModel.getBanksNames(response.body().getBody());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
            }
        });
    }

    public void addSenderWebservice(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.SenderRegistrationWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
                        return;
                    }

                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIAddSenderBeneficiaryModel.addSenderResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") &&
                            !response.body().getBody().equals("")) {
                        aIAddSenderBeneficiaryModel.addSenderFailResponse(response.body().getBody());
                    } else {
                        aIAddSenderBeneficiaryModel.alert(response.body().getStatus());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
            }
        });
    }

    public void addBeneficiary(String encryptString, String keyData) {

        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);
        Call<WebserviceResponsePojo> call = webObject.AddBeneficiaryWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIAddSenderBeneficiaryModel.addBeneficiaryResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIAddSenderBeneficiaryModel.addBeneficiaryFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        aIAddSenderBeneficiaryModel.alert(response.body().getStatus());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
            }
        });
    }

    public void verifyBeneficiaryAccount(String encryptString, String keyData) {

        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);
        Call<WebserviceResponsePojo> call = webObject.VerifyBeneficiaryAccountWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIAddSenderBeneficiaryModel.verifyBeneficiaryAccountResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIAddSenderBeneficiaryModel.verifyBeneficiaryAccountFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        aIAddSenderBeneficiaryModel.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIAddSenderBeneficiaryModel.alert(AllMessages.internetError);
            }
        });
    }

    public void verifyOTPForAddBeneficiary(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.AddBeneficiarySubmitOTPWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIOTP.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        aIOTP.getAddBeneficiaryVerifyOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIOTP.getAddBeneficiaryVerifyOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        aIOTP.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIOTP.alert(AllMessages.internetError);
            }
        });
    }

    public void verifyOTPForAddSender(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.AddSenderSubmitOTPWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIOTP.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        aIOTP.getAddSenderVerifyOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIOTP.getAddSenderVerifyOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        aIOTP.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIOTP.alert(AllMessages.internetError);
            }
        });
    }

}
