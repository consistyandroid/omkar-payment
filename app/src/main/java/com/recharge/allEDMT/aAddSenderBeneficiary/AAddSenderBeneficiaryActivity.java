package com.recharge.allEDMT.aAddSenderBeneficiary;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.recharge.allEDMT.aAddSenderBeneficiary.json.AAddSenderBeneficiaryResponsePojo;

import com.recharge.allEDMT.aAddSenderBeneficiary.json.AGetBankNamesPojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;

import java.util.List;


import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityAddBeneficiaryBinding;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;

import static android.widget.Toast.LENGTH_LONG;


public class AAddSenderBeneficiaryActivity extends AppCompatActivity implements AIAddSenderBeneficiaryView {

    public static int flag_type, verifyBeneficiaryAccountCountFlag = 0;
    ActivityAddBeneficiaryBinding addBeneficiaryBinding;
    Context context = this;
    AIAddSenderBeneficiaryPresenter iCAddSenderBeneficiaryPresenter;
    String title, Type;
    AlertImpl alert;
    CustomProgressDialog pDialog;
    NetworkState ns = new NetworkState();
    LogWriter log = new LogWriter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addBeneficiaryBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_beneficiary);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setBundle();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

       /* TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);

        TextView txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        //txtToolbarBalance.setText("" + UserDetails.UserBalance);
        txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) );//+ "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));
*/
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);
        iCAddSenderBeneficiaryPresenter = new AAddSenderBeneficiaryPresenter(this);
        if (Type.equals("AddBeneficiary")) {
            flag_type = 2;
            addBeneficiaryBinding.LaySender.setVisibility(View.GONE);
        } else {
            flag_type = 1;
            addBeneficiaryBinding.editSenderMobileNumber.setFocusable(false);
            addBeneficiaryBinding.editSenderMobileNumber.setText(AMoneyTransferUserDetails.aSenderMobileNumber);
            addBeneficiaryBinding.LayBeneficiary.setVisibility(View.GONE);
        }
        addBeneficiaryBinding.txtBankName.setVisibility(View.GONE);
        iCAddSenderBeneficiaryPresenter.getBankNames();

        addBeneficiaryBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });
        addBeneficiaryBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Type.equals("AddSender")) {
                    flag_type = 1;
                    iCAddSenderBeneficiaryPresenter.validateSenderBeneficiary();
                } else if (Type.equals("AddBeneficiary")) {
                    flag_type = 2;
                    AMoneyTransferUserDetails.aBeneficiaryBankName = addBeneficiaryBinding.editBankName.getText().toString().trim();
                    iCAddSenderBeneficiaryPresenter.validateBeneficiary();
                }
                log.i("cc", "click");

            }
        });
        addBeneficiaryBinding.editBankName.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                AGetBankNamesPojo object = (AGetBankNamesPojo) parent.getItemAtPosition(pos);
                AMoneyTransferUserDetails.aBeneficiaryBankName = object.getBankName();
                AMoneyTransferUserDetails.aBeneficiaryIFSC = object.getDefaultIFSC();
                if (AMoneyTransferUserDetails.aBeneficiaryIFSC != null) {
                    addBeneficiaryBinding.editIFSC.setText(AMoneyTransferUserDetails.aBeneficiaryIFSC);
                }
            }
        });

        addBeneficiaryBinding.editIFSC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AMoneyTransferUserDetails.aBeneficiaryIFSC = addBeneficiaryBinding.editIFSC.getText().toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addBeneficiaryBinding.editAccNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (verifyBeneficiaryAccountCountFlag == 0) {
                    if (s.length() > 5) {
                        addBeneficiaryBinding.txtVerifyAccount.setVisibility(View.VISIBLE);
                    }
                    if (s.length() < 6) {
                        addBeneficiaryBinding.txtVerifyAccount.setVisibility(View.GONE);
                    }
                } else {
                    addBeneficiaryBinding.txtVerifyAccount.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addBeneficiaryBinding.txtVerifyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyBeneficiaryAccountCountFlag = 1;
                addBeneficiaryBinding.txtVerifyAccount.setVisibility(View.GONE);
                iCAddSenderBeneficiaryPresenter.validateVerifyAccount();
            }
        });
    }

    private void clear() {

        verifyBeneficiaryAccountCountFlag = 0;

        addBeneficiaryBinding.editSenderName.setText("");

        addBeneficiaryBinding.editBenifName.setText("");
        addBeneficiaryBinding.editBankName.setText("");
        addBeneficiaryBinding.editAccNo.setText("");
        addBeneficiaryBinding.txtVerifiedAccount.setVisibility(View.GONE);
        addBeneficiaryBinding.editIFSC.setText("");

        AMoneyTransferUserDetails.aBeneficiaryBankName = "";
        AMoneyTransferUserDetails.aBeneficiaryIFSC = "";

        if (Type.equalsIgnoreCase("AddSender")) {
            addBeneficiaryBinding.editSenderName.requestFocus();
            return;
        }
        addBeneficiaryBinding.editBenifName.requestFocus();
    }

    private void setBundle() {

        verifyBeneficiaryAccountCountFlag = 0;
        Bundle extras = getIntent().getExtras();
        AMoneyTransferUserDetails.aSenderMobileNumber = extras.getString("SenderMobileNumber");
        Type = extras.getString("Type");

        try {
            title = extras.getString("Title");
        } catch (Exception ex) {
            title = "Add Beneficiary";
        }
    }

    @Override
    public void resetValidationFlag() {
        verifyBeneficiaryAccountCountFlag = 0;
        addBeneficiaryBinding.txtVerifyAccount.setVisibility(View.VISIBLE);
    }

    @Override
    public void resendOTPAlreadyExistAlert(String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView title = dialog.findViewById(R.id.a);
        title.setText(getResources().getString(R.string.app_name));
        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent intent = getIntent();
                intent.putExtra("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onResultIntent() {
        Intent intent = getIntent();
        intent.putExtra("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public String beneficiaryName() {
        if (addBeneficiaryBinding.editBenifName.getText().toString().length() > 1) {
            AMoneyTransferUserDetails.aBeneficiaryFirstName = addBeneficiaryBinding.editBenifName.getText().toString().trim();
        }
        return addBeneficiaryBinding.editBenifName.getText().toString().trim();
    }

    @Override
    public String mobileNumber() {
        return AMoneyTransferUserDetails.aSenderMobileNumber;
    }

    @Override
    public String accountNumber() {
        return addBeneficiaryBinding.editAccNo.getText().toString().trim();
    }

    @Override
    public String IFSCCode() {
        if (AMoneyTransferUserDetails.aBeneficiaryIFSC == null ||
                AMoneyTransferUserDetails.aBeneficiaryIFSC.isEmpty() ||
                AMoneyTransferUserDetails.aBeneficiaryIFSC.equals("") ||
                AMoneyTransferUserDetails.aBeneficiaryIFSC.length() < 4) {
            AMoneyTransferUserDetails.aBeneficiaryIFSC = addBeneficiaryBinding.editIFSC.getText().toString().trim();
        }
        return AMoneyTransferUserDetails.aBeneficiaryIFSC;
    }

    @Override
    public String bankName() {
        return AMoneyTransferUserDetails.aBeneficiaryBankName;
    }

    @Override
    public String senderName() {
        return addBeneficiaryBinding.editSenderName.getText().toString().trim();
    }

    @Override
    public String senderMobileNumber() {
        return addBeneficiaryBinding.editSenderMobileNumber.getText().toString().trim();
    }

    @Override
    public void editBeneficiaryNameError() {
        addBeneficiaryBinding.editBenifName.setError(String.format(AllMessages.pleaseEnter, "first name"));
        addBeneficiaryBinding.editBenifName.requestFocus();
    }

    @Override
    public void editAccNoError() {
        addBeneficiaryBinding.editAccNo.setError(String.format(AllMessages.pleaseEnter, "account number"));
        addBeneficiaryBinding.editAccNo.requestFocus();
    }

    @Override
    public void confirmDialog() {

        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
            return;
        }

        if (Type.equals("AddSender")) {
            iCAddSenderBeneficiaryPresenter.addSenderBeneficiary();
        } else if (Type.equals("AddBeneficiary")) {
            iCAddSenderBeneficiaryPresenter.addBeneficiary();
        }
        pDialog.showPDialog();
    }

    @Override
    public void verifyBeneficiaryAccount() {
        if (!ns.isInternetAvailable(context)) {
            Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
            return;
        }
        iCAddSenderBeneficiaryPresenter.verifyBeneficiaryAccount();
        pDialog.showPDialog();
    }

    @Override
    public void verifyBeneficiaryAccountSuccessAlert(final AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(aVerifyBeneficiaryAccountPojo.getMessage());
        if (aVerifyBeneficiaryAccountPojo.getStatus().equals("SUCCESS")) {
            dialogTitle.setTextColor(Color.WHITE);
        }
        text.setText("Beneficiary Name : " + aVerifyBeneficiaryAccountPojo.getBeneficiaryName() +
                "\nBank Name : " + bankName() + "\nAccount Number : " + accountNumber());
        text.setGravity(0);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                addBeneficiaryBinding.txtVerifiedAccount.setVisibility(View.VISIBLE);
                addBeneficiaryBinding.editBenifName.setText(aVerifyBeneficiaryAccountPojo.getBeneficiaryName());
            }
        });
        dialog.show();
    }

    @Override
    public void editIFSCError() {
        Toast.makeText(context, "Please select Bank or Enter Correct IFSCCode", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void editSenderNameError() {
        addBeneficiaryBinding.editSenderName.setError("Please enter sender name");
        addBeneficiaryBinding.editSenderName.requestFocus();
    }

    @Override
    public void editSenderMobileNumberError() {
        addBeneficiaryBinding.editSenderMobileNumber.setError("Please enter sender mobile number");
        addBeneficiaryBinding.editSenderMobileNumber.requestFocus();
    }

    @Override
    public void alert(String msg) {
        pDialog.dismissPDialog();
        alert.errorAlert(msg);
    }

    @Override
    public void otpVerifyFailAlert(String msg) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary OTP Verification");
        text.setText(msg + ". We will send new OTP on your registered mobile number.");
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                iCAddSenderBeneficiaryPresenter.resendOTP();
                dialog.dismiss();
                pDialog.showPDialog();
                reopenOTPDialog();
            }
        });
        dialog.show();
    }

    private void reopenOTPDialog() {
        pDialog.dismissPDialog();
        final Dialog OTPDialog = new Dialog(context);
        OTPDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }

                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                    return;
                }
                iCAddSenderBeneficiaryPresenter.verifyOTPForAddBeneficiary(editOTP.getText().toString());
                OTPDialog.dismiss();
                pDialog.showPDialog();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });

        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AMoneyTransferUserDetails.aSenderMobileNumber.equals("")) {
                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    iCAddSenderBeneficiaryPresenter.resendOTP();
                    btnResend.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    pDialog.showPDialog();
                } else {
                    alert.errorAlert("Something Wrong");
                }
            }
        });
        try {
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    btnCancel.setVisibility(View.GONE);
                    btnResend.setVisibility(View.VISIBLE);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    @Override
    public void editBankNameError() {
        addBeneficiaryBinding.editBankName.setError(String.format(AllMessages.pleaseEnter, "bank name"));
        addBeneficiaryBinding.editBankName.requestFocus();
    }

    @Override
    public void addSenderSuccessAlert(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView title = dialog.findViewById(R.id.a);
        title.setText(getResources().getString(R.string.app_name));
        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent intent = getIntent();
                intent.putExtra("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        dialog.show();
    }



    @Override
    public void getBankNames(List<AGetBankNamesPojo> bankList) {
        AllEAutoBankNameAdapter autocompleteTextAdapter =
                new AllEAutoBankNameAdapter(context, R.layout.adapter_autocomplete_text, bankList);
        addBeneficiaryBinding.editBankName.setThreshold(1);
        addBeneficiaryBinding.editBankName.setAdapter(autocompleteTextAdapter);
    }

    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to send/receive data!");
    }

    @Override
    public void unableToFetchError(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive " + message + " Please restart your app.");
    }

    @Override
    public void addBeneficiarySuccessResponse(AAddSenderBeneficiaryResponsePojo addBeneficiaryResponsePojo) {
        pDialog.dismissPDialog();
        if (flag_type == 1) {
            senderOTPDialog(addBeneficiaryResponsePojo);
            return;
        }
        Toast.makeText(context, addBeneficiaryResponsePojo.getMessage(), LENGTH_LONG).show();
        otpDialog(addBeneficiaryResponsePojo);
    }

    private void senderOTPDialog(final AAddSenderBeneficiaryResponsePojo addBeneficiaryResponsePojo) {

        Toast.makeText(context, addBeneficiaryResponsePojo.getMessage(), LENGTH_LONG).show();

        final Dialog OTPDialog = new Dialog(context);
        OTPDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                    return;
                }
                pDialog.showPDialog();
                iCAddSenderBeneficiaryPresenter.verifyOTPForAddSender(editOTP.getText().toString(), senderMobileNumber(), addBeneficiaryResponsePojo.getSenderID());
                OTPDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });

        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AMoneyTransferUserDetails.aSenderMobileNumber.equals("")) {
                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    pDialog.showPDialog();
                    iCAddSenderBeneficiaryPresenter.resendOTP();
                    btnResend.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    return;
                }
                OTPDialog.dismiss();
                alert.errorAlert("Sender mobile number not found! Please try again!");
            }
        });

        try {
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    btnCancel.setVisibility(View.GONE);
                    btnResend.setVisibility(View.VISIBLE);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    @Override
    public void registrationSuccess(String message) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary Registration");
        text.setText(message);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onResultIntent();
            }
        });
        dialog.show();
    }

    @Override
    public void resendOTPSuccessAlert(String response) {
        pDialog.dismissPDialog();
        Toast.makeText(context, response, LENGTH_LONG).show();
    }

    private void otpDialog(AAddSenderBeneficiaryResponsePojo addBeneficiaryResponsePojo) {
        AMoneyTransferUserDetails.aSenderID = String.valueOf(addBeneficiaryResponsePojo.getSenderID());
        AMoneyTransferUserDetails.aBeneficiaryIDForAddVerification = String.valueOf(addBeneficiaryResponsePojo.getBenficiaryID());
        final Dialog OTPDialog = new Dialog(context);
        OTPDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(context)) {
                    Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                    return;
                }
                iCAddSenderBeneficiaryPresenter.verifyOTPForAddBeneficiary(editOTP.getText().toString());
                OTPDialog.dismiss();
                pDialog.showPDialog();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });

        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AMoneyTransferUserDetails.aSenderMobileNumber.equals("")) {
                    if (!ns.isInternetAvailable(context)) {
                        Toast.makeText(context, AllMessages.internetError, LENGTH_LONG).show();
                        return;
                    }
                    iCAddSenderBeneficiaryPresenter.resendOTP();
                    btnResend.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    pDialog.showPDialog();
                } else {
                    alert.errorAlert("Something Wrong");
                }
            }
        });
        try {
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    btnCancel.setVisibility(View.GONE);
                    btnResend.setVisibility(View.VISIBLE);
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
