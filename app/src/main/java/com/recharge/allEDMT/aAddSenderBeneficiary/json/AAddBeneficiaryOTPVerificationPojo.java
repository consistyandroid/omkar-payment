package com.recharge.allEDMT.aAddSenderBeneficiary.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AAddBeneficiaryOTPVerificationPojo {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("PaymentRequest")
    @Expose
    private String paymentRequest;
    @SerializedName("PaymentResponse")
    @Expose
    private String paymentResponse;
    @SerializedName("ValidationRequest")
    @Expose
    private String validationRequest;
    @SerializedName("ValidationResponse")
    @Expose
    private String validationResponse;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public String getValidationRequest() {
        return validationRequest;
    }

    public void setValidationRequest(String validationRequest) {
        this.validationRequest = validationRequest;
    }

    public String getValidationResponse() {
        return validationResponse;
    }

    public void setValidationResponse(String validationResponse) {
        this.validationResponse = validationResponse;
    }

}
