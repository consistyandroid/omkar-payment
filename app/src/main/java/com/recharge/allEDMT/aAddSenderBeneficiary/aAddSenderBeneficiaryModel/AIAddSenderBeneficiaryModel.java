package com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel;

public interface AIAddSenderBeneficiaryModel {

    void addSenderResponse(String response);

    void addSenderFailResponse(String response);

    void alert(String status);

    void verifyBeneficiaryAccountResponse(String response);

    void verifyBeneficiaryAccountFailResponse(String response);

    void getBanksNames(String body);

    void addBeneficiaryFailResponse(String response);

    void addBeneficiaryResponse(String response);

}
