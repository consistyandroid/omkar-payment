package com.recharge.allEDMT.aAddSenderBeneficiary;


import com.recharge.allEDMT.aAddSenderBeneficiary.json.AAddSenderBeneficiaryResponsePojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AGetBankNamesPojo;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;

import java.util.List;

import in.omkarpayment.app.json.GetBankNamesPojo;

public interface AIAddSenderBeneficiaryView {

    String beneficiaryName();

    String mobileNumber();

    String accountNumber();

    String IFSCCode();

    String bankName();

    String senderName();

    String senderMobileNumber();

    void editBeneficiaryNameError();

    void editAccNoError();

    void confirmDialog();

    void editIFSCError();

    void editSenderNameError();

    void editSenderMobileNumberError();

    void alert(String msg);

    void editBankNameError();

    void getBankNames(List<AGetBankNamesPojo> bankList);

    void addSenderSuccessAlert(String message);

    void addBeneficiarySuccessResponse(AAddSenderBeneficiaryResponsePojo addBeneficiaryResponsePojo);

    void registrationSuccess(String message);

    void resendOTPSuccessAlert(String response);

    void otpVerifyFailAlert(String body);

    void verifyBeneficiaryAccount();

    void verifyBeneficiaryAccountSuccessAlert(AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo);

    void dismissPDialog();

    void unableToFetchError(String message);

    void resetValidationFlag();

    void resendOTPAlreadyExistAlert(String message);

}
