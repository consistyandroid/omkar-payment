package com.recharge.allEDMT.aAddSenderBeneficiary;

public interface AIAddSenderBeneficiaryPresenter {

    void validateBeneficiary();

    void validateSenderBeneficiary();

    boolean validation();

    void addBeneficiary();

    void addSenderBeneficiary();

    void getBankNames();

    void resendOTP();

    void verifyOTPForAddBeneficiary(String otp);

    void validateVerifyAccount();

    void verifyBeneficiaryAccount();

    void verifyOTPForAddSender(String otp, String senderMobileNumber, String senderID);

}
