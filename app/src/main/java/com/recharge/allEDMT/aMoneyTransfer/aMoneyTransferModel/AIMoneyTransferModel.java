package com.recharge.allEDMT.aMoneyTransfer.aMoneyTransferModel;

public interface AIMoneyTransferModel {


    void getTransferResponseFail(String response);

    void getTransferResponse(String response);

    void alert(String status);

}
