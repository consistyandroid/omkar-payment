package com.recharge.allEDMT.aMoneyTransfer;

import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;

/**
 * Created by ${user} on 16/2/18.
 */

public interface AIMoneyTransferView {


    String TransferType();

    String Amount();

    void confirmClick();

    void editAmountError();

    void alert(String body);

    void transferSuccess(AVerifyBeneficiaryAccountPojo pojo);

    void dismissPDialog();

    void unableToFetchError(String message);

    void spTransferTypeError();

    void errorAlert(String message);
}
