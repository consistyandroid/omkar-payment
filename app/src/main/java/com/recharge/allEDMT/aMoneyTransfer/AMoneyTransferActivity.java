package com.recharge.allEDMT.aMoneyTransfer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;

import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.databinding.ActivityTransferBinding;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;


public class AMoneyTransferActivity extends AppCompatActivity implements AIMoneyTransferView {
    public static String[] MoneyTransType = {"IMPS", "NEFT"}; //
    String TransferType;
    ActivityTransferBinding activityTransferBinding;
    ArrayAdapter<String> spAdapter;
    Context context = this;
    AlertImpl alert;
    CustomProgressDialog pDialog;
    AIMoneyTransferPresenter iCMoneyTransferPresenter;
    NetworkState ns = new NetworkState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityTransferBinding = DataBindingUtil.setContentView(this, R.layout.activity_transfer);
        iCMoneyTransferPresenter = new AMoneyTransferPresenter(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String title = "DMT";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);

        SpannableString set = new SpannableString(title);
        set.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(set);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_arroww);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        /*TextView txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(title);

        TextView txtToolbarBalance = findViewById(R.id.txtToolbarBalance);
        txtToolbarBalance.setText("RCH : ₹ " + String.format("%.2f", UserDetails.UserBalance) + "\nDMT : ₹ " + String.format("%.2f", UserDetails.DMRBalance));
*/
        alert = new AlertImpl(this);
        pDialog = new CustomProgressDialog(this);

        activityTransferBinding.txtSenderName.setText(AMoneyTransferUserDetails.aSenderName);
        activityTransferBinding.txtBeneficiaryName.setText(AMoneyTransferUserDetails.aBeneficiaryNameForTransfer);
        activityTransferBinding.txtAccNumber.setText(AMoneyTransferUserDetails.aBeneficiaryAccountNumberForTransfer);
        activityTransferBinding.txtBankName.setText(AMoneyTransferUserDetails.aBeneficiaryBankNameForTransfer);
        activityTransferBinding.txtIFSCode.setText(AMoneyTransferUserDetails.aBeneficiaryIFSCForTransfer);


        spAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, MoneyTransType);
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityTransferBinding.spAccountType.setAdapter(spAdapter);

        activityTransferBinding.btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCMoneyTransferPresenter.validateTransfer();
            }
        });


        activityTransferBinding.spAccountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TransferType = MoneyTransType[position];
                if (position == 1) {
                    activityTransferBinding.btnTransfer.setVisibility(View.GONE);
                    Toast.makeText(context, "NEFT is coming soon! Please try with IMPS.", Toast.LENGTH_LONG).show();
                } else {
                    activityTransferBinding.btnTransfer.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }


    @Override
    public String TransferType() {
        return TransferType;
    }

    @Override
    public String Amount() {
        return activityTransferBinding.edtAmount.getText().toString().trim();
    }

    @Override
    public void confirmClick() {

        activityTransferBinding.btnTransfer.setVisibility(View.GONE);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_confirmtrans);
        dialog.setCancelable(false);
        TextView a = dialog.findViewById(R.id.a);
        a.setText("Confirm Transfer");
        TextView txtName = dialog.findViewById(R.id.txtName);
        TextView txtAmount = dialog.findViewById(R.id.txtAmount);
        TextView txtAccNo = dialog.findViewById(R.id.txtAccNo);
        final TextView txtType = dialog.findViewById(R.id.txtType);
        final TextView txtBank = dialog.findViewById(R.id.txtBank);

        final Button btnYes = dialog.findViewById(R.id.btnYes);
        final Button btnNo = dialog.findViewById(R.id.btnNo);
        final ImageView btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityTransferBinding.btnTransfer.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });
        txtName.setText(AMoneyTransferUserDetails.aBeneficiaryNameForTransfer);

        txtAmount.setText(Amount());
        txtAccNo.setText(AMoneyTransferUserDetails.aBeneficiaryAccountNumberForTransfer);
        txtType.setText(TransferType());
        txtBank.setText(AMoneyTransferUserDetails.aBeneficiaryBankNameForTransfer);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!ns.isInternetAvailable(context)) {
                    internetError();
                    return;
                }
                iCMoneyTransferPresenter.makeTransfer();
                pDialog.showPDialog();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityTransferBinding.btnTransfer.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void internetError() {
        activityTransferBinding.btnTransfer.setVisibility(View.VISIBLE);
        Toast.makeText(context, AllMessages.internetError, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void editAmountError() {
        activityTransferBinding.edtAmount.setError(String.format(AllMessages.pleaseEnter, "valid amount"));
        activityTransferBinding.edtAmount.requestFocus();
        activityTransferBinding.btnTransfer.setVisibility(View.VISIBLE);
    }

    @Override
    public void errorAlert(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert(message);
    }

    @Override
    public void alert(String body) {
        pDialog.dismissPDialog();
        callIntent("Message", body);
    }

    @Override
    public void transferSuccess(AVerifyBeneficiaryAccountPojo body) {
        pDialog.dismissPDialog();
        String TransferTitle = body.getTransactionType() + " " + body.getStatus();
        String TransferMessage = "Beneficiary : " + body.getBeneficiaryName() +
                "\nA/C : " + body.getAccountNumber() +
                "\nAmount : " + body.getNetAmount() +
                "\nType : " + body.getTransactionType() +
                "\nRef No : " + body.getTransactionId();
        callIntent(TransferTitle, TransferMessage);
    }


    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive data!");
    }

    @Override
    public void unableToFetchError(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive " + message + ". Please restart your app.");
    }

    @Override
    public void spTransferTypeError() {
        alert.errorAlert("Please select transfer type.");
        activityTransferBinding.spTransType.requestFocus();
    }

    private void callIntent(String title, String status) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.warning_dialog);

        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(title);
        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(status);
        text.setGravity(Gravity.START);
        Button btnNo = dialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                moveToIntent();
            }
        });
       /* ImageView btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                moveToIntent();
            }
        });*/
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                moveToIntent();
            }
        });
        dialog.show();
    }

    private void moveToIntent() {
        Intent intent = getIntent();
        intent.putExtra("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
        setResult(3, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            //  finish(); // close this activity and return to preview activity (if there is any)
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        return super.onOptionsItemSelected(item);
    }

}
