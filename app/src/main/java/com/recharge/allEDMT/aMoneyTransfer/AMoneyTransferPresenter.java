package com.recharge.allEDMT.aMoneyTransfer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;
import com.recharge.allEDMT.aMoneyTransfer.aMoneyTransferModel.AIMoneyTransferModel;
import com.recharge.allEDMT.aMoneyTransfer.aMoneyTransferModel.AMoneyTransferModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;

public class AMoneyTransferPresenter implements AIMoneyTransferPresenter, AIMoneyTransferModel {

    private LogWriter log = new LogWriter();
    private AIMoneyTransferView aIMoneyTransferView;

    public AMoneyTransferPresenter(AIMoneyTransferView aIMoneyTransferView) {
        this.aIMoneyTransferView = aIMoneyTransferView;
    }

    @Override
    public void makeTransfer() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject validateTransfer = new JSONObject();
        try {
            validateTransfer.put("Amount", aIMoneyTransferView.Amount());
            validateTransfer.put("BeneficiaryId", AMoneyTransferUserDetails.aBeneficiaryIDForTransfer);
            validateTransfer.put("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
            validateTransfer.put("AccountNumber", AMoneyTransferUserDetails.aBeneficiaryAccountNumberForTransfer);
            validateTransfer.put("BeneficiaryName", AMoneyTransferUserDetails.aBeneficiaryNameForTransfer);
            validateTransfer.put("Bank", AMoneyTransferUserDetails.aBeneficiaryBankNameForTransfer);
            validateTransfer.put("SenderId", AMoneyTransferUserDetails.aSenderID);
            validateTransfer.put("SenderName", AMoneyTransferUserDetails.aSenderName);
            validateTransfer.put("IFSC", AMoneyTransferUserDetails.aBeneficiaryIFSCForTransfer);
            validateTransfer.put("TransferType", aIMoneyTransferView.TransferType());

            String jsonRequest = validateTransfer.toString();
            log.i("transfer request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIMoneyTransferView.dismissPDialog();
                return;
            }

            AMoneyTransferModel aMoneyTransferModel = new AMoneyTransferModel();
            aMoneyTransferModel.aIMoneyTransferModel = this;
            aMoneyTransferModel.transferWebservice(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void validateTransfer() {

        if (aIMoneyTransferView.Amount().trim().isEmpty() ||
                aIMoneyTransferView.Amount().trim().equals("")) {
            aIMoneyTransferView.editAmountError();
            return;
        }

        Double amount;
        amount = Double.parseDouble(aIMoneyTransferView.Amount());

        if (amount > UserDetails.UserBalance) {
            aIMoneyTransferView.alert("Insufficient balance to transfer amount, please contact to admin.");
            return;
        }
        if (amount > 25000 || amount < 10) {
            aIMoneyTransferView.errorAlert("Please enter amount between ₹ 10 and ₹ 25000");
            return;
        }

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(aIMoneyTransferView.TransferType().trim())) {
            aIMoneyTransferView.spTransferTypeError();
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryIDForTransfer)) {
            aIMoneyTransferView.unableToFetchError("Beneficiary ID");
            return;
        }
        if (!validation.isValidSenderMobileNumber(AMoneyTransferUserDetails.aSenderMobileNumber)) {
            aIMoneyTransferView.unableToFetchError("sender mobile number");
            return;
        }
        if (!validation.isValidAccountNumber(AMoneyTransferUserDetails.aBeneficiaryAccountNumberForTransfer)) {
            aIMoneyTransferView.unableToFetchError("Beneficiary account number");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryNameForTransfer)) {
            aIMoneyTransferView.unableToFetchError("Beneficiary name");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryBankNameForTransfer)) {
            aIMoneyTransferView.unableToFetchError("Beneficiary bank name");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderID)) {
            aIMoneyTransferView.unableToFetchError("Sender ID");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderName)) {
            aIMoneyTransferView.unableToFetchError("Sender name");
            return;
        }
        if (!validation.isValidIFSCode(AMoneyTransferUserDetails.aBeneficiaryIFSCForTransfer)) {
            aIMoneyTransferView.unableToFetchError("Beneficiary IFSCode");
            return;
        }
        aIMoneyTransferView.confirmClick();
    }

    @Override
    public void getTransferResponseFail(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getTransferResponseFail", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIMoneyTransferView.dismissPDialog();
                return;
            }
            aIMoneyTransferView.alert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getTransferResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getTransferResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIMoneyTransferView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            ArrayList<AVerifyBeneficiaryAccountPojo> aVerifyBeneficiaryAccountPojo =
                    gson.fromJson(body, new TypeToken<ArrayList<AVerifyBeneficiaryAccountPojo>>() {
                    }.getType());

            if (!aVerifyBeneficiaryAccountPojo.get(0).getStatus().toLowerCase().equalsIgnoreCase("SUCCESS".toLowerCase())) {
                aIMoneyTransferView.alert(aVerifyBeneficiaryAccountPojo.get(0).getMessage());
                return;
            }
            aIMoneyTransferView.transferSuccess(aVerifyBeneficiaryAccountPojo.get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {

    }


}
