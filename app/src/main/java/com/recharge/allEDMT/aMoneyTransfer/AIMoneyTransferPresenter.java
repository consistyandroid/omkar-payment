package com.recharge.allEDMT.aMoneyTransfer;

public interface AIMoneyTransferPresenter {

    void makeTransfer();

    void validateTransfer();

}
