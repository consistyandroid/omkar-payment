package com.recharge.allEDMT.aMoneyTransfer.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ATransferReportPojo {

    @SerializedName("Statuscode")
    @Expose
    private String statuscode;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ProviderTransactionID")
    @Expose
    private String providerTransactionID;
    @SerializedName("ProviderRefNo")
    @Expose
    private String providerRefNo;
    @SerializedName("ProviderOperatorTransactionID")
    @Expose
    private String providerOperatorTransactionID;
    @SerializedName("APIOpeningBal")
    @Expose
    private String aPIOpeningBal;
    @SerializedName("Lockedamt")
    @Expose
    private String lockedamt;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("BeneficiaryID")
    @Expose
    private String beneficiaryID;
    @SerializedName("SenderID")
    @Expose
    private String senderID;
    @SerializedName("SenderMobileNumber")
    @Expose
    private String senderMobileNumber;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("APIRequestID")
    @Expose
    private String aPIRequestID;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("SenderOpeningBalance")
    @Expose
    private String senderOpeningBalance;
    @SerializedName("NetAmount")
    @Expose
    private Double netAmount;
    @SerializedName("PaymentType")
    @Expose
    private String paymentType;
    @SerializedName("OperatorID")
    @Expose
    private Integer operatorID;
    @SerializedName("MoneyTransferProviderID")
    @Expose
    private Integer moneyTransferProviderID;
    @SerializedName("BeneficiaryName")
    @Expose
    private String beneficiaryName;
    @SerializedName("BeneficiaryMobileNo")
    @Expose
    private String beneficiaryMobileNo;
    @SerializedName("BeneficiaryAccountNo")
    @Expose
    private String beneficiaryAccountNo;

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    @SerializedName("AccountNumber")
    @Expose
    private String AccountNumber;
    @SerializedName("Charge")
    @Expose
    private Double charge;
    @SerializedName("MoneyTransferServiceID")
    @Expose
    private Integer moneyTransferServiceID;
    @SerializedName("Through")
    @Expose
    private String through;
    @SerializedName("IPAddress")
    @Expose
    private String iPAddress;
    @SerializedName("MoneyTransferProviderClosingBal")
    @Expose
    private String moneyTransferProviderClosingBal;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("ToDate")
    @Expose
    private String toDate;
    @SerializedName("Bank")
    @Expose
    private String bank;
    @SerializedName("UserClosingBalance")
    @Expose
    private Double userClosingBalance;
    @SerializedName("ShopeName")
    @Expose
    private String shopeName;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("ConsumerNo")
    @Expose
    private String consumerNo;
    @SerializedName("RetailerName")
    @Expose
    private String retailerName;
    @SerializedName("TRANS_TYPE")
    @Expose
    private String tRANSTYPE;
    @SerializedName("MoneyTransferID")
    @Expose
    private Integer moneyTransferID;
    @SerializedName("MoneyTransferProviderOpningBal")
    @Expose
    private String moneyTransferProviderOpningBal;
    @SerializedName("IFSC")
    @Expose
    private String iFSC;
    @SerializedName("DistCommisson")
    @Expose
    private String distCommisson;
    @SerializedName("DMRRequest")
    @Expose
    private String dMRRequest;
    @SerializedName("DMRResponse")
    @Expose
    private String dMRResponse;
    @SerializedName("UserOpeningBalance")
    @Expose
    private Double userOpeningBalance;
    @SerializedName("SenderName")
    @Expose
    private String senderName;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("ParentName")
    @Expose
    private String parentName;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("DUserTypeID")
    @Expose
    private Integer dUserTypeID;
    @SerializedName("RUserTypeID")
    @Expose
    private Integer rUserTypeID;
    @SerializedName("SessionID")
    @Expose
    private String sessionID;
    @SerializedName("ChargedAmount")
    @Expose
    private String chargedAmount;
    @SerializedName("InboxID")
    @Expose
    private Integer inboxID;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("PinCode")
    @Expose
    private String pinCode;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("OperatorName")
    @Expose
    private String operatorName;
    @SerializedName("IP")
    @Expose
    private String iP;
    @SerializedName("BillNumber")
    @Expose
    private String billNumber;
    @SerializedName("AmountList")
    @Expose
    private String amountList;
    @SerializedName("MoneyTransferProviderName")
    @Expose
    private String moneyTransferProviderName;

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProviderTransactionID() {
        return providerTransactionID;
    }

    public void setProviderTransactionID(String providerTransactionID) {
        this.providerTransactionID = providerTransactionID;
    }

    public String getProviderRefNo() {
        return providerRefNo;
    }

    public void setProviderRefNo(String providerRefNo) {
        this.providerRefNo = providerRefNo;
    }

    public String getProviderOperatorTransactionID() {
        return providerOperatorTransactionID;
    }

    public void setProviderOperatorTransactionID(String providerOperatorTransactionID) {
        this.providerOperatorTransactionID = providerOperatorTransactionID;
    }

    public String getAPIOpeningBal() {
        return aPIOpeningBal;
    }

    public void setAPIOpeningBal(String aPIOpeningBal) {
        this.aPIOpeningBal = aPIOpeningBal;
    }

    public String getLockedamt() {
        return lockedamt;
    }

    public void setLockedamt(String lockedamt) {
        this.lockedamt = lockedamt;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBeneficiaryID() {
        return beneficiaryID;
    }

    public void setBeneficiaryID(String beneficiaryID) {
        this.beneficiaryID = beneficiaryID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getAPIRequestID() {
        return aPIRequestID;
    }

    public void setAPIRequestID(String aPIRequestID) {
        this.aPIRequestID = aPIRequestID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSenderOpeningBalance() {
        return senderOpeningBalance;
    }

    public void setSenderOpeningBalance(String senderOpeningBalance) {
        this.senderOpeningBalance = senderOpeningBalance;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    public Integer getMoneyTransferProviderID() {
        return moneyTransferProviderID;
    }

    public void setMoneyTransferProviderID(Integer moneyTransferProviderID) {
        this.moneyTransferProviderID = moneyTransferProviderID;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryMobileNo() {
        return beneficiaryMobileNo;
    }

    public void setBeneficiaryMobileNo(String beneficiaryMobileNo) {
        this.beneficiaryMobileNo = beneficiaryMobileNo;
    }

    public String getBeneficiaryAccountNo() {
        return beneficiaryAccountNo;
    }

    public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
        this.beneficiaryAccountNo = beneficiaryAccountNo;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Integer getMoneyTransferServiceID() {
        return moneyTransferServiceID;
    }

    public void setMoneyTransferServiceID(Integer moneyTransferServiceID) {
        this.moneyTransferServiceID = moneyTransferServiceID;
    }

    public String getThrough() {
        return through;
    }

    public void setThrough(String through) {
        this.through = through;
    }

    public String getIPAddress() {
        return iPAddress;
    }

    public void setIPAddress(String iPAddress) {
        this.iPAddress = iPAddress;
    }

    public String getMoneyTransferProviderClosingBal() {
        return moneyTransferProviderClosingBal;
    }

    public void setMoneyTransferProviderClosingBal(String moneyTransferProviderClosingBal) {
        this.moneyTransferProviderClosingBal = moneyTransferProviderClosingBal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Double getUserClosingBalance() {
        return userClosingBalance;
    }

    public void setUserClosingBalance(Double userClosingBalance) {
        this.userClosingBalance = userClosingBalance;
    }

    public String getShopeName() {
        return shopeName;
    }

    public void setShopeName(String shopeName) {
        this.shopeName = shopeName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getRetailerName() {
        return retailerName;
    }

    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }

    public String getTRANSTYPE() {
        return tRANSTYPE;
    }

    public void setTRANSTYPE(String tRANSTYPE) {
        this.tRANSTYPE = tRANSTYPE;
    }

    public Integer getMoneyTransferID() {
        return moneyTransferID;
    }

    public void setMoneyTransferID(Integer moneyTransferID) {
        this.moneyTransferID = moneyTransferID;
    }

    public String getMoneyTransferProviderOpningBal() {
        return moneyTransferProviderOpningBal;
    }

    public void setMoneyTransferProviderOpningBal(String moneyTransferProviderOpningBal) {
        this.moneyTransferProviderOpningBal = moneyTransferProviderOpningBal;
    }

    public String getIFSC() {
        return iFSC;
    }

    public void setIFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public String getDistCommisson() {
        return distCommisson;
    }

    public void setDistCommisson(String distCommisson) {
        this.distCommisson = distCommisson;
    }

    public String getDMRRequest() {
        return dMRRequest;
    }

    public void setDMRRequest(String dMRRequest) {
        this.dMRRequest = dMRRequest;
    }

    public String getDMRResponse() {
        return dMRResponse;
    }

    public void setDMRResponse(String dMRResponse) {
        this.dMRResponse = dMRResponse;
    }

    public Double getUserOpeningBalance() {
        return userOpeningBalance;
    }

    public void setUserOpeningBalance(Double userOpeningBalance) {
        this.userOpeningBalance = userOpeningBalance;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public Integer getDUserTypeID() {
        return dUserTypeID;
    }

    public void setDUserTypeID(Integer dUserTypeID) {
        this.dUserTypeID = dUserTypeID;
    }

    public Integer getRUserTypeID() {
        return rUserTypeID;
    }

    public void setRUserTypeID(Integer rUserTypeID) {
        this.rUserTypeID = rUserTypeID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getChargedAmount() {
        return chargedAmount;
    }

    public void setChargedAmount(String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }

    public Integer getInboxID() {
        return inboxID;
    }

    public void setInboxID(Integer inboxID) {
        this.inboxID = inboxID;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getIP() {
        return iP;
    }

    public void setIP(String iP) {
        this.iP = iP;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getAmountList() {
        return amountList;
    }

    public void setAmountList(String amountList) {
        this.amountList = amountList;
    }

    public String getMoneyTransferProviderName() {
        return moneyTransferProviderName;
    }

    public void setMoneyTransferProviderName(String moneyTransferProviderName) {
        this.moneyTransferProviderName = moneyTransferProviderName;
    }

}
