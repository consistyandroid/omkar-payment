package com.recharge.allEDMT.aMoneyTransfer.aMoneyTransferModel;


import com.recharge.allEDMT.aGetSenderDetails.aGetSenderDetailsModel.AIWebServicesMoneyTransfer;


import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.report.MoneyTransferReport.moneyTransferReceipt.MoneyTransferReportPresenter;
import in.omkarpayment.app.report.NewMoneyTxReport.TxReportPresenter;
import in.omkarpayment.app.report.NewMoneyTxReport.moneyTransferReceipt.NewMoneyTransferReportPresenter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AMoneyTransferModel {

    public AIMoneyTransferModel aIMoneyTransferModel = null;
    public TxReportPresenter aTxReportPresenter = null;
    public NewMoneyTransferReportPresenter newMoneyTransferReportPresenter = null;

    public void transferWebservice(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.MoneyTransferWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIMoneyTransferModel.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIMoneyTransferModel.getTransferResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") &&
                            !response.body().getBody().equals("")) {
                        aIMoneyTransferModel.getTransferResponseFail(response.body().getBody());
                    } else {
                        aIMoneyTransferModel.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIMoneyTransferModel.alert(AllMessages.internetError);
            }
        });
    }

    public void getTransactionReport(String Request, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.SenderDMRReportWebService(Request, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aTxReportPresenter.alert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseTxReportBodyS", response.body().getBody());
                        aTxReportPresenter.getReport(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseTxReportBodyF", response.body().getBody());
                        aTxReportPresenter.getReportFail(response.body().getBody());
                    } else {
                        log.i("onResponseTxReportBodyE", response.body().getBody());
                        aTxReportPresenter.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onTxReportFailure", t.getMessage());
                aTxReportPresenter.alert(AllMessages.internetError);
            }
        });
    }

    public void getReceiptWebservice(String Request, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.TransactionReceiptWebService(Request, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        newMoneyTransferReportPresenter.errorAlert(AllMessages.internetError);
                        return;
                    }

                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseTxReceiptBodyS", response.body().getBody());
                        newMoneyTransferReportPresenter.getReceiptResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") &&
                            !response.body().getBody().equals("")) {
                        log.i("onResponseTxReceiptBodyF", response.body().getBody());
                        newMoneyTransferReportPresenter.getReceiptFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseTxReceiptBodyE", response.body().getBody());
                        newMoneyTransferReportPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onResponseTxReceiptFailure", t.getMessage());
                newMoneyTransferReportPresenter.errorAlert(AllMessages.internetError);
            }
        });
    }

}
