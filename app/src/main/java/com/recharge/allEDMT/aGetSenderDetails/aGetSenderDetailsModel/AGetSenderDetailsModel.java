package com.recharge.allEDMT.aGetSenderDetails.aGetSenderDetailsModel;


import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AIOTP;
import com.recharge.allEDMT.aGetSenderDetails.AIGetSenderDetailsPresenter;


import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AllMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AGetSenderDetailsModel {

    public AIGetSenderDetailsPresenter aIGetSenderDetailsPresenter = null;
    public AIOTP aIOTP = null;

    public void SenderDetailsWebservice(String encryptString, String keyData) {

        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.SelectSenderDetailsWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIGetSenderDetailsPresenter.senderDetailsResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIGetSenderDetailsPresenter.senderDetailsFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        aIGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void ViewBeneficiaryListWebservice(String encryptString, String keyData) {

        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.ViewBeneficiaryListWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {
                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIGetSenderDetailsPresenter.beneficiaryListResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIGetSenderDetailsPresenter.beneficiaryListFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        aIGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void DeleteBeneficiaryWebService(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.DeleteBeneficiaryRequestOTPWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {
                    LogWriter log = new LogWriter();

                    if (response.code() != 200) {
                        aIGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
                        return;
                    }
                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIGetSenderDetailsPresenter.deleteBeneficiaryResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIGetSenderDetailsPresenter.deleteBeneficiaryFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyElse", response.body().getBody());
                        aIGetSenderDetailsPresenter.errorAlert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIGetSenderDetailsPresenter.errorAlert(AllMessages.internetError);
            }
        });
    }

    public void resendOTPForAddBeneficiary(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);

        Call<WebserviceResponsePojo> call = webObject.ResendOTPWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {

                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        aIOTP.alert(AllMessages.internetError);
                        return;
                    }

                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyS", response.body().getBody());
                        aIOTP.getResendOTPResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIOTP.getResendOTPFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        aIOTP.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIOTP.alert(AllMessages.internetError);
            }
        });
    }

    public void verifyBeneficiaryAccount(String encryptString, String keyData) {
        AIWebServicesMoneyTransfer webObject = AIWebServicesMoneyTransfer.retrofit.create(AIWebServicesMoneyTransfer.class);
        Call<WebserviceResponsePojo> call = webObject.VerifyBeneficiaryAccountWebService(encryptString, keyData);
        call.enqueue(new Callback<WebserviceResponsePojo>() {

            @Override
            public void onResponse(Call<WebserviceResponsePojo> call, Response<WebserviceResponsePojo> response) {

                try {

                    LogWriter log = new LogWriter();
                    if (response.code() != 200) {
                        aIOTP.alert(AllMessages.internetError);
                        return;
                    }

                    if (response.body().getStatus().equalsIgnoreCase("SUCCESS") && !response.body().getBody().equals("")) {
                        log.i("onResponseBody", response.body().getBody());
                        aIOTP.verifyBeneficiaryAccountResponse(response.body().getBody());
                    } else if (response.body().getStatus().equalsIgnoreCase("FAILURE") && !response.body().getBody().equals("")) {
                        log.i("onResponseBodyF", response.body().getBody());
                        aIOTP.verifyBeneficiaryAccountFailResponse(response.body().getBody());
                    } else {
                        log.i("onResponseBodyE", response.body().getBody());
                        aIOTP.alert(response.body().getStatus());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<WebserviceResponsePojo> call, Throwable t) {
                LogWriter log = new LogWriter();
                log.i("onFailure", t.getMessage());
                aIOTP.alert(AllMessages.internetError);
            }
        });
    }

}
