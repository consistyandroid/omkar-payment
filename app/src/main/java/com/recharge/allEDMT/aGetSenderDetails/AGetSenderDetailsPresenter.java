package com.recharge.allEDMT.aGetSenderDetails;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AAddSenderBeneficiaryModel;
import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AIOTP;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;
import com.recharge.allEDMT.aGetSenderDetails.aGetSenderDetailsModel.AGetSenderDetailsModel;
import com.recharge.allEDMT.aGetSenderDetails.json.Beneficiary;
import com.recharge.allEDMT.aGetSenderDetails.json.Remitter;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.omkarpayment.app.encryptionDecryption.Cryptography_Android;
import in.omkarpayment.app.encryptionDecryption.KeyDataReader;
import in.omkarpayment.app.logProcessing.LogWriter;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.UserDetails;
import in.omkarpayment.app.userContent.Validation;

public class AGetSenderDetailsPresenter implements AIGetSenderDetailsPresenter, AIOTP {
    private AIGetSenderDetailsView aIGetSenderDetailsView;
    private LogWriter log = new LogWriter();

    public AGetSenderDetailsPresenter(AIGetSenderDetailsView aIGetSenderDetailsView) {
        this.aIGetSenderDetailsView = aIGetSenderDetailsView;
    }

    @Override
    public void validate() {

        Validation validation = new Validation();

        if (validation.isValidSenderMobileNumber(aIGetSenderDetailsView.mobileNumber().trim())) {
            aIGetSenderDetailsView.SearchNumber();
            return;
        }
        aIGetSenderDetailsView.editMobileNumberError();
    }

    @Override
    public void getSenderDetails() {

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject senderDetailsRequest = new JSONObject();
        try {
            senderDetailsRequest.put("MobileNumber", aIGetSenderDetailsView.mobileNumber());

            String jsonRequest = senderDetailsRequest.toString();
            log.i("request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            AGetSenderDetailsModel model = new AGetSenderDetailsModel();
            model.aIGetSenderDetailsPresenter = this;
            model.SenderDetailsWebservice(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiary() {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderID)) {
            aIGetSenderDetailsView.unableToFetchData("SenderID.");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryIDForDelete)) {
            aIGetSenderDetailsView.unableToFetchData("Beneficiary ID.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject deleteBeneficiary = new JSONObject();

        try {
            deleteBeneficiary.put("SenderId", AMoneyTransferUserDetails.aSenderID);
            deleteBeneficiary.put("BeneficiaryId", AMoneyTransferUserDetails.aBeneficiaryIDForDelete);

            String jsonRequest = deleteBeneficiary.toString();
            log.i("beneficiary delete request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            AGetSenderDetailsModel model = new AGetSenderDetailsModel();
            model.aIGetSenderDetailsPresenter = this;
            model.DeleteBeneficiaryWebService(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resendOTP(String beneficiaryName, String lastName, String accountNumber, String IFSCode) {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderID)) {
            aIGetSenderDetailsView.unableToFetchData("SenderID.");
            return;
        }
        if (!validation.isValidSenderMobileNumber(AMoneyTransferUserDetails.aSenderMobileNumber)) {
            aIGetSenderDetailsView.unableToFetchData("Sender mobile number.");
            return;
        }
        if (!validation.isValidSenderPinCode(AMoneyTransferUserDetails.aSenderPinCode)) {
            aIGetSenderDetailsView.unableToFetchData("Sender PinCode.");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryIDForResend)) {
            aIGetSenderDetailsView.unableToFetchData("Beneficiary ID.");
            return;
        }
        if (!validation.isNullOrEmpty(beneficiaryName)) {
            aIGetSenderDetailsView.unableToFetchData("Beneficiary name.");
            return;
        }
        if (!validation.isNullOrEmpty(accountNumber)) {
            aIGetSenderDetailsView.unableToFetchData("Account number.");
            return;
        }
        if (!validation.isNullOrEmpty(IFSCode)) {
            aIGetSenderDetailsView.unableToFetchData("IFSCode.");
            return;
        }
        if (!validation.isNullOrEmpty(lastName)) {
            lastName = "Last Name";
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject resendOTP = new JSONObject();
        try {
            resendOTP.put("SenderID", AMoneyTransferUserDetails.aSenderID);
            /*resendOTP.put("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
            resendOTP.put("FirstName", beneficiaryName);
            resendOTP.put("LastName", lastName);
            resendOTP.put("PinCode", AMoneyTransferUserDetails.aSenderPinCode);*/
            resendOTP.put("BenficiaryID", AMoneyTransferUserDetails.aBeneficiaryIDForResend);
            /*resendOTP.put("BeneficiaryAccountNo", accountNumber);
            resendOTP.put("IFSC", IFSCode);*/

            String jsonRequest = resendOTP.toString();
            log.i("resend OTP request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            AGetSenderDetailsModel model = new AGetSenderDetailsModel();
            model.aIOTP = this;
            model.resendOTPForAddBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyOTPForResendOTP(String otp) {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderID)) {
            aIGetSenderDetailsView.unableToFetchData("SenderID.");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryIDForResend)) {
            aIGetSenderDetailsView.unableToFetchData("Beneficiary ID.");
            return;
        }
        if (!validation.isNullOrEmpty(otp)) {
            aIGetSenderDetailsView.unableToFetchData("OTP.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyOTPForResendOTP = new JSONObject();
        try {
            verifyOTPForResendOTP.put("OTP", otp);
            verifyOTPForResendOTP.put("SenderID", AMoneyTransferUserDetails.aSenderID);
            verifyOTPForResendOTP.put("BenficiaryID", AMoneyTransferUserDetails.aBeneficiaryIDForResend);

            String jsonRequest = verifyOTPForResendOTP.toString();
            log.i("resendOTP Verification request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            AAddSenderBeneficiaryModel model = new AAddSenderBeneficiaryModel();
            model.aIOTP = this;
            model.verifyOTPForAddBeneficiary(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getBeneficiaryList() {

        Validation validation = new Validation();

        if (!validation.isValidSenderMobileNumber(AMoneyTransferUserDetails.aSenderMobileNumber)) {
            aIGetSenderDetailsView.dismissProgressDialog();
            aIGetSenderDetailsView.editMobileNumberError();
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject senderDetailsRequest = new JSONObject();
        try {
            senderDetailsRequest.put("SenderMobileNumber", aIGetSenderDetailsView.mobileNumber());

            String jsonRequest = senderDetailsRequest.toString();
            log.i("request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            AGetSenderDetailsModel model = new AGetSenderDetailsModel();
            model.aIGetSenderDetailsPresenter = this;
            model.ViewBeneficiaryListWebservice(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void beneficiaryListResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {

            String body = data.Decrypt(response);
            log.i("senderDetailsResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            List<Beneficiary> beneficiaryList = gson.fromJson(body, new TypeToken<List<Beneficiary>>() {
            }.getType());

            if (beneficiaryList == null) {
                aIGetSenderDetailsView.unableToFetchData("beneficiary list.");
                return;
            }
            aIGetSenderDetailsView.getSenderDetailsResponse(beneficiaryList);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void beneficiaryListFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("senderDetailsFailResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            if (body.toLowerCase().contains("Beneficiary not found".toLowerCase())) {
                aIGetSenderDetailsView.noBeneficiaryFound();
                return;
            }
            aIGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiaryResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {

            String body = data.Decrypt(response);
            log.i("senderDetailsResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            aIGetSenderDetailsView.deleteBeneficiarySuccessResponse(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBeneficiaryFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("senderDetailsFailResponse", body);

            if (body == null) {
                body = "Unable to delete beneficiary";
            }

            aIGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void senderDetailsResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {

            String body = data.Decrypt(response);
            log.i("senderDetailsResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            Remitter remitter = gson.fromJson(body, Remitter.class);
            if (remitter == null) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }
            aIGetSenderDetailsView.getRemitterDetails(remitter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void senderDetailsFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("senderDetailsFailResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            if (body.toLowerCase().contains("Sender not found".toLowerCase())) {
                aIGetSenderDetailsView.noSenderFound();
                return;
            }

            aIGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorAlert(String status) {
        log.i("errorAlert", status);
        aIGetSenderDetailsView.alert(status);
    }

    @Override
    public void getAddBeneficiaryVerifyOTPResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getVerifyOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            /*Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());
            if (!messageResponsePojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                alert(messageResponsePojo.getMessage());
                return;
            }*/
            aIGetSenderDetailsView.resendOTPVerificationSuccessAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getVerifyOTPFailResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            if (body.toLowerCase().contains("OTP invalid".toLowerCase())) {
                aIGetSenderDetailsView.resendOTPVerificationFailAlert(body);
                return;
            }
            /*Gson gson = new Gson();
            AAddBeneficiaryOTPVerificationPojo messageResponsePojo = gson.fromJson(body, new TypeToken<AAddBeneficiaryOTPVerificationPojo>() {
            }.getType());
            if (!messageResponsePojo.getStatus().equalsIgnoreCase("FAIL")
                    || messageResponsePojo.getStatus().equalsIgnoreCase("FAILURE")) {
                AIGetSenderDetailsView.dismissPDialog();
                return;
            }*/
            aIGetSenderDetailsView.resendOTPVerificationSuccessAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alert(String status) {
        aIGetSenderDetailsView.alert(status);
    }

    @Override
    public void getResendOTPResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            if (!body.toLowerCase().contains("OTP has been sent on your registered mobile number".toLowerCase())) {
                aIGetSenderDetailsView.alert(body);
                return;
            }

            /*Gson gson = new Gson();
            AResendOTPResponsePojo AResendOTPResponsePojo = gson.fromJson(body, new TypeToken<AResendOTPResponsePojo>() {
            }.getType());
            log.i("resendOTPResponseStatus", AResendOTPResponsePojo.getStatus());*/
            aIGetSenderDetailsView.resendOTPSuccessAlert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getResendOTPFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("getResendOTPResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            /*Gson gson = new Gson();
            AResendOTPResponsePojo AResendOTPResponsePojo = gson.fromJson(body, new TypeToken<AResendOTPResponsePojo>() {
            }.getType());
            log.i("resendOTPResponseFailStatus", AResendOTPResponsePojo.getStatus());*/
            aIGetSenderDetailsView.alert(body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verifyBeneAccountResponse", body);

            if (body == null || body.isEmpty() || body.equals("[]")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            Gson gson = new Gson();
            AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo = gson.fromJson(body, new TypeToken<AVerifyBeneficiaryAccountPojo>() {
            }.getType());

            if (aVerifyBeneficiaryAccountPojo.getStatus().equalsIgnoreCase("SUCCESS")) {
                aIGetSenderDetailsView.verifyBeneficiaryAccountSuccessAlert(aVerifyBeneficiaryAccountPojo);
                return;
            }
            aIGetSenderDetailsView.alert(aVerifyBeneficiaryAccountPojo.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyBeneficiaryAccountFailResponse(String response) {

        Cryptography_Android data = new Cryptography_Android();
        try {
            String body = data.Decrypt(response);
            log.i("verifyBeneAccountFailResponse", body);

            if (body == null) {
                body = "Unable to verify beneficiary account";
            }
            aIGetSenderDetailsView.alert(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddSenderVerifyOTPFailResponse(String response) {


    }

    @Override
    public void getAddSenderVerifyOTPResponse(String response) {

    }

    @Override
    public void verifyBeneficiaryAccount(String beneficiaryName, String beneficiaryAccountNumber, String IFSCode) {

        Validation validation = new Validation();

        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderName)) {
            aIGetSenderDetailsView.unableToFetchData("Sender name.");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aSenderID)) {
            aIGetSenderDetailsView.unableToFetchData("SenderID.");
            return;
        }
        if (!validation.isNullOrEmpty(AMoneyTransferUserDetails.aBeneficiaryBankNameForVerify)) {
            aIGetSenderDetailsView.unableToFetchData("Bank name.");
            return;
        }
        if (!validation.isValidSenderMobileNumber(AMoneyTransferUserDetails.aSenderMobileNumber)) {
            aIGetSenderDetailsView.unableToFetchData("Sender Mobile Number.");
            return;
        }
        if (!validation.isNullOrEmpty(beneficiaryName)) {
            aIGetSenderDetailsView.unableToFetchData("Beneficiary name.");
            return;
        }
        if (!validation.isNullOrEmpty(beneficiaryAccountNumber)) {
            aIGetSenderDetailsView.unableToFetchData("Beneficiary account number.");
            return;
        }
        if (!validation.isNullOrEmpty(IFSCode)) {
            aIGetSenderDetailsView.unableToFetchData("IFSCode.");
            return;
        }

        String keyData = new KeyDataReader().get();
        Cryptography_Android data = new Cryptography_Android();
        JSONObject verifyBeneficiaryAccount = new JSONObject();
        try {

            verifyBeneficiaryAccount.put("BeneficiaryName", beneficiaryName);
            verifyBeneficiaryAccount.put("Bank", AMoneyTransferUserDetails.aBeneficiaryBankNameForVerify);
            verifyBeneficiaryAccount.put("SenderName", AMoneyTransferUserDetails.aSenderName);
            verifyBeneficiaryAccount.put("AccountNumber", beneficiaryAccountNumber);
            verifyBeneficiaryAccount.put("SenderId", AMoneyTransferUserDetails.aSenderID);
            verifyBeneficiaryAccount.put("SenderMobileNumber", AMoneyTransferUserDetails.aSenderMobileNumber);
            verifyBeneficiaryAccount.put("IFSC", IFSCode);
            verifyBeneficiaryAccount.put("IPAddress", UserDetails.IMEI);

            String jsonRequest = verifyBeneficiaryAccount.toString();
            log.i("verify initially bene Account request", jsonRequest);
            String encryptString = data.Encrypt(jsonRequest);
            Log.i("encrypted String", encryptString);

            if (encryptString.equals("")) {
                aIGetSenderDetailsView.dismissPDialog();
                return;
            }

            AGetSenderDetailsModel model = new AGetSenderDetailsModel();
            model.aIOTP = this;
            model.verifyBeneficiaryAccount(encryptString, keyData);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
