package com.recharge.allEDMT.aGetSenderDetails;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.recharge.allEDMT.aGetSenderDetails.json.Beneficiary;


import java.util.List;

import in.omkarpayment.app.R;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;


public class ASenderBeneficiaryListAdapter extends RecyclerView.Adapter<ASenderBeneficiaryListAdapter.MyViewHolder> {
    public static ProgressDialog AdapterDialog;
    Context activity;
    AGetSenderDetailsFragment aGetSenderDetailsFragment;
    private List<Beneficiary> list;

    public ASenderBeneficiaryListAdapter(Context activity, List<Beneficiary> list, AGetSenderDetailsFragment aGetSenderDetailsFragment) {
        this.list = list;
        this.activity = activity;
        this.aGetSenderDetailsFragment = aGetSenderDetailsFragment;
    }

    @Override
    public ASenderBeneficiaryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beneficiary_list_adapter, parent, false);
        AdapterDialog = new ProgressDialog(activity);
        AdapterDialog.setMessage("Please wait..");
        AdapterDialog.setIndeterminate(true);
        AdapterDialog.setCancelable(false);

        return new ASenderBeneficiaryListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ASenderBeneficiaryListAdapter.MyViewHolder holder, final int position) {
        final Beneficiary pojo = list.get(position);

        holder.txtBeneficiaryName.setText(pojo.getFirstName().trim());
        holder.txtAccountNumber.setText(pojo.getAccountNumber().trim());
        holder.txtBankName.setText(pojo.getBank());
        holder.txtIFSC.setText(pojo.getIFSC().trim());

        holder.txtBeneficiaryName.setSelected(true);
        holder.txtBeneficiaryName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtBeneficiaryName.setSingleLine(true);

        holder.txtBankName.setSelected(true);
        holder.txtBankName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtBankName.setSingleLine(true);

        holder.txtAccountNumber.setSelected(true);
        holder.txtAccountNumber.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtAccountNumber.setSingleLine(true);

        holder.txtIFSC.setSelected(true);
        holder.txtIFSC.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txtIFSC.setSingleLine(true);

        if (pojo.getIsVerified()) {
            holder.imageVerify.setVisibility(View.GONE);
            holder.imageVerified.setVisibility(View.VISIBLE);
        } else {
            holder.imageVerify.setVisibility(View.VISIBLE);
            holder.imageVerified.setVisibility(View.GONE);
        }


        holder.imageVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(activity);
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.warning_dialog);
                TextView text = dialog.findViewById(R.id.text_dialog);
                TextView dialogTitle = dialog.findViewById(R.id.a);
                dialogTitle.setText(activity.getResources().getString(R.string.app_name));
                text.setText("Are you sure to verify this beneficiary's account?");
                Button dialogButton = dialog.findViewById(R.id.btn_dialog);
                Button btnNo = dialog.findViewById(R.id.btnNo);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AMoneyTransferUserDetails.aBeneficiaryBankNameForVerify = pojo.getBank();
                        AMoneyTransferUserDetails.aBeneficiaryAccountNumberForVerify = pojo.getAccountNumber().trim();
                        aGetSenderDetailsFragment.verifyBeneficiaryAccount(pojo.getIFSC().trim(), pojo.getFirstName());
                        dialog.dismiss();
                    }
                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        holder.btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AMoneyTransferUserDetails.aBeneficiaryNameForTransfer = pojo.getFirstName().trim();
                AMoneyTransferUserDetails.aBeneficiaryAccountNumberForTransfer = pojo.getAccountNumber().trim();
                AMoneyTransferUserDetails.aBeneficiaryIFSCForTransfer = pojo.getIFSC().trim();
                AMoneyTransferUserDetails.aBeneficiaryIDForTransfer = String.valueOf(pojo.getBeneficiaryId()).trim();
                AMoneyTransferUserDetails.aBeneficiaryBankNameForTransfer = pojo.getBank().trim();

                aGetSenderDetailsFragment.transferIntent();
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(activity);
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.warning_dialog);
                TextView text = dialog.findViewById(R.id.text_dialog);
                TextView dialogTitle = dialog.findViewById(R.id.a);
                dialogTitle.setText(activity.getResources().getString(R.string.app_name));
                text.setText("Are you sure to delete this beneficiary?");
                Button dialogButton = dialog.findViewById(R.id.btn_dialog);
                Button btnNo = dialog.findViewById(R.id.btnNo);
                //ImageView btnClose = dialog.findViewById(R.id.btnClose);
//                btnClose.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                    }
//                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AMoneyTransferUserDetails.aBeneficiaryIDForDelete = pojo.getBeneficiaryId().trim();
                        aGetSenderDetailsFragment.deleteBeneficiary();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

    }


    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtBeneficiaryName, txtAccountNumber, txtIFSC, txtBankName,btnDelete,btnTransfer;
        Button   btnResendOTP;
        ImageView imageVerify, imageVerified;

        public MyViewHolder(View rowView) {
            super(rowView);
            btnTransfer = rowView.findViewById(R.id.txtAction);
            btnDelete = rowView.findViewById(R.id.txtdelete);
            txtBeneficiaryName = rowView.findViewById(R.id.txtName);
            txtAccountNumber = rowView.findViewById(R.id.txtAccountNumber);
            txtIFSC = rowView.findViewById(R.id.txtIfscCode);
            txtBankName = rowView.findViewById(R.id.txtBankName);
            imageVerify = rowView.findViewById(R.id.imgNotVerfied);
            imageVerified = rowView.findViewById(R.id.imgVerfied);

        }

    }
}
