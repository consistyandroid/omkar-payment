package com.recharge.allEDMT.aGetSenderDetails;

import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;
import com.recharge.allEDMT.aGetSenderDetails.json.Beneficiary;
import com.recharge.allEDMT.aGetSenderDetails.json.Remitter;

import java.util.List;

public interface AIGetSenderDetailsView {

    String mobileNumber();

    void editMobileNumberError();

    void SearchNumber();

    void getSenderDetailsResponse(List<Beneficiary> beneficiaryList);

    void alert(String status);

    void noSenderFound();

    void noBeneficiaryFound();

    void getRemitterDetails(Remitter remitter);

    void deleteBeneficiarySuccessResponse(String message);

    void resendOTPSuccessAlert(String message);

    void resendOTPVerificationSuccessAlert(String message);

    void verifyBeneficiaryAccountSuccessAlert(AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo);

    void dismissPDialog();

    void unableToFetchData(String message);

    void resendOTPVerificationFailAlert(String message);

    void dismissProgressDialog();

}
