package com.recharge.allEDMT.aGetSenderDetails.aGetSenderDetailsModel;


import java.util.concurrent.TimeUnit;

import in.omkarpayment.app.json.WebserviceResponsePojo;
import in.omkarpayment.app.userContent.ApplicationURL;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface AIWebServicesMoneyTransfer {
    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(300, TimeUnit.SECONDS)
            .writeTimeout(300, TimeUnit.SECONDS)
            .readTimeout(300, TimeUnit.SECONDS)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ApplicationURL.allE_MoneyTransfer_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build();

    @GET("AlleMoneyTransfer.asmx/SelectSenderDetails")
    Call<WebserviceResponsePojo> SelectSenderDetailsWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("AlleMoneyTransfer.asmx/ViewBeneficiaryList")
    Call<WebserviceResponsePojo> ViewBeneficiaryListWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata

    );

    @GET("AlleMoneyTransfer.asmx/SenderRegistration")
    Call<WebserviceResponsePojo> SenderRegistrationWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/AddBeneficiary")
    Call<WebserviceResponsePojo> AddBeneficiaryWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/GetBankDetail")
    Call<WebserviceResponsePojo> GetBankDetailWebService(
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/AddBeneficiarySubmitOTP")
    Call<WebserviceResponsePojo> AddBeneficiarySubmitOTPWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/SenderRegistrationSubmitOTP")
    Call<WebserviceResponsePojo> AddSenderSubmitOTPWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/DeleteBeneficiary")
    Call<WebserviceResponsePojo> DeleteBeneficiaryRequestOTPWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/MoneyTransfer")
    Call<WebserviceResponsePojo> MoneyTransferWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/SenderDMRReport")
    Call<WebserviceResponsePojo> SenderDMRReportWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/VerifyBeneficiaryAccount")
    Call<WebserviceResponsePojo> VerifyBeneficiaryAccountWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/SenderRegistrationResendOTP")
    Call<WebserviceResponsePojo> ResendOTPWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/TransactionDetails")
    Call<WebserviceResponsePojo> TransactionDetailsWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );

    @GET("AlleMoneyTransfer.asmx/TransactionReceipt")
    Call<WebserviceResponsePojo> TransactionReceiptWebService(
            @Query("Request") String Request,
            @Query("Keydata") String Keydata
    );


}
