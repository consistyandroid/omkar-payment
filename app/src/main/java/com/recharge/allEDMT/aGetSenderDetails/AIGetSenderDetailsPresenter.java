package com.recharge.allEDMT.aGetSenderDetails;

public interface AIGetSenderDetailsPresenter {

    void validate();

    void getSenderDetails();

    void deleteBeneficiary();

    void resendOTP(String beneficiaryName, String lastName, String accountNumber, String ifsc);

    void verifyOTPForResendOTP(String otp);

    void verifyBeneficiaryAccount(String beneficiaryName, String beneficiaryAccountNumber, String IFSCode);

    void senderDetailsResponse(String response);

    void senderDetailsFailResponse(String response);

    void errorAlert(String status);

    void deleteBeneficiaryResponse(String response);

    void deleteBeneficiaryFailResponse(String response);

    void getBeneficiaryList();

    void beneficiaryListResponse(String response);

    void beneficiaryListFailResponse(String response);

}
