package com.recharge.allEDMT.aGetSenderDetails.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Remitter {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("IsVerified")
    @Expose
    private Boolean isVerified;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("UtilisedFund")
    @Expose
    private String utilisedFund;
    @SerializedName("RemainingFund")
    @Expose
    private String remainingFund;
    @SerializedName("SenderId")
    @Expose
    private String senderId;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUtilisedFund() {
        return utilisedFund;
    }

    public void setUtilisedFund(String utilisedFund) {
        this.utilisedFund = utilisedFund;
    }

    public String getRemainingFund() {
        return remainingFund;
    }

    public void setRemainingFund(String remainingFund) {
        this.remainingFund = remainingFund;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}