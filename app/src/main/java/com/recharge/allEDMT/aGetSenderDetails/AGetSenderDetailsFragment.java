package com.recharge.allEDMT.aGetSenderDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.recharge.allEDMT.aAddSenderBeneficiary.AAddSenderBeneficiaryActivity;
import com.recharge.allEDMT.aAddSenderBeneficiary.aAddSenderBeneficiaryModel.AIOTP;
import com.recharge.allEDMT.aAddSenderBeneficiary.json.AVerifyBeneficiaryAccountPojo;
import com.recharge.allEDMT.aGetSenderDetails.json.Beneficiary;
import com.recharge.allEDMT.aGetSenderDetails.json.Remitter;
import com.recharge.allEDMT.aMoneyTransfer.AMoneyTransferActivity;


import java.util.ArrayList;
import java.util.List;

import in.omkarpayment.app.MainActivity;
import in.omkarpayment.app.NetworkState;
import in.omkarpayment.app.R;
import in.omkarpayment.app.alert.AlertImpl;
import in.omkarpayment.app.chekAvailableBalance.AvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalancePresenter;
import in.omkarpayment.app.chekAvailableBalance.IAvailableBalanceView;
import in.omkarpayment.app.databinding.ActivitySearchNumberAmoneyTransferBinding;
import in.omkarpayment.app.json.AvailableBalancePojo;
import in.omkarpayment.app.json.SaleCommissionPojo;
import in.omkarpayment.app.progressDialog.CustomProgressDialog;
import in.omkarpayment.app.userContent.AMoneyTransferUserDetails;
import in.omkarpayment.app.userContent.AllMessages;
import in.omkarpayment.app.userContent.UserDetails;

import static android.app.Activity.RESULT_OK;
import static android.widget.Toast.LENGTH_LONG;


public class AGetSenderDetailsFragment extends Fragment implements AIGetSenderDetailsView, IAvailableBalanceView, AIOTP {

    View rovView;
    ActivitySearchNumberAmoneyTransferBinding searchNumber2Binding;
    AIGetSenderDetailsPresenter iCGetSenderDetailsPresenter;
    IAvailableBalancePresenter iAvailableBalancePresenter;
    CustomProgressDialog pDialog;
    AlertImpl alert;
    int onActivityResultCount = 0, onPauseCount, onStopCount, onStartCount, inResultIntentCount = 0;
    NetworkState ns = new NetworkState();

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        searchNumber2Binding = DataBindingUtil.inflate(inflater, R.layout.activity_search_number_amoney_transfer, container, false);
        rovView = searchNumber2Binding.getRoot();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        searchNumber2Binding.listView.setLayoutManager(mLayoutManager);
        searchNumber2Binding.listView.setItemAnimator(new DefaultItemAnimator());
        alert = new AlertImpl(getActivity());
        pDialog = new CustomProgressDialog(getActivity());
        iCGetSenderDetailsPresenter = new AGetSenderDetailsPresenter(this);

        iAvailableBalancePresenter = new AvailableBalancePresenter(this);
        iAvailableBalancePresenter.getAvailableBalance();

        searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
        searchNumber2Binding.layoutSenderNumber.setVisibility(View.GONE);
        searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
        searchNumber2Binding.editSearchNumber.requestFocus();
        searchNumber2Binding.editSearchNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (searchNumber2Binding.editSearchNumber.getText().toString().length() != 10) {
                    searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
                    searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
                    searchNumber2Binding.listView.setAdapter(null);
                    return;
                }

                if (onPauseCount == 1 && onStartCount != 1 && inResultIntentCount != 1) {
                    searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
                    searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
                    searchNumber2Binding.listView.setAdapter(null);
                    onPauseCount = 0;
                    return;
                }

                if (!ns.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_SHORT).show();
                    return;
                }

                resetAMoneyTransferUserDetails(); // reset all CMoneyTransferUserDetails

                searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
                searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
                searchNumber2Binding.listView.setAdapter(null);
                AMoneyTransferUserDetails.aSenderMobileNumber = searchNumber2Binding.editSearchNumber.getText().toString().trim();
                iCGetSenderDetailsPresenter.validate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchNumber2Binding.txtAddBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AAddSenderBeneficiaryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("SenderMobileNumber", mobileNumber());
                bundle.putString("Type", "AddBeneficiary");
                bundle.putString("Title", "Add Beneficiary");
                intent.putExtras(bundle);
                startActivityForResult(intent, 2);
            }
        });
        return rovView;
    }

    private void resetAMoneyTransferUserDetails() {

        AMoneyTransferUserDetails.aSenderFirstName = "";
        AMoneyTransferUserDetails.aSenderLastName = "";
        AMoneyTransferUserDetails.aSenderMobileNumber = "";
        AMoneyTransferUserDetails.aSenderID = "";
        AMoneyTransferUserDetails.aSenderPinCode = "";
        AMoneyTransferUserDetails.aSenderName = "";

        AMoneyTransferUserDetails.aBeneficiaryBankNameForVerify = "";
        AMoneyTransferUserDetails.aBeneficiaryFirstName = "";
        AMoneyTransferUserDetails.aBeneficiaryLastName = "";
        AMoneyTransferUserDetails.aBeneficiaryBankName = "";
        AMoneyTransferUserDetails.aBeneficiaryIFSC = "";
        AMoneyTransferUserDetails.aBeneficiaryIDForAddVerification = "";

        AMoneyTransferUserDetails.aBeneficiaryAccountNumberForTransfer = "";
        AMoneyTransferUserDetails.aBeneficiaryIFSCForTransfer = "";
        AMoneyTransferUserDetails.aBeneficiaryIDForTransfer = "";
        AMoneyTransferUserDetails.aBeneficiaryNameForTransfer = "";
        AMoneyTransferUserDetails.aBeneficiaryBankNameForTransfer = "";

        AMoneyTransferUserDetails.aBeneficiaryIDForDelete = "";
        AMoneyTransferUserDetails.aBeneficiaryIDForResend = "";

    }


    @Override
    public String mobileNumber() {
        return searchNumber2Binding.editSearchNumber.getText().toString().trim();
    }

    @Override
    public void editMobileNumberError() {
        searchNumber2Binding.editSearchNumber.setError("Please enter sender number");
        searchNumber2Binding.editSearchNumber.requestFocus();
    }

    @Override
    public void SearchNumber() {
        if (!ns.isInternetAvailable(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        iCGetSenderDetailsPresenter.getSenderDetails();
        pDialog.showPDialog();
    }

    @Override
    public void getSenderDetailsResponse(List<Beneficiary> beneficiaryList) {
        pDialog.dismissPDialog();
        hideKeyPad();
        searchNumber2Binding.txtAddBeneficiary.setVisibility(View.VISIBLE);
        ASenderBeneficiaryListAdapter adapter = new ASenderBeneficiaryListAdapter(getActivity(), beneficiaryList, AGetSenderDetailsFragment.this);
        searchNumber2Binding.listView.setAdapter(adapter);
    }

    @Override
    public void alert(String status) {
        pDialog.dismissPDialog();
        iAvailableBalancePresenter.getAvailableBalance();
        alert.errorAlert(status);
    }

    @Override
    public void getResendOTPResponse(String response) {

    }

    @Override
    public void getResendOTPFailResponse(String response) {

    }

    @Override
    public void verifyBeneficiaryAccountResponse(String response) {

    }

    @Override
    public void verifyBeneficiaryAccountFailResponse(String body) {

    }

    @Override
    public void getAddSenderVerifyOTPFailResponse(String response) {

    }

    @Override
    public void getAddSenderVerifyOTPResponse(String response) {

    }


    @Override
    public void noSenderFound() {
        pDialog.dismissPDialog();
        searchNumber2Binding.listView.setAdapter(null);
        Intent intent = new Intent(getActivity(), AAddSenderBeneficiaryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("SenderMobileNumber", mobileNumber());
        bundle.putString("Type", "AddSender");
        bundle.putString("Title", "Add Sender");
        intent.putExtras(bundle);
        startActivityForResult(intent, 2);
    }

    @Override
    public void noBeneficiaryFound() {
        pDialog.dismissPDialog();
        searchNumber2Binding.listView.setAdapter(null);
        Intent intent = new Intent(getActivity(), AAddSenderBeneficiaryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("SenderMobileNumber", mobileNumber());
        bundle.putString("Type", "AddBeneficiary");
        bundle.putString("Title", "Add Beneficiary");
        intent.putExtras(bundle);
        startActivityForResult(intent, 2);
    }

    @Override
    public void deleteBeneficiarySuccessResponse(String message) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(getContext().getResources().getString(R.string.app_name));
        text.setText(message);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                searchNumber2Binding.listView.setAdapter(null);
                if (!ns.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
                searchNumber2Binding.layoutSenderNumber.setVisibility(View.GONE);
                searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
                SearchNumber();
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            onActivityResultCount = 1;
            if (resultCode == RESULT_OK || requestCode == 3) {
                inResultIntentCount = 1;
                String SenderNumber = data.getStringExtra("SenderMobileNumber");
                if (SenderNumber != null) {
                    AMoneyTransferUserDetails.aSenderMobileNumber = SenderNumber;
                    searchNumber2Binding.editSearchNumber.setText(SenderNumber);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void transferIntent() {
        Intent in = new Intent(getActivity(), AMoneyTransferActivity.class);
        startActivityForResult(in, 3);
    }

    private void hideKeyPad() {
        try {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAddBeneficiaryVerifyOTPResponse(String response) {

    }

    @Override
    public void getAddBeneficiaryVerifyOTPFailResponse(String response) {

    }

    @Override
    public void getRemitterDetails(Remitter remitter) {
        if (remitter.getSenderId() != null) {
            searchNumber2Binding.layoutSenderDetails.setVisibility(View.VISIBLE);
            searchNumber2Binding.layoutSenderNumber.setVisibility(View.VISIBLE);
            AMoneyTransferUserDetails.aSenderID = remitter.getSenderId();
            //AMoneyTransferUserDetails.aSenderPinCode = remitter.getPincode();
            AMoneyTransferUserDetails.aSenderName = remitter.getFirstName();
            AMoneyTransferUserDetails.aSenderMobileNumber = remitter.getMobileNumber();
            searchNumber2Binding.txtSenderName.setText(remitter.getFirstName());
            searchNumber2Binding.txtSenderMobileNumber.setText(remitter.getMobileNumber());
            searchNumber2Binding.txtRemainingLimit.setText(String.valueOf("₹ " + remitter.getRemainingFund()));
            searchNumber2Binding.txtConsumeLimit.setText(String.valueOf("₹ " + remitter.getUtilisedFund()));
            try {
                String str[] = remitter.getFirstName().split(" ");

                String FistName = str[0];
                String LastName = "A";
                if (str.length >= 2) {
                    LastName = str[1];
                }
                AMoneyTransferUserDetails.aSenderFirstName = FistName;
                AMoneyTransferUserDetails.aSenderLastName = LastName;
            } catch (Exception e) {
                AMoneyTransferUserDetails.aSenderFirstName = remitter.getFirstName();
                AMoneyTransferUserDetails.aSenderLastName = remitter.getFirstName();
                e.printStackTrace();
            }

            iCGetSenderDetailsPresenter.getBeneficiaryList();

            return;
        }
        searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
        searchNumber2Binding.layoutSenderNumber.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        onStopCount = 1;
        onStartCount = 0;
        inResultIntentCount = 0;
    }

    @Override
    public void onStart() {
        super.onStart();
        onStartCount = 1;
    }

    @Override
    public void onPause() {
        super.onPause();
        onPauseCount = 1;
        inResultIntentCount = 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (onActivityResultCount == 1) {
            onActivityResultCount = 0;
            return;
        }
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        searchNumber2Binding.listView.setAdapter(null);
        searchNumber2Binding.editSearchNumber.setText("");
        searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
        searchNumber2Binding.layoutSenderNumber.setVisibility(View.GONE);
        searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
    }

    public void deleteBeneficiary() {
        if (!ns.isInternetAvailable(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        iCGetSenderDetailsPresenter.deleteBeneficiary();
        pDialog.showPDialog();
    }

    public void ResendOTPForBeneficiaryAdapter() {

        if (!ns.isInternetAvailable(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        iCGetSenderDetailsPresenter.resendOTP(AMoneyTransferUserDetails.aBeneficiaryNameForResendOTP, "LastName",
                AMoneyTransferUserDetails.aBeneficiaryAccountNumberForResendOTP, AMoneyTransferUserDetails.aBeneficiaryIFSCForResendOTP);
        pDialog.showPDialog();
    }

    @Override
    public void resendOTPVerificationSuccessAlert(String message) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary OTP Verification");
        text.setText(message);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                searchNumber2Binding.editSearchNumber.setText(AMoneyTransferUserDetails.aSenderMobileNumber);
                searchNumber2Binding.listView.setAdapter(null);
                searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
                searchNumber2Binding.layoutSenderNumber.setVisibility(View.GONE);
                searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
                SearchNumber();
            }
        });
        dialog.show();
    }

    @Override
    public void verifyBeneficiaryAccountSuccessAlert(AVerifyBeneficiaryAccountPojo aVerifyBeneficiaryAccountPojo) {
        pDialog.dismissPDialog();
        iAvailableBalancePresenter.getAvailableBalance();
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText(aVerifyBeneficiaryAccountPojo.getMessage());
        if (aVerifyBeneficiaryAccountPojo.getStatus().equals("SUCCESS")) {
            dialogTitle.setTextColor(Color.WHITE);
        }
        text.setText("Beneficiary Name :- " + aVerifyBeneficiaryAccountPojo.getBeneficiaryName() +
                "\nBank Name :- " + AMoneyTransferUserDetails.aBeneficiaryBankNameForVerify +
                "\nAccount Number :- " + AMoneyTransferUserDetails.aBeneficiaryAccountNumberForVerify);
        text.setGravity(0);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                searchNumber2Binding.layoutSenderDetails.setVisibility(View.GONE);
                searchNumber2Binding.layoutSenderNumber.setVisibility(View.GONE);
                searchNumber2Binding.txtAddBeneficiary.setVisibility(View.GONE);
                searchNumber2Binding.listView.setAdapter(null);
                SearchNumber();
            }
        });
        dialog.show();
    }

    @Override
    public void dismissPDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to send/receive data!");
    }

    @Override
    public void unableToFetchData(String message) {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
        alert.errorAlert("Unable to fetch/receive " + message + " Please restart your app.");
    }

    @Override
    public void resendOTPVerificationFailAlert(String response) {
        pDialog.dismissPDialog();
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        TextView dialogTitle = dialog.findViewById(R.id.a);
        dialogTitle.setText("Beneficiary OTP Verification");
        text.setText(response + ". We will send new OTP on your registered mobile number.");
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), AllMessages.internetError, LENGTH_LONG).show();
                    return;
                }
                dialog.dismiss();
                iCGetSenderDetailsPresenter.resendOTP(AMoneyTransferUserDetails.aBeneficiaryNameForResendOTP, "LastName",
                        AMoneyTransferUserDetails.aBeneficiaryAccountNumberForResendOTP, AMoneyTransferUserDetails.aBeneficiaryIFSCForResendOTP);
                pDialog.showPDialog();
            }
        });
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (pDialog != null) {
            pDialog.dismissPDialog();
        }
    }

    @Override
    public void resendOTPSuccessAlert(String message) {
        pDialog.dismissPDialog();
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        openOTPForResendOTP();
    }

    private void openOTPForResendOTP() {
        final Dialog OTPDialog = new Dialog(getActivity());
        OTPDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OTPDialog.setContentView(R.layout.dialog_otp);
        OTPDialog.setCancelable(false);
        final EditText editOTP = OTPDialog.findViewById(R.id.editOTP);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        Button btnSubmit = OTPDialog.findViewById(R.id.btnSubmit);
        final Button btnCancel = OTPDialog.findViewById(R.id.btnCancel);
        final Button btnResend = OTPDialog.findViewById(R.id.btnResend);
        btnResend.setVisibility(View.GONE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ns.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
                    return;
                }
                if (editOTP.getText().toString().equals("")) {
                    editOTP.setError(String.format(AllMessages.pleaseEnter, "OTP"));
                    editOTP.requestFocus();
                    return;
                }
                iCGetSenderDetailsPresenter.verifyOTPForResendOTP(editOTP.getText().toString());
                OTPDialog.dismiss();
                pDialog.showPDialog();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);

        OTPDialog.show();
    }

    public void verifyBeneficiaryAccount(String IFSCode, String beneficiaryName) {
        if (!ns.isInternetAvailable(getActivity())) {
            Toast.makeText(getActivity(), AllMessages.internetError, Toast.LENGTH_LONG).show();
            return;
        }
        pDialog.showPDialog();
        iCGetSenderDetailsPresenter.verifyBeneficiaryAccount(beneficiaryName,
                AMoneyTransferUserDetails.aBeneficiaryAccountNumberForVerify, IFSCode);
    }

    @Override
    public void availableBalance(ArrayList<AvailableBalancePojo> lastrasaction) {
        for (AvailableBalancePojo obj : lastrasaction) {
            try {
                double CurrentBalance = obj.getCurrentBalance();
                double currentDMRBalance = obj.getDMRBalance();
                UserDetails.UserBalance = obj.getCurrentBalance();
                //UserDetails.DMRBalance = obj.getDMRBalance();
                ((MainActivity) getActivity()).refreshBalance();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void saleCommission(ArrayList<SaleCommissionPojo> lastrasaction) {

    }
}
