# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\EclipseForAndroid\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);


    public static *** v(...);
    public static *** i(...);
    public static *** w(...);
    public static *** d(...);
    public static *** e(...);
}

-keep class org.json.** { *; }
-keep class com.android.volley.** {*;}
-keep class com.tapits.fingpay.** {*;}
-dontwarn java.awt.**
-dontwarn java.beans.Beans
-dontwarn javax.security.**
-keep class mimetypes.** {*;}
-keep class myjava.awt.datatransfer.** {*;}
-keep class org.apache.harmony.awt.** {*;}
-keep class org.apache.harmony.misc.** {*;}
-dontwarn java.awt.**,javax.activation.**,java.beans.**
-dontwarn com.google.gson.**
-dontwarn com.squareup.okhttp.internal.huc.**
-keep class android.support.design.widget.** { *; }
-keep interface android.support.design.widget.** { *; }
-dontwarn android.support.design.**
-dontwarn android.test.**
-keep class myjava.awt.datatransfer.** {*;}
-ignorewarnings

-dontwarn com.fasterxml.**
-dontwarn okio.**
-dontwarn retrofit2.**

-keep class org.xmlpull.v1.** { *; }
#required to keep this class for AEPS SDK to work in release
#-keep class com.aeps.aepslib.AepsActivity
#-keep class com.mantra.rdservice.RDServiceActivity
#-keep class com.minkspay.library.aepslibrary.**

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature
# For using GSON @Expose annotation
-keepattributes *Annotation*
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }
# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
##---------------End: proguard configuration for Gson  ----------